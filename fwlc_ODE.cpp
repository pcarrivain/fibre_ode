#include "functions.h"
#include "variables.h"
#include <assert.h>
#include <cmath>
#include <cstring>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <thread>
#include <time.h>

using namespace std;

int vl, nc, retour;
double T0_CPU, Et, sum_abs_depth, max_abs_depth;
char nom_s[3000], nom_g[3000], nom_v[3000], path_to[1000], parameters[1000];
double cos_Tw2[2];
vec3 t0 = vec3(.0, .0, 1.);
vec3 u0 = vec3(1., .0, .0);
vec3 v0 = vec3(.0, 1., .0);
FILE *fSortie, *fSPQ, *fStructure, *fMT, *export_world_state;
unsigned long *s_mt = new unsigned long[624](), s_mt_i;
int s_mti;
int3p nc_test_Vl;
bool success, test_Vl, split_solver;

// extern variables
int *is_linker_nucleosome = new int[1]();
int *nucleosome_start = new int[1]();
double xmass = 1., xfriction = 1.;
bool same_mass = false;
// variables: monomers partition
int nbins = 1, lhit = 0, nperb = 10, nnperb = 10;
int *binn = NULL;
int *ninb = NULL;
int *bhit = NULL;
int *nhit = NULL;
int *lperb = NULL;
// variables: nucleosomes partition
int *nbinn = NULL;
int *nninb = NULL;
int *nbhit = NULL;
int *nnhit = NULL;
int *nlperb = NULL;

// main
int main(int argc, char **argv) {
  // nearCallback function
  void (*my_nearCallback)(void *, dGeomID, dGeomID);
  my_nearCallback = &nearCallback;
  // default inputs
  int seed = 1;
  int N = 5714;
  int nicks = 0;
  double l_bp = 10.5; // we choose the number of bp per segment to be multiple
                      // of one DNA turn
  double force = .0, phi = -1.;
  double torque = .0;
  double overtwist = .0;
  int iterations = 1000000;
  int every = (int)(iterations / 100);
  double x_ghost = 1.2;
  int gyroscopic = 1;
  test_Vl = split_solver = false;
  int Vl_every = 10;
  retour = sprintf(path_to, "%s", "/scratch/fwlc");
  int n_args = 0, arg_i;
  const option long_opts[] = {{"seed", required_argument, nullptr, 's'},
                              {"N", required_argument, nullptr, 'N'},
                              {"l_bp", required_argument, nullptr, 'l'},
                              {"force", required_argument, nullptr, 'F'},
                              {"phi", required_argument, nullptr, 'f'},
                              {"torque", required_argument, nullptr, 't'},
                              {"overtwist", required_argument, nullptr, 'o'},
                              {"nicks", required_argument, nullptr, 'n'},
                              {"iterations", required_argument, nullptr, 'i'},
                              {"every", required_argument, nullptr, 'e'},
                              {"scale", required_argument, nullptr, 'X'},
                              {"Vl_every", required_argument, nullptr, 'V'},
                              {"test_Vl", required_argument, nullptr, 'T'},
                              {"split_solver", required_argument, nullptr, 'S'},
                              {"gyroscopic", required_argument, nullptr, 'g'},
                              {"path_to", required_argument, nullptr, 'p'},
                              {"help", optional_argument, nullptr, 'h'}};
  while ((arg_i = getopt_long(argc, argv, "s:N:l:F:f:t:o:n:i:e:X:V:T:S:g:p:h",
                              long_opts, nullptr)) != -1) {
    switch (arg_i) {
    case 's':
      seed = (int)(atof(optarg));
      n_args++;
      break;
    case 'N':
      N = (int)(atof(optarg));
      n_args++;
      break;
    case 'l':
      l_bp = atof(optarg);
      n_args++;
      break;
    case 'F':
      force = atof(optarg) * pN / fF;
      n_args++;
      break;
    case 'f':
      phi = atof(optarg);
      n_args++;
      break;
    case 't':
      torque = atof(optarg) * pN * nm / (kB * temp);
      n_args++;
      break;
    case 'o':
      overtwist = atof(optarg);
      n_args++;
      break;
    case 'n':
      nicks = (int)atof(optarg);
      n_args++;
      break;
    case 'i':
      iterations = (int)(atof(optarg));
      n_args++;
      break;
    case 'e':
      every = (int)(atof(optarg));
      n_args++;
      break;
    case 'X':
      x_ghost = atof(optarg);
      n_args++;
      break;
    case 'V':
      Vl_every = (int)(atof(optarg));
      n_args++;
      break;
    case 'T':
      test_Vl = (strcmp(optarg, "yes") == 0);
      n_args++;
      break;
    case 'S':
      split_solver = (strcmp(optarg, "yes") == 0);
      n_args++;
      break;
    case 'g':
      gyroscopic = (int)(atof(optarg));
      n_args++;
      break;
    case 'p':
      retour = sprintf(path_to, "%s", optarg);
      n_args++;
      break;
    case 'h':
      printf("\nfibre_ODE, usage :\n");
      printf("\n");
      printf("      simulation of stretched Worm-Like-Chain\n");
      printf("      example, wlc (4*l_p) of 86 monomers of 7 bp each and "
             "overtwist of 0.0 and stretching force of 1 pN :\n");
      printf("      ./fwlc -N 86 -l 7.0 -F 1.0 -t 0.0 -o 0.0 -s 1 -i 100000 -e "
             "10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      example, wlc (8*l_p) of 171 monomers of 7 bp each and "
             "overtwist of 0.0 and stretching force of 1 pN :\n");
      printf("      ./fwlc -N 171 -l 7.0 -F 1.0 -t 0.0 -o 0.0 -s 1 -i 100000 "
             "-e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      example, wlc (16*l_p) of 343 monomers of 7 bp each and "
             "overtwist of 0.0 and stretching force of 1 pN :\n");
      printf("      ./fwlc -N 343 -l 7.0 -F 1.0 -t 0.0 -o 0.0 -s 1 -i 100000 "
             "-e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      example, wlc (32*l_p) of 686 monomers of 7 bp each and "
             "overtwist of 0.0 and stretching force of 1 pN :\n");
      printf("      ./fwlc -N 686 -l 7.0 -F 1.0 -t 0.0 -o 0.0 -s 1 -i 100000 "
             "-e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      example, wlc (320*l_p) of 6857 monomers of 7 bp each and "
             "overtwist of 0.0 and stretching force of 1 pN :\n");
      printf("      ./fwlc -N 6857 -l 7.0 -F 1.0 -t 0.0 -o 0.0 -s 1 -i 100000 "
             "-e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      example, wlc (32*l_p) of 686 monomers of 7 bp each and "
             "overtwist of 0.0 and reduced stretching force 0.1,1 and 10 :\n");
      printf("      ./fwlc -N 686 -l 7.0 -f 0.1 -t 0.0 -o 0.0 -n 0 -s 1 -i "
             "100000 -e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      ./fwlc -N 686 -l 7.0 -f 1.0 -t 0.0 -o 0.0 -n 0 -s 1 -i "
             "100000 -e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      ./fwlc -N 686 -l 7.0 -f 10.0 -t 0.0 -o 0.0 -n 0 -s 1 -i "
             "100000 -e 10000 --scale 1.2 --Vl_every 10 -p "
             "/scratch/pcarriva/simulations/fwlc\n");
      printf("      monomers     (N) : number of monomers.\n");
      printf("      l_bp         (l) : bp per monomer.\n");
      printf("      force        (F) : stretching force applied to the "
             "magnetic bead (in pN).\n");
      printf("      phi          (f) : reduced stretching force applied to the "
             "magnetic bead (force*l_p/kBT).\n");
      printf("      torque       (t) : torque applied to the magnetic bead (in "
             "pN.nm).\n");
      printf("      overtwist    (o) : overtwist.\n");
      printf("      nicks        (n) : number of nicks.\n");
      printf("      seed         (s) : seed for the pRNGs\n");
      printf("      iterations   (i) : number of iterations\n");
      printf("      every        (e) : save conformation every 'every' "
             "iterations\n");
      printf("      scale        (X) : scale the object in every dimension\n");
      printf("                         it is used for the within cut-off "
             "distance of the Verlet-list\n");
      printf("      Vl_every     (V) : build the Verlet-list every this many "
             "time-steps\n");
      printf("      test_Vl      (T) : test the Verlet-list implementation "
             "(--test_Vl yes)\n");
      printf("      split_solver (S) : split solver, first resolve collisions, "
             "apply feedback forces to the articulated system (--split_solver "
             "yes)\n");
      printf("      gyroscopic   (g) : 0 do not consider term dot(inertia) "
             "while 1 does\n");
      printf("      path_to      (p) : where to write the data\n\n");
      printf("For any kind of problems, please consider contact me "
             "p.carrivain_at_gmail.com.\n");
      return 0;
      break;
    }
  }

  if (phi != -1.)
    force = phi / (.5 * (lk / fL));
  double fT = sqrt(fM * pow(fL, 2.) / (kB * temp));
  double mass = l_bp * m1bp / fM;
  double l = (l_bp / 10.5) * 3.5721 * nm / fL;
  double Lk0 = N * l_bp / 10.5;
  double lt = .0; // 86.*nm/fL;// twist persistence length = 0
  double radius =
      1. * nm / fL; // max(1.5*nm/fL,(1.+1./sqrt(.62*8.*pi*.7*(.15)))*nm/fL);
  // double Lk=(overtwist+1.)*Lk0;// sigma=(Lk-Lk0)/Lk0
  double Lk = overtwist * Lk0; // sigma=Lk/Lk0
  double *l_dna = new double[N]();
  std::fill_n(l_dna, N, l);
  double L = .0;
  for (int i = 0; i < N; i++)
    L += l_dna[i];
  double *m_dna = new double[N]();
  std::fill_n(m_dna, N, mass);
  dBodyID *dna = new dBodyID[N];
  dBodyID *copy_dna = new dBodyID[N];
  int3p *ddna = new int3p[N]();
  dGeomID *gdna = new dGeomID[N];
  dGeomID *ggdna = new dGeomID[N];
  dJointID *jdna = new dJointID[N - 1];

  // ode-0.7
  dInitODE();
  T0_CPU = clock();

  // pRNG
  std::mt19937_64 e1(2 * seed), e2(2 * seed + 1), e3(seed);
  std::uniform_real_distribution<> u1(.0, 1.);

  // create the world and the spaces (ODE)
  dWorldID WorldODE = create_world_ODE(.0, wERP, wCFM, nSOR, wSOR);
  dSpaceID SpaceODE = hash_space(radius, 1.01 * (l + 2. * radius));
  dSpaceID gSpaceODE = ghost_hash_space(1.01 * (l + 2. * radius), x_ghost);
  dJointGroupID contactgroup = dJointGroupCreate(0);

  // create wlc dna
  create_linear_chain(WorldODE, SpaceODE, gSpaceODE, N, &dna, ddna, &gdna,
                      &ggdna, &jdna, l, l_bp, radius, mass, x_ghost, overtwist,
                      gyroscopic, e3);
  // copy only the bodies from the world (no joints)
  copy_system_with_no_joints(WorldODE, dna, &copy_dna, N);
  // initialize data
  initialize_body_data(&ddna, N, 20);
  // center of mass to 0,0,0
  remove_com(dna, 0, N);
  vec3 anchor = PositionPosRelBodyODE(dna[0], .0, .0, -.5 * l_dna[0]);
  // bending and twisting strengths
  double *gb = new double[N - 1]();
  double *gt = new double[N - 1]();
  bending_twisting_constants(lk / fL, lt, l_dna, &gb, &gt, N - 1, 1., true);
  // intrinsic twist angle
  double *be = new double[N - 1]();
  double *twe = new double[N - 1]();
  if (lt == .0)
    std::fill_n(twe, N - 1, .0);
  else
    std::fill_n(twe, N - 1, (2. * pi / 10.5) * fmod(l_bp, 10.5));
  // local twist is in ]-pi,pi[ interval
  for (int j = 0; j < (N - 1); j++)
    if (twe[j] > pi)
      twe[j] -= 2. * pi;
  // <(tw-twe)^2> estimation
  int IntStep = 1000000;
  double IntNum = .0, IntDen = .0, IntDx = 2. * acos(-1.) / IntStep, Tw;
  for (int i = 0; i <= IntStep; i++) {
    Tw = -acos(-1.) + i * IntDx;
    IntNum += Tw * Tw * exp(-.5 * gt[0] * Tw * Tw) * IntDx;
    IntDen += exp(-.5 * gt[0] * Tw * Tw) * IntDx;
  }
  // Langevin parameters
  double dt = 0;
  double *friction = new double[N]();
  double *sigmaT = new double[N]();
  double *sigmaR = new double[N]();
  for (int n = 0; n < N; n++) {
    friction[n] = 6. * pi * (viscosity / (fM / (fL * fT))) * (.5 * l);
    sigmaT[n] = friction[n] / mass;
    sigmaR[n] = sigmaT[n];
    dt = (n == 0) ? .01 / sigmaT[n] : fmin(dt, .01 / sigmaT[n]);
  }
  int dofs = 6 * N - 3 * N - 3;
  double mE = .5 * dofs;
  vector<mat33> Hii(N), Hij(N);
  vector<vec3> lambdas(N);

  // parameters
  retour = sprintf(parameters, "n%i_%ix%.1fbp_F%.3f_T%.3f_o%.3f_nicks%i", seed,
                   N, l_bp, force * fF / pN, torque * kB * temp / (pN * nm),
                   overtwist, nicks);
  // description of bodies
  retour = sprintf(nom_s, "%s/visualisation/s%s.out", path_to, parameters);
  fStructure = fopen(nom_s, "w");
  for (int i = 0; i < N; i++)
    fprintf(fStructure, "%i %f %f %i %i %i\n", 0, radius, l, i,
            (int)(i / (int)(N / 8)), 0);
  fclose(fStructure);

  // does a conformation can be found ?
  int start = 0;
  for (int s = (10000 * every); s > 0; s -= every) {
    retour =
        sprintf(nom_g, "%s/conformations/n%s_s%i.out", path_to, parameters, s);
    fSPQ = fopen(nom_g, "rb");
    if (fSPQ != NULL) {
      fclose(fSPQ);
      start = s;
      break;
    }
  }
  if (start > 0) {
    retour = sprintf(nom_g, "%s/conformations/%s_s%i.out", path_to, parameters,
                     start);
    fSPQ = fopen(nom_g, "rb");
    if (fSPQ != NULL) {
      fclose(fSPQ);
      retour = read_write_conformation(dna, nom_g, "read", 0, N);
    }
  }

  // print the parameters
  printf("%s\n", dGetConfiguration());
  printf("%i geoms\n", dSpaceGetNumGeoms(SpaceODE));
  printf("fT:%e s\n", fT);
  printf("fM:%e kg\n", fM);
  printf("fL:%e m\n", fL);
  printf("fF:%e N\n", fF);
  printf("<E>=%f\n", mE);
  printf("dt=%f\n", dt);
  printf("l=%f nm,r=%f nm\n", l * fL / nm, radius * fL / nm);
  printf("erp=%.1f\n", wERP);
  printf("cfm=%.1e\n", wCFM);
  printf("w(SOR):%f\n", dWorldGetQuickStepW(WorldODE));
  printf("n(SOR):%i\n", dWorldGetQuickStepNumIterations(WorldODE));
  printf("%i iterations and sampling every %i steps\n", iterations, every);
  printf("force:%.3f pN,torque:%.3f pN.nm,Lk0:%.3f,Lk:%.3f\n", force * fF / pN,
         torque * kB * temp / (pN * nm), Lk0, Lk);
  retour = sprintf(nom_s, "%s/fwlc_%i.out", path_to, seed);
  fSortie = fopen(nom_s, "a");
  retour = fprintf(fSortie, "%s\n", dGetConfiguration());
  retour = fprintf(fSortie, "%i geoms\n", dSpaceGetNumGeoms(SpaceODE));
  retour = fprintf(fSortie, "fT:%e s\n", fT);
  retour = fprintf(fSortie, "fM:%e kg\n", fM);
  retour = fprintf(fSortie, "fL:%e m\n", fL);
  retour = fprintf(fSortie, "fF:%e N\n", fF);
  retour = fprintf(fSortie, "<E>=%f\n", mE);
  retour = fprintf(fSortie, "dt=%f\n", dt);
  retour = fprintf(fSortie, "l=%f nm,r=%f nm\n", l * fL / nm, radius * fL / nm);
  retour = fprintf(fSortie, "erp=%.1f\n", wERP);
  retour = fprintf(fSortie, "cfm=%.1e\n", wCFM);
  retour = fprintf(fSortie, "w(SOR):%f\n", dWorldGetQuickStepW(WorldODE));
  retour = fprintf(fSortie, "n(SOR):%i\n",
                   dWorldGetQuickStepNumIterations(WorldODE));
  retour = fprintf(fSortie, "%i iterations and sampling every %i steps\n",
                   iterations, every);
  retour =
      fprintf(fSortie, "force:%.3f pN,torque:%.3f pN.nm,Lk0:%.3f,Lk:%.3f\n",
              force * fF / pN, torque * kB * temp / (pN * nm), Lk0, Lk);
  fclose(fSortie);

  // loop over time, confinement ?
  for (int s = (start + (start > 0)); s <= (start + iterations); s++) {

    // nicks in the [0.1,0.9] chain interval ?
    // for 'n' nicks we split the simulation duration into 'n+1' sub-intervals
    if (nicks > 0 && s > 0) {
      if ((s % (int)(iterations / (nicks + 1))) == 0) {
        retour = (int)((.1 + .8 * u1(e3)) * N);
        printf("nick %i\n", retour);
        gb[retour] = .0;
        gt[retour] = .0;
      }
    }
    // "flying-ice-cube" correction
    if ((s % 10) == 0 && s >= 1000 && false)
      flying_ice_cube_correction(dna, N);
    // bending and twist
    ground_bending_twist(dna[0], gb[0], gt[0], u0, v0, t0);
    bending_twist(dna, gb, gt, 1., be, twe, 0, N - 1, true, cos_Tw2[0],
                  cos_Tw2[1]);
    // Langevin-Euler (local thermostat)
    std::thread first(langevin_euler, dna, sigmaT, sigmaR, dt, 1., 0,
                      (N - N % 2) / 2, std::ref(e1));
    std::thread second(langevin_euler, dna, sigmaT, sigmaR, dt, 1.,
                       (N - N % 2) / 2 + 1, N, std::ref(e2));
    first.join();
    second.join();
    // add stretching force
    addPosRelForceBodyODE(
        dna[N - 1],
        PositionPosRelBodyODE(dna[N - 1], .0, .0, .5 * l_dna[N - 1]),
        vec3(.0, .0, force));
    if (false) {
      // enable joints
      for (int j = 0; j < (N - 1); j++)
        dJointEnable(jdna[j]);
      // build the Verlet-list
      vl = build_Verlet_list(gSpaceODE, s - start, Vl_every, &ddna, N, 4,
                             my_nearCallback);
      // collide all in the normal space
      nc = collide_all(WorldODE, gdna, ddna, N, wERP, wCFM, contactgroup,
                       sum_abs_depth, max_abs_depth);
      if (true) {
        // copy state of each body + no articulated system to resolve excluded
        // volume constraints
        success = copy_state_body(&dna, &copy_dna, N, 1);
        // step the world with contact joints only
        // get the feedback and apply it to the articulated system
        if (success)
          step_the_world_contact_joints(WorldODE, &dna, &copy_dna, N, dt, 1);
      }
    }
    // do not solve collisions and joints at the same time
    solution_LC(dna, m_dna, l_dna, &N, 1, 1, 0, dt, anchor, Hii, Hij, lambdas);
    // disable joints
    for (int j = 0; j < (N - 1); j++)
      dJointDisable(jdna[j]);
    // step the world
    if (nc == 0 && false) {
      std::thread third(vsemi_implicit_euler, dna, dt, 0, (N - N % 2) / 2);
      std::thread fourth(vsemi_implicit_euler, dna, dt, (N - N % 2) / 2 + 1, N);
      third.join();
      fourth.join();
      std::thread fifth(semi_implicit_euler, dna, dt, 0, (N - N % 2) / 2);
      std::thread sixth(semi_implicit_euler, dna, dt, (N - N % 2) / 2 + 1, N);
      fifth.join();
      sixth.join();
    } else
      dWorldStep(WorldODE, dt);
    // clear contact group
    if (contactgroup)
      dJointGroupEmpty(contactgroup);

    if ((s % every) == 0) {
      // save the conformation
      retour =
          sprintf(nom_s, "%s/conformations/%s_s%i.out", path_to, parameters, s);
      read_write_conformation(dna, nom_s, "write", 0, N);
      // save for visualisation
      retour = sprintf(nom_v, "%s/visualisation/v%s_s%i.out", path_to,
                       parameters, s);
      write_visualisation(dna, nom_v, 0, N, "w");
      // print some info
      printf("%.3f e/<e>=%f %i collision(s) z/L=%f <cos>=%f(%f) "
             "<Tw^2>=%f(%f/%f)\n",
             (double)s / (double)iterations, Ktr(dna, 0, N, false, 2) / mE, nc,
             sub3(PositionPosRelBodyODE(dna[N - 1], .0, .0, .5 * l_dna[N - 1]),
                  PositionPosRelBodyODE(dna[0], .0, .0, -.5 * l_dna[0]))
                     .z /
                 L,
             cos_Tw2[0], (lk / fL - l) / (lk / fL + 1), cos_Tw2[1],
             IntNum / IntDen, lt / l);
      printf("PC time=%f seconds\n", (clock() - T0_CPU) / CLOCKS_PER_SEC);
      retour = sprintf(nom_s, "%s/fwlc_%i.out", path_to, seed);
      fSortie = fopen(nom_s, "a");
      retour = fprintf(
          fSortie,
          "%.3f e/<e>=%f %i collision(s) z/L=%f <cos>=%f(%f) "
          "<Tw^2>=%f(%f/%f)\n",
          (double)s / (double)iterations, Ktr(dna, 0, N, false, 2) / mE, nc,
          sub3(PositionPosRelBodyODE(dna[N - 1], .0, .0, .5 * l_dna[N - 1]),
               PositionPosRelBodyODE(dna[0], .0, .0, -.5 * l_dna[0]))
                  .z /
              L,
          cos_Tw2[0], (lk / fL - l) / (lk / fL + 1), cos_Tw2[1],
          IntNum / IntDen, lt / l);
      retour = fprintf(fSortie, "PC time=%f seconds\n",
                       (clock() - T0_CPU) / CLOCKS_PER_SEC);
      fclose(fSortie);
    }
  } // end of simulation loop

  // close ODE
  if (contactgroup)
    dJointGroupDestroy(contactgroup);
  dWorldDestroy(WorldODE);
  dSpaceSetCleanup(SpaceODE, 1);
  dSpaceDestroy(SpaceODE);
  dCloseODE();
  // clean pointers
  delete[] be;
  delete[] twe;
  delete[] l_dna;
  delete[] m_dna;
  delete[] dna;
  delete[] ddna;
  delete[] gdna;
  delete[] ggdna;
  delete[] jdna;
  delete[] friction;
  delete[] sigmaT;
  delete[] sigmaR;
  return 0;
}
