CCX=g++

SRC_DIR=$(PWD)

PATH_TO_ODE=${SRC_DIR}/ode-0.16/myBuild_static
PATH_TO_EIGEN=${SRC_DIR}/eigen_340

# CXXFLAGS=-I${PATH_TO_ODE}/include -Wextra -Wall -ansi -pedantic -g -std=c++0x -O3 -s -DdDOUBLE -fopenmp
# LDFLAGS=-L${PATH_TO_ODE}/lib -lode -lpthread -fopenmp
# CXXFLAGS=-I${PATH_TO_ODE}/include -I${PATH_TO_EIGEN} -Wextra -Wall -ansi -pedantic -g -std=c++0x -O3 -s -DdDOUBLE
CXXFLAGS=-I${PATH_TO_ODE}/include -I${PATH_TO_EIGEN} -Wextra -Wall -ansi -pedantic -g -std=c++17 -O3 -s -DdDOUBLE
LDFLAGS=-L${PATH_TO_ODE}/lib -lode -lpthread
# LDFLAGS=-L/usr/lib

COMMON_SOURCES=$(SRC_DIR)/functions.cpp ${SRC_DIR}/read_nucleosome.cpp
COMMON_OBJECTS=$(COMMON_SOURCES:.cpp=.o)

SOURCES0=fibre_ODE.cpp
OBJECTS0=$(SOURCES0:.cpp=.o)
SOURCES1=bacteria_ODE.cpp
OBJECTS1=$(SOURCES1:.cpp=.o)
SOURCES2=fwlc_ODE.cpp
OBJECTS2=$(SOURCES2:.cpp=.o)
SOURCES3=patches_ODE.cpp
OBJECTS3=$(SOURCES3:.cpp=.o)
SOURCES4=fibre_MC.cpp
OBJECTS4=$(SOURCES4:.cpp=.o)
SOURCES5=yeast_ODE.cpp
OBJECTS5=$(SOURCES5:.cpp=.o)

EXECUTABLE0=$(SOURCES0:.cpp=)
EXECUTABLE1=$(SOURCES1:.cpp=)
EXECUTABLE2=$(SOURCES2:.cpp=)
EXECUTABLE3=$(SOURCES3:.cpp=)
EXECUTABLE4=$(SOURCES4:.cpp=)
EXECUTABLE5=$(SOURCES5:.cpp=)

.PHONY: all fibre bacteria fwlc patches fibre_mc yeast

all: fibre bacteria fwlc patches fibre_mc yeast

fibre: $(EXECUTABLE0)

bacteria: $(EXECUTABLE1)

fwlc: $(EXECUTABLE2)

patches: $(EXECUTABLE3)

fibre_mc: $(EXECUTABLE4)

yeast: $(EXECUTABLE5)


%.o: %.cpp
	$(CCX) -o $@ -c $< $(CXXFLAGS)

$(EXECUTABLE0): $(COMMON_OBJECTS) $(OBJECTS0)
	$(CCX) -o $@ $^ $(LDFLAGS)

$(EXECUTABLE1): $(COMMON_OBJECTS) $(OBJECTS1)
	$(CCX) -o $@ $^ $(LDFLAGS)

$(EXECUTABLE2): $(COMMON_OBJECTS) $(OBJECTS2)
	$(CCX) -o $@ $^ $(LDFLAGS)

$(EXECUTABLE3): $(COMMON_OBJECTS) $(OBJECTS3)
	$(CCX) -o $@ $^ $(LDFLAGS)

$(EXECUTABLE4): $(COMMON_OBJECTS) $(OBJECTS4)
	$(CCX) -o $@ $^ $(LDFLAGS)

$(EXECUTABLE5): $(COMMON_OBJECTS) $(OBJECTS5)
	$(CCX) -o $@ $^ $(LDFLAGS)

mrproper:
	rm -f *.o $(EXECUTABLE0) $(EXECUTABLE1) $(EXECUTABLE2) $(EXECUTABLE3) $(EXECUTABLE4) $(EXECUTABLE5)
