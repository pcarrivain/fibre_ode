#include "functions.h"
#include "read_nucleosome.h"
#include <getopt.h>
#include <time.h>

using namespace std;

const int nchr = 16;
int N = 0, dofs = 0, *segments, *chrC, *chrT, *centromeres, *Lchr, *Gchr;
dBodyID *dna;
dGeomID *gdna;
dGeomID *ggdna;
dJointID *jdna;
int3p *ddna;
dTriMeshDataID tnucleus;
dGeomID gnucleus;
dVector3 center, Extents, *Vertices, *Normals, *Barycentres;
dTriIndex *Indices;
double *lf, *rf, *mf;
int *gf;
// units
double yT = sqrt(yM * yL * yL / (kB * temp)); // s
double yE = yM * yL * yL / (yT * yT);         // J
double inv_kBT = 1. / (kB * temp / yE);
const double R = 1000. * nm / yL;
int retour, nc, nb, interaction, vl;
double PCTime, Et, mE, ratioE, d0, d1, d2, theta, sum_abs_depth, max_abs_depth;
const double r_e_m = 380. * nm / yL;
const double int_Tel[4] = {
    20. / inv_kBT, 5. * nm / yL, log(2.) / int_Tel[1],
    int_Tel[1] -
        (1. / int_Tel[2]) * log(1. - sqrt(1. - 1. / (100. * int_Tel[0])))};
const double int_HC = 20. / inv_kBT;
char cname[50], vname[50], sname[50];
vector<vec3> vertex;
vec3 ri, rj, rij, force, axe;
FILE *fOut;
const int nlevels = 5;
const double t = (1. + sqrt(5.)) / 2.;
int ntriangles = 20;
int nt_triangle = (int)(20 * pow(4., nlevels));
int nt_vertex = nt_triangle / 2 + 2;
vector<int3> triangle, triangle2;
int3 indice;
vector<vector<int>> cache(nt_vertex, vector<int>(nt_vertex));
int3p nc_test_Vl;

// extern variables
int *is_linker_nucleosome = new int[1]();
int *nucleosome_start = new int[1]();
double xmass = 1., xfriction = 1.;
bool same_mass = false;
// variables: monomers partition
int nbins = 1, lhit = 0, nperb = 10, nnperb = 10;
int *binn = NULL;
int *ninb = NULL;
int *bhit = NULL;
int *nhit = NULL;
int *lperb = NULL;
// variables: nucleosomes partition
int *nbinn = NULL;
int *nninb = NULL;
int *nbhit = NULL;
int *nnhit = NULL;
int *nlperb = NULL;

// main
int main(int argc, char **argv) {
  void (*my_nearCallback)(void *, dGeomID, dGeomID);
  my_nearCallback = &nearCallback;
  // default inputs
  int seed = 1;
  int step0 = 0;
  int iterations = 1000000;
  int every = (int)(iterations / 100);
  double x_ghost = 1.2;
  int Vl_every = 10;
  bool test_Vl = false;
  bool btrimesh = false;
  int gyroscopic = 1;
  bool accurate_step = false;
  bool start_at_zero = false;
  int MSTEPS = 400000;
  int n_args = 0, arg_i;
  const option long_opts[] = {
      {"seed", required_argument, nullptr, 's'},
      {"iterations", required_argument, nullptr, 'i'},
      {"every", required_argument, nullptr, 'e'},
      {"scale", required_argument, nullptr, 'X'},
      {"Vl_every", required_argument, nullptr, 'V'},
      {"test_Vl", required_argument, nullptr, 'T'},
      {"nucleus_trimesh", required_argument, nullptr, 't'},
      {"gyroscopic", required_argument, nullptr, 'g'},
      {"accurate_step", required_argument, nullptr, 'A'},
      {"start_at_zero", required_argument, nullptr, 'Z'},
      {"MSTEPS", required_argument, nullptr, 'M'},
      {"help", optional_argument, nullptr, 'h'}};
  while ((arg_i = getopt_long(argc, argv, "s:i:e:X:V:T:tg:AM:Zh", long_opts,
                              nullptr)) != -1) {
    switch (arg_i) {
    case 's':
      seed = (int)(atof(optarg));
      n_args++;
      break;
    case 'i':
      iterations = (int)(atof(optarg));
      n_args++;
      break;
    case 'e':
      every = (int)(atof(optarg));
      n_args++;
      break;
    case 'X':
      x_ghost = atof(optarg);
      n_args++;
      break;
    case 'V':
      Vl_every = (int)(atof(optarg));
      n_args++;
      break;
    case 'T':
      test_Vl = (strcmp(optarg, "yes") == 0);
      n_args++;
      break;
    case 't':
      btrimesh = true;
      n_args++;
      break;
    case 'g':
      gyroscopic = (int)(atof(optarg));
      n_args++;
      break;
    case 'A':
      accurate_step = true;
      n_args++;
      break;
    case 'M':
      MSTEPS = (int)(atof(optarg));
      n_args++;
      break;
    case 'Z':
      start_at_zero = true;
      n_args++;
      break;
    case 'h':
    default:
      printf("\nyeast, usage:\n");
      printf("      example:\n");
      printf("      ./yeast -s 1 -i 100000 -e 10000 --scale 1.01 Vl_every 1 -g "
             "1 -M 400000 -Z\n\n");
      printf("      seed        (s) : seed for the pRNGs\n");
      printf("      iterations  (i) : number of iterations\n");
      printf("      every       (e) : save conformation every 'every' "
             "iterations\n");
      printf("                        stack conformation in the visualisation "
             "file\n");
      printf("      scale       (X) : scale the object in every dimension\n");
      printf("                        it is used for the within cut-off "
             "distance of the Verlet-list\n");
      printf("      Vl_every    (V) : build the Verlet-list every this many "
             "time-steps\n");
      printf("      test_Vl     (T) : test the Verlet-list implementation "
             "(--test_Vl yes)\n");
      printf("      btrimesh    (t) : use trimesh to build nucleus\n");
      printf("                        default is exponential potential\n");
      printf("      gyroscopic  (g) : 0 do not consider term dot(inertia) "
             "while 1 does\n");
      printf("      accurate_step (A) : use 'dWorldStep' or 'dWorldQuickStep' "
             "function from 'Open-Dynamics-Engine'\n");
      printf("      MSTEPS      (M) : number of steps to slowly confine yeast "
             "genome to nucleus\n");
      printf(
          "      start_at_zero (Z) : do not look for initial conformation\n\n");
      printf("      For any kind of problems, please consider contact me "
             "p.carrivain_at_gmail.com.\n");
      return 0;
      break;
    }
  }

  // pRNG
  std::mt19937_64 e0(seed), e1(2 * seed), e2(2 * seed + 1);

  // init ODE
  dInitODE();
  PCTime = clock();

  // create the world and the spaces (ODE)
  dWorldID WorldODE = create_world_ODE(.0, wERP, wCFM, nSOR, wSOR);
  dSpaceID SpaceODE = hash_space(nm / yL, 2. * 75. * nm / yL);
  dSpaceID gSpaceODE = ghost_hash_space(2. * 75. * nm / yL, x_ghost);
  dJointGroupID contactgroup = dJointGroupCreate(0);

  printf("create genome\n");
  N = create_yeast(&dna, &gdna, &ggdna, ddna, &jdna, WorldODE, SpaceODE,
                   gSpaceODE, &gf, &lf, &rf, &mf, x_ghost, gyroscopic, dofs,
                   segments, centromeres, chrC, chrT, Lchr, Gchr, e0);
  if (N == -1)
    return 0;
  mE = .5 * dofs / inv_kBT;

  // restart conformation ?
  sprintf(cname, "conformations/yeast_n%i.out", seed);
  sprintf(vname, "visualisation/yeast_n%i.out", seed);
  if (!start_at_zero) {
    printf("read restart conformation: %s\n", cname);
    retour = read_write_conformation(dna, cname, "read", 0, N);
    if (retour == 0) {
      printf("did not find restart conformation: %s\n", cname);
      return 0;
    }
  } else
    step0 = 0;

  // size, chr color ...
  sprintf(sname, "visualisation/syeast_n%i.out", seed);
  fOut = fopen(sname, "w");
  nb = 0;
  for (int n = 0; n < nchr; n++) {
    for (int i = 0; i < segments[n]; i++) {
      if (i == centromeres[n])
        nc = 0;
      else
        nc = (n == 11 && i >= 90 && i < (90 + 150)) ? 1 : 2;
      if (n == 11 && i >= 90 && i < (90 + 150))
        fprintf(fOut, "%i %f %f %i %i %i\n", 1, rf[nb], rf[nb], nb, nc, n);
      else
        fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[nb], lf[nb], nb, nc, n);
      nb++;
    }
  }
  fclose(fOut);

  // parameters: Langevin dynamics
  double dt = .0, tau = .0;
  double *friction = new double[N]();
  double *sigmaT = new double[N]();
  double *sigmaR = new double[N]();
  for (int n = 0; n < N; n++) {
    friction[n] =
        xfriction * 6. * pi * (viscosity / (yM / (yL * yT))) * (.5 * lf[n]);
    sigmaT[n] = friction[n] / mf[n];
    sigmaR[n] = sigmaT[n];
    tau = (n == 0) ? 1. / sigmaT[n] : fmin(tau, 1. / sigmaT[n]);
  }
  dt = .01 * tau;

  // write parameters
  printf("%s\n", dGetConfiguration());
  retour = sprintf(sname, "parameters/yeast_n%i.out", seed);
  fOut = fopen(sname, "w");
  retour = fprintf(fOut, "nbonds %i\n", N);
  retour = fprintf(fOut, "iterations %i\n", iterations);
  retour = fprintf(fOut, "every %i\n", every);
  retour = fprintf(fOut, "length_unit %e\n", yL);
  retour = fprintf(fOut, "time_unit %e\n", yT);
  retour = fprintf(fOut, "mass_unit %e\n", yM);
  retour = fprintf(fOut, "xmass %e\n", xmass);
  retour = fprintf(fOut, "force_unit %e\n", yM * yL / (yT * yT));
  retour = fprintf(fOut, "energy_unit %e\n", yE);
  retour = fprintf(fOut, "kBT %e\n", kB * temp / yE);
  retour = fprintf(fOut, "dt %e\n", dt);
  retour = fprintf(fOut, "tau %e\n", tau);
  retour = fprintf(fOut, "dofs %i\n", dofs);
  retour = fprintf(fOut, "nucleus_radius_nm %e\n", R * yL / nm);
  for (int i = 0; i < nchr; i++)
    printf("chr%i: N=%i, L=%f nm, G=%i bp, L/D=%f\n", i, segments[i],
           Lchr[i] * yL / nm, Gchr[i], Lchr[i] / (2. * R));
  retour = fprintf(fOut, "gyroscopic %i\n", gyroscopic);
  retour = fprintf(fOut, "erp %.2f\n", wERP);
  retour = fprintf(fOut, "cfm %.2e\n", wCFM);
  retour = fprintf(fOut, "w(SOR) %f\n", dWorldGetQuickStepW(WorldODE));
  retour =
      fprintf(fOut, "n(SOR) %i\n", dWorldGetQuickStepNumIterations(WorldODE));
  fclose(fOut);

  // initialize data
  initialize_body_data(&ddna, N, 20);

  // confine the genome
  double moutside = .0, mforce = .0, dv = .0, DV = .0;
  int noutsides = 0;
  if (start_at_zero) {
    for (int n = 0; n < N; n++)
      moutside = fmax(moutside, norm3(PositionBodyODE(dna[n])));
    printf("outside/R=%f\n", moutside / R);
    for (int r = 0; r < 2; r++) {
      for (int s = 0; s < ((r == 0) ? 1000 : MSTEPS); s++) {
        // spherical confinement
        noutsides = 0;
        moutside = mforce = DV = .0;
        dv = DBL_MAX;
        ratioE = (Ktr(dna, 0, N, false, 2) > (3. * mE))
                     ? sqrt(3. * mE / Ktr(dna, 0, N, false, 2))
                     : 1.;
        for (int n = 0; n < N; n++) {
          set3_velocities(dna[n], mult3(ratioE, LinVelBodyODE(dna[n])),
                          mult3(ratioE, AngVelBodyODE(dna[n])));
          dv = fmin(dv, norm3(LinVelBodyODE(dna[n])));
          DV = fmax(DV, norm3(LinVelBodyODE(dna[n])));
          ri = PositionBodyODE(dna[n]);
          d0 = norm3(ri);
          if (d0 > (R - fmax(rf[n], .5 * lf[n]))) {
            theta = (double)(s + 1) / (double)MSTEPS;
            d1 = (1. * nm / yL) / fmin(1., theta);
            d2 = (64. / inv_kBT) *
                 (exp((d0 - (R - fmax(rf[n], .5 * lf[n]))) / d1) - 1.) / d0;
            mforce = fmax(mforce, d2);
            dBodyAddForce(dna[n], -d2 * ri.x, -d2 * ri.y, -d2 * ri.z);
            noutsides++;
            moutside = fmax(moutside, d0 - (R - fmax(rf[n], .5 * lf[n])));
          }
        }
        if (noutsides == 0) {
          for (int n = 0; n < N; n++) {
            dBodySetForce(dna[n], .0, .0, .0);
            dBodySetTorque(dna[n], .0, .0, .0);
          }
          break;
        }
        // Langevin-Euler (local thermostat)
        // theta=(r==0)?1.:fmin(1.,(double)(s+1)/(double)MSTEPS);
        theta = 1.;
        std::thread first(langevin_euler, dna, sigmaT, sigmaR, theta * dt,
                          inv_kBT, 0, (N - N % 2) / 2, std::ref(e1));
        std::thread second(langevin_euler, dna, sigmaT, sigmaR, theta * dt,
                           inv_kBT, (N - N % 2) / 2 + 1, N - 1, std::ref(e2));
        first.join();
        second.join();
        // build Verlet-list
        vl = build_Verlet_list(gSpaceODE, 0, 1, &ddna, N, 4, my_nearCallback);
        // collide all in the normal space
        nc = collide_all(WorldODE, gdna, ddna, N, wERP, wCFM, contactgroup,
                         sum_abs_depth, max_abs_depth);
        if ((s % 100) == 0)
          printf("s=%i/%i: E/<E>=%f, %i outside(s) max(outside)=%e, dr=%e/%e, "
                 "max(force)=%e, %i collision(s)\n",
                 s, MSTEPS, Ktr(dna, 0, N, false, 2) / mE, noutsides,
                 moutside / d1, dv * theta * dt, DV * theta * dt, mforce, nc);
        if ((s % 500) == 0) {
          if (r == 0 && s == 0)
            write_visualisation(dna, vname, 0, N, "w");
          else
            write_visualisation(dna, vname, 0, N, "a");
        }
        // step the dWorldID
        if (accurate_step)
          dWorldStep(WorldODE, theta * dt);
        else
          dWorldQuickStep(WorldODE, theta * dt);
        if (contactgroup)
          dJointGroupEmpty(contactgroup);
      }
    }
  }

  if (btrimesh) {
    printf("create nucleus trimesh\n");
    // add nucleus (icosahedron)
    // 12 vertex
    for (int i = 0; i < (nt_vertex * nt_vertex); i++)
      cache[(i - i % nt_vertex) / nt_vertex][i % nt_vertex] = -1;
    vertex.push_back(normalize3(vec3(-1., t, 0.)));
    vertex.push_back(normalize3(vec3(1., t, 0.)));
    vertex.push_back(normalize3(vec3(-1., -t, 0.)));
    vertex.push_back(normalize3(vec3(1., -t, 0.)));
    vertex.push_back(normalize3(vec3(0., -1., t)));
    vertex.push_back(normalize3(vec3(0., 1., t)));
    vertex.push_back(normalize3(vec3(0., -1., -t)));
    vertex.push_back(normalize3(vec3(0., 1., -t)));
    vertex.push_back(normalize3(vec3(t, 0., -1.)));
    vertex.push_back(normalize3(vec3(t, 0., 1.)));
    vertex.push_back(normalize3(vec3(-t, 0., -1.)));
    vertex.push_back(normalize3(vec3(-t, 0., 1.)));
    // 20 triangles
    // five triangles around point 0
    triangle.push_back(int3(0, 11, 5));
    triangle.push_back(int3(0, 5, 1));
    triangle.push_back(int3(0, 1, 7));
    triangle.push_back(int3(0, 7, 10));
    triangle.push_back(int3(0, 10, 11));
    // five faces around point 0
    triangle.push_back(int3(1, 5, 9));
    triangle.push_back(int3(5, 11, 4));
    triangle.push_back(int3(11, 10, 2));
    triangle.push_back(int3(10, 7, 6));
    triangle.push_back(int3(7, 1, 8));
    // five triangles around point 3
    triangle.push_back(int3(3, 9, 4));
    triangle.push_back(int3(3, 4, 2));
    triangle.push_back(int3(3, 2, 6));
    triangle.push_back(int3(3, 6, 8));
    triangle.push_back(int3(3, 8, 9));
    // five faces around point 0
    triangle.push_back(int3(4, 9, 5));
    triangle.push_back(int3(2, 4, 11));
    triangle.push_back(int3(6, 2, 10));
    triangle.push_back(int3(8, 6, 7));
    triangle.push_back(int3(9, 8, 1));
    // create new triangles (from one to four triangles)
    for (int i = 0; i < nlevels; i++) {
      // new triangles array
      triangle2.resize(0);
      // loop over old triangles
      for (int j = 0; j < ntriangles; j++) {
        // does vertex a, vertex b and vertex c exist ?
        // no: add at the end of the list
        // yes: find vertex index
        if (cache[triangle[j].i][triangle[j].j] == -1) {
          vertex.push_back(normalize3(
              mult3(.5, add3(vertex[triangle[j].i], vertex[triangle[j].j]))));
          indice.i = (int)(vertex.size()) - 1;
          cache[triangle[j].i][triangle[j].j] = indice.i;
          cache[triangle[j].j][triangle[j].i] = indice.i;
        } else
          indice.i = cache[triangle[j].i][triangle[j].j];
        if (cache[triangle[j].j][triangle[j].k] == -1) {
          vertex.push_back(normalize3(
              mult3(.5, add3(vertex[triangle[j].j], vertex[triangle[j].k]))));
          indice.j = (int)(vertex.size()) - 1;
          cache[triangle[j].j][triangle[j].k] = indice.j;
          cache[triangle[j].k][triangle[j].j] = indice.j;
        } else
          indice.j = cache[triangle[j].j][triangle[j].k];
        if (cache[triangle[j].k][triangle[j].i] == -1) {
          vertex.push_back(normalize3(
              mult3(.5, add3(vertex[triangle[j].k], vertex[triangle[j].i]))));
          indice.k = (int)(vertex.size()) - 1;
          cache[triangle[j].k][triangle[j].i] = indice.k;
          cache[triangle[j].i][triangle[j].k] = indice.k;
        } else
          indice.k = cache[triangle[j].k][triangle[j].i];
        triangle2.push_back(int3(triangle[j].i, indice.i, indice.k));
        triangle2.push_back(int3(triangle[j].j, indice.j, indice.i));
        triangle2.push_back(int3(triangle[j].k, indice.k, indice.j));
        triangle2.push_back(indice);
      }
      // recopy new triangles
      ntriangles = (int)triangle2.size();
      triangle = triangle2;
    }
    // three vertex per triangle
    Indices = new dTriIndex[3 * ntriangles];
    Vertices = new dVector3[nt_vertex];
    Normals = new dVector3[ntriangles];
    Barycentres = new dVector3[ntriangles];
    for (int i = 0; i < ntriangles; i++) {
      Indices[3 * i + 0] = triangle[i].k;
      Indices[3 * i + 1] = triangle[i].j;
      Indices[3 * i + 2] = triangle[i].i;
      for (int j = 0; j < 3; j++) {
        Vertices[Indices[3 * i + j]][0] = R * vertex[Indices[3 * i + j]].x;
        Vertices[Indices[3 * i + j]][1] = R * vertex[Indices[3 * i + j]].y;
        Vertices[Indices[3 * i + j]][2] = R * vertex[Indices[3 * i + j]].z;
        Vertices[Indices[3 * i + j]][3] = 0.;
      }
      // Barycentre 3G=I+J+K
      ri = mult3(1. / 3.,
                 add3(vertex[triangle[i].i],
                      add3(vertex[triangle[i].j], vertex[triangle[i].k])));
      Barycentres[i][0] = ri.x;
      Barycentres[i][1] = ri.y;
      Barycentres[i][2] = ri.z;
      // normal to the triangle
      Normals[i][0] = -ri.x / norm3(ri);
      Normals[i][1] = -ri.y / norm3(ri);
      Normals[i][2] = -ri.z / norm3(ri);
    }
    // TriMesh
    tnucleus = dGeomTriMeshDataCreate();
    dGeomTriMeshDataBuildDouble1(tnucleus, Vertices, sizeof(dVector3),
                                 nt_vertex, Indices, 3 * ntriangles,
                                 3 * sizeof(dTriIndex), Normals);
    gnucleus = dCreateTriMesh(SpaceODE, tnucleus, 0, 0, 0);
    dGeomSetPosition(gnucleus, 0., 0., 0.);
  }

  // simulation loop
  for (int s = step0; s < (step0 + iterations); s++) {

    // kinetic energy
    Et = Ktr(dna, 0, N, false, 2);

    // // centromeres-membranes interactions
    // for(int i=0;i<nchr;i++){
    //   ri=PositionBodyODE(dna[chrC[i]]);
    //   rij=sub3(ri,vec3(0.,0.,-R));
    //   force=mult3(-int_HC*(norm3(rij)-r_e_m),normalize3(rij));
    //   addPosForceBodyODE(dna[chrC[i]],force);
    // }
    // if(interaction!=0){
    //   // telomeres-membrane interactions
    //   for(int i=0;i<(2*nchr);i++){
    // 	ri=PositionBodyODE(dna[chrT[i]]);
    // 	rj=mult3(R,normalize3(ri));
    // 	axe=normalize3(sub3(ri,rj));
    // 	ri=add3(ri,mult3(-r[chrT[i]],axe));
    // 	rij=sub3(ri,rj);
    // 	force=ForceMorse(int_Tel[0],int_Tel[1],int_Tel[2],rij);
    // 	addPosForceBodyODE(dna[chrT[i]],ri,force);
    //   }
    // }

    if (!btrimesh) {
      // spherical confinement (do not use nucleus trimesh)
      noutsides = 0;
      moutside = mforce = .0;
      for (int n = 0; n < N; n++) {
        ri = PositionBodyODE(dna[n]);
        d0 = norm3(ri);
        if (d0 > (R - fmax(rf[n], .5 * lf[n]))) {
          d1 = 5. * nm / yL;
          d2 = (32. / inv_kBT) *
               (exp((d0 - (R - fmax(rf[n], .5 * lf[n]))) / d1) - 1.) / d0;
          mforce = fmax(mforce, d2);
          dBodyAddForce(dna[n], -d2 * ri.x, -d2 * ri.y, -d2 * ri.z);
          noutsides++;
          moutside = fmax(moutside, d0 - (R - fmax(rf[n], .5 * lf[n])));
        }
      }
    }

    // Langevin-Euler (local thermostat)
    std::thread first(langevin_euler, dna, sigmaT, sigmaR, dt, inv_kBT, 0,
                      (N - N % 2) / 2, std::ref(e1));
    std::thread second(langevin_euler, dna, sigmaT, sigmaR, dt, inv_kBT,
                       (N - N % 2) / 2 + 1, N - 1, std::ref(e2));
    first.join();
    second.join();

    // test Verlet-list implementation ?
    if (test_Vl) {
      nc_test_Vl = int3p(4, 0, 0);
      dSpaceCollide(SpaceODE, &nc_test_Vl, &test_nearCallback);
    }
    // build the Verlet-list
    vl = build_Verlet_list(gSpaceODE, s - step0, Vl_every, &ddna, N, 4,
                           my_nearCallback);
    // collide all in the normal space
    nc = collide_all(WorldODE, gdna, ddna, N, wERP, wCFM, contactgroup,
                     sum_abs_depth, max_abs_depth);
    if (test_Vl && nc != nc_test_Vl.j) {
      // for(int n=0;n<N;n++) for(int v=2;v<(ddna[n].p)[0];v++) printf("VL %i
      // %i\n",n,(ddna[n].p)[v]);
      printf("s=%i ODE=%i VL=%i/%i\n", s, nc_test_Vl.j, nc, vl);
      return 0;
    }

    // step the dWorldID
    if (accurate_step)
      dWorldStep(WorldODE, dt);
    else
      dWorldQuickStep(WorldODE, dt);
    if (contactgroup)
      dJointGroupEmpty(contactgroup);

    // save conformations
    if ((s % every) == 0) {
      // if(start_at_zero && s==0) write_visualisation(dna,vname,0,N,"w");
      // else write_visualisation(dna,vname,0,N,"a");
      write_visualisation(dna, vname, 0, N, "a");
      read_write_conformation(dna, cname, "write", 0, N);
    }

    // print data
    if (((s - step0) % every) == 0)
      printf("%.2f: E/<E>=%f, %i collision(s), %i outside(s), %f minutes\n",
             (double)(s - step0) / (double)iterations, Et / mE, nc, noutsides,
             (clock() - PCTime) / CLOCKS_PER_SEC / 60.);
  }

  // close ODE
  delete[] Indices;
  delete[] Vertices;
  delete[] Barycentres;
  delete[] Normals;
  delete[] dna;
  delete[] ddna;
  delete[] gdna;
  delete[] ggdna;
  delete[] jdna;
  delete[] friction;
  delete[] sigmaT;
  delete[] sigmaR;
  if (contactgroup)
    dJointGroupDestroy(contactgroup);
  dWorldDestroy(WorldODE);
  dSpaceSetCleanup(SpaceODE, 1);
  dSpaceDestroy(SpaceODE);
  dCloseODE();
  printf("Temps PC de la simulation:%f minutes\n",
         (clock() - PCTime) / CLOCKS_PER_SEC / 60.);
  return 0;
}
