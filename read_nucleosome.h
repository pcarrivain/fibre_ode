#ifndef READ_NUCLEOSOME_H
#define READ_NUCLEOSOME_H
#include <cstdio>
#include "variables.h"

// read nucleosomal dna positions (in nm), scale it and return it
std::vector<vec3> read_dna_nucl_pos(const char *name,double l_scale);

// read histones positions (in nm), scale it and return it
std::vector<vec3> read_hist_pos(const char *name,double l_scale);

// read histones joints positions (in nm), scale it and return it
std::vector<vec3> read_hist_joint_pos(const char *name,double l_scale);

// read nucleosomal dna rotations and return it
std::vector<std::vector<vec3>> read_dna_rot(const char *name);

// read nucleosomal histones rotations and return it
std::vector<std::vector<vec3>> read_hist_rot(const char *name);

// read SHLs positions (in nm), scale it and return it
std::vector<vec3> read_shls_pos(const char *name,double l_scale);

// read dockings positions (in nm), scale it and return it
std::vector<vec3> read_dockings_pos(const char *name,double l_scale);

// read SHLs bodies and return it
std::vector<std::vector<int>> read_shls_bodies(const char *name);

// read dockings bodies and return it
std::vector<std::vector<int>> read_dockings_bodies(const char *name);
#endif
