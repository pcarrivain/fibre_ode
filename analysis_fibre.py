#!/usr/bin/python
# -*- coding: utf-8 -*-
import data_analysis as da
import json
import numpy as np
import os
import struct
import sys, getopt
import time

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    # default parameters
    nm_10_5bp=3.5721
    nm_1bp = nm_10_5bp / 10.5
    cutoff=11.0
    seed0,seed1=1,2
    IC="chromatin_nrl189bp"
    platform="ODE"
    linear=True
    step,step0,step1=100000,100000,1000000
    every=1
    is_step_range=False
    compute=True
    compute_Wr=False
    compute_msd=False
    compute_acf=False
    compute_rouse_modes=False
    txyz_nucleosome=-1
    txyz_nucleosome_i,txyz_nucleosome_j=-1,-1
    bp_per_nm3=0.01
    tmpdir="/scratch/pcarriva/simulations/fibre_ODE"
    path_to_data="/scratch/pcarriva/simulations/fibre_ODE/visualisation"
    save_json=False
    try:
        opts,args=getopt.getopt(argv,"hs:S:i:",["initial_conformation=","range=","every=","compute=","compute_Wr=","compute_msd=","compute_acf=","compute_rouse_modes=","txyz_nucleosome=","bp_per_nm3=","tmpdir=","path_to_data=","save_json"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('analysis_fibre.py -s <first seed>')
            print('                  -S <last seed>')
            print('                  -i <initial conformation>')
            print('                  --range=from.to.by')
            print('                  --every=<compute every x*by number of steps>')
            print('                          it does not affect g_1, g_2 and g_3 computation')
            print('                  --compute=<no or yes>')
            print('                  --compute_Wr=<no or yes>')
            print('                  --compute_msd=<no or yes>')
            print('                  --compute_acf=<no or yes>')
            print('                  --compute_rouse_modes=<no or yes>')
            print('                  --txyz_nucleosome=<i, write t x y z (nucleosome i com)>')
            print('                  --txyz_nucleosome_nucleosome=<i.j, write t x y z (distance vector r_i-r_j) between nucleosome i and j>')
            print('                  --bp_per_nm3=<bp per nm^3>')
            print('                  --tmpdir=<where to save the data>')
            print('                  --path_to_data=<where to find the raw data>')
            print('                  --save_json save analysis.json')
            print('for c in chromatin_nrl168bp chromatin_nrl189bp chromatin_nrl210bp chromatin_nrl168bp_breakshls6.5 chromatin_nrl189bp_breakshls6.5 chromatin_nrl210bp_breakshls6.5; do /scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S 40 --range=0.2000000.10000 -i $c --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=yes --save_json; done')
            sys.exit()
        elif opt=="-s":
            seed0=int(arg)
        elif opt=="-S":
            seed1=int(arg)
        elif opt=="-i" or opt=="--initial_conformation":
            IC=str(arg)
        elif opt=="-g" or opt=="--platform":
            platform=str(arg)
        elif opt=="--bp_per_nm3":
            bp_per_nm3=float(arg)
        elif opt=="--range":
            sarg=arg.split('.')
            if len(sarg)==3:
                step0=int(sarg[0])
                step1=int(sarg[1])
                step=int(sarg[2])
                is_step_range=True
            else:
                pass
        elif opt=="--every":
            every=int(arg)
        elif opt=="--compute":
            compute=bool(arg=="yes")
        elif opt=="--compute_Wr":
            compute_Wr=bool(arg=="yes")
        elif opt=="--compute_msd":
            compute_msd=bool(arg=="yes")
        elif opt=="--compute_acf":
            compute_acf=bool(arg=="yes")
        elif opt=="--compute_rouse_modes":
            compute_rouse_modes=bool(arg=="yes")
        elif opt=="--txyz_nucleosome":
            txyz_nucleosome=int(arg)
        elif opt=="--txyz_nucleosome_nucleosome":
            sarg=arg.split('.')
            if len(sarg)==2:
                txyz_nucleosome_i=int(sarg[0])
                txyz_nucleosome_j=int(sarg[1])
            else:
                pass
        elif opt=="--tmpdir":
            tmpdir=arg
        elif opt=="--path_to_data":
            path_to_data=arg
        elif opt=="--save_json":
            save_json=True
        else:
            pass

    # do we swap the seed ?
    if seed0>seed1:
        seed0,seed1=seed1,seed0
    seed_range=np.arange(seed0,seed1+1,1,dtype=np.uintc)
    seeds=np.prod(seed_range.shape)
    step_range=np.arange(step0,step1+1,step,dtype=np.uintc)
    T=np.prod(step_range.shape)

    cmap=plt.get_cmap("rainbow")
    abox=[0.15,0.15,0.7,0.7]

    # read chromatin fibres
    oks=0
    for s in seed_range:
        fname="fibres/"+IC+"_n"+str(s)+".in"
        try:
            with open(fname,"r") as in_file:
                oks+=1
        except IOError:
            print("did not find "+fname+", do nothing")
    if oks!=len(seed_range):
        print("did not find all the files in the given seed range, exit.")
        sys.exit()

    # variables for the contact matrix, contact probability P(s)
    ind_contact=[0]
    ind_Ps=[0.0]
    # sample sizes for the contact matrix average over time
    ind_sample=0
    # do we have data ?
    ok_data=np.zeros((T,seeds))
    mds=np.zeros((T,seeds))
    # variables for bending and twisting energy
    Ub=np.zeros((T,seeds),dtype=np.float64)
    Ut=np.zeros((T,seeds),dtype=np.float64)
    # variables: curvature
    nbinsC=100
    maxC=2.0
    binC=maxC/nbinsC
    hcurvature=np.zeros((T,nbinsC),dtype=np.double)
    # variables: torsion
    nbinsT=100
    maxT=1.0
    binT=maxT/nbinsT
    htorsion=np.zeros((T,nbinsT),dtype=np.double)
    # sliding window (in number of monomers and number of bp)
    dg,dw=5.0,3
    Wg=100.0*dg*dw
    W=int(np.rint(Wg/dg))
    # variables: R(s)^2, R_g^2
    init_Rs2=True
    Rg2t=np.zeros((T,seeds,W),dtype=np.float64)
    Rs2t=np.zeros((T,seeds,W),dtype=np.float64)
    kappa2t=np.zeros((T,seeds,W),dtype=np.float64)
    asphericityt=np.zeros((T,seeds,W),dtype=np.float64)
    acylindricityt=np.zeros((T,seeds,W),dtype=np.float64)
    counts=np.zeros((T,seeds,W),dtype=np.uintc)
    # variables: kinetic energy
    ket=np.zeros((T,seeds),dtype=np.double)
    # variables: twist, writhe and linking number
    Tws=np.zeros((T,seeds),dtype=np.float64)
    Tw2s=np.zeros((T,seeds),dtype=np.float64)
    Wrs=np.zeros((T,seeds),dtype=np.float64)
    Wr2s=np.zeros((T,seeds),dtype=np.float64)
    Lks=np.zeros((T,seeds),dtype=np.float64)
    Lk2s=np.zeros((T,seeds),dtype=np.float64)

    # to store t x y z
    if txyz_nucleosome>-1:
        with open("txyz_nucleosome"+str(txyz_nucleosome)+"_"+str(IC)+".out","w") as out_file:
            out_file.write("step x_in_nm y_in_nm z_in_nm\n")
    if txyz_nucleosome_i>-1 and txyz_nucleosome_j>-1:
        with open("txyz_nucleosome"+str(txyz_nucleosome_i)+"_nucleosome"+str(txyz_nucleosome_j)+"_"+str(IC)+".out","w") as out_file:
            out_file.write("step x_in_nm y_in_nm z_in_nm\n")

    # compute genomic size
    gbp,NUCLEOSOMES=0,0
    for i,n in enumerate(seed_range):
        # read chromatin fibre
        fname="fibres/"+IC+"_n"+str(n)+".in"
        with open(fname,"r") as in_file:
            gbp_i,n_i=0,0
            lines=in_file.readlines()
            for l in lines:
                sl=l.split()
                n_i+=1 if float(sl[0])==147.0 else 0
                gbp_i+=float(sl[0])
            gbp=np.maximum(gbp,gbp_i)
            NUCLEOSOMES=max(NUCLEOSOMES,n_i)
    bingbp=5.0 if NUCLEOSOMES>0 else gbp*1e-3
    sbp=np.arange(0.0,gbp,bingbp,dtype=np.double)

    # to store nucleosome square distances matrix
    nucleosome_sq_distances_matrix=np.zeros((NUCLEOSOMES,NUCLEOSOMES,T))

    # to store correlations between two nucleosomes enter/exit
    enter_exit_counts=np.zeros((T,NUCLEOSOMES),dtype=np.uintc)
    enter_bending=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)
    enter_torsion=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)
    enter_twist=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)
    exit_bending=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)
    exit_torsion=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)
    exit_twist=np.zeros((T,seeds,NUCLEOSOMES),dtype=np.double)

    # variables: distances histograms
    max_d=gbp*nm_1bp
    bin_d=2.0 if NUCLEOSOMES>0 else max_d*1e-3
    nbins_d=int(np.rint(max_d/bin_d))
    hdistances=np.full(nbins_d,0)

    # square distances matrix
    binR=10.0 if NUCLEOSOMES>0 else gbp*1e-3
    R,C=int(np.rint(gbp/binR)),int(np.rint(gbp/binR))
    mcmatrix=np.zeros((R,C),dtype=np.double)
    sqdmatrix=np.zeros((R,C),dtype=np.double)
    sqinternaldistances=np.zeros(R,dtype=np.double)

    # nucleosomes coverage
    nucl_coverage=np.zeros(int(np.rint(gbp/10.5)))

    Kb,Kt=0.0,0.0

    # loop over the files
    first_call,init_msd,init_cmsd,init_cacf=True,True,True,True
    G1,G2,G3=np.zeros(T),np.zeros(T),np.zeros(T)
    nucleosome_G1,nucleosome_G2,nucleosome_G3=np.zeros(T),np.zeros(T),np.zeros(T)
    imsd,iacf=0,0
    for i,n in enumerate(seed_range):
        print("simulation {0:d}".format(n))
        # read parameters
        with open("parameters/"+IC+"_n"+str(n)+".in_F0.00_T0.00.out") as in_file:
            lines=in_file.readlines()
            for l in lines:
                sl=l.split()
                if sl[0]=="dt":
                    dt=float(sl[1])
                elif sl[0]=="time_unit":
                    time_unit=float(sl[1])
                elif sl[0]=="length_unit":
                    length_unit=float(sl[1])
                elif sl[0]=="Kb":
                    Kb=float(sl[1])
                elif sl[0]=="Kt":
                    Kt=float(sl[1])
            dt *= time_unit
        # from monomer to genomic position
        lbp=[]
        start_nucleosome=[]
        # number of nucleosomes, number of monomers, number of histone dimers
        nucleosomes,F,H=0,0,0
        # read chromatin fibre
        fname="fibres/"+IC+"_n"+str(n)+".in"
        with open(fname,"r") as in_file:
            gbp_i=0
            lines=in_file.readlines()
            for l in lines:
                sl=l.split()
                sl0=float(sl[0])
                if sl0==147.0:
                    start_nucleosome.append(F)
                    nucleosomes+=1
                    F+=14
                    H+=4
                    lbp.extend([10.5]*14)
                    nucl_coverage[np.floor(np.multiply(1.0/10.5,np.arange(gbp_i,gbp_i+147.0,10.5))).astype(np.uintc)]+=1
                    gbp_i+=sl0
                else:
                    lbp.append(sl0)
                    F+=1
                    gbp_i+=sl0
        cumlbp=np.subtract(np.cumsum(np.array(lbp)),lbp[0])
        # 0.5*(lbp(i+1)+lbp(i))
        difflbp=np.multiply(0.5,np.add(np.array(lbp[1:]),np.array(lbp[:(F-1)])))
        # lnm(i+1)-lnm(i)
        difflnm=np.multiply(nm_10_5bp/10.5,difflbp)
        # compute ?
        if not compute:
            continue
        # check if last step exists ...
        try:
            print(path_to_data+"/cn"+str(n)+"_"+IC+"_n"+str(n)+".in_F0.00_T0.00_s"+str(step1)+".out")
            ps,us,vs,ts,xs=da.read_data(path_to_data+"/cn"+str(n)+"_"+IC+"_n"+str(n)+".in_F0.00_T0.00_s"+str(step1)+".out",F+H,1,"ODE")
            N,D=ps.shape
            bread=True if N==(F+H) else False
        except IOError:
            bread=False
        # if file exists
        init_msd,init_sqd,ok_trajectory=True,True,True
        if bread:
            if nucleosomes>0:
                print("chromatin fibre {0:.1f} bp".format(gbp_i))
            else:
                print("dna {0:.1f} bp".format(gbp_i))
            cn12=0
            n12=np.zeros((nucleosomes*nucleosomes,3),dtype=np.uintc)
            for o in range(nucleosomes):
                for p in range(o,nucleosomes):
                    n12[cn12,:3]=start_nucleosome[o],start_nucleosome[p],p-o
                    cn12+=1
            # sliding window 'w' start and end (to compute R_g^2(w))
            wrange=np.arange(dw,W,dw)
            okw=np.full(np.max(wrange)+1,0)
            for w in wrange:
                if nucleosomes>0:
                    # do not consider first and last nucleosomes
                    for f in range(start_nucleosome[1],start_nucleosome[nucleosomes-2]-w):
                        # compute only if the genomic size is lesser than the sliding window
                        okw[w]+=int((lbp[f+w-1]-lbp[f])<=(w*dg))
                else:
                    # do not consider first and last DNA segments
                    for f in range(F//3,F-F//3-w):
                        # compute only if the genomic size is lesser than the sliding window
                        okw[w]+=int((lbp[f+w-1]-lbp[f])<=(w*dg))
            # compute again and keep only valid values of 'w'
            wrange=np.arange(dw,np.max(np.where(okw>0)[0])+1,dw)
            okw=np.full(np.max(wrange)+1,0)
            starts=np.full((np.max(wrange)+1,F),-1)
            ends=np.full((np.max(wrange)+1,F),-1)
            for w in wrange:
                if nucleosomes>0:
                    # do not consider first and last nucleosomes
                    for f in range(start_nucleosome[1],start_nucleosome[nucleosomes-2]-w):
                        # compute only if the genomic size is lesser than the sliding window
                        if (lbp[f+w-1]-lbp[f])<=(w*dg):
                            starts[w,okw[w]],ends[w,okw[w]]=f,f+w-1
                            okw[w]+=1
                else:
                    # do not consider first and last DNA segments
                    for f in range(F//3,F-F//3-w):
                        # compute only if the genomic size is lesser than the sliding window
                        if (lbp[f+w-1]-lbp[f])<=(w*dg):
                            starts[w,okw[w]],ends[w,okw[w]]=f,f+w-1
                            okw[w]+=1
            # to store nucleosome com
            nucleosome_com=np.full((nucleosomes,3),0.0)
            # do not consider first and last third parts
            nucleosome_sub_range=np.arange(nucleosomes//3,nucleosomes-nucleosomes//3,1)
            # loop over step range
            for j,s in enumerate(step_range):
                # read positions and orientations of the rigid bodies
                ps,us,vs,ts,xs=da.read_data(path_to_data+"/cn"+str(n)+"_"+IC+"_n"+str(n)+".in_F0.00_T0.00_s"+str(s)+".out",F+H,1,"ODE")
                # spatial positions in nm
                np.multiply(length_unit*1e9,ps,out=ps)
                sub_range=np.arange(0,F,1)
                ok_data[j,i]=1
                mds[j,i]=s
                # compute nucleosome com
                for o in range(nucleosomes):
                    nucleosome_com[o,:3]=0.0,0.0,0.0
                    np.add(nucleosome_com[o,:3],ps[F+o*4],out=nucleosome_com[o,:3])
                    np.add(nucleosome_com[o,:3],ps[F+o*4+1],out=nucleosome_com[o,:3])
                    np.add(nucleosome_com[o,:3],ps[F+o*4+2],out=nucleosome_com[o,:3])
                    np.add(nucleosome_com[o,:3],ps[F+o*4+3],out=nucleosome_com[o,:3])
                    np.multiply(0.25,nucleosome_com[o,:3],out=nucleosome_com[o,:3])
                    if o==txyz_nucleosome or o==txyz_nucleosome_i or o==txyz_nucleosome_j:
                        with open("txyz_nucleosome"+str(o)+"_"+str(IC)+".out","a") as out_file:
                            out_file.write("{0:d} {1:f} {2:f} {3:f}\n".format(s,nucleosome_com[o,0],nucleosome_com[o,1],nucleosome_com[o,2]))
                if txyz_nucleosome_i>-1 and txyz_nucleosome_j>-1:
                    with open("txyz_nucleosome"+str(txyz_nucleosome_i)+"_nucleosome"+str(txyz_nucleosome_j)+"_"+str(IC)+".out","a") as out_file:
                        out_file.write("{0:d} {1:f} {2:f} {3:f}\n".format(s,nucleosome_com[txyz_nucleosome_i,0]-nucleosome_com[txyz_nucleosome_j,0],nucleosome_com[txyz_nucleosome_i,1]-nucleosome_com[txyz_nucleosome_j,1],nucleosome_com[txyz_nucleosome_i,2]-nucleosome_com[txyz_nucleosome_j,2]))
                print("seed={0:d} step={1:d} {2:d} nucleosomes {3:d} dna {4:d} histones".format(n,s,nucleosomes,F,H))
                # Frenet-Serret formulas
                if (j%every)==0 or s==step1:
                    Ts,Ns,Bs,curvatures,torsions=da.Frenet_Serret_frame(ts[sub_range],difflnm)
                    for t in torsions:
                        htorsion[j,int(t/binT)]+=1
                # compute correlations between two nucleosomes enter/exit
                if (j%every)==0 or s==step1:
                    for c in n12[:cn12]:
                        enter_exit_counts[j,c[2]]+=1
                        enter_bending[j,i,c[2]]+=np.dot(ts[c[0]],ts[c[1]])
                        exit_bending[j,i,c[2]]+=np.dot(ts[c[0]+13],ts[c[1]+13])
                        # enter_torsion[j,i,c[2]]+=np.dot(Bs[c[0]],Bs[c[1]+1])
                        # exit_torsion[j,i,c[2]]+=np.dot(Bs[c[0]+13],Bs[c[1]+13])
                        enter_torsion[j,i,c[2]]+=np.dot(da.normalize(da.my_cross(ts[c[0]],ts[c[0]+1])),da.normalize(da.my_cross(ts[c[1]],ts[c[1]+1])))
                        exit_torsion[j,i,c[2]]+=np.dot(da.normalize(da.my_cross(ts[c[0]+13-1],ts[c[0]+13])),da.normalize(da.my_cross(ts[c[1]+13-1],ts[c[1]+13])))
                        enter_twist[j,i,c[2]]+=da.chain_twist(us[np.array([c[0],c[1]])],vs[np.array([c[0],c[1]])],ts[np.array([c[0],c[1]])],True)**2
                        exit_twist[j,i,c[2]]+=da.chain_twist(us[np.array([c[0]+13,c[1]+13])],vs[np.array([c[0]+13,c[1]+13])],ts[np.array([c[0]+13,c[1]+13])],True)**2
                    del Ts,Ns,Bs,curvatures,torsions
                # twist
                if (j%every)==0 or s==step1:
                    tws=da.chain_twist(us[sub_range],vs[sub_range],ts[sub_range],linear)
                    np.subtract(tws,da.tw_e(np.array(lbp),10.5),out=tws)
                # twisting energy
                if (j%every)==0 or s==step1:
                    Ut[j,i]=0.5*np.sum(np.divide(np.square(tws),np.multiply(1.0/95.0,np.array(lbp[:(F-1)]))))
                # check Lk=Tw+Wr
                if (j%every)==0 or s==step1:
                    Tws[j,i]=np.sum(tws)/(2.0*np.pi)
                    Tw2s[j,i]=np.square(Tws[j,i])
                    del tws
                if ((j%every)==0 or s==step1) and compute_Wr:
                    time0=time.time()
                    # writhe
                    Wrs[j,i]=da.chain_writhe(ps[sub_range])
                    Wr2s[j,i]=np.square(Wrs[j,i])
                    # Lk
                    Lks[j,i]=Tws[j,i]+Wrs[j,i]
                    Lk2s[j,i]=np.square(Lks[j,i])
                    print("twist and writhe in "+str(time.time()-time0))
                if (j%every)==0 or s==step1:
                    # curvature
                    curvatures=np.divide(np.sqrt(np.multiply(2.0,np.subtract(1.0,da.chain_cosine_bending(ts[sub_range],linear)))),difflnm)
                    for c in curvatures:
                        hcurvature[j,int(c/binC)]+=1
                    del curvatures
                    # bending energy
                    Ub[j,i]=np.sum(np.divide(np.subtract(1.0,da.chain_cosine_bending(ts[sub_range],linear)),np.multiply(1.0/50.0,np.array(lbp[:(F-1)]))))
                if (j%every)==0 or s==step1:
                    # distances histogram
                    hbuffer,lbuffer=da.distances_histogram(ps[sub_range],0,F-1,1,bin_d)
                    np.add(hdistances[:lbuffer],hbuffer,out=hdistances[:lbuffer])
                    del hbuffer,lbuffer
                    # Micro-C and square distances matrix
                    if nucleosomes>0:
                        np.add(mcmatrix,da.micro_c_matrix(ps[sub_range],0,F-1,cumlbp,gbp,binR,5.0,True),out=mcmatrix)
                    np.add(sqdmatrix,da.micro_c_matrix(ps[sub_range],0,F-1,cumlbp,gbp,binR,5.0,False),out=sqdmatrix)
                # nucleosome-nucleosome distances
                if nucleosomes>0 and ((j%every)==0 or s==step1):
                    mbuffer=da.square_distances_matrix(nucleosome_com,0,nucleosomes-1,1)
                    np.add(nucleosome_sq_distances_matrix[:,:,j],mbuffer,out=nucleosome_sq_distances_matrix[:,:,j])
                    del mbuffer
                # 4C and Hi-C
                if False and ((j%every) or s==step1):
                    if first_call:
                        c4C=np.zeros(B)
                        first_call=False
                    c4C=np.add(c4C,da.contacts_4C(ps[sub_range],F//2,cutoff))
                    # contact matrix with indicative function to define +1 contact
                    time0=time.time()
                    contact,Ps=da.contact_matrix(ps[sub_range],np.array([B]),2,40.0,60.0,linear)
                    if ind_sample==0:
                        ind_contact=np.copy(contact)
                        ind_Ps=np.copy(Ps[sub_range])
                    else:
                        np.add(ind_contact,contact,out=ind_contact)
                        np.add(ind_Ps,Ps,out=ind_Ps)
                    ind_sample+=1
                    da.draw_symmetric_matrix(ind_contact,tmpdir+"/"+polymer+"/ind_contact_matrix_s"+str(s),True)
                    print("contacts in "+str(time.time()-time0))
                    # draw the contact probability
                    da.draw_Ps(ind_Ps,tmpdir+"/"+polymer+"/ind_Ps")
                # gyration radius and square internal distances (sliding window in bp)
                # do not consider first and last nucleosomes
                if (j%every)==0 or s==step1:
                    for w in wrange:
                        if okw[w] == 0:
                            continue
                        Rs2t[j,i,w]+=np.sum(np.sum(np.square(np.subtract(ps[starts[w,:okw[w]]],ps[ends[w,:okw[w]]])),axis=1))
                        Rg2,asphericity,acylindricity,kappa2,points,cw=da.multiple_Rg2_and_shape(ps,starts[w,:okw[w]],ends[w,:okw[w]],linear)
                        Rg2t[j,i,w]+=Rg2
                        asphericityt[j,i,w]+=asphericity
                        acylindricityt[j,i,w]+=acylindricity
                        kappa2t[j,i,w]+=kappa2
                        counts[j,0,w]+=cw
                # 2*kinetic energy/3*nbeads*kB*T
                # if (j%every)==0 or s==step1:
                #     ket[j,i]=0.5*mass*np.sum(np.square(xs[sub_range]))/(0.5*(6*(F+H)-3*(F-1)-3*nucleosomes*(14+3+2))*kBT)
                # g123
                if compute_msd and step0<step1:
                    if s==step0:
                        positions_t0=np.copy(ps)
                    if s==step0 and nucleosomes>0:
                        nucleosome_com_t0=np.copy(nucleosome_com)
                    # # add new dict to the msd list
                    # if init_msd:
                    #     trajectory1,msd1=da.msd(ps[sub_range],F//2,0,0,1,False)
                    #     trajectory2,msd2=da.msd(ps[sub_range],F//2,0,0,2,False)
                    #     trajectory3,msd3=da.msd(ps[sub_range],F//2,0,0,3,False)
                    #     init_msd=False
                    # else:
                    #     trajectory1,msd1=da.msd(ps[sub_range],F//2,0,0,1,bool(s==step1),trajectory1)
                    #     trajectory2,msd2=da.msd(ps[sub_range],F//2,0,0,2,bool(s==step1),trajectory2)
                    #     trajectory3,msd3=da.msd(ps[sub_range],F//2,0,0,3,bool(s==step1),trajectory3)
                    g1,g2,g3=da.g123(ps,np.arange(F//2-10,F//2+10,1),positions_t0)
                    G1[j]+=np.sum(g1)
                    G2[j]+=np.sum(g2)
                    G3[j]+=g3
                    del g1,g2,g3
                    if nucleosomes>0:
                        nucleosome_g1,nucleosome_g2,nucleosome_g3=da.g123(nucleosome_com,nucleosome_sub_range,nucleosome_com_t0)
                        nucleosome_G1[j]+=np.sum(nucleosome_g1)
                        nucleosome_G2[j]+=np.sum(nucleosome_g2)
                        nucleosome_G3[j]+=nucleosome_g3
                        del nucleosome_g1,nucleosome_g2,nucleosome_g3
                del ps,us,vs,ts,xs
            del okw,starts,ends,positions_t0
            if nucleosomes>0:
                del nucleosome_com,nucleosome_com_t0
        else:
            # did not find complete trajectory
            ok_trajectory=False
        # g123
        if ok_trajectory and compute_msd and step0<step1:
            print("complete trajectory",ok_trajectory)
            # if init_cmsd:
            #     cg1=np.copy(msd1)
            #     cg2=np.copy(msd2)
            #     cg3=np.copy(msd3)
            #     init_cmsd=False
            # else:
            #     cg1=np.add(cg1,msd1)
            #     cg2=np.add(cg2,msd2)
            #     cg3=np.add(cg3,msd3)
            imsd+=1
        del lbp

    if compute and nucleosomes>0:
        for j,s in enumerate(step_range):
            if (j%every)==0 or s==step1:
                da.draw_matrix(np.multiply(1.0/np.sum(ok_data[j,:]),nucleosome_sq_distances_matrix[:,:,j]),tmpdir+"/nucleosome_distances_matrix/nucleosome_distances_matrix_"+IC+"_step"+str(s)+".png",True,"from_red_to_blue")

    if compute and compute_msd and step0<step1:
        # normalize g_1, g_2 and g_3
        # cg123=np.zeros((cg1.size,3))
        # cg123[:,0]=np.copy(cg1)
        # cg123[:,1]=np.copy(cg2)
        # cg123[:,2]=np.copy(cg3)
        # np.multiply(1.0/imsd,cg123,out=cg123)
        np.multiply(1.0/(imsd*F),G1,out=G1)
        np.multiply(1.0/(imsd*F),G2,out=G2)
        np.multiply(1.0/imsd,G3,out=G3)
        if nucleosomes>0:
            np.multiply(1.0/(imsd*(nucleosome_sub_range.size)),nucleosome_G1,out=nucleosome_G1)
            np.multiply(1.0/(imsd*(nucleosome_sub_range.size)),nucleosome_G2,out=nucleosome_G2)
            np.multiply(1.0/imsd,nucleosome_G3,out=nucleosome_G3)

    # normalize square distances matrix
    if compute:
        if NUCLEOSOMES>0:
            np.multiply(1.0/np.sum(mcmatrix),mcmatrix,out=mcmatrix)
            da.draw_matrix(mcmatrix,tmpdir+"/Micro-C_"+IC+".png",True)
        norm=1.0/np.sum(ok_data[T-1,:])
        for r in range(R):
            for c in range(r,C):
                sqinternaldistances[c-r]+=sqdmatrix[r,c]*norm/(R-(c-r))
        np.multiply(norm,sqdmatrix,out=sqdmatrix)
        da.draw_matrix(np.sqrt(sqdmatrix),tmpdir+"/sqrtsqdmatrix_"+IC+".png",True)
    # normalize curvature histogram
    if compute:
        np.multiply(1.0/(np.sum(hcurvature[T-1,:])*binC),hcurvature[T-1,:],out=hcurvature[T-1,:])
        np.multiply(1.0/(np.sum(htorsion[T-1,:])*binC),htorsion[T-1,:],out=htorsion[T-1,:])

    # save data as json file
    if save_json and compute:
        try:
            with open(tmpdir+"/chromatin.json","r") as in_file:
                chromatin_json=json.load(in_file)
                if not IC in chromatin_json.keys():
                    chromatin_json[IC]={}
                for v in ["Rg","asphericity","acylindricity","kappa2","Rs","g1","g2","g3","nucleosome_g1","nucleosome_g2","nucleosome_g3"]:
                    chromatin_json[IC][v]={}
                for v in ["enter_bending","enter_torsion","enter_twist","exit_bending","exit_torsion","exit_twist"]:
                    chromatin_json[IC][v]={}
                for v in ["curvature","torsion"]:
                    chromatin_json[IC][v]={}
                for v in ["distances_histogram","sqinternaldistances"]:
                    chromatin_json[IC][v]={}
        except IOError:
            chromatin_json={}
            chromatin_json[IC]={}
            for v in ["Rg","asphericity","acylindricity","kappa2","Rs","g1","g2","g3","nucleosome_g1","nucleosome_g2","nucleosome_g3"]:
                chromatin_json[IC][v]={}
            for v in ["enter_bending","enter_torsion","enter_twist","exit_bending","exit_torsion","exit_twist"]:
                chromatin_json[IC][v]={}
            for v in ["curvature","torsion"]:
                chromatin_json[IC][v]={}
            for v in ["distances_histogram","sqinternaldistances"]:
                chromatin_json[IC][v]={}
        chromatin_json[IC]["seed_range"]=[int(s) for s in seed_range]
        chromatin_json[IC]["step_range"]=[int(s) for s in step_range]
        chromatin_json[IC]["dt"]=dt
        chromatin_json[IC]["wbp"]=[w*dg for w in wrange]
        # add curvature marginal density to json
        chromatin_json[IC]["curvature"]["nbins"]=nbinsC
        chromatin_json[IC]["curvature"]["bin_size"]=binC
        for b in range(nbinsC):
            chromatin_json[IC]["curvature"][str(b)]=hcurvature[T-1,b]
        # add torsion marginal density to json
        chromatin_json[IC]["torsion"]["nbins"]=nbinsT
        chromatin_json[IC]["torsion"]["bin_size"]=binT
        for b in range(nbinsT):
            chromatin_json[IC]["torsion"][str(b)]=htorsion[T-1,b]
        # add correlations between two nucleosomes enter/exit
        for j,s in enumerate(step_range):
            if (j%every)!=0 and s!=step1:
                continue
            chromatin_json[IC]["enter_bending"][str(s)]=[np.sum(enter_bending[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
            chromatin_json[IC]["enter_torsion"][str(s)]=[np.sum(enter_torsion[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
            chromatin_json[IC]["enter_twist"][str(s)]=[np.sum(enter_twist[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
            chromatin_json[IC]["exit_bending"][str(s)]=[np.sum(exit_bending[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
            chromatin_json[IC]["exit_torsion"][str(s)]=[np.sum(exit_torsion[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
            chromatin_json[IC]["exit_twist"][str(s)]=[np.sum(exit_twist[j,:,n])/enter_exit_counts[j,n] for n in range(NUCLEOSOMES)]
        for w in wrange:
            # init
            chromatin_json[IC]["Rg"][str(w*dg)+"bp"]={}
            chromatin_json[IC]["asphericity"][str(w*dg)+"bp"]={}
            chromatin_json[IC]["acylindricity"][str(w*dg)+"bp"]={}
            chromatin_json[IC]["kappa2"][str(w*dg)+"bp"]={}
            chromatin_json[IC]["Rs"][str(w*dg)+"bp"]={}
            # average over independent simulations (last snapshot)
            chromatin_json[IC]["Rg"][str(w*dg)+"bp"]["last"]=np.sqrt(np.sum(np.divide(Rg2t[T-1,:,w],counts[T-1,0,w])))
            chromatin_json[IC]["asphericity"][str(w*dg)+"bp"]["last"]=np.sqrt(np.sum(np.divide(asphericityt[T-1,:,w],counts[T-1,0,w])))
            chromatin_json[IC]["acylindricity"][str(w*dg)+"bp"]["last"]=np.sqrt(np.sum(np.divide(acylindricityt[T-1,:,w],counts[T-1,0,w])))
            chromatin_json[IC]["kappa2"][str(w*dg)+"bp"]["last"]=np.sqrt(np.sum(np.divide(kappa2t[T-1,:,w],counts[T-1,0,w])))
            chromatin_json[IC]["Rs"][str(w*dg)+"bp"]["last"]=np.sqrt(np.sum(np.divide(Rs2t[T-1,:,w],counts[T-1,0,w])))
            # write as a function of iterations (time-step is supposed to be the same)
            for j,s in enumerate(step_range):
                if (j%every)!=0 and s!=step1:
                    continue
                chromatin_json[IC]["Rg"][str(w*dg)+"bp"][str(s)]=np.sqrt(np.sum(np.divide(Rg2t[j,:,w],counts[j,0,w])))
                chromatin_json[IC]["asphericity"][str(w*dg)+"bp"][str(s)]=np.sqrt(np.sum(np.divide(asphericityt[j,:,w],counts[j,0,w])))
                chromatin_json[IC]["acylindricity"][str(w*dg)+"bp"][str(s)]=np.sqrt(np.sum(np.divide(acylindricityt[j,:,w],counts[j,0,w])))
                chromatin_json[IC]["kappa2"][str(w*dg)+"bp"][str(s)]=np.sqrt(np.sum(np.divide(kappa2t[j,:,w],counts[j,0,w])))
                chromatin_json[IC]["Rs"][str(w*dg)+"bp"][str(s)]=np.sqrt(np.sum(np.divide(Rs2t[j,:,w],counts[j,0,w])))
        # add distances histogram to json
        chromatin_json[IC]["distances_histogram"]["nbins"]=nbins_d
        chromatin_json[IC]["distances_histogram"]["bin_size"]=bin_d
        for d in range(nbins_d):
            chromatin_json[IC]["distances_histogram"][str(d)]=int(hdistances[d])
        # add square internal distances to json
        chromatin_json[IC]["sqinternaldistances"]["nbins"]=R
        chromatin_json[IC]["sqinternaldistances"]["bin_size_bp"]=binR
        for r in range(R):
            chromatin_json[IC]["sqinternaldistances"][str(r)]=sqinternaldistances[r]
        # add g_1, g_2 and g_3 to the json file
        if compute_msd and step0<step1:
            # for i,g in enumerate(cg123[:,0]):
            #     chromatin_json[IC]["g1"][str(i*step)]=g
            # for i,g in enumerate(cg123[:,1]):
            #     chromatin_json[IC]["g2"][str(i*step)]=g
            # for i,g in enumerate(cg123[:,2]):
            #     chromatin_json[IC]["g3"][str(i*step)]=g
            for i,g in enumerate(G1):
                chromatin_json[IC]["g1"][str(i*step)]=g
            for i,g in enumerate(G2):
                chromatin_json[IC]["g2"][str(i*step)]=g
            for i,g in enumerate(G3):
                chromatin_json[IC]["g3"][str(i*step)]=g
            if nucleosomes>0:
                for i,g in enumerate(nucleosome_G1):
                    chromatin_json[IC]["nucleosome_g1"][str(i*step)]=g
                for i,g in enumerate(nucleosome_G2):
                    chromatin_json[IC]["nucleosome_g2"][str(i*step)]=g
                for i,g in enumerate(nucleosome_G3):
                    chromatin_json[IC]["nucleosome_g3"][str(i*step)]=g
        with open(tmpdir+"/chromatin.json","w") as out_file:
            json.dump(chromatin_json,out_file,indent=1)

    lcolors=["black","red","green","blue"]
    lmarkers=["o","^","<","d","s","v",">","D"]
    try:
        with open(tmpdir+"/chromatin.json","r") as in_file:
            chromatin_json=json.load(in_file)
            # plot all chromatin fibres from json file
            Rmin,Rmax=0,50
            # cs=chromatin_json.keys()
            # cs=["chromatin_nrl187bp","chromatin_nrl189bp","chromatin_nrl191bp","chromatin_nrl187to191bp"]
            # cs=["chromatin_nrl195to199bp","chromatin_nrl193to201bp"]
            # cs=["chromatin_nrl192to202bp"]
            # cs=["chromatin_nrl189to189bp","chromatin_nrl190to190bp","chromatin_nrl191to191bp"]
            # cs=["chromatin_nrl_marginal_density_seed1_2N_linker_ts25.json"]
            cs=[(IC.replace("_breakshls6.5","")).replace("_rbreakshls6.5","")]
            C=len(cs)
            colors=[lcolors[0],lcolors[1],lcolors[2]]
            markers=[lmarkers[0],lmarkers[0],lmarkers[0]]
            if not "chromatin0_nrl" in IC:
                for i in range(C):
                    cs.append(cs[i]+"_breakshls6.5")
                    colors.append(lcolors[i])
                    markers.append(lmarkers[4])
                for i in range(C):
                    cs.append(cs[i]+"_rbreakshls6.5")
                    colors.append(lcolors[i])
                    markers.append(lmarkers[7])
            # does it find all the chromatin fibres ?
            C=0
            for c in cs:
                C+=int(c in chromatin_json.keys())
            if C==len(cs):
                for v in ["Rg","asphericity","acylindricity","kappa2","Rs"]:
                    fig=plt.figure(v)
                    if v=="Rg":
                        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$R_g\left(w\right)$',title="",xlim=(0,Wg),ylim=(Rmin,Rmax))
                    elif v=="acylindricity":
                        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=v,title="",xlim=(0,Wg),ylim=(0,10))
                    elif v=="asphericity":
                        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=v,title="",xlim=(0,Wg),ylim=(0,20))
                    elif v=="kappa2":
                        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$\kappa^2\left(w\right)$',title="",xlim=(0,Wg),ylim=(0,1))
                    elif v=="Rs":
                        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$R\left(w\right)$',title="",xlim=(0,Wg),ylim=(Rmin,Rmax))
                    for i,c in enumerate(cs):
                        print(c,v)
                        if "wbp" in chromatin_json[c].keys():
                            wbp=np.array(chromatin_json[c]["wbp"])
                        if v in chromatin_json[c].keys():
                            for w in wbp:
                                if w==wbp[0]:
                                    ax.plot(w,chromatin_json[c][v][str(w)+"bp"]["last"],linestyle="",marker=markers[i],markersize=3.0,color=colors[i],label=c)
                                else:
                                    ax.plot(w,chromatin_json[c][v][str(w)+"bp"]["last"],linestyle="",marker=markers[i],markersize=3.0,color=colors[i])
                    ax.locator_params(axis='x',nbins=5)
                    if v=="kappa2":
                        ax.legend(ncol=1,loc="lower left")
                    else:
                        ax.legend(ncol=1)
                    fig.savefig(tmpdir+"/"+v+".png",dpi=600)
                    fig.clf()
                    plt.close(v)
                # plot Rg as a function of g_2
                g2_min,g2_max=0.0,400.0
                Rg2_min,Rg2_max=0.0,400.0
                for i,c in enumerate(cs):
                    print("{0:s} plot g123 (from nm^2 to microm^2)".format(c))
                    dts=np.multiply(chromatin_json[IC]["dt"],np.array(chromatin_json[IC]["step_range"]))
                    iterations=chromatin_json[c]["g1"].keys()
                    g1=np.multiply(1e-6,np.array([chromatin_json[c]["g1"][s] for s in iterations]))
                    da.draw_msd(dts,g1,0.0,0.0,0.0,0.0,"time(s)",r"$g_1\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g1_"+c)
                    iterations=chromatin_json[c]["g2"].keys()
                    g2=np.multiply(1e-6,np.array([chromatin_json[c]["g2"][s] for s in iterations]))
                    da.draw_msd(dts,g2,0.0,0.0,0.0,0.0,"time(s)",r"$g_2\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g2_"+c)
                    iterations=chromatin_json[c]["g3"].keys()
                    g3=np.multiply(1e-6,np.array([chromatin_json[c]["g3"][s] for s in iterations]))
                    da.draw_msd(dts,g3,0.0,0.0,0.0,0.0,"time(s)",r"$g_3\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g3_"+c)
                    if not "chromatin0_nrl" in c:
                        print("{0:s} plot nucleosome g123".format(c))
                        dts=np.multiply(chromatin_json[IC]["dt"],np.array(chromatin_json[IC]["step_range"]))
                        iterations=chromatin_json[c]["nucleosome_g1"].keys()
                        g1=np.array([chromatin_json[c]["nucleosome_g1"][s] for s in iterations])
                        da.draw_msd(dts,g1,0.0,0.0,0.0,0.0,True,"mapping/hajjoul_msd_GR2013.in",tmpdir+"/nucleosome_g123/nucleosome_g1_"+c)
                        iterations=chromatin_json[c]["nucleosome_g2"].keys()
                        g2=np.array([chromatin_json[c]["nucleosome_g2"][s] for s in iterations])
                        da.draw_msd(dts,g2,0.0,0.0,0.0,0.0,False,"",tmpdir+"/nucleosome_g123/nucleosome_g2_"+c)
                        iterations=chromatin_json[c]["nucleosome_g3"].keys()
                        g3=np.array([chromatin_json[c]["nucleosome_g3"][s] for s in iterations])
                        da.draw_msd(dts,g3,0.0,0.0,0.0,0.0,False,"",tmpdir+"/nucleosome_g123/nucleosome_g3_"+c)
                    print("{0:s} plot square Rg as a function of g2".format(c))
                    fig=plt.figure("Rg_g_2")
                    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$g_2$',ylabel=r'$R_g\left(w\right)^2$',title="",xlim=(g2_min,g2_max),ylim=(Rg2_min,Rg2_max))
                    ax.plot(np.arange(g2_min,g2_max,1.0),np.arange(Rg2_min,Rg2_max,1.0),linestyle="--",linewidth=2.0,color="black")
                    iterations=chromatin_json[c]["g2"].keys()
                    g2=np.array([chromatin_json[c]["g2"][s] for s in iterations])
                    # search for the largest bp window
                    wtoplot=max(chromatin_json[c]["wbp"])
                    Rg2=np.zeros(g2.shape)
                    # if every>1 number of Rg computations is lesser than the number of g2 computations
                    ii=0
                    for s in iterations:
                        if s in chromatin_json[c]["Rg"]["{0:.1f}bp".format(wtoplot)].keys():
                            Rg2[ii]=chromatin_json[c]["Rg"]["{0:.1f}bp".format(wtoplot)][s]**2
                        ii+=1
                    ax.plot(g2[np.nonzero(Rg2)],Rg2[np.nonzero(Rg2)],marker="o",markersize=2.0,color=cmap(w/W),label="w={0:.1f} bp".format(wtoplot))
                    # for w in np.arange(3,min(W,F),dw):
                    #     Rg2=np.array([chromatin_json[c]["Rg"][str(w*dg)+"bp"][s]**2 for s in iterations])
                    #     ax.plot(g2,Rg2,marker="o",markersize=2.0,color=cmap(w/W),label="w={0:.1f} bp".format(w*dg))
                    ax.locator_params(axis='x',nbins=5)
                    ax.legend(ncol=3)
                    fig.savefig(tmpdir+"/Rg_g_2_"+c+".png",dpi=600)
                    fig.clf()
                    plt.close("Rg_g_2")
                    # plot curvature density distribution
                    print("{0:s} plot curvature density distribution".format(c))
                    nbinsC=chromatin_json[c]["curvature"]["nbins"]
                    binC=chromatin_json[c]["curvature"]["bin_size"]
                    fig=plt.figure("curvature")
                    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r"$\langle curvature\rangle$",ylabel=r"$k\left(\langle curvature\rangle\right)$",title="",xlim=(0,maxC),ylim=(0,10.0))
                    for b in range(nbinsC):
                        ax.add_patch(patches.Rectangle((b*binC,0.0),width=binC,height=chromatin_json[c]["curvature"][str(b)],angle=0.0,fill=True,color="black",alpha=1.0))
                    ax.locator_params(axis="x", nbins=7)
                    fig.savefig(tmpdir+"/curvature_density_distribution_"+c+".png",dpi=600)
                    fig.clf()
                    plt.close("curvature")
                    # plot torsion density distribution
                    print("{0:s} plot torsion density distribution".format(c))
                    nbinsT=chromatin_json[c]["torsion"]["nbins"]
                    binT=chromatin_json[c]["torsion"]["bin_size"]
                    fig=plt.figure("torsion")
                    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r"$\langle torsion\rangle$",ylabel=r"$k\left(\langle torsion\rangle\right)$",title="",xlim=(0,maxC),ylim=(0,10.0))
                    for b in range(nbinsT):
                        ax.add_patch(patches.Rectangle((b*binT,0.0),width=binT,height=chromatin_json[c]["torsion"][str(b)],angle=0.0,fill=True,color="black",alpha=1.0))
                    ax.locator_params(axis="x", nbins=7)
                    fig.savefig(tmpdir+"/torsion_density_distribution_"+c+".png",dpi=600)
                    fig.clf()
                    plt.close("torsion")
                    if NUCLEOSOMES>0:
                        # plot correlations between two nucleosomes enter/exit
                        print("{0:s} plot correlations between two nucleosomes enter/exit".format(c))
                        for s in [step_range[0],step_range[T//2],step_range[-1]]:
                            fig=plt.figure("enter_exit")
                            ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r"$|n-m|$",ylabel=r"$C\left(|n-m|\right)$",title="",xlim=(0,NUCLEOSOMES),ylim=(-1.0,1.0))
                            ax.plot(np.arange(NUCLEOSOMES),np.array(chromatin_json[c]["enter_bending"][str(s)]),linestyle="solid",marker="o",markersize=4,color="black",alpha=0.9,label=r"$\cos{\theta_{b,enter}}$")
                            ax.plot(np.arange(NUCLEOSOMES),np.array(chromatin_json[c]["exit_bending"][str(s)]),linestyle="dotted",marker="o",markersize=4,color="black",alpha=0.9,label=r"$\cos{\theta_{b,exit}}$")
                            ax.plot(np.arange(NUCLEOSOMES),np.array(chromatin_json[c]["enter_torsion"][str(s)]),linestyle="solid",marker="s",markersize=4,color="red",alpha=0.9,label=r"$\cos{\tau_{enter}}$")
                            ax.plot(np.arange(NUCLEOSOMES),np.array(chromatin_json[c]["exit_torsion"][str(s)]),linestyle="dotted",marker="s",markersize=4,color="red",alpha=0.9,label=r"$\cos{\tau_{exit}}$")
                            ax.plot(np.arange(NUCLEOSOMES),np.sqrt(np.array(chromatin_json[c]["enter_twist"][str(s)]))/np.pi,linestyle="solid",marker="o",markersize=3,color="green",alpha=0.9,label=r"$\sqrt{tw_{enter}^2}/\pi$")
                            ax.plot(np.arange(NUCLEOSOMES),np.sqrt(np.array(chromatin_json[c]["exit_twist"][str(s)]))/np.pi,linestyle="dotted",marker="s",markersize=3,color="green",alpha=0.9,label=r"$\sqrt{tw_{exit}^2}/\pi$")
                            ax.legend(ncol=3)
                            fig.savefig(tmpdir+"/correlations_enter_exit_"+c+"_s"+str(s)+".png",dpi=600)
                            fig.clf()
                            plt.close("enter_exit")
                    # plot distances histogram
                    print("{0:s} plot distances histogram".format(c))
                    total=np.sum(np.array([chromatin_json[c]["distances_histogram"][str(d)] for d in range(nbins_d)]))
                    nbinsT=chromatin_json[c]["distances_histogram"]["nbins"]
                    binT=chromatin_json[c]["distances_histogram"]["bin_size"]
                    fig=plt.figure("distances_histogram")
                    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="distance (nm)",ylabel="# of counts / total",title="",xlim=(0.0,0.1*max_d),ylim=(0.0,0.05))
                    for d in range(nbins_d):
                        ax.add_patch(patches.Rectangle((d*bin_d,0.0),width=binT,height=chromatin_json[c]["distances_histogram"][str(d)]/total,angle=0.0,fill=True,color="black",alpha=1.0))
                    ax.plot(np.multiply(bin_d, np.arange(nbins_d)), np.array([chromatin_json[c]["distances_histogram"][str(d)]/total for d in range(nbins_d)]), linestyle="solid", color="red")
                    ax.locator_params(axis="x", nbins=7)
                    fig.savefig(tmpdir+"/distances_histogram_"+c+".png",dpi=600)
                    fig.clf()
                    plt.close("distances_histogram")
                    # plot square internal distances
                    print("{0:s} plot square internal distances".format(c))
                    R=chromatin_json[c]["sqinternaldistances"]["nbins"]
                    binR=chromatin_json[c]["sqinternaldistances"]["bin_size_bp"]
                    fig=plt.figure("sqinternaldistances")
                    ax=fig.add_axes(abox,xlabel=r"$\|j-i\|~bp$",ylabel=r"$\sqrt{\langle R^2\left(\|j-i\right)\rangle}$",title="",xlim=(1e0,4e3),ylim=(1e0,1e2),xscale="log",yscale="log")
                    for r in range(R):
                        ax.plot(np.multiply(binR,np.arange(R)),np.array([np.sqrt(chromatin_json[c]["sqinternaldistances"][str(r)]) for r in range(R)]),linestyle="solid",linewidth=2.0,color="black")
                    # ax.locator_params(axis="x", nbins=7)
                    fig.savefig(tmpdir+"/sqinternaldistances_"+c+".png",dpi=600)
                    fig.clf()
                    plt.close("sqinternaldistances")
    except IOError:
        print("no json file, nothing to plot.")

    with open("mapping/hajjoul_msd_GR2013.in","w") as out_file:
        out_file.write("seconds msd_nm2\n")
        for t in np.arange(0.0001,100.0,0.0001):
            out_file.write("{0:f} {1:f}\n".format(t,1e6*0.01*np.sqrt(t)))

    if compute:
        print('plot Rg')
        fig=plt.figure("Rg")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$R_g\left(w\right)$',title="",xlim=(0,Wg),ylim=(Rmin,Rmax))
        for w in wrange:
            ax.plot(w*dg,np.sqrt(np.sum(np.divide(Rg2t[T-1,:,w],counts[T-1,0,w]))),marker="o",markersize=2.0,color=cmap(w/W))
        ax.locator_params(axis='x',nbins=5)
        fig.savefig(tmpdir+"/Rg_"+IC+".png",dpi=600)
        fig.clf()
        plt.close("Rg")
        print('plot acylindricity')
        fig=plt.figure("acylindricity")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$acylindricity\left(w\right)$',title="",xlim=(0,Wg),ylim=(0,10))
        for w in wrange:
            ax.plot(w*dg,np.sqrt(np.sum(np.divide(acylindricityt[T-1,:,w],counts[T-1,0,w]))),marker="o",markersize=2.0,color=cmap(w/W))
        ax.locator_params(axis='x',nbins=5)
        fig.savefig(tmpdir+"/acylindricity_"+IC+".png",dpi=600)
        fig.clf()
        plt.close("acylindricity")
        print('plot asphericity')
        fig=plt.figure("asphericity")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$asphericity\left(w\right)$',title="",xlim=(0,Wg),ylim=(0,20))
        for w in wrange:
            ax.plot(w*dg,np.sqrt(np.sum(np.divide(asphericityt[T-1,:,w],counts[T-1,0,w]))),marker="o",markersize=2.0,color=cmap(w/W))
        ax.locator_params(axis='x',nbins=5)
        fig.savefig(tmpdir+"/asphericity_"+IC+".png",dpi=600)
        fig.clf()
        plt.close("asphericity")
        print('plot kappa2')
        fig=plt.figure("kappa2")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$\kappa^2\left(w\right)$',title="",xlim=(0,Wg),ylim=(0,1))
        for w in wrange:
            ax.plot(w*dg,np.sqrt(np.sum(np.divide(kappa2t[T-1,:,w],counts[T-1,0,w]))),marker="o",markersize=2.0,color=cmap(w/W))
        ax.locator_params(axis='x',nbins=5)
        fig.savefig(tmpdir+"/kappa2_"+IC+".png",dpi=600)
        fig.clf()
        plt.close("kappa2")
        print('plot Rs')
        fig=plt.figure("Rs")
        ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="w (bp)",ylabel=r'$R\left(w\right)$',title="",xlim=(0,Wg),ylim=(Rmin,Rmax))
        for w in wrange:
            ax.plot(w*dg,np.sqrt(np.sum(np.divide(Rs2t[T-1,:,w],counts[T-1,0,w]))),marker="o",markersize=2.0,color=cmap(w/W))
        ax.locator_params(axis='x',nbins=5)
        fig.savefig(tmpdir+"/Rs_"+IC+".png",dpi=600)
        fig.clf()
        plt.close("Rs")
        print('plot g123 (from nm^2 to microm^2)')
        da.draw_msd(np.multiply(dt,step_range),np.multiply(1e-6,G1),0.0,0.0,0.0,0.0,"time(s)",r"$g_1\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g1_"+IC)
        da.draw_msd(np.multiply(dt,step_range),np.multiply(1e-6,G2),0.0,0.0,0.0,0.0,"time(s)",r"$g_2\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g2_"+IC)
        da.draw_msd(np.multiply(dt,step_range),np.multiply(1e-6,G3),0.0,0.0,0.0,0.0,"time(s)",r"$g_3\left( t\right)\lbrack\mu m^2\rbrack$",False,"",tmpdir+"/g3_"+IC)
        if nucleosomes>0:
            print('plot nucleosome g123')
            da.draw_msd(np.multiply(dt,step_range),nucleosome_G1,0.0,0.0,0.0,0.0,"time(s)",r"$g_1\lbrack t\rbrack$",True,"mapping/hajjoul_msd_GR2013.in",tmpdir+"/nucleosome_g123/nucleosome_g1_"+IC)
            da.draw_msd(np.multiply(dt,step_range),nucleosome_G2,0.0,0.0,0.0,0.0,"time(s)",r"$g_2\lbrack t\rbrack$",False,"",tmpdir+"/nucleosome_g123/nucleosome_g2_"+IC)
            da.draw_msd(np.multiply(dt,step_range),nucleosome_G3,0.0,0.0,0.0,0.0,"time(s)",r"$g_3\lbrack t\rbrack$",False,"",tmpdir+"/nucleosome_g123/nucleosome_g3_"+IC)

if __name__=="__main__":
    main(sys.argv[1:])
