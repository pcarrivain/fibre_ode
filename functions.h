/*********************************************************************************
 *                                                                               *
 * This file includes some functions that aim to speed-up computation time *
 * when using Open-Dynamics-Engine software to solve articulated system. * This
 *code has been developped by Pascal Carrivain (p.carrivain_at_gmail.com). * It
 *is distributed under MIT licence.                                          *
 *                                                                               *
 ********************************************************************************/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <Eigen/Dense>
#include <cassert>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <ode/ode.h>
#include <ode/odecpp.h>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <thread>
#include <vector>
// #include <Eigen/SVD>
#include "variables.h"

/*! @brief compute damped oscillator parameters from ERP and CFM (see
  Open-Dynamics-Engine) Jv = -erp * c / dt + cfm * l l = Jv / cfm + erp * c /
  (dt * cfm) erp = dt * k / (dt * k + mu) cfm = 1 / (dt * k + mu) k = dt * erp /
  cfm mu = 1 / cfm - dt * k = (1 - dt^2 * erp) / cfm
*/
void k_and_mu_from_erp_cfm(double dt, double erp, double cfm,
                           double &k_from_erp_cfm, double &mu_from_erp_cfm);

/*! @brief compute partition of space and overlap between list of bodies and
  partition. attach contact joint if collision between two bodies. return the
  number of contacts.
  @param bodies list of dBodyID
  @param geoms list of dGeomID
  @param N number of dGeomID
  @param start consider dBodyID from ...
  @param end ... to this index
  @param world dWorldID (see ODE documentation)
  @param contactgroup contact group from Open-Dynamics-Engine
  @param gbin size of the grid bin
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param KKT use KKT conditions to compute overlap between two capsules
 */
int partition(const dBodyID *bodies, const dGeomID *geoms, int N,
              const int *starts, const int *end, dWorldID world,
              dJointGroupID &contactgroup, double gbin, double &sum_abs_depth,
              double &max_abs_depth, bool KKT = false);

/*! @brief compute partition of space and overlap between list of bodies and
  partition. attach contact joint if collision between two bodies. return the
  number of contacts.
  @param bodies list of dBodyID
  @param geoms list of dGeomID
  @param N number of dGeomID
  @param start consider dBodyID from ...
  @param end ... to this index
  @param world dWorldID (see ODE documentation)
  @param contactgroup contact group from Open-Dynamics-Engine
  @param gbin size of the grid bin
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param KKT use KKT conditions to compute overlap between two capsules
  @param verlet_list build Verlet list
 */
int partition_v0(const dBodyID *bodies, const dGeomID *geoms, int N,
                 const int *starts, const int *end, dWorldID world,
                 dJointGroupID &contactgroup, double gbin,
                 double &sum_abs_depth, double &max_abs_depth, bool KKT = false,
                 bool verlet_list = false);

/*! @brief is n prime ?
  @param n number
*/
bool is_prime(int n);

/*! @brief
  @param bodies list of dBodyID objects
  @param lb list of lengths of each object
  @param nucleosomes number of nucleosomes
  @param F number of DNA monomers
 */
void partition_nucleosomes(const dBodyID *bodies, const double *rb,
                           const double *lb, int nucleosomes, int F,
                           double kspring, double espring, double Dtail_dna,
                           double tail_size);

/*! @brief callback for collision between two geometries.
  it is designed to test the Verlet-list implementation.
  @param data see Open-Dynamics-Engine definition of nearCallback
  @param o1 first geometry
  @param o2 second geometry
*/
void test_nearCallback(void *data, dGeomID o1, dGeomID o2);

/*! @brief callback for collision between two geometries.
  @param data see Open-Dynamics-Engine definition of nearCallback
              here, data is the contact size (int)
  @param o1 first geometry
  @param o2 second geometry
*/
void nearCallback(void *data, dGeomID o1, dGeomID o2);

/*! return a ghost hash space.
  https://ode.org/wiki/index.php?title=Manual
  all the geometries in the ghost space have to be extruded.
  it is kind of a Verlet-list.
  @param max_dim largest dimension
  @param xfactor scale factor to apply
*/
dSpaceID ghost_hash_space(double max_dim, double xfactor);

/*! return an hash space.
  https://ode.org/wiki/index.php?title=Manual
  @param min_dim dimension to determine hash space level
  @param max_dim dimension to determine hash space level
*/
dSpaceID hash_space(double min_dim, double max_dim);

/*! return an quad-tree space.
  https://ode.org/wiki/index.php?title=Manual
  @param center see ODE documentation
  @param extents see ODE documentation
  @param depth see ODE documentation
*/
dSpaceID quadtree_space(vec3 center, vec3 extents, int depth);

/*! @brief creates and returns ghost geometry.
  @param space 'dSpaceID' where to create the ghost geometry
  @param b 'dBodyID' object to set to the ghost geometry
  @param g1 type/name of the geometry (sphere, capsule, cylinder)
  @param g2 type/name of the ghost geometry (sphere, capsule, cylinder)
  @param xdim multiply the geometry size by this factor
  @param dim1 size 1 of the geometry (radius for sphere, capsule and cylinder)
  @param dim2 size 2 of the geometry (radius for sphere, capsule and cylinder)
  @param dim3 size 3 of the geometry (radius for sphere, length for capsule and
  cylinder)
*/
dGeomID create_ghost_geometry(dSpaceID space, dBodyID b, const char *g1,
                              const char *g2, double xdim, double dim1,
                              double dim2, double dim3);

/*! @brief initialize the 'dBodyID' data.
  it is used to store the geometries that potentially overlap.
  @param ds list of data per 'dBodyID'
  @param N number of 'dBodyID' in the ODE world
  @param operg number of pairwize per geometry
*/
void initialize_body_data(int3p **ds, int N, int operg);

/*! @brief clean 'dBodyID' data.
  @param ds list of data per 'dBodyID'
  @param N number of 'dBodyID' in the ODE world
*/
void clean_body_data(int3p **ds, int N);

/*! @brief collide capsule o1 with capsule o2 (add feedback to the contact
  joint).
  @param w 'dWorldID' where to find the bodies
  @param g1 first geometry
  @param g2 second geometry
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup contact group from Open-Dynamics-Engine
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param do_not_attach if true do not create contact joint
*/
int collide_c1_c2(dWorldID w, dGeomID g1, dGeomID g2, double wERP, double wCFM,
                  dJointGroupID &contactgroup, double &sum_abs_depth,
                  double &max_abs_depth, bool do_not_attach);

/*! @brief collide geom o1 with geom o2.
  @param w 'dWorldID' where to find the bodies
  @param g1 first geometry
  @param g2 second geometry
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup contact group from Open-Dynamics-Engine
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param do_not_attach if true do not create contact joint
*/
int collide_g1_g2(dWorldID w, dGeomID g1, dGeomID g2, double wERP, double wCFM,
                  dJointGroupID &contactgroup, double &sum_abs_depth,
                  double &max_abs_depth, bool do_not_attach = false);

/*! @brief collide all the pairwize (from Verlet-list) in the normal space and
  return the number of collisions.
  @param w 'dWorldID' where to find the bodies
  @param gs list of geometries (one per 'dBodyID') passed by reference
  @param ds list of data (one per 'dBodyID') passed by reference
  @param N number of 'dBodyID' in the ODE world
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param stop_if return 1 if one collision occurs and if stop_if is equal to
  true
  @param do_not_attach if true do not create contact joint
*/
int collide_all(dWorldID w, dGeomID *&gs, int3p *ds, int N, double wERP,
                double wCFM, dJointGroupID &contactgroup, double &sum_abs_depth,
                double &max_abs_depth, bool stop_if = false,
                bool do_not_attach = false);

/*! @brief build kind of Verlet-list and return the potentially number of
  collisions. geometries have been extruded in every dimensions. they are used
  to build the Verlet-list.
  @param space ghost space
  @param step current step
  @param every build Verlet-list 'every' steps
  @param bs list of 'dBodyID' in the ODE world passed by reference
  @param ds list of data per 'dBodyID' passed by reference
  @param bdata list of data (one per 'dBodyID') passed by reference
  @param N number of 'dBodyID' in the ODE world
  @param contact_size size of 'dContact' (see Open-Dynamics-Engine for more
  details)
  @param nearCallback callback function (see Open-Dynamics-Engine for more
  details)
  @param return_vl if true return the number of ghost collisions, otherwize
  return -1 (default)
*/
int build_Verlet_list(dSpaceID &space, int step, int every, int3p **ds, int N,
                      int contact_size,
                      void (*nearCallback)(void *, dGeomID, dGeomID),
                      bool return_vl = false);

/*! @brief increase the size of a list to 'b' while preserving the 'a' elements
  @param p int* to increase size
  @param a
  @param b
*/
void int_new_copy(int **p, int a, int b);

/*! @brief increase the size of a list to 'b' while preserving the 'a' elements
  @param p double* to increase size
  @param a
  @param b
*/
void double_new_copy(double **p, int a, int b);

// dichotomy
double dichotomy(double (*f)(double), double y, double a, double b, double p);

/*! @brief return the Langevin function at 'x'.
  @param x point where to evaluate Langevin function
*/
double flangevin(double x);

/*! @brief print 'vec3 u' and its norm.
  @param u vec3 to print
*/
void print3(vec3 u);

/*! @brief
 */
vec3 random_vec3(std::mt19937_64 &mt, std::uniform_real_distribution<> u01);

/*! @brief add force from Morse potential at point
  com(i)+u_i*u(i)+v_i*v(i)+t_i*t(i)
  @param D strength
  @param r_e equilibrium distance
  @param a inverse of range
  @param b1 first dBodyID object
  @param u1 position of the point (wrt com) along u
  @param v1 position of the point (wrt com) along v
  @param t1 position of the point (wrt com) along t
  @param b2 second dBodyID object
  @param u2 position of the point (wrt com) along u
  @param v2 position of the point (wrt com) along v
  @param t2 position of the point (wrt com) along t
*/
void ForceMorse(double D, double re, double a, dBodyID b1, double u1, double v1,
                double t1, dBodyID b2, double u2, double v2, double t2);

/*! @brief return force derived from Morse potential
  @param Dm strength
  @param em equilibrium distance
  @param am inverse of range
  @param rij distance vector
*/
vec3 force_from_Morse_potential(double Dm, double em, double am, vec3 rij);

/*! @brief return Morse potential
  @param D strength
  @param r_e equilibrium distance
  @param a inverse of range
  @param rij distance vector
*/
double Morse_potential(double Dm, double em, double am, vec3 rij);

/*! @brief return the 3d cross product.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 cross_product(vec3 u, vec3 v);

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
  @param seed init rPNGs
*/
vec3 solve_cross_a_b_equal_c_v1(vec3 a, vec3 c, int seed);

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
  @param mt Mersenne-Twister from std
*/
vec3 solve_cross_a_b_equal_c_v2(vec3 a, vec3 c, std::mt19937_64 &mt);

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
*/
vec3 solve_cross_a_b_equal_c_v3(vec3 a, vec3 c);

/*! @brief return 'vec3 u' multiplied by a scalar 'a'.
  @param a scalar
  @param u vector
*/
vec3 mult3(double a, vec3 u);

/*! @brief return the addition of vec3 'u' with vec3 'v'.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 add3(vec3 u, vec3 v);

/*! @brief return the subtraction of vec3 'u' with vec3 'v'.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 sub3(vec3 u, vec3 v);

/*! @brief return normalized 'vec3 u'.
  @param u 3d vector to normalize
*/
vec3 normalize3(vec3 u);

/*! @brief return the norm of 'vec3 u'.
  @param u 3d vector
*/
double norm3(vec3 u);

/*! @brief return the tranpose of 'mat33 m'.
  @param m the 'mat33' to transpose
*/
mat33 mat3T(mat33 m);

/*! @brief return the matricial element-wize addition.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 add33(mat33 m1, mat33 m2);

/*! @brief return the matricial element-wize subtract.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 sub33(mat33 m1, mat33 m2);

/*! @brief return the matricial product.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 mult33(mat33 m1, mat33 m2);

/*! @brief return the product of mat33 'm' with vec3 'v'.
  @param m mat33
  @param v vec3
*/
vec3 mult_33_3(mat33 m, vec3 v);

/*! @brief return the skew mat33 'm' from vec3 'v'.
  @param v vec3
*/
mat33 skew_from_vec3(vec3 v);

/*! @brief return 'dWorldID'.
  @param g gravity
  @param wERP error-reduction-parameter
  @param wCFM constraint-force-mixing
  @param nSOR number of successive over-iterations
  @param wSOR over-relaxation value
*/
dWorldID create_world_ODE(double g, double wERP, double wCFM, int nSOR,
                          double wSOR);

/*! @brief return first, second or third principal axis of 'dBodyID b'.
  @param b 'dBodyID' object
  @param a index of the principal axis
*/
vec3 FrameODE(dBodyID b, int a);

/*! @brief return rotation matrix of 'dBodyID b'.
  @param b 'dBodyID' object
*/
mat33 RotationBodyODE(dBodyID b);

/*! @brief return position of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 PositionBodyODE(dBodyID b);

/*! @brief return position of 'dGeomID b'.
  @param g 'dGeomID' object
*/
vec3 PositionGeomODE(dGeomID g);

/*! @brief return the position (with respect to the frame world) of a point
  writing with respect to the body frame.
  @param b 'dBodyID' object
  @param x x-coordinate writing with respect to the body frame
  @param y y-coordinate writing with respect to the body frame
  @param z z-coordinate writing with respect to the body frame
*/
vec3 PositionPosRelBodyODE(dBodyID b, double x, double y, double z);

/*! @brief return linear velocity of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 LinVelBodyODE(dBodyID b);

/*! @brief return angular velocity of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 AngVelBodyODE(dBodyID b);

/*! @brief return angular velocity of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 AngVel2BodyODE(dBodyID b);

/*! @brief return the velocity (with respect to the frame world) of a point
  writing with respect to the body frame.
  @param b 'dBodyID' object
  @param x x-coordinate writing with respect to the body frame
  @param y y-coordinate writing with respect to the body frame
  @param z z-coordinate writing with respect to the body frame
*/
vec3 VelPosRelBodyODE(dBodyID b, double x, double y, double z);

/*! @brief zero to the force and torque of dBodyID 'b'.
  @param b 'dBodyID' object
*/
void ZeroForceTorqueBodyODE(dBodyID b);

/*! @brief zero to the force and torque of dBodyID 'b'.
  @param b 'dBodyID' object
*/
void addForceBodyODE(dBodyID b, vec3 f);

/*! @brief add 'vec3' force 'f' to dBodyID 'b' (x,y,z) coordinates with respect
  to the frame u,v,t
  @param b 'dBodyID' object
  @param r point where to add the force
  @param f force to add
*/
void addPosRelForceBodyODE(dBodyID b, vec3 r, vec3 f);

/*! @brief add vec3 force 'f' to dBodyID 'b' (x,y,z) coordinates with respect to
  the world frame
  @param b 'dBodyID' object
  @param r point where to add the force
  @param f force to add
*/
void addPosForceBodyODE(dBodyID b, vec3 r, vec3 f);

/*! @brief return the force applied to a body.
  @param b 'dBodyID' object
*/
vec3 ForceBodyODE(dBodyID b);

/*! @brief add 'vec3' torque t to dBodyID 'b'.
  @param b 'dBodyID' object
  @param t torque to add
*/
void addTorqueBodyODE(dBodyID b, vec3 t);

/*! @brief return the torque applied to a body.
  @param b 'dBodyID' object
*/
vec3 TorqueBodyODE(dBodyID b);

/*! @brief rotation of 'vec3' b around 'vec3' a and angle 'r'.
  @param a axe
  @param r angle
  @param b 3d vector to rotate
*/
vec3 rotation(vec3 a, double r, vec3 b);

/*! @brief Reset center-of-mass.
  @param bodies list of dBodyID
  @param B number of dBodyID
*/
void reset_com(const dBodyID *bodies, int B);

/*! @brief rotation of the frames of a list of dBodyID.
  @param a axe
  @param r angle
  @param bodies rotation of the frame of each of the body
  @param start from body ...
  @param end ... to body
*/
void rotations(vec3 a, double r, const dBodyID *bodies, int start, int end,
               vec3 p0 = vec3(.0, .0, .0));

/*! @brief intrinsic twist angle along the chromatin fibre.
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
double *tw_e_chromatin_fibre(char *chromatin);

/*! @brief check if linker segmentation is greater than 'BP_limit' argument.
  if yes, create a new file such that linker segmentation is not greater than
  'BP_limit'. copy the old one to the backup folder.
  @param fname name of the chromatin fiber input file
  @param BP_limit max linker segmentation in bp
 */
void check_chromatin_input_file(const char *fname, double BP_limit = 10.5);

/*! @brief write a file that describe a random chromatin fibre, nrls are drawn
  from uniform distribution between 'nrl' and 'NRL'
  @param nucleosomes number of nucleosomes to add
  @param NUCLEOSOMES number of nucleosomes (target)
  @param nrl 147 (only if nucleosomes>0) + size of the linker in bp (min)
  @param NRL 147 (only if nucleosomes>0) + size of the linker in bp (max)
  @param bp_limit linker (only if nucleosomes>0) or DNA (only if nucleosomes=0)
  segmentation lower limit
  @param BP_limit linker (only if nucleosomes>0) or DNA (only if nucleosomes=0)
  segmentation upper limit
  @param break_shl number of shls to break (|...|<= 7 because we have 7 shls on
  both side, negative means random)
  @param break_dock number of docking domains to break (-1, 0 or 1, negative
  means random)
  @param symmetric_break if true break the same number of shls on both side
  (work only if 'break_shl'<0 and 'break_dock'<0)
  @param fname name of the file where to write the chromatin fibre
  @param mt random number from std::mt19937_64
  @param what_to_do write a new file "w" or append to an existing file "a"
  @param prob_h1 probability to add H1
  @param prob_no_nucleosome probability of no nucleosome (add a DNA linker
  instead of a nucleosome) if 'prob_no_nucleosome' is an negative integer then
  no nucleosome every this number
  @param prob_hexasome probability to add an hexasome instead of a nucleosome
         if 'prob_hexasome' is an negative integer then hexasome every this
  number
  @param prob_tetrasome probability to add a tetrasome instead of a nucleosome
         if 'prob_tetrasome' is an negative integer then tetrasome every this
  number
 */
void create_random_fibre(int nucleosomes, int NUCLEOSOMES, int nrl, int NRL,
                         double bp_limit, double BP_limit, int break_shl,
                         int break_dock, bool symmetric_break, char *fname,
                         std::mt19937_64 &mt, const char *what_to_do,
                         double prob_h1 = .0, double prob_no_nucleosome = .0,
                         double prob_hexasome = .0, double prob_tetrasome = .0);

/*! @brief write a file that describe a chromatin fibre (use nrl density from
  data file)
  @param nucleosomes number of nucleosomes
  @param ndata file name where to find nrl>=147 density (1 bp bin)
         header for nrl distribution density file (two columns file) has to be
  'nrl p(nrl)' nrl is integer and p(nrl) is double
  @param break_shl number of shls to break (|...|<= 7 because we have 7 shls on
  both side, negative means random)
  @param break_dock number of docking domains to break (-1, 0 or 1, negative
  means random)
  @param symmetric_break if true break the same number of shls on both side
  (work only if 'break_shl'<0 and 'break_dock'<0)
  @param prob_h1 probability to add H1
  @param fname name of the file where to write the chromatin fibre
  @param mt random number from std::mt19937_64
  @param what_to_do write a new file "w" or append to an existing file "a"
  @param gmean add normal distribution (gmean>147)
  @param gstd normal distribution standard deviation
  @param prob_no_nucleosome probability of no nucleosome
  draw a random number, if it is less than 'prob_no_nucleosome' add a new linker
  instead of a nucleosome
 */
void create_fibre_from_data(int nucleosomes, const char *ndata, int break_shl,
                            int break_dock, bool symmetric_break,
                            double prob_h1, char *fname, std::mt19937_64 &mt,
                            const char *what_to_do, double gmean = .0,
                            double gstd = -1., double prob_no_nucleosome = .0);

/*! @brief create Yeast genome
  @param dna array of dBodyID
  @param gdna array of dGeomID
  @param ggdna array of ghost dGeomID
  @param ddna
  @param jdna array of dJointID
 */
int create_yeast(dBodyID **dna, dGeomID **gdna, dGeomID **ggdna, int3p *&dfibre,
                 dJointID **jdna, dWorldID world, dSpaceID space,
                 dSpaceID gspace, int **gf, double **lf, double **rf,
                 double **mf, double xghost, int gyroscopic, int &dofs,
                 int *&segments, int *&centromeres, int *&chrC, int *&chrT,
                 int *&Lchr, int *&Gchr, std::mt19937_64 &mt);

/*! @brief compute constraint matrix J of the nucleosome.
  compute its rank that is the number of degres of freedom of the object.
  @param simplified_nucleosome 0 for full description of the histones core
*/
long int compute_rank_of_J_nucleosome(int simplified_nucleosome);

/*! @brief create chromatin fibre
  @param chromatin_name of the file where to find the chromatin fibre
  description
  @param simplified_nucleosome 0 for full description of the histones core
  @param mbead attached magnetic bead to the chromatin fibre
  @param fibre
  @param gfibre
  @param ggfibre
  @param dfibre
  @param jfibre array of DNA dJointID object
  @param fjfibre
  @param world dWorldID (see Open-Dynamics-Engine)
  @param space dSpaceID (see Open-Dynamics-Engine)
  @param gspace dSpaceID for ghost geometries
  @param lf array of bond length
  @param rf array of bond radius
  @param mf array of bond mass
  @param lbp number of bp per DNA segment
  @param sbp genomic position
  @param xghost
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
  @param dofs
  @param phase1 fluctuations around DNA phase in [phase1,phase2]
  @param phase2 fluctuations around DNA phase in [phase1,phase2]
  @param seed seed phase fluctuations pRNG
  @param intrinsic_twist add intrinsic twist to between two consecutive monomer
  of DNA linker
  @param use_table
*/
int *create_chromatin_fibre(
    char *chromatin, int simplified_nucleosome, bool mbead, dBodyID **fibre,
    dGeomID **gfibre, dGeomID **ggfibre, int3p *&dfibre, dJointID **jfibre,
    dJointFeedback **fjfibre, dWorldID world, dSpaceID space, dSpaceID gspace,
    double **lf, double **rf, double **mf, double **lbp, double **sbp,
    double xghost, int gyroscopic, int &dofs, bool phase = true,
    double phase1 = .0, double phase2 = .0, const int *seeds = NULL,
    double intrinsic_twist = .0, bool use_table = false);

/*! @brief disable joints for reversome simulation
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
 */
int reversome_disable_joints(dJointID **jfibre, int n1, int n2, int J);

/*! @brief disable "Four-Helix-Bundle" joints
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
 */
int disable_FHB_joints(dJointID **jfibre, int n1, int n2, int J);

/*! @brief disable nucleosome joints
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
*/
int disable_nucleosome_joints(dJointID **jfibre, int n1, int n2, int J);

/*! @brief apply FHBs, SHLs and DDs forces (use 'disable_nucleosome_joints'
  function before) and return number of dofs added to the system. it also store
  forces and energies from Morse potential.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
  @param Dm Morse potential strength
  @param em Morse potential equilibrium length
  @param am Morse potential range (inverse of length unit)
  @param fm to store forces (size has to be nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param Em to store energies (size has to be nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param rm to store vector distances (size has to be
  nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param replace replace shls, fhbs, dds or all joints by Morse potentials
  @param use_min_dist if true use minimal distance
  @param use_omp if true use omp parallel for
*/
int replace_nucleosome_joints_by_potentials(dJointID **jfibre, int n1, int n2,
                                            int J, double *&Dm, double *&em,
                                            double *&am, vec3 *&fm, double *&Em,
                                            vec3 *&rm, vec3 *&uvtm1,
                                            vec3 *&uvtm2, const char *replace,
                                            bool use_min_dist, bool use_omp);

/*! @brief digest chromatin fiber and write fragment size in bp
  @param chromatin_name of the file where to find the chromatin fibre
  description
  @param prob_to_cut probability to cut a linker
  @param mt Mersenne-Twister from std
  @param full if true linkers are fully digested
*/
void digestion(char *chromatin, double prob_to_cut, std::mt19937_64 &mt,
               bool full = true);

/*! @brief fill an array with negative integer for linkers and positive integer
  for nuclesomes
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
int *is_linker_or_nucleosome(char *chromatin);

/*! @brief fill an array with 0 if no H1 otherwize 1 for all the nuclesomes
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
int *nucleosome_with_h1(char *chromatin);

/*! @brief fill an array with the DNA/histone monomer start of each nucleosome.
  if the fiber is made of N nucleosomes the array will be of size 2*N
  the first N are the start of DNA nucleosomes and the last N are the start of
  histone cores. First, run 'create_chromatin_fibre' function and then run
  'dna_histone_starts' function.
  @param chromatin_name of the file where to find the chromatin fibre
  description
*/
int *dna_histone_starts(char *chromatin);

/*! @brief create a linear chain with overtwist.
  @param world dWorlID where to create the ring chain
  @param space dSpaceID where to create the ring chain
  @param gspace dSpaceID where to create the ghost ring chain (related to
  Verlet-list)
  @param N number of bonds
  @param bs list of 'dBodyID' objects (ring chain bodies)
  @param ds list of data attached to 'dBodyID'
  @param gs list of 'dGeomID' objects (ring chain geometries)
  @param ggs list of 'dGeomID' objects (ghost geometries)
  @param js list of 'dJointID' objects (ring chain joints)
  @param l length of the bond
  @param l_bp length of the bond in bp
  @param r radius of the bond
  @param m mass of the bond
  @param xghost scale factor (related to Verlet-list)
  @param overtwist overtwist to inject in the ring chain
  @param gyroscopic 0: diff(inertia,t)=0, 1: diff(inertia,t)!=0
  @param mt Mersenne-Twister from std
 */
void create_linear_chain(dWorldID world, dSpaceID space, dSpaceID gspace, int N,
                         dBodyID **bs, int3p *&ds, dGeomID **gs, dGeomID **ggs,
                         dJointID **js, double l, double l_bp, double r,
                         double m, double xghost, double overtwist,
                         int gyroscopic, std::mt19937_64 &mt);

/*! @brief create a ring chain with overtwist.
  @param world dWorlID where to create the ring chain
  @param space dSpaceID where to create the ring chain
  @param gspace dSpaceID where to create the ghost ring chain (related to
  Verlet-list)
  @param N number of bonds
  @param bs list of 'dBodyID' objects (ring chain bodies)
  @param ds list of data attached to 'dBodyID'
  @param gs list of 'dGeomID' objects (ring chain geometries)
  @param ggs list of 'dGeomID' objects (ghost geometries)
  @param js list of 'dJointID' objects (ring chain joints)
  @param l length of the bond
  @param l_bp length of the bond in bp
  @param r radius of the bond
  @param m mass of the bond
  @param xghost scale factor (related to Verlet-list)
  @param overtwist overtwist to inject in the ring chain
  @param gyroscopic 0: diff(inertia,t)=0, 1: diff(inertia,t)!=0
  @param conformation initial conformation in 'circular', 'linear'
 */
void create_ring_chain(dWorldID world, dSpaceID space, dSpaceID gspace, int N,
                       dBodyID **bs, int3p *&ds, dGeomID **gs, dGeomID **ggs,
                       dJointID **js, double l, double l_bp, double r, double m,
                       double xghost, double overtwist, int gyroscopic,
                       char *conformation);

/*! @brief center of mass to 0,0,0 (objects in [S,E[).
  @param bs 'dBodyID' objects
  @param S start
  @param E end
 */
void remove_com(const dBodyID *bs, int S, int E);

/*! @brief center of mass motion to 0,0,0 (objects in [S,E[).
  @param bs 'dBodyID' objects
  @param S start
  @param E end
 */
void remove_com_motion(const dBodyID *bs, int S, int E);

/*! @brief "Flying-Ice-Cube" correction
  @param bs array of dBodyID objects
  @param B number of objects
 */
void flying_ice_cube_correction(dBodyID *bs, int B);

/*! @brief semi-implicit Euler integration of velocities
 */
void vsemi_implicit_euler(dBodyID *bs, double dt, int S, int E);

/*! @brief semi-implicit Euler integration of both positions and orientations
 */
void semi_implicit_euler(dBodyID *bs, double dt, int S, int E);

/*! @brief add force and torque from Langevin-Euler dynamics to 'dBodyID'
  objects.
  @param bs 'dBodyID' objects
  @param sigma coupling frequencies (translation) to the thermostat
  @param sigma coupling frequencies (rotation) to the thermostat
  @param dt time-step
  @param beta inverse of kBT
*/
void langevin_euler(dBodyID *bs, const double *sigmaT, const double *sigmaR,
                    double dt, double beta, int S, int E, std::mt19937_64 &e1);

/*! @brief add force and torque from local/global Langevin-Euler thermostat to
  'dBodyID' objects in the interval [S,E[.
  @param bs 'dBodyID' objects
  @param dt time-step
  @param dofs unconstrained dofs
  @param beta inverse of kBT
  @param sigma coupling frequency to the thermostat
  @param xx sigma*xx is the coupling frequency to the local thermostat (xx in
  [0,1])
  @param S start of the interval
  @param E end (non-included) of the interval
  @param e1 Mersenne-Twister object from random c++11
  @param e2 Mersenne-Twister object from random c++11 (two threads)
*/
void mlangevin_euler(dBodyID *bs, double dt, int dofs, double beta,
                     double sigma, double xx, int S, int E, std::mt19937_64 &e1,
                     std::mt19937_64 &e2);

/*! @brief add force and torque from Langevin-Euler global thermostat to
  'dBodyID' object.
  @param b 'dBodyID' object
  @param m mass
  @param i principal inertia momenta
  @param gg global thermostat factor
*/
void langevin_euler_global(dBodyID b, double m, vec3 mi, vec3 gg);

/*! @brief rescale the linear and angular velocities.
  @param bodies list of dBodyID objects
*/
void global_rescaling(const dBodyID *bodies, int start, int end, int dofs,
                      double kBT, double sigma, double dt,
                      std::mt19937_64 &std_mt);

/*! @brief add force and torque to the magnetic bead.
  @param b bead 'dBodyID'
  @param force force along the z-axis
  @param torque torque around the z-axis
  @param locked do we lock the bead rotation around the z-axis ?
*/
void bead(dBodyID b, double force, double torque, bool locked);

/*! @brief bending and twisting for each joints.
  @param lk Kuhn length
  @param lt twist persistence length
  @param l length of the bonds, N bonds
  @param gb bending strength
  @param gt twisting strength
  @param joints number of joints J with J<N
  @param beta inverse of kBT
  @param linear is the chain linear ?
*/
void bending_twisting_constants(double lk, double lt, double *l, double **gb,
                                double **gt, int joints, double beta,
                                bool linear);

/*! @brief add bending and twisting torques between 'dBodyID' object and ground.
  last torques is between object 'E-1' and object 'E'.
  return the cosine of bending and twisting angle.
  @param b 'dBodyID' object
  @param gb bending strength
  @param gt twisting strength
  @param u0 first vector of ground frame
  @param v0 second vector of ground frame
  @param t0 third vector of ground frame
*/
vec2 ground_bending_twist(dBodyID b, double gb, double gt,
                          vec3 u0 = vec3(0., 0., 1.),
                          vec3 v0 = vec3(0., 1., 0.),
                          vec3 t0 = vec3(-1., 0., 0.));

/*! @brief return twist between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
  @param l1 bond length (first object)
  @param l2 bond length (second object)
*/
double local_curvature(dBodyID bs1, dBodyID bs2, double l1, double l2);

/*! @brief return chain curvature (sum of local curvature) between I and S.
  @param bs 'dBodyID' objects
  @param lbond array of bond lengths
  @param N number of bonds
  @param I compute curvature between bond 'I' ...
  @param S ... and bond 'S'
  @param normalize if true return average of local curvature
*/
double sum_of_local_curvature(const dBodyID *bs, const double *lbond, int N,
                              int I, int S, bool normalize = false);

/*! @brief return cosine of bending between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
*/
double local_cosine_of_bending(dBodyID bs1, dBodyID bs2);

/*! @brief return twist between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
  @param tw0 intrinsic twist
*/
double local_twist(dBodyID bs1, dBodyID bs2, double tw0 = .0);

/*! @brief return chain twist (sum of local twist) between I and S.
  @param bs 'dBodyID' objects
  @param N number of bonds
  @param I compute twist between bond 'I' ...
  @param S ... and bond 'S'
  @param tw0 array of local intrinsic twist
*/
double sum_of_local_twist(const dBodyID *bs, int N, int I, int S,
                          const double *tw0 = NULL);

/*! @brief return local twist between I and S.
  @param bs 'dBodyID' objects
  @param N number of bonds
  @param I compute twist between bond 'I' ...
  @param S ... and bond 'S'
  @maram tws to store local twist
*/
void chain_local_twist(const dBodyID *bs, int N, int I, int S, double *&tws);

/*! @brief return local torsion between 'I' and 'S'.
  @param bs 'dBodyID' objects
  @param lb bond length
  @param N number of bonds
  @param I compute twist between bond 'I' ...
  @param S ... and bond 'S'
  @param torsion to store local torsion
*/
void chain_local_torsion(const dBodyID *bs, const double *lb, int N, int I,
                         int S, double *&torsion);

/*! @brief add bending and twisting torques between 'dBodyID' object 1 and
  'dBodyID' object 2 last torques is between object 'E-1' and object 'E'. return
  (by reference) the cosine average of bending and square twisting angle
  average.
  @param bs 'dBodyID' objects
  @param gb bending strength
  @param gt twisting strength
  @param xg multiply bending and twisting strengths with
  @param be bending equilibrium angle
  @param twe twisting equilibrium angle
  @param S start of the half-open interval
  @param E end of the half-open interval
  @param linear is the chain linear ? if yes add torques between object 'E' and
  object 'S'
*/
void bending_twist(const dBodyID *bs, const double *gb, const double *gt,
                   const double xg, const double *be, const double *twe, int S,
                   int E, bool linear, double &mcos, double &mtw2);

/*! @brief add intrinsic bending between 'dBodyID' object 1 and 'dBodyID'
  object 2. last torques is between object 'E-1' and object 'E'.
  @param bs 'dBodyID' objects
  @param gb bending strength
  @param is_linker_nucleosome negative values if linker DNA
  @param S start of the half-open interval
  @param E end of the half-open interval
  @param linear is the chain linear ? if yes add torques between object 'E' and
  object 'S'
*/
void add_intrinsic_bending(const dBodyID *bs, const double gb,
                           const int *is_linker_nucleosome, int S, int E,
                           bool linear);

/*! return total kinetic energy.
  @param bs 'dBodyID' objects
  @param S start of the half-open interval
  @param E end of the half-open interval
  @param remove_com does the function remove com motion ?
  @param tr 0 returns kinetic energy, 1 returns rotational kinetic energy, 2
  returns total kinetic energy
*/
double Ktr(const dBodyID *bs, int S, int E, bool remove_com, int tr);

/*! @brief assign position (x,y,z) and quaternion (w,i,j,k) to dBodyID 'b'.
  @param b 'dBodyID' object
  @param p position
  @param w angle (quaternion)
  @param i i-axis (quaternion)
  @param j j-axis (quaternion)
  @param k k-axis (quaternion)
*/
void set_pos_quat(dBodyID b, vec3 p, double w, double i, double j, double k);

/*! @brief assign linear and angular velocities.
  @param b 'dBodyID' object
*/
void set_velocities(dBodyID b, double vx, double vy, double vz, double wx,
                    double wy, double wz);

/*! @brief assign linear and angular velocities.
  @param b 'dBodyID' object
  @param v linear velocity
  @param w angular velocity
*/
void set3_velocities(dBodyID b, vec3 v, vec3 w);

/*! @brief assign position.
  @param b 'dBodyID' object
  @param r position
*/
void set_position(dBodyID b, vec3 r);

/*! @brief assign axes to body.
  @param b 'dBodyID' object
  @param t tangent
  @param u normal
  @param v cross(t,u)
*/
void set_axes(dBodyID b, vec3 t, vec3 u, vec3 v);

/*! @brief copy only the bodies to new world (no joints)
 */
void copy_system_with_no_joints(dWorldID world, const dBodyID *bs,
                                dBodyID **bs_copy, int B);

/*! @brief copy state of each body
 */
bool copy_state_body(dBodyID **bs, dBodyID **copy_bs, int B, int method);

/*! @brief step the world with contact joints only and get the feedback
  @param world world
  @param bs bodies used to step the world without contact joints
  @param copy_bs bodies used to step the world with only contact joints
  @param B number of bodies
  @param dt time-step
 */
void step_the_world_contact_joints(dWorldID world, dBodyID **bs,
                                   dBodyID **copy_bs, int B, double dt,
                                   int method);

/*! @brief returns 'dBodyID' and creates its corresponding 'dGeomID' sphere.
  @param w world where to create the capsule body
  @param s space where to create the sphere geometry
  @param g 'dGeomID' object
  @param r radius
  @param m mass
*/
dBodyID b_sphere(dWorldID w, dSpaceID s, dGeomID &g, double r, double m);

/*! @brief returns 'dBodyID' and creates its corresponding 'dGeomID' capsule.
  @param w world where to create the capsule body
  @param s space where to create the capsule geometry
  @param g 'dGeomID' object
  @param r radius
  @param l length (without caps)
  @param m mass
  @param gyroscopic 0: diff(inertia,t)=0, 1: diff(inertia,t)!=0
*/
dBodyID b_capsule(dWorldID w, dSpaceID s, dGeomID &g, double r, double l,
                  double m, int gyroscopic);

/*! @brief create 'dBodyID' and its corresponding 'dGeomID' cylinder.
  @param w world where to create the cylinder body
  @param s space where to create the cylinder geometry
  @param g geometry passed by reference
  @param r radius
  @param l length
  @param m mass
  @param gyroscopic 0: diff(inertia,t)=0, 1: diff(inertia,t)!=0
*/
dBodyID b_cylinder(dWorldID w, dSpaceID s, dGeomID &g, double r, double l,
                   double m, int gyroscopic);

/*! @brief return the inertia momenta with respect to the frame of dBodyID 'b'.
  @param b 'dBodyID' object
*/
vec3 inertia(dBodyID b);

/*! @brief return inertia matrix I(t) or its inverse.
  @param b 'dBodyID' object
  @param inverse 0: I(t), 1: I(t)^-1
*/
mat33 inertia_t(dBodyID b, int inverse);

/*! @brief return joint anchor
  @param j dJointID
  @param t add vec3 t to joint anchor
*/
double *get_joint_anchor(dJointID j, vec3 t = vec3(.0, .0, .0));

/*! @brief return an universal joint (no angle limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
  @param a1 first axis
  @param a2 second axis
*/
dJointID universal_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a, vec3 a1,
                         vec3 a2);

/*! @brief return an hinge joint (no angle limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
  @param ax hinge axis
*/
dJointID hinge_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a, vec3 ax);

/*! @brief return a slider joint (no limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a slider axis
*/
dJointID slider_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a);

/*! @brief return a ball joint.
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
*/
dJointID ball_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a);

/*! @brief return a double ball joint.
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a1 first anchor point
  @param a2 second anchor point
*/
dJointID dball_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a1, vec3 a2);

/*! @brief return the sign of 'a'.
  @param a value
*/
double sign(double a);

/*! @brief return the arc cosine of 'a'.
  @param a value
*/
double arccos(double a);

/*! @brief return the arc sine of 'a'.
  @param a value
*/
double arcsin(double a);

/*! @brief return the solid angle described by four points.
  from Klenin & Langowski Computation of writhe in modeling of supercoiled DNA.
  https://doi.org/10.1002/1097-0282(20001015)54:5<307::AID-BIP20>3.0.CO;2-Y).
  @param r1 position of the point 1
  @param r2 position of the point 2
  @param r3 position of the point 3
  @param r4 position of the point 4
*/
double solid_angle(vec3 r1, vec3 r2, vec3 r3, vec3 r4);

/*! @brief This function returns the writhe of the chain (length N).
  The function uses OpenMP functionality to handle the different cases.
  @param positions positions of the N beads
*/
double chain_writhe(std::vector<vec3> positions);

/*! @brief This function returns the writhe of the chain (between I and S).
  The function uses OpenMP functionality to handle the different cases.
  If the curve A is closed the next bond to 'S' is 'I'.
  If the curve A is non-closed, the function uses a closure A+L1+C1+D+C2+L2.
  See E. L. Starostin, ON THE WRITHING NUMBER OF A NON-CLOSED CURVE, Physical
  and Numerical Models in Knot Theory 2005. The function uses OpenMP
  functionality to handle the different cases.
  @param bs array of dBodyID objects (bonds)
  @param ls array of bond lengths
  @param I compute writhe between bond 'I' ...
  @param S ... and bond 'S' (interval [I,S])
  @param nonclosed is the curve non-closed
  @param print print double integral value for all the combinations
  @param nl number of steps per line
  @param sc number of smoothing steps
*/
double chain_writhe_ODE(const dBodyID *bs, const double *ls, int I, int S,
                        bool nonclosed = true, bool print = false, int nl = 4,
                        int sc = 4);

/*! @brief read or write the conformation and return the number of elements in
  [S,E[ red/written.
  @param bs 'dBodyID' objects
  @param name name of the file
  @param what_to_do read or write
  @param S start to write
  @param E end
*/
int read_write_conformation(dBodyID *bs, char *name, const char *what_to_do,
                            int S, int E);

/*! @brief write (binary format) the conformation for visualisation and return
  the number of elements in [S,E[ written.
  @param bs 'dBodyID' objects
  @param name name of the file
  @param S start to write
  @param E end
  @param what_to_do write 'w' or append 'a', default to 'w'
*/
int write_visualisation(dBodyID *bs, char *name, int S, int E,
                        const char *what_to_do);

/*! @brief write squared distances intra/inter nucleosomes.
  @param bs 'dBodyID' objects
  @param N number of nucleosomes
  @param F number of DNA monomers
  @param name name of the file
  @param what_to_do write 'w' or append 'a', default to 'w'
  @param dn 0 for intra nucleosome, 1 for consecutive nucleosomes ...
*/
void write_sqdistances(dBodyID *bs, const int N, const int F, char *name,
                       const char *what_to_do, int dn);

/*! @brief write squared distances between pair of nucleosomes (skip first and
  last one).
  @param bs 'dBodyID' objects
  @param N number of nucleosomes
  @param F number of DNA monomers
  @param name name of the file
  @param what_to_do write 'w' or append 'a', default to 'w'
*/
void write_nucnuc_sqdistances(dBodyID *bs, const int N, const int F, char *name,
                              const char *what_to_do);

// return the determinant of the mat33 'm'
double determinant_mat3(mat33 m);

// return the trace of the mat33 'm'
double trace_mat3(mat33 m);

// return invert mat33 'm' (Cayley-Hamilton)
mat33 inverseCH_mat3(mat33 m);

// return mat33 from cross-product with vec3 'u'
mat33 mat_vect(vec3 u);

// return mat33 'm' multiplied by scalar 'r'
mat33 mult_33_1(double r, mat33 m);

// LDLT decompostion to solve A * l = b where A is tridiagonal
void solution_Al_b_LDLT(std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                        std::vector<vec3> &rhs1, std::vector<vec3> &rhs2, int C,
                        std::vector<vec3> &lambda);

// LDU decompostion to solve A * l = b where A is tridiagonal
void solution_Al_b_LDU(std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                       std::vector<vec3> &rhs1, std::vector<vec3> &rhs2, int C,
                       std::vector<vec3> &lambda, bool split_rhs);

// constraints ("velocity-based" method) for a linear chain of rigid body
// connected with mechanical "Ball-in-Socket" joint
void solution_LC(dBodyID *&b, const double *m, const double *l, int Nchr[],
                 int C, int sol, int ext, double dt, vec3 anchor,
                 std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                 std::vector<vec3> &lambdas);

/*! @brief dot product u^Tv
 */
double dot_product(vec3 u, vec3 v);

/*! @brief dot product u^Tu
 */
double dot_product2(vec3 u);

/*! @brief This function returns the two points (each on one segment) such that
  the distance is minimal. This function compares the result to an "exact
  enumeration" too (if test_result is true). The number of points (along each
  segment) is given by the integer variable enumeration. The precision of the
  "enumeration" increases with the integer variable enumeration. The function
  uses OpenMP "section" functionality to handle the different cases.
*/
void minDist2segments_KKT(vec3 A0, vec3 B0, vec3 A1, vec3 B1, bool test_result,
                          int enumeration, std::vector<vec3> &cp);

/*! @brief square minimal distance between point and segment
 */
double sqminDistPoint2Segment(vec3 p0, vec3 p1, vec3 p);

/*! @brief For segmentation between 1 and 3 bp per DNA segment, length is lesser
  than radius. Therefore we remove collision(s) between segment 'i' and segment
  'i+x'.
 */
void remove_artificial_collisions(int nucleosomes, int F, const double *sbp,
                                  const double *lbp, const double *radius,
                                  const dBodyID *fibre, const dGeomID *gfibre,
                                  int &nc, double &sum_abs_depth);

/*! @brief print collisions.
 */
void print_collisions(int N, const dBodyID *fibre);

void solve_for_shls_forces(const dBodyID *bodies, const double *lb,
                           const vec3 *uvtm, double force, int seed);

/*! @brief compute gyration tensor as-well-as eigenvalues and eigenvectors
  @param bodies list of dBodyID
  @param start compute gyration tensor from this dBodyID
  @param end to this dBodyID (included)
  @param ev to store eigenvalues
  @param evec1 to store first eigenvector
  @param evec2 to store second eigenvector
  @param evec3 to store third eigenvector
 */
void gyration_tensor(const dBodyID *bodies, int start, int end, vec3 &ev,
                     vec3 &evec1, vec3 &evec2, vec3 &evec3);
#endif
