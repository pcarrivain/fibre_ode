The rigid-\/body-\/dynamics is useful for mechanical articulated system. In addition to that the tool allows the user to simulate complex shape and resolve excluded volume constraint. It is used in the industry of video games to accurately reproduce physics. However, a software like {\itshape Open-\/\+Dynamics-\/\+Engine} compute pairwize overlap check every time-\/step. The engine starts with a partition of the space and then loop over all the blocks of partition. For each blocks it runs a nested loops to check the overlaps between the objects inside the block.

The module implements external functions that can be used to compute \href{https://en.wikipedia.org/wiki/Verlet_list}{\tt Verlet-\/list}. Therefore, the user does not call the pairwize overlap check every time-\/step. He only needs to loop over the Verlet-\/list with the pairwize of objects within a given cut-\/off distance. However, the Verlet-\/list has to be updated according to the displacement length of the objects.

The module can be used to speed-\/up the {\itshape Open-\/\+Dynamics-\/\+Engine} simulation of polymers and complex objects system.

We test the module with the example of chromatin fiber. It is an assembly of D\+NA wrapped around nucleosomes (histones core) that compact the genome. We model the D\+NA at the scale of 10 base-\/pair as an articulated system. In particular, we use the work of \href{https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003456}{\tt Carrivain, Barbi and Victor}. The nucleosomes is built with complex shape. We run a Langevin dynamics and check that our Verlet-\/list implementation gives the same results {\itshape Open-\/\+Dynamics-\/\+Engine} would give. The name of the executable followed by a -\/h explains how to use the example \+: 
\begin{DoxyCode}
./fibre\_ODE -h
./bacteria -h
\end{DoxyCode}



\begin{DoxyEnumerate}
\item The chromatin fibre example is the file \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/blob/master/fibre_ODE.cpp}{\tt fibre\+\_\+\+O\+D\+E.\+cpp}
\item The bacteria example is the file \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/blob/master/bacteria.cpp}{\tt fibre\+\_\+\+O\+D\+E.\+cpp}
\item The Verlet-\/list for O\+DE functions are described in the file \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/blob/master/functions.cpp}{\tt functions.\+cpp}
\item {\itshape Open-\/\+Dynamics-\/\+Engine} (O\+DE) can be found \href{https://www.ode.org/}{\tt here}
\item The file \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/blob/master/read_nucleosome.cpp}{\tt read\+\_\+nucleosome.\+cpp} read the structure of the nucleosome
\item The structure of the nucleosome can be found \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/blob/master/nucleosome_in}{\tt here} 


\end{DoxyEnumerate}

\section*{Build Verlet list for O\+DE}

Here, the protocol to build Verlet-\/list for O\+DE is described \+:


\begin{DoxyEnumerate}
\item Create the normal {\itshape d\+Body\+ID} and {\itshape d\+Geom\+ID} from O\+DE A\+PI
\item Create kind of a ghost geometry with {\itshape create\+\_\+ghost\+\_\+geometry} function
\item Set data to the {\itshape d\+Body\+ID} using {\itshape struct \hyperlink{structint3p}{int3p}}
\item The member \textquotesingle{}p\textquotesingle{} of {\itshape struct \hyperlink{structint3p}{int3p}} does not need work from your side
\begin{DoxyEnumerate}
\item first element is the number of collisions made by object {\itshape i}
\item second element is the maximal number of collisions
\item next elements are the objects {\itshape j} that are potentially overlapping object {\itshape i}
\end{DoxyEnumerate}
\item However, \textquotesingle{}j\textquotesingle{} member of {\itshape struct \hyperlink{structint3p}{int3p}} needs to be filled with the index of \textquotesingle{}d\+Body\+ID\textquotesingle{}
\item Initialize the \textquotesingle{}d\+Body\+ID\textquotesingle{} data with {\itshape initialize\+\_\+body\+\_\+data} function
\item Build the Verlet-\/list with {\itshape build\+\_\+\+Verlet\+\_\+list} function
\item Collide the \textquotesingle{}d\+Geom\+ID\textquotesingle{} with {\itshape collide\+\_\+all} function
\end{DoxyEnumerate}

The ghost geometry is the normal geometry that has been extruded in all dimensions. You can choose to bound your geometry with a sphere of radius x-\/times her biggest dimension.

Do not forget to write your own callback function. It is well described by O\+DE manual. We provide a example that has to be copy-\/paste. In this example, we do not consider collision between two connected bodies as-\/well-\/as between two bodies of the same nucleosomes (chromatin fibre example). This is the only part of the function you can change. The other is needed to correctly build the Verlet-\/list. 
\begin{DoxyCode}
\{c++\}
void nearCallback(void *data,dGeomID o1,dGeomID o2)\{
  assert(o1);
  assert(o2);
  bool order\_12;
  dBodyID b1=dGeomGetBody(o1);
  dBodyID b2=dGeomGetBody(o2);
  // no collision between two linked bodies
  if(b1 && b2 && dAreConnected(b1,b2)) return;
  int3p *c1=(int3p*)(dBodyGetData(b1));
  int3p *c2=(int3p*)(dBodyGetData(b2));
  // no need to look for contacts inside nucleosome
  if((c1->k)==(c2->k) && (c1->k)>=0) return;
  // build contacts
  const int dcontact\_size=*((const int *)data);
  dContact *contact=new dContact[dcontact\_size];
  int nc\_VL,nc\_max;
  int n=dCollide(o1,o2,dcontact\_size,&(contact[0].geom),sizeof(dContact));
  // add to the Verlet-list
  if(n>0)\{
    // sort pairwize according to the index of each geometry
    order\_12=(bool)((c1->j)<=(c2->j));
    if(order\_12)\{
      nc\_VL=(c1->p)[0];
      nc\_max=(c1->p)[1];
    \}else\{
      nc\_VL=(c2->p)[0];
      nc\_max=(c2->p)[1];
    \}
    // reallocation ?
    if(nc\_VL>=nc\_max)\{
      printf("reallocation %i/%i\(\backslash\)n",nc\_VL,nc\_max);
      nc\_max=nc\_VL+1;
      if(order\_12)\{
    int\_new\_copy(&(((int3p*)(dBodyGetData(b1)))->p),nc\_VL,nc\_max);
    (((int3p*)(dBodyGetData(b1)))->p)[1]=nc\_max;
      \}else\{
    int\_new\_copy(&(((int3p*)(dBodyGetData(b2)))->p),nc\_VL,nc\_max);
    (((int3p*)(dBodyGetData(b2)))->p)[1]=nc\_max;
      \}
    \}
    // add the geometry to the Verlet-List
    // increment the length of the Verlet-List
    if(order\_12)\{
      (((int3p*)(dBodyGetData(b1)))->p)[nc\_VL]=c2.j;
      (((int3p*)(dBodyGetData(b1)))->p)[0]=nc\_VL+1;
    \}else\{
      (((int3p*)(dBodyGetData(b2)))->p)[nc\_VL]=c1.j;
      (((int3p*)(dBodyGetData(b2)))->p)[0]=nc\_VL+1;
    \}
  \}// Fi "n>0"
  c1=NULL;
  c2=NULL;
  delete[] contact;
  contact=NULL;
\}
\end{DoxyCode}


I declare the function {\itshape my\+\_\+near\+Callback} like \+: 
\begin{DoxyCode}
\{c++\}
void (*my\_nearCallback)(void*,dGeomID,dGeomID);
  my\_nearCallback=&nearCallback;
\end{DoxyCode}
 



\section*{Check Verlet-\/list for {\itshape Open-\/\+Dynamics-\/\+Engine}}

You can test the Verlet-\/list with the option \+: 
\begin{DoxyCode}
./fibre\_ODE --test yes
./bacteria --test yes
\end{DoxyCode}
 The test compares the number of collisions built from Verlet-\/list with the number of collisions built from normal O\+DE usage. If the two numbers are not equal, please consider to change the scale factor of ghost geometries and/or the number of steps between two builds of the Verlet-\/list. If there is still problems please contact me. 



\section*{$\ast$\+Open-\/\+Dynamics-\/\+Engine$\ast$}

I work with \href{https://bitbucket.org/odedevs/ode/downloads/}{\tt 0.\+16 O\+DE version}. The installation steps are \+: 
\begin{DoxyCode}
cd /scratch/pcarriva
tar -zxvf ode-0.16.tar.gz
cd ode-0.16
mkdir myBuild
./configure --prefix="/scratch/pcarriva/ode-0.16/myBuild" --enable-double-precision --enable-libccd
make install
make
\end{DoxyCode}
 I provide a \href{https://gitlab.e-cam2020.eu/carrivain/verlet_list_for_ode/master/Makefile}{\tt Makefile} to compile the examples. 