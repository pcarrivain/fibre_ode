\contentsline {chapter}{\numberline {1}Verlet\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}list\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}for\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}DE}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Class Index}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Class List}{5}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}int3p Struct Reference}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{7}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}matrix3 Struct Reference}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}vec2 Struct Reference}{8}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{9}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Constructor \& Destructor Documentation}{9}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}vec2()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{9}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}vec2()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{9}{subsubsection.3.3.2.2}
\contentsline {subsection}{\numberline {3.3.3}Member Data Documentation}{9}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}x}{9}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}y}{9}{subsubsection.3.3.3.2}
\contentsline {section}{\numberline {3.4}vec3 Struct Reference}{9}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{10}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Constructor \& Destructor Documentation}{10}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}vec3()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{10}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}vec3()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{10}{subsubsection.3.4.2.2}
\contentsline {subsection}{\numberline {3.4.3}Member Data Documentation}{10}{subsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.3.1}x}{10}{subsubsection.3.4.3.1}
\contentsline {subsubsection}{\numberline {3.4.3.2}y}{11}{subsubsection.3.4.3.2}
\contentsline {subsubsection}{\numberline {3.4.3.3}z}{11}{subsubsection.3.4.3.3}
