#include "functions.h"
#include "read_nucleosome.h"

using namespace std;

// read nucleosomal dna positions (in nm), scale it and return it
std::vector<vec3> read_dna_nucl_pos(const char *name,double l_scale){
  int retour;
  float f0,f1,f2;
  std::vector<vec3> pos;
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f\n",&f0,&f1,&f2);
    if(retour==3)
      pos.push_back(mult3(nm/l_scale,vec3(f0,f1,f2)));
  }
  fclose(f);
  return pos;
}

// read histones positions (in nm), scale it and return it
std::vector<vec3> read_hist_pos(const char *name,double l_scale){
  int retour;
  float f0,f1,f2;
  std::vector<vec3> pos;
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f\n",&f0,&f1,&f2);
    if(retour==3)
      pos.push_back(mult3(nm/l_scale,vec3(f0,f1,f2)));
  }
  fclose(f);
  return pos;
}

// read histones joints positions (in nm), scale it and return it
std::vector<vec3> read_hist_joint_pos(const char *name,double l_scale){
  int retour;
  float f0,f1,f2;
  std::vector<vec3> pos;
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f\n",&f0,&f1,&f2);
    if(retour==3)
      pos.push_back(mult3(nm/l_scale,vec3(f0,f1,f2)));
  }
  fclose(f);
  return pos;
}

// read nucleosomal dna rotations and return it
std::vector<std::vector<vec3>> read_dna_rot(const char *name){
  int retour,i=0;
  float f0,f1,f2;
  std::vector<std::vector<vec3>> rot(NNC,vector<vec3>(3));
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    for(int j=0;j<3;j++){
      retour=fscanf(f,"%f %f %f\n",&f0,&f1,&f2);
      if(retour==3)
	rot[i][j]=vec3(f0,f1,f2);
    }
    i++;
  }
  fclose(f);
  // f=fopen(name,"w");
  // for(int n=0;n<NNC;n++){
  //   fprintf(f,"%f %f %f\n",-rot[n][0].x,-rot[n][0].y,-rot[n][0].z);
  //   fprintf(f,"%f %f %f\n",-rot[n][1].x,-rot[n][1].y,-rot[n][1].z);
  //   fprintf(f,"%f %f %f\n",rot[n][2].x,rot[n][2].y,rot[n][2].z);
  // }
  // fclose(f);
  return rot;
}

// read nucleosomal histones rotations and return it
std::vector<std::vector<vec3>> read_hist_rot(const char *name){
  int retour,i=0;
  float f0,f1,f2;
  std::vector<std::vector<vec3>> rot(NHC,vector<vec3>(3));
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    for(int j=0;j<3;j++){
      retour=fscanf(f,"%f %f %f\n",&f0,&f1,&f2);
      if(retour==3)
	rot[i][j]=vec3(f0,f1,f2);
    }
    i++;
  }
  fclose(f);
  return rot;
}

// read SHLs positions (in nm), scale it and return it
std::vector<vec3> read_shls_pos(const char *name,double l_scale){
  int retour,i0,i1;
  float f0,f1,f2;
  std::vector<vec3> pos;
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f %i %i\n",&f0,&f1,&f2,&i0,&i1);
    if(retour==5)
      pos.push_back(mult3(nm/l_scale,vec3(f0,f1,f2)));
  }
  fclose(f);
  return pos;
}

// read dockings positions (in nm), scale it and return it
std::vector<vec3> read_dockings_pos(const char *name,double l_scale){
  int retour,i0,i1;
  float f0,f1,f2;
  std::vector<vec3> pos;
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f %i %i\n",&f0,&f1,&f2,&i0,&i1);
    if(retour==5)
      pos.push_back(mult3(nm/l_scale,vec3(f0,f1,f2)));
  }
  fclose(f);
  return pos;
}

// read SHLs bodies and return it
std::vector<std::vector<int>> read_shls_bodies(const char *name){
  int retour,i=0,i0,i1;
  float f0,f1,f2;
  std::vector<std::vector<int>> bodies(nSHLs,vector<int>(2));
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f %i %i\n",&f0,&f1,&f2,&i0,&i1);
    if(retour==5){
      bodies[i][0]=i0;
      bodies[i][1]=i1;
      i++;
    }
  }
  fclose(f);
  return bodies;
}

// read dockings bodies and return it
std::vector<std::vector<int>> read_dockings_bodies(const char *name){
  int retour,i=0,i0,i1;
  float f0,f1,f2;
  std::vector<std::vector<int>> bodies(nDOCKs,vector<int>(2));
  FILE *f=fopen(name,"r");
  while(!feof(f)){
    retour=fscanf(f,"%f %f %f %i %i\n",&f0,&f1,&f2,&i0,&i1);
    if(retour==5){
      bodies[i][0]=i0;
      bodies[i][1]=i1;
      i++;
    }
  }
  fclose(f);
  return bodies;
}
