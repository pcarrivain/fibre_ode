#!/bin/bash
visualisation=/scratch/pcarriva/simulations/fibre_ODE/visualisation;
output=/scratch/pcarriva/simulations/fibre_ODE;
path_to_blender=/scratch/pcarriva/blender-2.79b-linux-glibc219-x86_64;

while getopts hs:v:B:p:o: option
do
    case "${option}"
    in
        s) structure=${OPTARG};;
	v) visualisation=${OPTARG};;
	B) path_to_blender=${OPTARG};;
        p) path_to_data=${OPTARG};;
        o) output=${OPTARG};;
	h) echo "usage :"
	   echo "      -s <structure>"
	   echo "      -c <visualisation name>"
	   echo "      -B <path to your blender>"
	   echo "      -p <path to the data>"
	   echo "      -o <where to write the png>"
	   echo "example :"
	   echo "./visualisation.sh -s 1 -s sn1_chromatin.in_F0.00_T0.0.out -v vn1_chromatin.in_F0.00_T0.0.out -B /scratch/pcarriva/blender-2.79b-linux-glibc219-x86_64 -p /scratch/pcarriva/simulations/fibre_ODE/visualisation -o /scratch/pcarriva/simulations/fibre_ODE"
	   exit 0
	   ;;
    esac;
done;

if [[ ${1} != "-h" ]];
then
    ${path_to_blender}/blender -b -P ${output}/visualisation.py -- -c ensemble -v $visualisation -s $structure --epi_colors -p $path_to_data -w $output -x 0.0 -y -50.0 -z 0.0 --e1=1.57 --e2=0 --e3=0 --nvertices=32 --make_movie;
fi;
