#!/usr/bin/python
# -*- coding: utf-8 -*-

# ffmpeg -r 24 -s 1920x1080 -i n1_10000bp_c24_im6_i%d_ensemble.png -b:v 5000k -vcodec mpeg4 -q 31 test.avi

import os
import bpy,bmesh
import mathutils
# import numba
import sys,getopt
import time
import struct
import numpy as np
from numpy import linalg as LA
from bpy import *
from mathutils import *

def add_plane(x: float=0.0,y: float=0.0,z: float=0.0,pname: str="plan"):
    """add plane
    Parameters
    ----------
    x     : location in x (float)
    y     : location in y (float)
    z     : location in z (float)
    pname : name of the plane (str)
    Returns
    -------
    Raises
    ------
    """
    bpy.ops.mesh.primitive_plane_add(radius=R,location=(x,y,z))
    bpy.context.object.name=pname
    bpy.data.objects[pname].data.materials.append(bpy.data.materials['mat_blanc'])
    bpy.data.objects[pname].data.materials[0].diffuse_shader='FRESNEL'
    bpy.data.objects[pname].data.materials[0].diffuse_fresnel=2.0
    bpy.data.objects[pname].data.materials[0].diffuse_fresnel_factor=2.0
    bpy.data.objects[pname].data.materials[0].ambient=0.0
    # bpy.data.objects[pname].data.materials[0].specular_shader='COOKTORR'
    bpy.data.objects[pname].select=False

def add_nucleus(x: float=0.0,y: float=0.0,z: float=0.0,R: float=0.0):
    """add nucleus (WIRE draw type)
    Parameters
    ----------
    x : position in x
    y : position in y
    z : position in z
    R : radius of the nucleus
    Returns
    -------
    Raises
    ------
    """
    bpy.ops.mesh.primitive_uv_sphere_add(segments=64,ring_count=64,size=R)
    bpy.context.object.location=x,y,z
    bpy.context.object.draw_type='WIRE'
    bpy.context.object.show_wire
    bpy.context.scene.objects.unlink(bpy.context.object)
    bpy.context.object.select=False

def clean_initial_scene():
    """clean initial blender scene
    """
    bpy.data.objects.remove(bpy.data.objects["Cube"],True)
    bpy.data.objects.remove(bpy.data.objects["Camera"],True)
    bpy.data.objects.remove(bpy.data.objects["Lamp"],True)

def create_scene_world():
    """create the sky and ligth for the blender world
    """
    bpy.context.scene.world.use_sky_paper=False
    bpy.context.scene.world.use_sky_blend=False
    bpy.context.scene.world.use_sky_real=True
    bpy.context.scene.world.ambient_color=(0.0,0.0,0.0)
    bpy.context.scene.world.horizon_color=(1.0,1.0,1.0)
    bpy.context.scene.world.zenith_color=(1.0,1.0,1.0)
    bpy.context.scene.world.light_settings.use_environment_light=False
    bpy.context.scene.world.light_settings.use_ambient_occlusion=True
    bpy.context.scene.world.light_settings.use_indirect_light=True
    # bpy.context.scene.world.mist_settings.use_mist=True

def create_persp_camera(name: str="camera",x: float=0.0,y: float=100.0,z: float=0.0,ex: float=0.0,ey: float=0.0,ez: float=0.0):
    """create perspective camera
    Parameters
    ----------
    name  : name of the camera (str)
    x     : position in x
    y     : position in y
    z     : position in z
    ex    : euler angle X
    ey    : euler angle Y
    ez    : euler angle Z
    Returns
    -------
    Raises
    ------
    """
    bpy.ops.object.camera_add()
    bpy.context.object.name=name
    bpy.context.scene.camera=bpy.data.objects[name]
    bpy.data.objects[name].data.type='PERSP'
    bpy.data.objects[name].data.lens_unit='FOV'
    bpy.data.objects[name].data.lens=25.0
    bpy.data.objects[name].data.show_limits=True
    bpy.data.objects[name].data.gpu_dof.use_high_quality=True
    bpy.data.objects["camera"].location=x,y,z
    bpy.data.objects["camera"].rotation_mode='XYZ'
    bpy.data.objects["camera"].rotation_euler=ex,ey,ez
    bpy.data.objects["camera"].data.dof_distance=np.sqrt(x*x+y*y+z*z)
    bpy.data.objects["camera"].data.clip_start=0.01*np.sqrt(x*x+y*y+z*z)
    bpy.data.objects["camera"].data.clip_end=8.0*np.sqrt(x*x+y*y+z*z)
    bpy.data.objects["camera"].data.show_safe_areas=True
    bpy.data.objects['camera'].select=False

def create_sun_lamp(name: str="lamp",x: float=0.0,y: float=0.0,z: float=100.0):
    """create sun lamp
    Parameters
    ----------
    name : name of the lamp (str)
    x     : position in x
    y     : position in y
    z     : position in z
    Returns
    -------
    Raises
    ------
    """
    bpy.ops.object.lamp_add(type='SUN')
    bpy.context.object.name=name
    bpy.data.objects[name].rotation_mode='XYZ'
    bpy.data.objects[name].location=x,y,z
    bpy.data.objects[name].rotation_euler=0.5*np.pi,0.0,0.0
    bpy.data.objects[name].data.type='SUN'
    bpy.data.objects[name].data.shadow_ray_samples=16
    # bpy.data.objects[name].data.sky.use_atmosphere=True
    # bpy.data.objects[name].data.sky.use_sky=True
    bpy.data.objects[name].data.energy=0.8
    bpy.data.objects[name].select=False

def rainbow_materials(levels: int=100,linear: bool=True) -> list:
    """return colors according to rainbow palette.
    Parameters
    ----------
    levels : number of levels (int)
    linear : does it linear or circular ? (bool)
    Returns
    -------
    list of str
    Raises
    ------
    """
    if linear:
        hlevels=levels//2
        for l in range(0,hlevels,1):
            bpy.data.materials.new("mat_rainbow"+str(l))
            bpy.data.materials["mat_rainbow"+str(l)].diffuse_color=0.0,float(l)/hlevels,1.0-float(l)/hlevels
        for l in range(hlevels,levels,1):
            bpy.data.materials.new("mat_rainbow"+str(l))
            bpy.data.materials["mat_rainbow"+str(l)].diffuse_color=float(l-hlevels)/hlevels,1.0-float(l-hlevels)/hlevels,0.0
    else:
        qlevels=levels//4
        hlevels=levels//2+1
        for l in range(0,qlevels,1):
            bpy.data.materials.new("mat_rainbow"+str(l))
            bpy.data.materials["mat_rainbow"+str(l)].diffuse_color=0.0,float(l)/qlevels,1.0-float(l)/qlevels
        for l in range(qlevels,hlevels,1):
            bpy.data.materials.new("mat_rainbow"+str(l))
            bpy.data.materials["mat_rainbow"+str(l)].diffuse_color=float(l-qlevels)/qlevels,1.0-float(l-qlevels)/qlevels,0.0
        for l in range(hlevels,levels,1):
            bpy.data.materials.new("mat_rainbow"+str(l))
            r,g,b=bpy.data.materials["mat_rainbow"+str(levels-l)].diffuse_color
            bpy.data.materials["mat_rainbow"+str(l)].diffuse_color=r,g,b
    for m in bpy.data.materials:
        m.diffuse_shader='TOON'
        m.diffuse_toon_size=0.9
        m.diffuse_intensity=0.9
        m.specular_shader='TOON'
        m.specular_toon_size=0.9
        m.specular_color=m.diffuse_color
        m.specular_intensity=0.1
        m.use_transparency=False
    # for l in range(levels):
    #     bpy.data.materials["mat_rainbow"+str(l)].diffuse_shader='TOON'
    #     bpy.data.materials["mat_rainbow"+str(l)].diffuse_toon_size=0.9
    #     bpy.data.materials["mat_rainbow"+str(l)].diffuse_intensity=0.9
    #     bpy.data.materials["mat_rainbow"+str(l)].specular_shader='TOON'
    #     bpy.data.materials["mat_rainbow"+str(l)].specular_toon_size=0.9
    #     bpy.data.materials["mat_rainbow"+str(l)].specular_color=bpy.data.materials["mat_rainbow"+str(l)].diffuse_color
    #     bpy.data.materials["mat_rainbow"+str(l)].specular_intensity=0.1
    #     bpy.data.materials["mat_rainbow"+str(l)].use_transparency=False
    return ["mat_rainbow"+str(l) for l in range(levels)]
    
def load_materials():
    bpy.data.materials.new("mat_noir")
    bpy.data.materials["mat_noir"].diffuse_color=.1,.1,.1
    bpy.data.materials.new("mat_rouge")
    bpy.data.materials["mat_rouge"].diffuse_color=1.,.0,.0
    bpy.data.materials.new("mat_vert")
    bpy.data.materials["mat_vert"].diffuse_color=.0,1.,.0
    bpy.data.materials.new("mat_bleu")
    bpy.data.materials["mat_bleu"].diffuse_color=.0,.0,1.
    bpy.data.materials.new("mat_violet")
    bpy.data.materials["mat_violet"].diffuse_color=.2,.0,.8
    bpy.data.materials.new("mat_violet_clair")
    bpy.data.materials["mat_violet_clair"].diffuse_color=.2,.0,.8
    bpy.data.materials.new("mat_blanc")
    bpy.data.materials["mat_blanc"].diffuse_color=1.,1.,1.
    bpy.data.materials.new("mat_noir_clair")
    bpy.data.materials["mat_noir_clair"].diffuse_color=.2,.2,.2
    bpy.data.materials.new("mat_rouge_clair")
    bpy.data.materials["mat_rouge_clair"].diffuse_color=8.,.1,.1
    bpy.data.materials.new("mat_vert_clair")
    bpy.data.materials["mat_vert_clair"].diffuse_color=.1,.8,.1
    bpy.data.materials.new("mat_bleu_clair")
    bpy.data.materials["mat_bleu_clair"].diffuse_color=.0,.2,.8
    bpy.data.materials.new("mat_jaune")
    bpy.data.materials["mat_jaune"].diffuse_color=0.8,0.6,0.0
    bpy.data.materials.new("mat_jaune_clair")
    bpy.data.materials["mat_jaune_clair"].diffuse_color=0.8,0.6,0.0
    bpy.data.materials.new("mat_rose")
    bpy.data.materials["mat_rose"].diffuse_color=0.8,0.0,0.3
    bpy.data.materials.new("mat_rose_clair")
    bpy.data.materials["mat_rose_clair"].diffuse_color=0.8,0.0,0.3
    bpy.data.materials.new("mat_cyan")
    bpy.data.materials["mat_cyan"].diffuse_color=0.0,0.8,0.7
    bpy.data.materials.new("mat_cyan_clair")
    bpy.data.materials["mat_cyan_clair"].diffuse_color=0.0,0.8,0.7
    for m in bpy.data.materials:
        # m.type='HALO'# ???
        m.diffuse_shader='TOON'
        m.diffuse_toon_size=0.9
        m.diffuse_intensity=0.9
        m.specular_shader='TOON'
        m.specular_toon_size=0.9
        m.specular_color=m.diffuse_color
        m.specular_intensity=0.1
        m.use_transparency=False
    if False:
        bpy.data.materials["mat_violet"].diffuse_intensity=0.5
        bpy.data.materials["mat_rose"].diffuse_intensity=0.5
        bpy.data.materials["mat_jaune"].diffuse_intensity=0.5
        bpy.data.materials["mat_cyan"].diffuse_intensity=0.5

def simple_materials() -> list:
    titre=['noir','rouge','vert','bleu','noir_clair','rouge_clair','vert_clair','bleu_clair','blanc','violet','jaune','rose','cyan','violet_clair','jaune_clair','rose_clair','cyan_clair']
    titre=["mat_"+t for t in titre]
    return titre

def transparency_on(only: str,titre: list):
    """make material transparent if it is not 'only'
    Parameters
    ----------
    only  : material that is not transparent (str)
    titre : list of material names (list of str)
    Returns
    -------
    Raises
    ------
    """
    if only!="":
        print("transparency on except",only)
        for t in titre:
            if not t.replace("mat_","") in only:
                bpy.data.materials[t].use_transparency=True
                bpy.data.materials[t].transparency_method="Z_TRANSPARENCY"
                bpy.data.materials[t].alpha=0.1

def read_details(fname: str,cdraw: int,titre: list) -> tuple:
    """read details of the system you want to draw
    Parameters
    ----------
    fname : name of the file with the system details (str)
    cdraw : first or second color column (int)
    titre : list of material names (list of int)
    Returns
    -------
    number of objects (int),radius (list of float),length (list of float),color indices (list of int),chain indices (list of int) geometry type (list of int),colors (list of str)
    Raises
    ------
    """
    with open(fname,'r') as fichierStructure:
        data=fichierStructure.readlines()
        n_corps=int(len(data))
        radius=[0.0]*n_corps
        length=[0.0]*n_corps
        i_color=[-1]*n_corps
        ichr=np.full(n_corps,-1,dtype='int64')
        gtype=[-1]*n_corps
        for l,ligne in enumerate(data):
            lsp=ligne.split()
            gtype[l]=int(lsp[0])
            radius[l]=float(lsp[1])
            length[l]=float(lsp[2])
            i_color[l]=int(lsp[4+cdraw])
            ichr[l]=int(lsp[5])
    colors=[titre[c] for c in i_color]
    return n_corps,radius,length,i_color,ichr,gtype,colors

def create_cycles():
    bpy.context.scene.render.engine='CYCLES'
    bpy.context.scene.cycles.samples=25
    bpy.context.scene.cycles.caustics_reflective=False
    bpy.context.scene.cycles.caustics_refractive=False

def create_render(movie: bool=False,resx: int=1920,resy: int=1080):
    """create render parameters
    Parameters
    ----------
    movie : if yes make movie, if no make render (bool)
    resx  : resolution in x
    resy  : resolution in y
    Returns
    -------
    Raises
    ------
    """
    bpy.context.scene.render.fps=24
    bpy.context.scene.render.fps_base=5
    bpy.context.scene.render.use_antialiasing=True
    bpy.context.scene.render.antialiasing_samples='8'
    # bpy.context.scene.render.use_edge_enhance=True
    # bpy.context.scene.render.edge_threshold=255
    # bpy.context.scene.render.ffmpeg.use_lossless_output=False
    # bpy.context.scene.render.ffmpeg.audio_codec='FLAC'
    if not movie:
        bpy.context.scene.render.image_settings.file_format='PNG'
    else:
        bpy.context.scene.render.image_settings.file_format='AVI_JPEG'
    # bpy.context.scene.render.image_settings.file_format='FFMPEG'
    bpy.context.scene.render.use_file_extension=True
    bpy.context.scene.render.resolution_x=2*resx
    bpy.context.scene.render.resolution_y=2*resy
    bpy.context.scene.render.resolution_percentage=50.0
    bpy.context.scene.render.image_settings.quality=100

# @numba.jit
def from_i_to_bead(i: int,N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int)
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i<0:
        return N-(-i)%N
    elif i>=N:
        return i%N
    else:
        return i

# @numba.jit
def normalize(u):
    """return the normalized numpy array
    Parameters
    ----------
    u : 3d vector to normalize (np.ndarray)
    Returns
    -------
    the normalized vector 'u' (np.ndarray)
    """
    norm=LA.norm(u)
    if norm<0.000000001:
        return u
    else:
        return np.multiply(1.0/norm,u)

def rotation(a: np.ndarray,angle: float,b: np.ndarray) -> np.ndarray:
    """return (x',y',z') that is the rotation of b=(x,y,z) around a=(X,Y,Z) with angle.
    Parameters
    ----------
    a     : rotation axis (list of float)
    angle : rotation angle (float)
    b     : the OpenMM.Vec3 to rotate
    Returns
    -------
    the rotated vector (x',y',z') (list).
    """
    if a[0]==b[0] and a[1]==b[1] and a[2]==b[2]:
        return b
    else:
        n=np.sqrt(b[0]**2+b[1]**2+b[2]**2)
        c=np.cos(angle)
        s=np.sin(angle)
        new_b=normalize(b)
        res=np.multiply(c,new_b)
        ab=np.dot(a,new_b)
        axb=my_cross(a,new_b)
        res=np.add(res,np.add(np.multiply((1.0-c)*ab,a),np.multiply(s,axb)))
        return np.multiply(n,normalize(res))

# @numba.jit(nopython=True)
def my_cross(u: np.ndarray,v: np.ndarray) -> np.ndarray:
    """return the cross product u x v.
    do not use numpy cross because it does not work (from my experience) with numba.
    Parameters
    ----------
    u : first vector (np.ndarray)
    v : second vector (np.ndarray)
    Returns
    -------
    cross product u x v (np.ndarray)
    """
    return np.array([u[1]*v[2]-u[2]*v[1],u[2]*v[0]-u[0]*v[2],u[0]*v[1]-u[1]*v[0]])

# @numba.jit(nopython=True)
def Rg2_and_shape(positions: np.ndarray,us: np.ndarray,vs: np.ndarray,ts: np.ndarray,start: int,end: int,linear: bool=True,align: bool=False) -> tuple:
    """calculates the tensor and radius of gyration for the part within [start,end].
    Parameters
    ----------
    positions   : list of positions (complete system) (np.ndarray)
    us          : list of normals (np.ndarray)
    vs          : list of cross(t,u) (np.ndarray)
    ts          : list of tangents (np.ndarray)
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    align       : align the sub-part along the eigenvectors of the gyration tensor (bool)
    Returns
    -------
    A tuple made of the radius of gyration (float), the asphericity (float), the acylindricity (float), the shape factor (float), the 1d size of each part.
    Raises
    ------
    if start is equal to end, return a ZeroDivisionError.
    """
    N,D=positions.shape
    # if the polymer is linear the start < end for each parts we ask for
    if linear and start>end:
        start,end=end,start
    # com
    com=positions[end]
    ii=start
    P=1
    while ii!=end:
        com=np.add(com,positions[ii])
        ii=from_i_to_bead(ii+1,N)
        P+=1
    com=np.multiply(1.0/P,com)
    # gyration tensor
    Tij=np.zeros((3,3))
    new_positions=np.subtract(positions,com)
    ii=start
    for i in range(P):
        Tij=np.add(Tij,np.outer(new_positions[ii],new_positions[ii]))
        ii=from_i_to_bead(ii+1,N)
    # eigenvalues of the gyration tensor
    EVs,evs=LA.eigh(np.multiply(1.0/P,Tij))
    Rg2=EVs[0]**2+EVs[1]**2+EVs[2]**2
    # sort the eigenvalues
    iw=np.argsort(EVs)
    EVs=EVs[iw]
    # align
    r2=normalize(my_cross(evs[:,iw[2]],np.array([0.0,0.0,1.0])))
    a2=np.arccos(np.dot(evs[:,iw[2]],np.array([0.0,0.0,1.0])))
    evs[:,iw[0]]=rotation(r2,a2,evs[:,iw[0]])
    evs[:,iw[1]]=rotation(r2,a2,evs[:,iw[1]])
    evs[:,iw[2]]=rotation(r2,a2,evs[:,iw[2]])
    if True:
        r1=normalize(my_cross(evs[:,iw[1]],np.array([0.0,1.0,0.0])))
        a1=np.arccos(np.dot(evs[:,iw[1]],np.array([0.0,1.0,0.0])))
    else:
        avg_vs_01=normalize(np.add(np.multiply(EVs[0]/(EVs[0]+EVs[1]),evs[:,iw[0]]),np.multiply(EVs[1]/(EVs[0]+EVs[1]),evs[:,iw[1]])))
        r1=normalize(my_cross(avg_vs_01,np.array([0.0,1.0,0.0])))
        a1=np.arccos(np.dot(avg_vs_01,np.array([0.0,1.0,0.0])))
    # new positions
    if align:
        new_positions=np.array([rotation(r1,a1,rotation(r2,a2,p)) for p in new_positions])
        new_us=np.array([rotation(r1,a1,rotation(r2,a2,u)) for u in us])
        new_vs=np.array([rotation(r1,a1,rotation(r2,a2,v)) for v in vs])
        new_ts=np.array([rotation(r1,a1,rotation(r2,a2,t)) for t in ts])
    else:
        new_us=np.copy(us)
        new_vs=np.copy(vs)
        new_ts=np.copy(ts)
    # shape
    asphericity=EVs[2]-0.5*(EVs[0]+EVs[1])
    acylindricity=EVs[1]-EVs[0]
    kappa2=(asphericity*asphericity+0.75*acylindricity*acylindricity)/((EVs[0]+EVs[1]+EVs[2])**2)
    return Rg2,asphericity,acylindricity,kappa2,P,new_positions,new_us,new_vs,new_ts

def align_gyration_tensor(positions: np.ndarray,matrices: list) -> tuple:
    """gyration tensor along world frame
    Parameters
    ----------
    positions : positions of the objects (np.ndarray)
    matrices  : orientations of the objects (list of mathutils.Matrix)
    Returns
    -------
    new positions (np.ndarray), new orientations (list of mathutils.Matrix)
    Raises
    ------
    """
    # com to 0,0,0
    com=np.array([0.0,0.0,0.0],dtype=float)
    for p in positions:
        com=np.add(com,np.array(p,dtype=float))
    com=np.multiply(1.0/n_corps,com)
    new_positions=[np.subtract(p,com) for p in positions]
    com=[0.0,0.0,0.0]
    # gyration tensor
    Tij=mathutils.Matrix(((0.0,0.0,0.0),(0.0,0.0,0.0),(0.0,0.0,0.0)))
    for p in new_positions:
        for i in range(0,3):
            for j in range(0,3):
                Tij[i][j]+=(p[i]-com[i])*(p[j]-com[j])/n_corps
    inv_Tij=Tij.inverted()
    new_matrices=[inv_Tij*m for m in matrices]
    for p in new_positions:
        v=mathutils.Vector((p[0],p[1],p[2]))
        v=inv_Tij*v
        p=np.array([v[0],v[1],v[2]],dtype=float)
    return new_positions,new_matrices

def zoom_in(positions: np.ndarray,ichr: list,i_color: list,colors: list,titre: list,zoom: str=""):
    """zoom in chain part and change its color to violet
    Parameters
    ----------
    positions : positions (np.ndarray)
    ichr      : list of chain indices (list of int)
    i_color   : list of color indices (list of int)
    colors    : list of colors (list of str)
    titre     : list of material names (list of str)
    zoom      : index of the chain (int)
    Returns
    -------
    positions (np.ndarray)
    Raises
    ------
    """
    if zoom!="":
        print("zoom in",int(zoom))
        N_zoom=np.count_nonzero(ichr==int(zoom))
        zoom_start=np.where(ichr==int(zoom))[0][0]
        start=-1
        end=-1
        loop_start=False
        loop_end=False
        # extract one loop
        for i,c in enumerate(ichr):
            if c==int(zoom):
                if i_color[i]==10 and loop_start and not loop_end:
                    loop_end=True
                    end=i
                    break
                if i_color[i]==10 and i>(zoom_start+int(0.5*N_zoom)) and not loop_start:
                    loop_start=True
                    start=i
        for i in range(start+1,end):
            i_color[i]=12
            colors[i]=titre[i_color[i]]
        bpy.data.materials[colors[start+1]].use_transparency=False
        com=np.multiply(1.0/N_zoom,np.sum(np.array([positions[i,:3] for i,c in enumerate(ichr) if c==int(zoom)]),axis=0))
        return np.subtract(positions,com)
    return positions

def align_chain(positions: np.ndarray,ichr: list,us: np.ndarray,vs: np.ndarray,ts: np.ndarray,align: str=""):
    """align chain along the world frame (if 'align' is '' do nothing)
    Parameters
    ----------
    positions : positions (np.ndarray)
    ichr      : list of chain indices (list of int)
    us        : (np.ndarray)
    vs        : (np.ndarray)
    ts        : (np.ndarray)
    align     : chain to align (str)
                example "0.1.2" : chain 0, 1 and 2 to align
    Returns
    -------
    Raises
    ------
    """
    if align!="":
        salign=align.split('.')
        new_positions=np.copy(positions)
        new_us=np.copy(us)
        new_vs=np.copy(vs)
        new_ts=np.copy(ts)
        ps=np.copy(positions)
        for a in salign:
            N_align=np.count_nonzero(ichr==int(a))
            part=np.array([i for i,c in enumerate(ichr) if c==int(a)],dtype='int64')
            Rg2,asphericity,acylindricity,kappa2,P,p0,u0,v0,t0=Rg2_and_shape(ps,us,vs,ts,int(np.amin(part,axis=0)),int(np.amax(part,axis=0)),True,True)
            new_positions[part,:3]=p0[part,:3]
            new_us[part,:3]=u0[part,:3]
            new_vs[part,:3]=v0[part,:3]
            new_ts[part,:3]=t0[part,:3]
        return new_positions,new_us,new_vs,new_ts
    else:
        return positions,us,vs,ts

def behind_camera(positions: np.ndarray,ichr: list,zoom: str="",align: str=""):
    """move chain behind camera (if 'zoom'/'align' is '' do nothing)
    Parameters
    ----------
    positions : positions (np.ndarray)
    ichr      : list of chain indices (list of int)
    zoom      : index of the chain (str)
    align     : chain to align (str)
    Returns
    -------
    Raises
    ------
    """
    new_positions=np.copy(positions)
    if zoom!="" or align!="":
        if zoom!="":
            cbuffer=zoom
        else:
            cbuffer=align
        # ... everything else behind the camera
        for i,c in enumerate(ichr):
            if not str(c) in cbuffer:
                new_positions[i,:3]=10000.0,10000.0,10000.0
    return new_positions

def average_along_the_contour_length(positions: np.ndarray,ichr: list,wavg: int=2) -> tuple:
    """window average along the chain contour length ('wavg' has to be > 1)
    Parameters
    ----------
    positions : positions (np.ndarray)
    ichr      : list of chain indices (list of int)
    wavg      : window average (int)
    Returns
    -------
    positions (np.ndarray)
    Raises
    ------
    """
    # monomers per chromosome
    chains=int(np.unique(np.array(ichr)).size)
    n_per_chr=[0]*chains
    for c in range(chains):
        n_per_chr[c]=sum([int(i==c) for i in ichr])
    # average window is obviously lesser than the monomers per chromosome
    wavg_c=min(wavg,2)
    wavg_c=[min(n,wavg) for n in n_per_chr]
    N,D=positions.shape
    W=N-sum(wavg_c)
    f_Rg=1.0
    n_corps=W
    wpositions=np.zeros((W,3),dtype=np.double)
    wradius=[0.0]*W
    wbond=[0.0]*W
    wRg=np.zeros((W,3))
    wu=np.zeros((W,3),dtype=np.double)
    wv=np.zeros((W,3),dtype=np.double)
    wt=np.zeros((W,3),dtype=np.double)
    wcolors=[0]*W
    wichr=[-1]*W
    wgtype=[1]*W
    print("average (w="+str(wavg_c)+") along the contour length")
    i_avg=0
    monomer=0
    abs_tol=1e-6
    ## loop over the eight chromosomes
    for c in range(chains):
        # average
        for i in range(n_per_chr[c]-wavg_c[c]):
            # com
            for j in range(wavg_c[c]):
                wpositions[i_avg]=np.add(wpositions[i_avg],np.array(positions[monomer+j],dtype=np.double))
            wpositions[i_avg]=np.multiply(1.0/wavg_c[c],wpositions[i_avg])
            # gyration tensor
            Tab=np.zeros((3,3),dtype=float)
            for j in range(wavg_c[c]):
                for a in range(3):
                    for b in range(a,3):
                        Tab[a,b]+=(positions[monomer+j][a]-wpositions[i_avg][a])*(positions[monomer+j][b]-wpositions[i_avg][b])/wavg_c[c]
                        Tab[b,a]=Tab[a,b]
            w,v=np.linalg.eigh(Tab)
            # idx=w.argsort()
            idx=[0,1,2]
            # eigenvalues should be >= 0
            for j in range(3):
                if w[j]<(-abs_tol):
                    print(w)
                    sys.exit()
            if wavg_c[c]==2 or wavg_c[c]==3:
                wRg[i_avg,:3]=np.sqrt(sum(w)/3.0),np.sqrt(sum(w)/3.0),np.sqrt(sum(w)/3.0)
            else:
                wRg[i_avg,:3]=f_Rg*np.sqrt(max(0.0,w[idx[0]])),f_Rg*np.sqrt(max(0.0,w[idx[1]])),f_Rg*np.sqrt(max(0.0,w[idx[2]]))
            wRg[i_avg,:3]=0.1,0.1,0.1
            wu[i_avg]=v[idx[0]]/np.sqrt(np.dot(v[idx[0]],v[idx[0]]))
            wv[i_avg]=v[idx[1]]/np.sqrt(np.dot(v[idx[1]],v[idx[1]]))
            wt[i_avg]=v[idx[2]]/np.sqrt(np.dot(v[idx[2]],v[idx[2]]))
            ## radius and length
            wradius[i_avg]=np.sqrt(sum(w))
            wbond[i_avg]=2.0*np.sqrt(sum(w))
            wcolors[i_avg]=colors[monomer]
            wichr[i_avg]=ichr[monomer]
            i_avg+=1
            monomer+=1
        monomer=np.cumsum(n_per_chr)[c]
    # u,v,t frame
    # ts=[np.subtract(wpositions[i+1],p) for i,p in enumerate(wpositions[:-1]) if wichr[i]==wichr[i+1]]
    # ts=[np.multiply(1.0/np.sqrt(np.dot(t,t)),t) for t in ts]
    # us=[np.multiply(1.0/np.sqrt(t[0]**2+t[2]**2),np.array([t[2],0.0,-t[0]])) for t in ts]
    # vs=[np.cross(t,us[i]) for i,t in enumerate(ts)]
    us=np.copy(wu)
    vs=np.copy(wv)
    ts=np.copy(wt)            
    wmatrices=[mathutils.Matrix(((us[i][0],us[i][1],us[i][2]),(vs[i][0],vs[i][1],vs[i][2]),(t[0],t[1],t[2]))) for i,t in enumerate(ts)]
    if i_avg!=W or len(wmatrices)!=W:
        print(monomer,len(wmatrices),W)
        sys.exit()
    return wpositions,wichr,wichr,wradius,wbond,wgtype,wmatrices

def main(argv):
    cdraw=1
    save_blend=False
    wavg=0
    gyration_tensor=False
    choix="ensemble"
    only=""
    draw_with_rainbow_colors=False
    bcom=False
    bclock=False
    bslice=False
    zoom=""
    align=""
    eframes=1
    nvertices=64
    x_camera,y_camera,z_camera=-100.0,0.0,0.0
    e1_camera,e2_camera,e3_camera=0.5*np.pi,0.0,-0.5*np.pi
    resX,resY=1920,1080
    make_movie=False
    make_render=False
    R=2000.0/np.sqrt(3.0*10000.0)
    try:
        opts,args=getopt.getopt(argv,"hv:s:c:a:p:w:x:y:z:",["visualisation=","structure=","choix=","avgdomains=","e1=","e2=","e3=","epi_colors","chr_colors","gyration_tensor","eframes=","path_to=","write_to=","only=","zoom=","align=","nvertices=","resX=","resY=","rainbow","com","clock","slice","save","make_render","make_movie"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('visualisation.py -v <visualisation>')
            print('                 -s <structure>')
            print('                 -c <choix>')
            print('                 -a <avgdomains>')
            print('                 --epi_colors')
            print('                 --chr_colors')
            print('                 --gyration_tensor')
            print('                 --eframes=<render frame every>')
            print('                 -p <path to the data>')
            print('                 -w <path to where to write the render>')
            print('                 -x <x-camera>')
            print('                 -y <y-camera>')
            print('                 -z <z-camera>')
            print('                 --e1 <euler angle 1 camera>')
            print('                 --e2 <euler angle 2 camera>')
            print('                 --e3 <euler angle 3 camera>')
            print('                 --only=<color>')
            print('                 --zoom=<index>')
            print('                 --align=<index>')
            print('                 --nvertices=<number of vertices per mesh>')
            print('                 --resX=<resolution in X>')
            print('                 --resY=<resolution in Y>')
            print('                 --rainbow <rainbow materials>')
            print('                 --com')
            print('                 --clock')
            print('                 --save')
            print('                 --make_render')
            print('                 --make_movie')
            sys.exit()
        elif opt=="-v" or opt=="--visualisation":
            nom_visualisation=str(arg)
        elif opt=="-s" or opt=="--structure":
            sname=str(arg)
        elif opt=="-c" or opt=="--choix":
            choix=arg
        elif opt=="-a" or opt=="--avgdomains":
            wavg=int(arg)
        elif opt=="--epi_colors":
            cdraw=0
        elif opt=="--chr_colors":
            cdraw=1
        elif opt=="--gyration_tensor":
            gyration_tensor=True
        elif opt=="--eframes":
            eframes=int(arg)
        elif opt=="-p" or opt=="--path_to":
            path_to=str(arg)
        elif opt=="-w" or opt=="--write_to":
            write_to=str(arg)
        elif opt=="-x":
            x_camera=float(arg)
        elif opt=="-y":
            y_camera=float(arg)
        elif opt=="-z":
            z_camera=float(arg)
        elif opt=="--e1":
            e1_camera=float(arg)
        elif opt=="--e2":
            e2_camera=float(arg)
        elif opt=="--e3":
            e3_camera=float(arg)
        elif opt=="--only":
            only=arg
        elif opt=="--zoom":
            zoom=arg
        elif opt=="--align":
            align=arg
        elif opt=="--nvertices":
            nvertices=int(arg)
        elif opt=="--resX":
            resX=int(arg)
        elif opt=="--resY":
            resY=int(arg)
        elif opt=="--rainbow":
            draw_with_rainbow_colors=True
        elif opt=="--com":
            bcom=True
        elif opt=="--clock":
            bclock=True
        elif opt=="--slice":
            bslice=True
        elif opt=="--save":
            save_blend=True
        elif opt=="--make_render":
            make_render=True
        elif opt=="--make_movie":
            make_movie=True
        else:
            pass

    # names
    sname=path_to+"/"+sname
    nom_visualisation=path_to+"/"+nom_visualisation

    clean_initial_scene()
    create_scene_world()
    create_persp_camera("camera",x_camera,y_camera,z_camera,e1_camera,e2_camera,e3_camera)
    create_sun_lamp("lamp",x_camera,y_camera,z_camera)
    create_render(make_movie,resX,resY)
    titre=simple_materials()
    load_materials()
    # show only color from the value of 'only'
    transparency_on(only,titre)

    # read the colors and geometries
    n_corps,radius,length,i_color,ichr,gtype,colors=read_details(sname,cdraw,titre)

    if draw_with_rainbow_colors:
        i_color=np.arange(0,n_corps,1)
        titre=rainbow_materials(n_corps,False)
        colors=["mat_rainbow"+str(c) for c in i_color]

    # read positions and orientations
    for r in range(2):
        if r==1:
            nframes=monomers//n_corps
            positions=np.zeros((nframes*n_corps,3),dtype=float)
            us=np.zeros((nframes*n_corps,3))
            vs=np.zeros((nframes*n_corps,3))
            ts=np.zeros((nframes*n_corps,3))
        with open(nom_visualisation,'rb') as fichierVisualisation:
            monomers=0
            b=fichierVisualisation.read(8)
            while b!=b"":
                # (x,y,z)
                x=(struct.unpack('d',b))[0]
                b=fichierVisualisation.read(8)
                y=(struct.unpack('d',b))[0]
                b=fichierVisualisation.read(8)
                z=(struct.unpack('d',b))[0]
                if r==1:
                    positions[monomers,:3]=x,y,z
                # # (ux,uy,uz)
                # b=fichierVisualisation.read(8)
                # ux=(struct.unpack('d',b))[0]
                # b=fichierVisualisation.read(8)
                # uy=(struct.unpack('d',b))[0]
                # b=fichierVisualisation.read(8)
                # uz=(struct.unpack('d',b))[0]
                # u=ux,uy,uz
                # # (vx,vy,vz)
                # b=fichierVisualisation.read(8)
                # vx=(struct.unpack('d',b))[0]
                # b=fichierVisualisation.read(8)
                # vy=(struct.unpack('d',b))[0]
                # b=fichierVisualisation.read(8)
                # vz=(struct.unpack('d',b))[0]
                # v=vx,vy,vz
                # (tx,ty,tz)
                b=fichierVisualisation.read(8)
                tx=(struct.unpack('d',b))[0]
                b=fichierVisualisation.read(8)
                ty=(struct.unpack('d',b))[0]
                b=fichierVisualisation.read(8)
                tz=(struct.unpack('d',b))[0]
                inv_norm=1.0/np.sqrt(tx**2+tz**2)
                ux,uy,uz=-tz*inv_norm,0.0,tx*inv_norm
                if r==1:
                    us[monomers,:3]=ux,uy,uz
                    vs[monomers,:3]=ty*uz-tz*uy,-tx*uz+tz*ux,tx*uy-ty*ux
                    ts[monomers,:3]=tx,ty,tz
                b=fichierVisualisation.read(8)# 8 bytes suivants
                monomers+=1

    # zoom in chain ?
    positions=zoom_in(positions,ichr,i_color,colors,titre,zoom)
    # align ?
    positions,us,vs,ts=align_chain(positions,ichr,us,vs,ts,align)
    # ???
    if bclock:
        for i,a in enumerate(np.array([0,4,1,5,3,7,2,6],dtype='int64')):
            part=np.array([j for j,c in enumerate(ichr) if c==a],dtype='int64')
            com=np.multiply(1.0/part.size,np.sum(positions[part,:3],axis=0))
            #
            binormal=np.array([1.0,0.0,0.0],dtype=np.double)
            if i<6:
                angle=i*2.0*np.pi/7.0
            elif i==6:
                angle=5.5*2.0*np.pi/7.0
            elif i==7:
                angle=6.0*2.0*np.pi/7.0
            else:
                pass
            #
            axe=np.array([0.0,-np.sin(angle),np.cos(angle)],dtype=np.double)
            for j in part:
                us[j,:3]=rotation(binormal,angle,us[j,:3])
                vs[j,:3]=rotation(binormal,angle,vs[j,:3])
                ts[j,:3]=rotation(binormal,angle,ts[j,:3])
                positions[j,:3]=np.add(com,rotation(binormal,angle,np.subtract(positions[j,:3],com)))
                positions[j,:3]=np.add(positions[j,:3],np.multiply(30.0,axe))
    else:
        # if align or zoom ?
        if zoom!="" or align!="":
            positions=behind_camera(positions,ichr,zoom,align)
        # slice ?
        if bslice:
            P,D=positions.shape
            com=np.multiply(1.0/P,np.sum(positions,axis=0))
            positions=np.subtract(positions,com)
            for i,p in enumerate(positions):
                if abs(p[0])>0.5:
                    # does not consider the direction towards camera point to
                    # i need to correct it
                    positions[i,:3]=10.0*x_camera,10.0*y_camera,10.0*z_camera

    # get blender matrix from u,v,t frames
    matrices=[mathutils.Matrix()]*(nframes*n_corps)
    uvts=np.hstack((np.hstack((us,vs)),ts))
    for i in range(nframes*n_corps):
        uvt=uvts[i,:9]
        matrices[i]=mathutils.Matrix(((uvt[0],uvt[1],uvt[2]),(uvt[3],uvt[4],uvt[5]),(uvt[6],uvt[7],uvt[8])))

    # average along the contour length ?
    if wavg>1:
        positions,ichr,i_color,radius,length,gtype,matrices=average_along_the_contour_length(positions,ichr,wavg)

    # align gyration tensor along the world frame ?
    if gyration_tensor:
        positions,matrices=align_gyration_tensor(positions,matrices)

    # com to 0,0,0
    if bcom:
        if zoom=="":
            P,D=positions.shape
            com=np.multiply(1.0/P,np.sum(positions,axis=0))
            positions=np.subtract(positions,com)
        else:
            part=np.array([i for i,c in enumerate(ichr) if c==int(zoom)],dtype='int64')
            com=np.multiply(1.0/part.size,np.sum(positions[part,:3],axis=0))
            positions[part,:3]=np.subtract(positions[part,:3],com)

    # number of frames
    nframes=int(len(positions)/n_corps)
    print(str(len(positions))+' position(s)')
    print(str(nframes)+' frame(s)')
    print(str(n_corps)+' object(s)')
    bpy.context.scene.frame_start=0
    bpy.context.scene.frame_end=max(0,nframes//eframes-1)

    # create fcurves
    print("create f-curves")
    geometries=['cylinder0','uvsphere0','icosphere0','cube0']
    for g in geometries:
        if g=='cylinder0':
            bpy.ops.mesh.primitive_cylinder_add(vertices=nvertices,radius=1,depth=1,end_fill_type=['NOTHING','NGON'][1])
        elif g=='uvsphere0':
            bpy.ops.mesh.primitive_uv_sphere_add(segments=nvertices,ring_count=nvertices,size=1)
        elif g=='icosphere0':
            bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=nvertices,size=1)
        elif g=='cube0':
            bpy.ops.mesh.primitive_cube_add(radius=1)
        else:
            sys.exit()
        bpy.context.object.name=g
        bpy.context.object.location=0.,0.,0.
        bpy.context.object.rotation_euler=0.,0.,0.
        bpy.context.object.animation_data_create()
        bpy.context.object.animation_data.action=bpy.data.actions.new(name='myAction_'+g)
        for i in range(3):
            bpy.context.object.animation_data.action.fcurves.new(data_path="location",index=i)
        for i in range(3):
            bpy.context.object.animation_data.action.fcurves.new(data_path="rotation_euler",index=i)
        bpy.context.object.animation_data.action.fcurves.new(data_path="color_"+g,index=-1)
        for f in bpy.data.objects[g].animation_data.action.fcurves:
            f.keyframe_points.add(nframes)
        bpy.data.objects[g].select=False

    ## low-level api
    print("low level api")
    meshes=[]
    for f in np.arange(0,nframes,eframes):
        i_vertices=0
        vertices=[]
        edges=[]
        polygons=[]
        fcolors=[]
        print("loading vertices frame "+str(f)+"/"+str(nframes))
        # loop over the objects
        for c in range(n_corps):
            if (gtype[c]==1 or gtype[c]==2) and wavg>0:
                sx,sy,sz=wRg0[c],wRg1[c],wRg2[c]
            else:
                sx,sy,sz=radius[c],radius[c],int(gtype[c]==0)*length[c]+int(gtype[c]>0)*radius[c]
            objet=bpy.data.objects[geometries[gtype[c]]].data
            debut=time.time()
            if choix=='ensemble':
                offset=0.0,0.0,0.0
            elif choix=='splitted_epi':
                if colors[c]=='noir':
                    offset=R,0.0,R
                elif colors[c]=='rouge':
                    offset=R,0.0,-R
                elif colors[c]=='vert':
                    offset=-R,0.0,R
                elif colors[c]=='bleu':
                    offset=-R,0.0,-R
                else:
                    offset=100.0*R,100.0*R,100.0*R
            elif choix=='splitted_chr':
                # x*c*c+y*c+z
                c=2
                z=ichr[c]%2
                y=((ichr[c]-z)//c)%c
                x=(ichr[c]-y*c-z)//(c*c)
                offset=-R+2*x*R,-R+2*y*R,-R+2*z*R
            elif 'extrait' in choix and choix.replace("extrait_","")==colors[c].replace("mat_",""):
                offset=0.7*R,0.0,0.0
            else:
                offset=0.0,0.0,0.0
            uvt=(matrices[f*n_corps+c]).transposed()
            pos=np.add(positions[f*n_corps+c,:3],np.array([offset[0],offset[1],offset[2]]))
            for e in objet.edges:
                edge=[i_vertices+i for i in e.vertices]
                edges.append(edge)
            for p in objet.polygons:
                P=p.loop_total
                polygon=[i_vertices+p.vertices[i] for i in range(P)]
                polygons.append(polygon)
                fcolors.append(i_color[c])
            for v in objet.vertices:
                r=uvt*(mathutils.Matrix(((sx,0,0),(0,sy,0),(0,0,sz)))*v.co)
                vertices.append((r.x+pos[0],r.y+pos[1],r.z+pos[2]))
                i_vertices+=1
            if(c%1000==0):
                print(c,n_corps,time.time()-debut,nframes,len(bpy.context.scene.objects))
        # current frame
        bpy.context.scene.frame_current=f//eframes
        # from mesh to object
        meshes.append(bpy.data.meshes.new('test'+str(f)))
        meshes[f//eframes].from_pydata(vertices,[],polygons)
        bpy.data.objects.new('test'+str(f),meshes[f//eframes])
        bpy.data.objects['test'+str(f)].data=meshes[f//eframes]
        bpy.data.objects['test'+str(f)].data.show_normal_face=True
        bpy.data.objects['test'+str(f)].data.show_normal_loop=True
        for t in titre:
            bpy.data.objects['test'+str(f)].data.materials.append(bpy.data.materials[t])
        M=len(meshes[f//eframes].polygons)
        for m in range(M):
            (bpy.data.objects['test'+str(f)].data.polygons)[m].material_index=fcolors[m]
        meshes[f//eframes].update(calc_edges=True,calc_tessface=True)
        # link object to the scene
        bpy.context.scene.objects.link(bpy.data.objects['test'+str(f)])
        # hide geometries
        for g in geometries:
            bpy.data.objects[g].hide=True
            bpy.data.objects[g].hide_render=True
            bpy.data.objects[g].keyframe_insert('hide',frame=f//eframes)
            bpy.data.objects[g].keyframe_insert('hide_render',frame=f//eframes)
        # hide frames
        if nframes>1:
            bpy.data.objects['test'+str(f)].hide=False
            bpy.data.objects['test'+str(f)].hide_render=False
            bpy.data.objects['test'+str(f)].keyframe_insert('hide',frame=f//eframes)
            bpy.data.objects['test'+str(f)].keyframe_insert('hide_render',frame=f//eframes)
            # hide
            bpy.data.objects['test'+str(f)].hide=True
            bpy.data.objects['test'+str(f)].hide_render=True
            # hide to previous and next frames
            for h in np.arange(0,nframes,eframes):
                if h!=f:
                    bpy.data.objects['test'+str(f)].keyframe_insert('hide',frame=h//eframes)
                    bpy.data.objects['test'+str(f)].keyframe_insert('hide_render',frame=h//eframes)
        if make_render and not make_movie:
            bpy.context.scene.render.filepath=write_to+'/frame'+str(f//eframes)
            bpy.ops.render.render(animation=False,write_still=True)

    for g in geometries:
        bpy.data.objects.remove(bpy.data.objects[g],True)

    if False:
        add_plane(0.0,0.0,-R,"plan1")
        add_plane(0.0,R,0.0,"plan2")

    if False:
        add_nucleus(0.0,0.0,0.0,2000.0/np.sqrt(3.0*10000.0))

    bpy.context.scene.update()
    # bpy.context.scene.cycles.device='GPU'
    try:
        bpy.context.user_preferences.system.compute_device_type='CUDA'
    except:
        print('CUDA does not found')

    if make_movie:
        bpy.context.scene.render.filepath=write_to+'/movie'
        # bpy.ops.render.OpenGL(animation=True)
        bpy.ops.render.render(animation=True,write_still=True)
    if save_blend:
        bpy.ops.wm.save_as_mainfile(filepath=write_to+'/save.blend')
    print("render finished.")
    # print(dir(bpy.context.user_preferences.system))
    # for b in bpy.data.objects:
    #     print(b.name)

if __name__=="__main__":
    print('\nplatform: %s\n'%sys.platform)
    main(sys.argv[(sys.argv.index("--")+1):])
