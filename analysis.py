#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys,getopt
import struct
import numpy as np
from numpy import linalg as LA
import numba

import data_analysis as da

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    # default parameters
    nm_10_5bp=3.5721
    cutoff=11.0/nm_10_5bp
    kbp=1000
    Mbp=1000000
    nucleosomes=100
    linker_bp=53
    nucleosome_dna=147
    tw=23046321
    E1=23044000+400#23044000+(400+1400)//2
    enhancers=[23049440,23053901,23120077,22865023,23097536,22971775,23084945,21381841,22996502]
    types=["wt","m1","m2","m3","m4","m5","m6","m7","m8"]
    de=23050530-23049440
    xinf=23035000
    xsup=xinf+(nucleosome_dna+linker_bp)*nucleosomes
    # get arguments
    try:
        opts,args=getopt.getopt(argv,"hN:s:S:",["nucleosomes="])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print('analysis.py -N <nucleosomes>')
            print('            -s <seed to start from>')
            print('            -S <seed to finisih with>')
            sys.exit()
        elif opt=="-N" or opt=="--nucleosomes":
            nucleosomes=int(arg)
        elif opt=="-s":
            seed0=int(arg)
        elif opt=="-S":
            seed1=int(arg)
        else:
            pass

    # do we swap the seed ?
    if seed0>seed1:
        seed0,seed1=seed1,seed0

    # read the data file
    with open("/scratch/pcarriva/resultats/Yad/twi_A.bedGraph","r") as in_file:
        lines=in_file.readlines()
        L=len(lines)
        arm=[""]*L
        start=np.zeros(L)
        mid=np.zeros(L)
        end=np.zeros(L)
        amplitude=np.zeros(L)
        border_chr=[]
        cum=0
        for i,l in enumerate(lines):
            sl=l.split()
            # print(sl)
            arm[i]=sl[0]
            if i>0:
                if arm[i-1]!=arm[i]:
                    border_chr.append(end[i-1])
                    cum=end[i-1]
            start[i]=cum+int(sl[1])
            end[i]=cum+int(sl[2])
            mid[i]=0.5*(start[i]+end[i])
            amplitude[i]=float(sl[3])

    # variables : msd
    msd_w={}
    # loop over the files
    first_call=True
    c4C=np.zeros(int(320*Mbp/10.5))
    step=10000
    seeds=(seed1-seed0)+1
    step0=100000
    step1=200000
    T=(step1-step0)//step+1
    Rg2t=np.zeros((T,seeds))
    for n in range(seed0,seed1+1,1):
        print("simulation "+str(n))
        for s in range(step0,step1+step,step):
            # number of monomers
            visualisation="visualisation/vn"+str(n)+"_N"+str(nucleosomes)+"_F0.00_T0.0_D0_s"+str(s)+".out"
            with open(visualisation,'rb') as in_file:
                monomers=0
                b=in_file.read(8)
                while b!=b"":
                    x,y,z,tx,ty,tz,b=get_xyz_t(b,in_file)
                    monomers+=1
            # read positions and orientations
            positions=np.zeros((monomers,3),dtype=float)
            us=np.zeros((monomers,3),dtype=float)
            vs=np.zeros((monomers,3),dtype=float)
            ts=np.zeros((monomers,3),dtype=float)
            with open(visualisation,'rb') as in_file:
                monomers=0
                b=in_file.read(8)
                while b!=b"":
                    x,y,z,tx,ty,tz,b=get_xyz_t(b,in_file)
                    positions[monomers,:3]=x,y,z
                    ts[monomers,:3]=tx,ty,tz
                    us[monomers,:3]=-tz/np.sqrt(tx**2+tz**2),0.0,tx/np.sqrt(tx**2+tz**2)
                    vs[monomers]=np.cross(ts[monomers],us[monomers])
                    monomers+=1
            positions=np.multiply(nm_10_5bp,positions)
            # 4C
            if first_call:
                c4C=np.zeros(monomers)
                first_call=False
            dna=monomers-4*nucleosomes
            # c4C=np.add(c4C,da.contacts_4C(positions,int((tw-xinf)/10.5),cutoff))
            c4C=np.add(c4C,da.contacts_4C(positions,int((E1-xinf)/10.5),cutoff))
            # R_g^2
            Rg2,asphericity,acylindricity,kappa2,P=da.Rg2_and_shape(positions,0,monomers-1,True)
            Rg2t[(s-step0)//step,n-1]=Rg2
            # msd
            if n==1:
                msd_w=da.msd(positions,dna//2,1,s,msd_w)

    # normalize 4C
    c4C=np.multiply(1.0/np.sum(c4C),c4C)
    # draw 4C
    name="data_v_simulation.png"
    arm0="2R"
    # plot only one arm from Yad's data
    xmin=np.min(np.array([start[i] for i,a in enumerate(arm) if a==arm0]))
    xmax=np.max(np.array([end[i] for i,a in enumerate(arm) if a==arm0]))
    # zoom in tw region
    shift_start=np.array([start[i]-xmin for i,a in enumerate(arm) if a==arm0 and (start[i]-xmin)>=xinf and (end[i]-xmin)<=xsup])
    shift_end=np.array([end[i]-xmin for i,a in enumerate(arm) if a==arm0 and (start[i]-xmin)>=xinf and (end[i]-xmin)<=xsup])
    shift_amplitude=np.array([amplitude[i] for i,a in enumerate(arm) if a==arm0 and (start[i]-xmin)>=xinf and (end[i]-xmin)<=xsup])
    shift_amplitude=np.multiply(1.0/np.sum(shift_amplitude),shift_amplitude)
    # data v. simulation
    ymin=0
    ymax=0.05
    sleft=np.arange(0,dna*10.5,10.5)
    fig=plt.figure(name.replace(".png",""))
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="genomic position (kbp)",ylabel="amplitude",title=arm0+" from "+str(xinf)+" to "+str(xsup),xlim=(xinf/kbp,xsup/kbp),ylim=(ymin,ymax))
    ax.locator_params(nbins=6)
    # data
    for i,s in enumerate(shift_start):
        ax.add_patch(patches.Rectangle((s/kbp,0.0),width=(shift_end[i]-s)/kbp,height=shift_amplitude[i],angle=0.0,fill=True,color="red",alpha=0.75))
    # simulation
    for i,s in enumerate(sleft):
        ax.add_patch(patches.Rectangle(((xinf+s)/kbp,0.0),width=10.5/kbp,height=c4C[i],angle=0.0,fill=True,color="blue",alpha=0.75))
    fig.savefig(name,dpi=600)
    fig.clf()
    plt.close(name.replace(".png",""))

    # plot R_g^2
    fig=plt.figure("R_g^2")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel="iterations",ylabel=r'$R_g$',title="",xlim=(step0,step1),ylim=(0.0,200.0))
    print(np.sqrt(Rg2t[:,0]))
    for s in np.arange(seed0,seed1,1):
        ax.plot(np.arange(step0,step1+step,step),np.sqrt(Rg2t[:,s-1]))
    fig.savefig("Rgt.png",dpi=600)
    fig.clf()
    plt.close("R_g^2")

    # plot msd
    da.draw_msd(msd_w,True,"mapping/hajjoul_msd_GR2013.in","/scratch/pcarriva/simulations/fibre_ODE/msd_w1")

if __name__=="__main__":
    main(sys.argv[1:])
