#!/bin/bash

if [[ ${1} == "compute" ]];
then
    compute="yes";
else
    compute="no";
fi;

S=20

CF=chromatin_nrl_marginal_density_seed1_2N_linker_ts25.json
/scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.1000000.2500 -i $CF --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;

CF=chromatin_nrl_marginal_density_seed1_2N_linker_ts25.json_breakshls6.5
/scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.1000000.2500 -i $CF --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;

CF=chromatin_nrl_marginal_density_seed1_2N_linker_ts25.json_rbreakshls6.5;
/scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.1000000.2500 -i $CF --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;

exit 0;

S=20

for c in chromatin_nrl189to189bp chromatin_nrl190to190bp chromatin_nrl191to191bp;
do
    /scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.4000000.5000 -i $c --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;
done;

for c in chromatin_nrl189to189bp_breakshls6.5 chromatin_nrl190to190bp_breakshls6.5 chromatin_nrl191to191bp_breakshls6.5;
do
    /scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.4000000.5000 -i $c --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;
done;

for c in chromatin_nrl189to189bp_rbreakshls6.5 chromatin_nrl190to190bp_rbreakshls6.5 chromatin_nrl191to191bp_rbreakshls6.5;
do
    /scratch/pcarriva/anaconda3/envs/my_py37/bin/python3.7 analysis_fibre.py -s 1 -S $S --range=0.4000000.5000 -i $c --compute_Wr=no --compute_msd=yes --path_to_data=/scratch/pcarriva/simulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simmulations/fibre_ODE/conformations --tmpdir=/scratch/pcarriva/simulations/fibre_ODE/plots --compute=${compute} --save_json;
done;
