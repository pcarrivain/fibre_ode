#include "functions.h"
#include "variables.h"
#include <assert.h>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <thread>
#include <time.h>
#include <vector>

using namespace std;

const int L = 6;
const int N = L * L * L;
dGeomID mur[6];
dVector3 a1, a2;
double x_ghost = 1.2;
double inv_kBT = 1. / (kB * temp / fE);
double r_s = lk / fL;
double r_c = (lk / 3.) / fL;
double l_c = 2. * lk / fL;
double pas = 1.5 * l_c;
double l_m = (L + 1) * pas;
double masse_s = kbp * m1bp / fM;
double masse_c = kbp * m1bp / fM;
double m_energie = 6. * N / (2. * inv_kBT);
int sampling = 1000, stot = 1000 * sampling, compteur;
double InitialPCTime, dt, Ke;
float f0, f1, f2, f3, f4, f5, f6;
char nom[50], nom_s[50], nom_g[50], nom_v[50];
vec3 mi_s, vt_s, wt_s, mi_c, vt_c, wt_c, ri, rj, rij, force;
FILE *fSPQ, *fVisualisation, *fStructure;

// extern variables
int *is_linker_nucleosome = new int[1]();
int *nucleosome_start = new int[1]();
double xmass = 1., xfriction = 1.;
bool same_mass = false;
// variables: monomers partition
int nbins = 1, lhit = 0, nperb = 10, nnperb = 10;
int *binn = NULL;
int *ninb = NULL;
int *bhit = NULL;
int *nhit = NULL;
int *lperb = NULL;
// variables: nucleosomes partition
int *nbinn = NULL;
int *nninb = NULL;
int *nbhit = NULL;
int *nnhit = NULL;
int *nlperb = NULL;

// main
int main(int argc, char **argv) {
  // nearCallback function
  void (*my_nearCallback)(void *, dGeomID, dGeomID);
  my_nearCallback = &nearCallback;
  // parameters
  int seed = 1;
  if (argc > 1)
    Ke = atof(argv[1]) * kB * temp / fE;

  // ode-0.7
  dInitODE();
  InitialPCTime = clock();

  // pRNG
  std::mt19937_64 e1(2 * seed), e2(2 * seed + 1);

  // world and space
  dWorldID WorldODE = create_world_ODE(.0, wERP, wCFM, nSOR, wSOR);
  dSpaceID SpaceODE = hash_space(r_c, 1.01 * (l_c + 2. * r_c));
  dSpaceID gSpaceODE = ghost_hash_space(1.01 * (l_c + 2. * r_c), x_ghost);
  dJointGroupID contactgroup = dJointGroupCreate(0);

  // bodies
  dBodyID *corps = new dBodyID[N];
  dGeomID *gcorps = new dGeomID[N];
  dGeomID *ggcorps = new dGeomID[N];
  for (int i = 0; i < N; i += 2) {
    corps[i] = b_sphere(WorldODE, SpaceODE, gcorps[i], r_s, masse_s);
    ggcorps[i] = create_ghost_geometry(gSpaceODE, corps[i], "sphere", "sphere", x_ghost,
                                       r_s, r_s, r_s);
    corps[i + 1] =
        b_capsule(WorldODE, SpaceODE, gcorps[i + 1], r_c, l_c, masse_c, 1);
    ggcorps[i + 1] = create_ghost_geometry(gSpaceODE, corps[i + 1], "capsule", "capsule",
                                           x_ghost, r_c, r_c, l_c);
  }
  // walls
  for (int i = 0; i < 6; i++)
    mur[i] = dCreateBox(SpaceODE, l_m, l_m, l_m);
  dGeomSetPosition(mur[0], l_m, 0., 0.);
  dGeomSetPosition(mur[1], -l_m, 0., 0.);
  dGeomSetPosition(mur[2], 0., l_m, 0.);
  dGeomSetPosition(mur[3], 0., -l_m, 0.);
  dGeomSetPosition(mur[4], 0., 0., l_m);
  dGeomSetPosition(mur[5], 0., 0., -l_m);
  // positions
  compteur = 0;
  for (int i = 0; i < (L / 2); i++) {
    for (int j = 0; j < L; j++) {
      for (int k = 0; k < L; k++) {
        dBodySetPosition(corps[compteur], -.5 * l_m + (i + 1) * pas,
                         -.5 * l_m + (j + 1) * pas, -.5 * l_m + (k + 1) * pas);
        compteur += 2;
      }
    }
  }
  compteur = 1;
  for (int i = (L / 2); i < L; i++) {
    for (int j = 0; j < L; j++) {
      for (int k = 0; k < L; k++) {
        dBodySetPosition(corps[compteur], -.5 * l_m + (i + 1) * pas,
                         -.5 * l_m + (j + 1) * pas, -.5 * l_m + (k + 1) * pas);
        compteur += 2;
      }
    }
  }

  double *friction = new double[N]();
  double *sigmaT = new double[N]();
  double *sigmaR = new double[N]();
  for (int n = 0; n < N; n++) {
    if ((n % 2) == 0) {
      friction[n] = 6. * pi * (viscosity / (fM / (fL * fT))) * r_s;
      sigmaT[n] = friction[n] / masse_s;
      sigmaR[n] = sigmaT[n];
    } else {
      friction[n] = 6. * pi * (viscosity / (fM / (fL * fT))) * (.5 * l_c);
      sigmaT[n] = friction[n] / masse_c;
      sigmaR[n] = sigmaT[n];
    }
    dt = (n == 0) ? .01 / sigmaT[n] : fmin(dt, .01 / sigmaT[n]);
  }
  mi_s = inertia(corps[0]);
  vt_s = mult3(1. / sqrt(masse_s * inv_kBT), vec3(1., 1., 1.));
  wt_s = vec3(1. / sqrt(mi_s.x * inv_kBT), 1. / sqrt(mi_s.y * inv_kBT),
              1. / sqrt(mi_s.z * inv_kBT));
  mi_c = inertia(corps[1]);
  vt_c = mult3(1. / sqrt(masse_c * inv_kBT), vec3(1., 1., 1.));
  wt_c = vec3(1. / sqrt(mi_c.x * inv_kBT), 1. / sqrt(mi_c.y * inv_kBT),
              1. / sqrt(mi_c.z * inv_kBT));

  printf("%s\n", dGetConfiguration());
  printf("geoms:%i\n", dSpaceGetNumGeoms(SpaceODE));
  printf("erp:%.1f cfm:%.1e\n", wERP, wCFM);

  // structure
  sprintf(nom, "mesures/structureC%i_e%i.out", N, (int)Ke);
  fStructure = fopen(nom, "w");
  fprintf(fStructure, "%i %f\n", L, pas);
  for (int i = 0; i < N; i++) {
    fprintf(fStructure, "%f %i\n", r_s, i);
    fprintf(fStructure, "%f %f %i\n", r_c, l_c, i);
  }
  fclose(fStructure);

  // initial conformation ?
  sprintf(nom_s, "mesures/sC%i_e%i.out", N, (int)Ke);
  sprintf(nom_g, "mesures/gC%i_e%i.out", N, (int)Ke);
  sprintf(nom_v, "mesures/vC%i_e%i.out", N, (int)Ke);
  sprintf(nom, "mesures/sC%i_e%i.out", N, (int)Ke);
  fSPQ = fopen(nom, "r");
  if (fSPQ != NULL) {
    printf("Configuration initiale:%s\n", nom);
    for (int i = 0; i < N; i++) {
      fscanf(fSPQ, "%f %f %f", &f0, &f1, &f2);
      fscanf(fSPQ, "%f %f %f %f", &f3, &f4, &f5, &f6);
      set_pos_quat(corps[i], vec3(f0, f1, f2), f3, f4, f5, f6);
      fscanf(fSPQ, "%f %f %f", &f0, &f1, &f2);
      fscanf(fSPQ, "%f %f %f", &f3, &f4, &f5);
      set_velocities(corps[i], f0, f1, f2, f3, f4, f5);
    }
  }

  // simulation loop
  for (int iseq = 0; iseq <= stot; iseq++) {

    // langevin-euler
    std::thread first(langevin_euler, corps, sigmaT, sigmaR, dt, 1., 0,
                      (N - N % 2) / 2, std::ref(e1));
    std::thread second(langevin_euler, corps, sigmaT, sigmaR, dt, 1.,
                       (N - N % 2) / 2 + 1, N, std::ref(e2));
    first.join();
    second.join();
    // step
    dSpaceCollide(SpaceODE, 0, my_nearCallback);
    dWorldQuickStep(WorldODE, dt);
    if (contactgroup)
      dJointGroupEmpty(contactgroup);

    // interactions
    for (int i = 0; i < N; i += 2) {
      for (int j = (i + 1); j < N; j += 2) {
        for (int c = -1; c < 2; c += 2) {
          rj = PositionPosRelBodyODE(corps[j], 0., 0., c * .5 * l_c);
          //---Z
          for (int z = -1; z < 2; z += 2) {
            ri = PositionPosRelBodyODE(corps[i], 0., 0., z * r_s);
            rij = sub3(rj, ri);
            if (norm3(rij) < (2. * r_c)) {
              force = mult3(Ke, normalize3(rij));
              dBodyAddForceAtRelPos(corps[i], force.x, force.y, force.z, 0., 0.,
                                    z * r_s);
              dBodyAddForceAtRelPos(corps[j], -force.x, -force.y, -force.z, 0.,
                                    0., c * .5 * l_c);
            }
          }
          //---Y
          for (int y = -1; y < 2; y += 2) {
            ri = PositionPosRelBodyODE(corps[i], 0., y * r_s, 0.);
            rij = sub3(rj, ri);
            if (norm3(rij) < (2. * r_c)) {
              force = mult3(Ke, normalize3(rij));
              dBodyAddForceAtRelPos(corps[i], force.x, force.y, force.z, 0.,
                                    y * r_s, 0.);
              dBodyAddForceAtRelPos(corps[j], -force.x, -force.y, -force.z, 0.,
                                    0., c * .5 * l_c);
            }
          }
          //---X
          for (int x = -1; x < 2; x += 2) {
            ri = PositionPosRelBodyODE(corps[i], x * r_s, 0., 0.);
            rij = sub3(rj, ri);
            if (norm3(rij) < (2. * r_c)) {
              force = mult3(Ke, normalize3(rij));
              dBodyAddForceAtRelPos(corps[i], force.x, force.y, force.z,
                                    x * r_s, 0., 0.);
              dBodyAddForceAtRelPos(corps[j], -force.x, -force.y, -force.z, 0.,
                                    0., c * .5 * l_c);
            }
          }
        }
      }
    }

    // write data
    if (iseq % sampling == 0 && iseq != 0) {
      // save the conformation
      read_write_conformation(corps, nom_s, "write", 0, N);
      // save for visualisation
      write_visualisation(corps, nom_v, 0, N, "a");
    }

    // print
    if (iseq % (stot / 10) == 0)
      printf("%.3f E/<E>=%f\n", (double)iseq / (double)stot,
             Ktr(corps, 0, N, false, 2));
  }

  for (int i = 0; i < N; i++) {
    dBodyDestroy(corps[i]);
    dGeomDestroy(gcorps[i]);
  }
  if (contactgroup)
    dJointGroupDestroy(contactgroup);
  dWorldDestroy(WorldODE);
  dSpaceDestroy(SpaceODE);
  dSpaceDestroy(gSpaceODE);
  dCloseODE();
  delete[] corps;
  delete[] gcorps;
  delete[] ggcorps;
  delete[] friction;
  delete[] sigmaT;
  delete[] sigmaR;
  printf("PC time=%f minutes\n",
         (clock() - InitialPCTime) / CLOCKS_PER_SEC / 60.);
  return 0;
}
