/*********************************************************************************
 *                                                                               *
 * This file includes some functions that aim to speed-up computation time *
 * when using Open-Dynamics-Engine software to solve articulated system. * This
 *code has been developped by Pascal Carrivain (p.carrivain_at_gmail.com). * It
 *is distributed under MIT licence.                                          *
 *                                                                               *
 ********************************************************************************/

#ifndef VARIABLES_H
#define VARIABLES_H
#include <cmath>
#include <ode/ode.h>
#include <ode/odecpp.h>

const int NNC = 14;   // number of 10.5 bp per nucleosome
const int NHC = 4;    // number of bodies per octamer (ODE)
const int NHC_MC = 1; // one object for octamer (Monte-Carlo)
const int nSHLs = 14; // number of SHLs per octamer
const int nDOCKs = 2; // number of docking domains per octamer
const int contact_array_size = 16;
const double pi = acos(-1.0);
const double pN = pow(10., -12.);
const double nm = pow(10., -9.);
const double m1bp = 700. * 1.66 * pow(10., -27.);   // kg
const double kB = 1.38 * pow(10., -23.);            // kg.m^2.s^-2.K^-1
const double temp = 300.;                           // Kelvins
const double fM = 10.5 * m1bp;                      // kg
const double fL = 3.5721 * nm;                      // m
const double fT = sqrt(fM * fL * fL / (kB * temp)); // s
const double fF = fM * fL / (fT * fT);              // Newton
const double fE = fL * fF;                          // kg*m^2/s^2
const double yM = 1000. * m1bp;                     // kg
const double yL = 50. * nm;                         // m
const double viscosity = .83 * .001;            // 0.83 mPa.s (Pa=kg.m^-1.s^-2)
const double viscosity_Socol_2019 = 5.4 * .001; // 5.4 mPa.s (Pa=kg.m^-1.s^-2)
const double sign_phase = 1.;
const dReal wERP = .1;
const dReal wCFM = pow(10., -9.);
const dReal wSOR = 1.3;
const int nSOR = 30;
// const int kbp=420;// dna Kuhn length in bp
// const double lk=140.*nm;// dna Kuhn length in nm
// const int kbp=360;// dna Kuhn length in bp
// const double lk=120.*nm;// dna Kuhn length in nm
const int kbp = 300;         // dna Kuhn length in bp
const double lk = 100. * nm; // dna Kuhn length in nm
// const int kbp=240;// dna Kuhn length in bp
// const double lk=80.*nm;// dna Kuhn length in nm
// const int kbp=180;// dna Kuhn length in bp
// const double lk=60.*nm;// dna Kuhn length in nm

/*!
  \struct vec3
  definition of a vector struct x,y,z.
*/
struct vec3 {
  double x; /*!< x coordinate */
  double y; /*!< y coordinate */
  double z; /*!< z coordinate */
  vec3() {  /*! default constructor, 0 to each of the three coordinates*/
    x = 0.;
    y = 0.;
    z = 0.;
  };
  vec3(double x1, double y1, double z1) { /*! constructor, x=x1, y=y1 and z=z1*/
    x = x1;
    y = y1;
    z = z1;
  }
};

/*!
  \struct vec2
  Definition of a vector struct x,y.
 */
struct vec2 {
  double x; /*!< x coordinate */
  double y; /*!< y coordinate */
  vec2() {  /*! default constructor, 0 to each of the three coordinates*/
    x = 0.;
    y = 0.;
  };
  vec2(double x1, double y1) { /*! constructor, x=x1 and y=y1*/
    x = x1;
    y = y1;
  }
};

/*!
  \struct int3
  Definition of a vector struct i,j,k.
*/
struct int3 {
  int i, j, k;
  int3(){};
  int3(int i1, int j1, int k1) {
    i = i1;
    j = j1;
    k = k1;
  }
};

struct mat33 {
  vec3 c1;
  vec3 c2;
  vec3 c3;
  mat33() {
    c1 = vec3();
    c2 = vec3();
    c3 = vec3();
  };
  mat33(vec3 v1, vec3 v2, vec3 v3) {
    c1 = v1;
    c2 = v2;
    c3 = v3;
  }
};

/*!
  \struct int3p
  Definition of a vector struct i,j,k,*p used to build Verlet-list.
*/
struct int3p {
  int i, j, k, *p;
  int3p() {
    i = 0;
    j = 0;
    k = 0;
    p = NULL;
  };
  int3p(int i1, int j1, int k1) {
    i = i1;
    j = j1;
    k = k1;
    p = NULL;
  }
};

extern double xmass;
extern double xfriction;
extern bool same_mass;

// partition
extern int *is_linker_nucleosome;
extern int *nucleosome_start;
extern int nbins, lhit, nperb, nnperb;
extern int *binn, *nbinn;
extern int *ninb, *nninb;
extern int *bhit, *nbhit;
extern int *nhit, *nnhit;
extern int *lperb, *nlperb;
#endif
