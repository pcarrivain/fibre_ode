#include <time.h>
#include <getopt.h>
#include "functions.h"

using namespace std;

int seed=1;
double kBT=1.;
double bp_per_nm3=0.01;
int iterations=100;
int every=10;
double k_nm=100.;
double k_bp=300.;
double ES=.0;
bool bEV=true;
bool bEVh=true;
bool pbc=true;
vector<int> Pivot_move,MPivot,PPivot,ICrankshaft,SCrankshaft;
vector<bool> mPivot,pPivot,iCrankshaft,sCrankshaft;
vector<vec3> min12(3);
double ax,ay,az,cx,cy,cz,T0,TPartition=.0,TOverlaps=.0,ES_ci,ES_cs;
int fd,hd,nc,ih1,ih2,nh1,nh2,ic,i0,i1,i2,i3,i4,i5,is0,is1,I1,I2,I3,I4,nb,acc=0,iteration,new_nc[2],chromosome,retour;
int ix,iy,iz,sx,sy,sz,ii,jj,kk,AM,contacts;
double Rg2xyz[3],ComputeTime,EbtzRg2[2],theta,unif1,unif2,new_phi,ctheta,expm,d0,d1,d2,d3,d4;
char chromatin[100],vname[3000],nmsd[3000],title[1200],sortie[3000],entree[1200],path_to[1000],inputs[2000],inputs_name[1000],s0[100],s1[100];
vec3 t0,u0,v0,u1,v1,u2,v2,p0,p1,p2,t1,t2,cc,axe,bn,P0,P1,M0,M1;
vector<double> l,r,gb,gt,phi_e;
FILE *fOut,*fIn,*fConformation;
bool bstart,twist,b0,b1,bPivot,sens;

/*! @brief re-allocate and copy
  @param p array of int
  @param a old size
  @param b new size
*/
void int_reallocation_recopie(int *&p,int a,int b){
  int *buffer=new int[a]();
  for(int i=0;i<a;i++) buffer[i]=p[i];
  delete[] p;
  p=NULL;
  p=new int[b]();
  for(int i=0;i<a;i++) p[i]=buffer[i];
  delete[] buffer;
  buffer=NULL;
}

void inf_sup(const double *xi,const double *yi,const double *zi,int i1,int i2,double &xINF,double &yINF,double &zINF,double &xSUP,double &ySUP,double &zSUP){
  double ax,ay,az;
  for(int i=i1;i<i2;i++){
    ax=xi[i];
    ay=yi[i];
    az=zi[i];
    xINF=fmin(xINF,ax);
    yINF=fmin(yINF,ay);
    zINF=fmin(zINF,az);
    xSUP=fmax(xSUP,ax);
    ySUP=fmax(ySUP,ay);
    zSUP=fmax(zSUP,az);
  }
}

void compute_sinb(int i1,int i2,std::vector<int> &stob,std::vector<int> &sinb,std::vector<int> &lsinb,int ngperb,int nhitb,int Lsinb){
  int ii;
  for(int i=i1;i<i2;i++){
    ii=stob[i];
    if((ii*ngperb+lsinb[ii])>=Lsinb) exit(EXIT_FAILURE);// ???
    sinb[ii*ngperb+lsinb[ii]]=i;// increasing order
    if(ii>=nhitb) exit(EXIT_FAILURE);
    lsinb[ii]++;
  }
}

// main
int main(int argc,char **argv){
  ComputeTime=clock();
  int arg_i;
  const option long_opts[]={
			    {"path_to",required_argument,nullptr,'p'},
                            {"input_file",required_argument,nullptr,'i'},
                            {"seed",required_argument,nullptr,'s'},
                            {"help",optional_argument,nullptr,'h'}
  };
  while((arg_i=getopt_long(argc,argv,"p:i:s:h",long_opts,nullptr))!=-1){
    switch(arg_i){
    case 'p':
      retour=sprintf(path_to,"%s",optarg);
      break;
    case 'i':
      retour=sprintf(inputs_name,"%s",optarg);
      break;
    case 's':
      seed=(int)atof(optarg);
      break;
    case 'h':
    default:
      printf("fibre_MC, usage: ./executable -p <path_to> -i <inputs_name> -s <seed>\n");
      printf("        ./fibre_MC -p /scratch/fibre -i inputs_MC.in -s 1\n");
      printf("        command to create 16 chromatin fibers of 100 nucleosomes (147 bp) each with 21 bp between two nucleosomes.\n");
      printf("        the 21 bp is splitted in two segments of 10.5 bp each.\n");
      printf("        no broken shls:\n");
      printf("        (for c in {0..15}; do for n in {1..100}; do echo $c 147 0 0; for l in {1..2}; do echo $c 10.5 -1 -1; done; done; done) >> chromatin_MC.in\n");
      printf("        command to create 36 chromatin fibers of 100 nucleosomes (147 bp) each with 42 bp between two nucleosomes.\n");
      printf("        the 42 bp is splitted in four segments of 10.5 bp each.\n");
      printf("        no broken 6.5 shls:\n");
      printf("        (for c in {0..35}; do for n in {1..100}; do echo $c 147 0 0; for l in {1..4}; do echo $c 10.5 -1 -1; done; done; done) >> chromatin_MC.in\n\n");
      printf("        broken 6.5 shls:\n");
      printf("        (for c in {0..35}; do for n in {1..100}; do echo $c 147 1 1; for l in {1..4}; do echo $c 10.5 -1 -1; done; done; done) >> chromatin_MC.in\n\n");
      return 0;
      break;
    }
  }
  // reading the inputs file
  retour=sprintf(inputs,"%s/%s",path_to,inputs_name);
  fIn=fopen(inputs,"r");
  if(fIn!=NULL){
    while(!feof(fIn)){
      retour=fscanf(fIn,"%s %s\n",s0,s1);
      if(strcmp(s0,"in")==0) sprintf(chromatin,"%s",s1);
      if(strcmp(s0,"twist")==0) twist=(strcmp(s1,"yes")==0);
      if(strcmp(s0,"excluded_volume")==0) bEV=(strcmp(s1,"yes")==0);
      if(strcmp(s0,"excluded_volume_histones")==0) bEVh=(strcmp(s1,"yes")==0);
      if(strcmp(s0,"k_nm")==0) k_nm=atof(s1);// Kuhn in nm
      if(strcmp(s0,"k_bp")==0) k_bp=atof(s1);// Kuhn in bp
      if(strcmp(s0,"bp_per_nm3")==0) bp_per_nm3=atof(s1);// bp per nm^3
      if(strcmp(s0,"ES")==0) ES=atof(s1);// energy
      if(strcmp(s0,"pbc")==0) pbc=(strcmp(s1,"yes")==0);
      if(strcmp(s0,"iterations")==0) iterations=(int)atof(s1);// # of iterations / # of nucleosomes
      if(strcmp(s0,"every")==0) every=(int)atof(s1);// save every * # of nucleosomes
      if(strcmp(s0,"path_to")==0) retour=sprintf(path_to,"%s",s1);
    }
    fclose(fIn);
  }else{
    printf("no inputs file found in %s\n",path_to);
    retour=sprintf(inputs,"%s/error_inputs_file.out",path_to);
    fOut=fopen(inputs,"w");
    retour=fprintf(fOut,"no inputs file found in %s\n",path_to);
    fclose(fOut);
    return 0;
  }

  // pRNG 
  std::mt19937_64 e1(seed);
  std::uniform_real_distribution<> ud1(.0,1.);

  retour=sprintf(title,"%s_%s",chromatin,inputs_name);

  double fL=3.5721*nm;// length scale
  double lt=95.*nm/fL;// twisting persistence length
  double am=1./(2.*nm/fL);// inverse of range (Morse potential)

  printf("read nucleosome ...\n");
  vec3 nucl_pos[NNC];
  retour=sprintf(entree,"%s/nucleosome_in/MC/nposition.in",path_to);
  fIn=fopen(entree,"r");
  for(int i=0;i<NNC;i++){
    retour=fscanf(fIn,"%lf %lf %lf\n",&d0,&d1,&d2);
    nucl_pos[i]=vec3(d0,d1,d2);
  }
  fclose(fIn);
  vec3 nucl_rot[3*NNC];
  retour=sprintf(entree,"%s/nucleosome_in/MC/nrotation.in",path_to);
  fIn=fopen(entree,"r");
  for(int i=0;i<NNC;i++){
    for(int j=0;j<3;j++){
      retour=fscanf(fIn,"%lf %lf %lf\n",&d0,&d1,&d2);
      nucl_rot[3*i+j]=vec3(d0,d1,d2);
    }
  }
  fclose(fIn);
  vec3 hist_pos[4];
  retour=sprintf(entree,"%s/nucleosome_in/MC/hposition.in",path_to);
  fIn=fopen(entree,"r");
  for(int i=0;i<4;i++){
    retour=fscanf(fIn,"%lf %lf %lf\n",&d0,&d1,&d2);
    hist_pos[i]=vec3(d0,d1,d2);
  }
  fclose(fIn);
  vec3 hist_rot[12];
  retour=sprintf(entree,"%s/nucleosome_in/MC/hrotation.in",path_to);
  fIn=fopen(entree,"r");
  for(int i=0;i<4;i++){
    for(int j=0;j<3;j++){
      retour=fscanf(fIn,"%lf %lf %lf\n",&d0,&d1,&d2);
      hist_rot[3*i+j]=vec3(d0,d1,d2);
    }
  }
  fclose(fIn);
  vec3 SHLs_pos[nSHLs];
  int3 SHLs_corps[nSHLs];
  retour=sprintf(entree,"%s/nucleosome_in/MC/SHLs.in",path_to);
  fIn=fopen(entree,"r");
  for(int i=0;i<nSHLs;i++){
    retour=fscanf(fIn,"%lf %lf %lf %lf %lf\n",&d0,&d1,&d2,&d3,&d4);
    SHLs_pos[i]=vec3(d0,d1,d2);
    SHLs_corps[i]=int3((int)d3,(int)d4,0);
  }
  fclose(fIn);
  // scale length
  for(int i=0;i<NNC;i++) nucl_pos[i]=mult3(nm/fL,nucl_pos[i]);
  for(int i=0;i<4;i++) hist_pos[i]=mult3(nm/fL,hist_pos[i]);
  for(int i=0;i<nSHLs;i++) SHLs_pos[i]=mult3(nm/fL,SHLs_pos[i]);
  printf("read the structure of the chromatin from input file ...\n");
  int N=0,fc=0,hc=0,cumul_nucleosomes=0,NUCLEOSOMES=0;
  double Gbp=.0,linker=.0,mlinker=.0;
  float f0;
  // # of nucleosomes
  FILE *chromatin_in=fopen(chromatin,"r");
  while(!feof(chromatin_in)){
    retour=fscanf(chromatin_in,"%i %f %i %i\n",&ic,&f0,&i0,&i1);
    if(((int)f0)==147 && retour==4){
      mlinker=fmax(mlinker,linker);
      linker=.0;
      NUCLEOSOMES++;
      N+=14;
    }else{
      linker+=f0;
      N++;
    }
    Gbp+=f0;
  }
  fclose(chromatin_in);
  // chromation radius (at rest)
  double cdiameter=(2.*11.+(mlinker/10.5)*3.5721)*nm/fL;
  double Pi=acos(-1.);
  double R0=((pbc)?.5*cbrt(Gbp/bp_per_nm3):cbrt(Gbp/(4.*Pi*bp_per_nm3/3.)))*nm/fL;
  double R=R0,L=2.*R,xbox,ybox,zbox;
  // bottom left corner of the simulation box
  if(pbc){
    xbox=ybox=zbox=-R;
  }else{
    xbox=ybox=zbox=-R0;
  }
  double ERg2=3.*kBT/(R0*R0),Rg2[2];
  EbtzRg2[0]=EbtzRg2[1]=Rg2[0]=Rg2[1]=.0;
  // from 'i' to 'chr'
  int *from_i_to_chr=new int[N]();
  N=0;
  chromatin_in=fopen(chromatin,"r");
  while(!feof(chromatin_in)){
    retour=fscanf(chromatin_in,"%i %f %i %i\n",&ic,&f0,&i0,&i1);
    if(((int)f0)==147 && retour==4){
      for(int i=0;i<14;i++) from_i_to_chr[N+i]=ic;
      N+=14;
    }else{
      from_i_to_chr[N]=ic;
      N++;
    }
  }
  fclose(chromatin_in);
  // # of chromosomes
  int CHROMOSOMES=1;
  for(int i=1;i<N;i++){
    if(from_i_to_chr[i-1]!=from_i_to_chr[i]) CHROMOSOMES++;
  }
  int N2=NUCLEOSOMES*NUCLEOSOMES,N3=NUCLEOSOMES*N2;
  iterations*=NUCLEOSOMES;
  every*=NUCLEOSOMES;
  // debut/fin(capsule) chromosome 'c'
  int *cstart=new int[CHROMOSOMES]();
  int *cend=new int[CHROMOSOMES]();
  cstart[0]=0;
  for(int i=1;i<N;i++){
    if(from_i_to_chr[i-1]!=from_i_to_chr[i]){
      cstart[from_i_to_chr[i]]=i;
      cend[from_i_to_chr[i-1]]=i-1;
    }
  }
  cend[from_i_to_chr[N-1]]=N-1;
  // # nucleosomes per chromosomes
  int *nucleosome=new int[CHROMOSOMES]();
  chromatin_in=fopen(chromatin,"r");
  while(!feof(chromatin_in)){
    retour=fscanf(chromatin_in,"%i %f %i %i\n",&ic,&f0,&i0,&i1);
    if(((int)f0)==147 && retour==4) nucleosome[ic]++;
  }
  fclose(chromatin_in);
  // debut/fin(nucleosome) chromosome 'c'
  int *nucleosome0=new int[CHROMOSOMES]();
  int *nucleosome1=new int[CHROMOSOMES]();
  nucleosome0[0]=0;
  nucleosome1[0]=nucleosome[0]-1;
  for(int c=1;c<CHROMOSOMES;c++){
    nucleosome0[c]=nucleosome1[c-1]+1;
    nucleosome1[c]=nucleosome0[c]+nucleosome[c]-1;
  }
  // from nucleosome to chromosome
  int NH=N+NUCLEOSOMES*NHC_MC;
  int_reallocation_recopie(from_i_to_chr,N,NH);
  for(int c=0;c<CHROMOSOMES;c++){
    for(int i=nucleosome0[c];i<=nucleosome1[c];i++){
      for(int j=0;j<NHC_MC;j++) from_i_to_chr[N+i*NHC_MC+j]=c;
    }
  }
  // SHLs 6.5<5.5~2.5~1.5<4.5<3.5<0.5 to kBT
  vector<vec3> Morse_SHL(NUCLEOSOMES*nSHLs);
  for(int n=0;n<NUCLEOSOMES;n++){
    Morse_SHL[n*nSHLs+0].x=1.5;// +6.5
    Morse_SHL[n*nSHLs+1].x=1000.;// +5.5
    Morse_SHL[n*nSHLs+2].x=1000.;// +4.5
    Morse_SHL[n*nSHLs+3].x=1000.;// +3.5
    Morse_SHL[n*nSHLs+4].x=1000.;// +2.5
    Morse_SHL[n*nSHLs+5].x=1000.;// +1.5
    Morse_SHL[n*nSHLs+6].x=1000.;// +0.5
    for(int i=7;i<14;i++) Morse_SHL[n*nSHLs+i].x=Morse_SHL[n*nSHLs+14-i-1].x;
  }
  // variables
  vec3 *pi=new vec3[NH]();
  vec3 *ti=new vec3[NH]();
  vec3 *ui=new vec3[NH]();
  vec3 *vi=new vec3[NH]();
  vec3 *tf=new vec3[NH]();
  vec3 *uf=new vec3[NH]();
  vec3 *vf=new vec3[NH]();
  int *base=new int[NUCLEOSOMES]();
  vector<int3> interaction(NUCLEOSOMES*nSHLs);
  vector<vec3> proj_f(NUCLEOSOMES*nSHLs);
  vector<vec3> proj_h(NUCLEOSOMES*nSHLs);
  vector<vec3> proj_t(NUCLEOSOMES*NHC_MC);
  vector<vec3> proj_u(NUCLEOSOMES*NHC_MC);
  vector<vec3> proj_v(NUCLEOSOMES*NHC_MC);
  vector<vec3> proj_r(NUCLEOSOMES*NHC_MC);
  int *colors=new int[NH]();
  int CX=1;
  while((CX*CX)<CHROMOSOMES) CX++;
  printf("create the structure of the chromatin from input file ...\n");
  printf("chromatin chains are located on square lattice of step=%f nm\n",fmax(2.*(R0-cdiameter)/CX,1.25*cdiameter)*fL/nm);
  chromatin_in=fopen(chromatin,"r");
  while(!feof(chromatin_in)){
    retour=fscanf(chromatin_in,"%i %f %i %i\n",&ic,&f0,&is0,&is1);
    // position of the chromosome
    if(cstart[ic]==fc){
      jj=ic%CX;
      ii=ic/CX;
      p0=mult3(fmax(2.*(R0-cdiameter)/CX,1.25*cdiameter),vec3(ii,jj,0.));
      u0=vec3(0.,0.,-1.);
      v0=vec3(0.,1.,0.);
      t0=vec3(1.,0.,0.);
      bstart=true;
    }else{
      p0=pi[fc-1];
      u0=ui[fc-1];
      v0=vi[fc-1];
      t0=ti[fc-1];
      bstart=false;
    }
    if(((int)f0)==147){
      // nucleosome
      fd=fc;
      for(int a=0;a<NNC;a++){
	r.push_back(nm/fL);
	l.push_back(3.5721*nm/fL);
	ui[fc]=nucl_rot[3*a+0];
	vi[fc]=nucl_rot[3*a+1];
	ti[fc]=nucl_rot[3*a+2];
	pi[fc]=nucl_pos[a];
	Pivot_move.push_back(0);
	mPivot.push_back(false);
	pPivot.push_back(false);
	iCrankshaft.push_back(false);
	sCrankshaft.push_back(false);
	colors[fc]=(a<2 || a>=(NNC-2))?3:0;
	fc++;
      }
      // break SHLs ?
      for(int i=fd;i<(fd+is0);i++) mPivot[i]=true;
      for(int i=(fd+NNC-1);i>=(fd+NNC-1-is1);i--) mPivot[i]=true;
      for(int i=fd;i<=(fd+is0);i++) pPivot[i]=true;
      for(int i=(fd+NNC-1);i>(fd+NNC-1-is1);i--) pPivot[i]=true;
      for(int i=fd;i<=(fd+is0);i++) iCrankshaft[i]=true;
      for(int i=(fd+NNC-1);i>(fd+NNC-1-is1);i--) iCrankshaft[i]=true;
      for(int i=fd;i<(fd+is0);i++) sCrankshaft[i]=true;
      for(int i=(fd+NNC-1);i>=(fd+NNC-1-is1);i--) sCrankshaft[i]=true;
      // histones (average over the four cylinders)
      hd=hc;
      cc=hist_pos[0];
      for(int h=1;h<4;h++) cc=add3(cc,hist_pos[h]);
      pi[N+hc]=mult3(.25,cc);
      ui[N+hc]=vec3(0.,0.,-1.);
      vi[N+hc]=vec3(0.,1.,0.);
      ti[N+hc]=vec3(1.,0.,0.);
      colors[N+hc]=1;
      hc++;
      if(cumul_nucleosomes>=NUCLEOSOMES) exit(EXIT_FAILURE);
      base[cumul_nucleosomes]=fd+NNC/2-1;
      nc=base[cumul_nucleosomes];
      proj_u[hd]=vec3(dot_product(ui[N+hd],ui[nc]),dot_product(ui[N+hd],vi[nc]),dot_product(ui[N+hd],ti[nc]));
      proj_v[hd]=vec3(dot_product(vi[N+hd],ui[nc]),dot_product(vi[N+hd],vi[nc]),dot_product(vi[N+hd],ti[nc]));
      proj_t[hd]=vec3(dot_product(ti[N+hd],ui[nc]),dot_product(ti[N+hd],vi[nc]),dot_product(ti[N+hd],ti[nc]));
      axe=sub3(pi[N+hd],pi[nc]);
      proj_r[hd]=vec3(dot_product(axe,ui[nc]),dot_product(axe,vi[nc]),dot_product(axe,ti[nc]));
      // SHLs
      nc=cumul_nucleosomes;
      for(int s=0;s<nSHLs;s++){
	interaction[nc*nSHLs+s].i=N+hd+0*SHLs_corps[s].i;
	interaction[nc*nSHLs+s].j=fd+SHLs_corps[s].j;
	i0=interaction[nc*nSHLs+s].i;
	i1=interaction[nc*nSHLs+s].j;
	// histones
	cc=sub3(SHLs_pos[s],pi[i0]);
	proj_h[nc*nSHLs+s]=vec3(dot_product(ui[i0],cc),dot_product(vi[i0],cc),dot_product(ti[i0],cc));
	// dna
	cc=sub3(SHLs_pos[s],pi[i1]);
	proj_f[nc*nSHLs+s]=vec3(dot_product(ui[i1],cc),dot_product(vi[i1],cc),dot_product(ti[i1],cc));
	Morse_SHL[nc*nSHLs+s].z=nm/fL;
      }
      // align DNA along the tangent
      bn=normalize3(cross_product(ti[fd],t0));
      theta=arccos(dot_product(ti[fd],t0));
      for(int a=0;a<NNC;a++){
	ui[fd+a]=rotation(bn,theta,ui[fd+a]);
	vi[fd+a]=rotation(bn,theta,vi[fd+a]);
	ti[fd+a]=rotation(bn,theta,ti[fd+a]);
      }
      // align histones along the tangent
      ui[N+hd]=rotation(bn,theta,ui[N+hd]);
      vi[N+hd]=rotation(bn,theta,vi[N+hd]);
      ti[N+hd]=rotation(bn,theta,ti[N+hd]);
      // align DNA along the normal
      bn=normalize3(cross_product(ui[fd],u0));
      theta=arccos(dot_product(ui[fd],u0));
      for(int a=0;a<NNC;a++){
      	ui[fd+a]=rotation(bn,theta,ui[fd+a]);
      	vi[fd+a]=rotation(bn,theta,vi[fd+a]);
      	ti[fd+a]=rotation(bn,theta,ti[fd+a]);
      }
      // align histones along the normal
      ui[N+hd]=rotation(bn,theta,ui[N+hd]);
      vi[N+hd]=rotation(bn,theta,vi[N+hd]);
      ti[N+hd]=rotation(bn,theta,ti[N+hd]);
      // dna positions
      if(bstart){
	pi[fd]=add3(p0,mult3(.5*l[fd],ti[fd]));
	for(int a=1;a<NNC;a++) pi[fd+a]=add3(pi[fd+a-1],add3(mult3(.5*l[fd+a-1],ti[fd+a-1]),mult3(.5*l[fd+a],ti[fd+a])));
      }else for(int a=0;a<NNC;a++) pi[fd+a]=add3(pi[fd+a-1],add3(mult3(.5*l[fd+a-1],ti[fd+a-1]),mult3(.5*l[fd+a],ti[fd+a])));
      // histones positions
      nc=base[cumul_nucleosomes];
      u1=ui[nc];
      v1=vi[nc];
      t1=ti[nc];
      axe=mult3(proj_r[hd].x,u1);
      axe=add3(axe,mult3(proj_r[hd].y,v1));
      axe=add3(axe,mult3(proj_r[hd].z,t1));
      pi[N+hd]=add3(axe,pi[nc]);
      cumul_nucleosomes++;
    }else{
      // not a nucleosome
      r.push_back(nm/fL);
      l.push_back((f0/10.5)*3.5721*nm/fL);
      ui[fc]=u0;
      vi[fc]=v0;
      ti[fc]=t0;
      if(bstart) pi[fc]=add3(p0,mult3(.5*l[fc],ti[fc]));
      else pi[fc]=add3(p0,add3(mult3(.5*l[fc-1],ti[fc-1]),mult3(.5*l[fc],ti[fc])));
      Pivot_move.push_back(1);
      mPivot.push_back(true);
      pPivot.push_back(true);
      iCrankshaft.push_back(true);
      sCrankshaft.push_back(true);
      colors[fc]=0;
      fc++;
    }
  }// end: read input file
  // com to 0
  cx=cy=cz=.0;
  for(int f=0;f<NH;f++){
    cx+=pi[f].x;
    cy+=pi[f].y;
    cz+=pi[f].z;
  }
  cx/=NH;
  cy/=NH;
  cz/=NH;
  for(int f=0;f<NH;f++){
    pi[f].x-=cx;
    pi[f].y-=cy;
    pi[f].z-=cz;
  }
  // check connectivity
  for(int c=0;c<CHROMOSOMES;c++){
    for(int i=cstart[c];i<cend[c];i++){
      p0=pi[i];
      t0=mult3(.5*l[i],ti[i]);
      p1=pi[i+1];
      t1=mult3(-.5*l[i+1],ti[i+1]);
      d0=norm3(sub3(add3(p0,t0),add3(p1,t1)));
      if(d0>.001) printf("check connectivity, warning %i %i/%i/%i -> %f\n",c,cstart[c],i,cend[c],d0);
    }
  }
  // histones sizes
  for(int i=0;i<(NUCLEOSOMES*NHC_MC);i++) l.push_back(4.5*nm/fL);
  for(int i=0;i<(NUCLEOSOMES*NHC_MC);i++) r.push_back(.5*l[N+i]);
  // DNA minimal bond length
  double lm=l[0],lM=l[0],rM=r[0];
  for(int i=1;i<N;i++) lm=fmin(lm,l[i]);
  for(int i=1;i<N;i++) lM=fmax(lM,l[i]);
  for(int i=1;i<N;i++) rM=fmax(rM,r[i]);
  // segment sizes
  retour=sprintf(vname,"%s/visualisation/sn%i_%s.out",path_to,seed,title);
  fOut=fopen(vname,"w");
  for(int f=0;f<NH;f++) retour=fprintf(fOut,"%i %f %f %i %i %i\n",(int)(f>=N),r[f],l[f],f,colors[f],from_i_to_chr[f]);
  fclose(fOut);
  // positions of the bonds
  double *xi=new double[NH]();
  double *yi=new double[NH]();
  double *zi=new double[NH]();
  double *xf=new double[NH]();
  double *yf=new double[NH]();
  double *zf=new double[NH]();
  double *wx=new double[NH]();
  double *wy=new double[NH]();
  double *wz=new double[NH]();
  for(int f=0;f<NH;f++){
    xi[f]=pi[f].x;
    yi[f]=pi[f].y;
    zi[f]=pi[f].z;
  }
  delete[] pi;
  pi=NULL;
  // space partition
  double cell0=1.001*(lM+rM),tbloc=cell0,inv_tbloc=1./tbloc;
  int xbloc,ybloc,zbloc;
  double xINF,yINF,zINF,xSUP,ySUP,zSUP;
  xINF=yINF=zINF=DBL_MAX;
  xSUP=ySUP=zSUP=DBL_MIN;
  inf_sup(xi,yi,zi,0,NH,xINF,yINF,zINF,xSUP,ySUP,zSUP);
  // box size to use with PBC
  if(pbc){
    d0=fmax(fabs(xSUP),fmax(fabs(ySUP),fabs(zSUP)));
    d1=fmax(fabs(xINF),fmax(fabs(yINF),fabs(zINF)));
    R=1.001*fmax(fmax(d0,d1),R0);
    L=2.*R;
    xbox=ybox=zbox=-R;
    xbloc=ybloc=zbloc=1;
    while(1){
      tbloc=(R-xbox)/xbloc;
      inv_tbloc=1./tbloc;
      if(tbloc<(8.*cell0) && tbloc>cell0) break;
      xbloc++;
      ybloc++;
      zbloc++;
    }
  }else{
    xbox=xINF-cell0;
    ybox=yINF-cell0;
    zbox=zINF-cell0;
    xSUP+=cell0;
    ySUP+=cell0;
    zSUP+=cell0;
    while(1){
      tbloc*=1.01;
      inv_tbloc=1./tbloc;
      xbloc=(int)((xSUP-xbox)*inv_tbloc)+1;
      ybloc=(int)((ySUP-ybox)*inv_tbloc)+1;
      zbloc=(int)((zSUP-zbox)*inv_tbloc)+1;
      if((xbloc*ybloc*zbloc)<1000000) break;
    }
  }
  int nbloc=xbloc*ybloc*zbloc,ngperb=0,Lsinb=0,nhitb=0;
  vector<int> sinb(nbloc*ngperb),lsinb(nbloc),stob(NH),lhitb(nbloc),hitb(nbloc);
  lsinb.assign(nbloc,0);
  hitb.assign(nbloc,-1);
  for(int i=0;i<N;i++) if(mPivot[i]) MPivot.push_back(i);
  for(int i=0;i<N;i++) if(pPivot[i]) PPivot.push_back(i);
  for(int i=0;i<N;i++) if(iCrankshaft[i]) ICrankshaft.push_back(i);
  for(int i=0;i<N;i++) if(sCrankshaft[i]) SCrankshaft.push_back(i);
  iCrankshaft.resize(0);
  sCrankshaft.resize(0);
  int l_MPivot=(int)(MPivot.size());
  int l_PPivot=(int)(PPivot.size());
  int l_ICrankshaft=(int)(ICrankshaft.size());
  int l_SCrankshaft=(int)(SCrankshaft.size());
  int T=1000,M=(int)(iterations/T)+1;
  int *tMSD=new int[3*CHROMOSOMES*M]();
  double *xMSD=new double[3*CHROMOSOMES*M]();
  double *yMSD=new double[3*CHROMOSOMES*M]();
  double *zMSD=new double[3*CHROMOSOMES*M]();
  // angle (Pivot and Crankshaft)
  double angle=((pbc)?.5:1.)*fmin(arccos((k_nm*nm/fL-lm)/(k_nm*nm/fL+lm)),sqrt(lm/lt));
  // size of the sub-chain to move (Pivot and Crankshaft)
  int NCrankshaft=0,NPivot=0;
  for(int c=0;c<CHROMOSOMES;c++){
    NCrankshaft+=.25*(cend[c]-cstart[c]);
    NPivot+=.5*(cend[c]-cstart[c]);
  }
  NCrankshaft/=(double)CHROMOSOMES;
  NPivot/=(double)CHROMOSOMES;
  NCrankshaft=max(NCrankshaft,2*NNC);
  NPivot=max(NPivot,2*NNC);
  // printf
  printf("fL=%e m\n",fL);
  printf("%i chromosome(s)\n",CHROMOSOMES);
  printf("%i nucleosome(s)\n",NUCLEOSOMES);
  printf("%.1f bp\n",Gbp);
  printf("R=%f nm\n",R0*fL/nm);
  printf("bp per nm^3=%f\n",bp_per_nm3);
  if(!pbc) printf("gyration radius potential %f*Rg^2\n",.5*ERg2);
  printf("%i blocks\n",nbloc);
  printf("block size=%f nm\n",tbloc*fL/nm);
  printf("%i %i\n",(int)(!l_MPivot),(int)(!l_ICrankshaft));
  printf("%i %i %i %i (%i)\n",l_MPivot,l_PPivot,l_ICrankshaft,l_SCrankshaft,N);
  printf("l(min,max)=%f,%f nm\n",lm*fL/nm,lM*fL/nm);
  printf("Pivot and Crankshaft angle=%f\n",angle);
  printf("Pivot and Crankshaft size=%i and %i\n",NPivot,NCrankshaft);
  // fprintf
  retour=sprintf(sortie,"%s/fibre_MC_n%i_%s.out",path_to,seed,title);
  fOut=fopen(sortie,"a");
  retour=fprintf(fOut,"fL=%e m\n",fL);
  retour=fprintf(fOut,"%i chromosome(s)\n",CHROMOSOMES);
  retour=fprintf(fOut,"%i nucleosome(s)\n",NUCLEOSOMES);
  retour=fprintf(fOut,"%.1f bp\n",Gbp);
  retour=fprintf(fOut,"R=%f nm\n",R0*fL/nm);
  retour=fprintf(fOut,"bp per nm^3=%f\n",bp_per_nm3);
  if(!pbc) retour=fprintf(fOut,"gyration radius potential %f*Rg^2\n",.5*ERg2);
  retour=fprintf(fOut,"%i blocks\n",nbloc);
  retour=fprintf(fOut,"block size=%f nm\n",tbloc*fL/nm);
  retour=fprintf(fOut,"%i %i\n",(int)(!l_MPivot),(int)(!l_ICrankshaft));
  retour=fprintf(fOut,"%i %i %i %i (%i)\n",l_MPivot,l_PPivot,l_ICrankshaft,l_SCrankshaft,N);
  retour=fprintf(fOut,"l(min,max)=%f,%f nm\n",lm*fL/nm,lM*fL/nm);
  retour=fprintf(fOut,"Pivot and Crankshaft angle=%f\n",angle);
  retour=fprintf(fOut,"Pivot and Crankshaft size=%i and %i\n",NPivot,NCrankshaft);
  fclose(fOut);
  // restart from conformation ?
  retour=sprintf(sortie,"%s/conformations/conformation_n%i_%s.out",path_to,seed,title);
  fConformation=fopen(sortie,"rb");
  if(fConformation!=NULL){
    retour=0;
    retour+=fread(&iteration,sizeof(int),1,fConformation);
    for(int f=0;f<NH;f++){
      retour+=fread(&xi[f],sizeof(double),1,fConformation);
      retour+=fread(&yi[f],sizeof(double),1,fConformation);
      retour+=fread(&zi[f],sizeof(double),1,fConformation);
      retour+=fread(&ui[f].x,sizeof(double),1,fConformation);
      retour+=fread(&ui[f].y,sizeof(double),1,fConformation);
      retour+=fread(&ui[f].z,sizeof(double),1,fConformation);
      retour+=fread(&vi[f].x,sizeof(double),1,fConformation);
      retour+=fread(&vi[f].y,sizeof(double),1,fConformation);
      retour+=fread(&vi[f].z,sizeof(double),1,fConformation);
      retour+=fread(&ti[f].x,sizeof(double),1,fConformation);
      retour+=fread(&ti[f].y,sizeof(double),1,fConformation);
      retour+=fread(&ti[f].z,sizeof(double),1,fConformation);
    }
    fclose(fConformation);
    if(retour!=(12*NH+1)) return 0;
  }
  // bending and twisting energies
  for(int f=0;f<(N-1);f++){
    d0=(k_nm*nm/fL)/(.5*(l[f]+l[f+1]));
    if(from_i_to_chr[f]!=from_i_to_chr[f+1]){
      // nothing between two chromosomes start/end
      gb.push_back(.0);
      gt.push_back(.0);
    }else{
      gb.push_back((d0>1.)?dichotomy(flangevin,(d0-1.)/(d0+1.),pow(10.,-10.),100.,pow(10.,-10.))*kBT:.0);
      gt.push_back((twist)*kBT*lt/(.5*(l[f]+l[f+1])));
    }
  }
  // calculating the twist between two consecutive segments
  EbtzRg2[0]=0.;
  for(int f=0;f<(N-1);f++){
    d0=2.*Pi*.5*(l[f]+l[f+1])/(3.5721*nm/fL);
    ctheta=dot_product(ti[f],ti[f+1]);
    d0=.5*(l[f]+l[f+1])/(3.5721*nm/fL);
    d2=fmod(2.*Pi*d0,2.*Pi);
    d2=(d2>Pi)?d2-2.*Pi:d2;
    if(from_i_to_chr[f]==from_i_to_chr[f+1]){
      phi_e.push_back(d2);
      d2=sign((dot_product(ui[f+1],vi[f])-dot_product(vi[f+1],ui[f]))/(1.+ctheta))*arccos((dot_product(ui[f+1],ui[f])+dot_product(vi[f+1],vi[f]))/(1.+ctheta))-d2;
      EbtzRg2[0]+=gb[f]*(1.-dot_product(ti[f],ti[f+1]))+.5*gt[f]*d2*d2;
    }else phi_e.push_back(.0);
  }
  // compute "stacking" energy
  ES_ci=20.*nm/fL;// "stacking" range
  ES_cs=22.*nm/fL;// "stacking" range
  for(int m=0;m<((ES!=0.)*(NUCLEOSOMES-1));m++){
    for(int n=(m+1);n<NUCLEOSOMES;n++){
      d0=pow(xi[N+NHC_MC*m]-xi[N+NHC_MC*n],2.)+pow(yi[N+NHC_MC*m]-yi[N+NHC_MC*n],2.)+pow(zi[N+NHC_MC*m]-zi[N+NHC_MC*n],2.);
      EbtzRg2[0]+=ES*(d0>=(ES_ci*ES_ci) && d0<=(ES_cs*ES_cs));
    }
  }
  // SHLs: energy
  if(0){
    for(int i=0;i<(nSHLs*NUCLEOSOMES);i++){
      i0=interaction[i].i;
      i1=interaction[i].j;
      // histones
      p0=add3(pi[i0],mult3(proj_h[i].x,ui[i0]));
      p0=add3(p0,mult3(proj_h[i].y,vi[i0]));
      p0=add3(p0,mult3(proj_h[i].z,ti[i0]));
      // dna
      p1=add3(pi[i1],mult3(proj_f[i].x,ui[i1]));
      p1=add3(p1,mult3(proj_f[i].y,vi[i1]));
      p1=add3(p1,mult3(proj_f[i].z,ti[i1]));
      expm=exp(-am*norm3(sub3(p0,p1)));
      EbtzRg2[0]+=Morse_SHL[i].x*(expm*expm-2.*expm);
    }
  }
  // compute gyration radius (just for dna)
  if(!pbc){
    cx=cy=cz=.0;
    for(int f=0;f<N;f++){
      cx+=xi[f];
      cy+=yi[f];
      cz+=zi[f];
    }
    cx/=N;
    cy/=N;
    cz/=N;
    Rg2[0]=.0;
    for(int f=0;f<N;f++) Rg2[0]+=pow(xi[f]-cx,2.)+pow(yi[f]-cy,2.)+pow(zi[f]-cz,2.);
    Rg2[0]/=N;
    EbtzRg2[0]+=.5*ERg2*Rg2[0];
  }
  // overlap(s) initial conformation
  new_nc[0]=0;
  for(int i=0;i<(NH-1);i++){
    ax=xi[i];
    ay=yi[i];
    az=zi[i];
    d0=.5*l[i];
    d1=r[i];
    t1=mult3(.5*l[i],tf[i]);
    P1=vec3(ax+t1.x,ay+t1.y,az+t1.z);
    M1=vec3(ax-t1.x,ay-t1.y,az-t1.z);
    for(int j=(i+1);j<NH;j++){
      d2=.5*l[j];
      d3=r[j];
      p2=vec3(xi[j],yi[j],zi[j]);
      if(dot_product2(vec3(ax-p2.x,ay-p2.y,az-p2.z))<((d0+d1)*(d2+d3))){
	if(i<N && j>=N) new_nc[0]+=(sqminDistPoint2Segment(M1,P1,p2)<((d1+d3)*(d1+d3)));
	if(i>=N && j>=N) new_nc[0]+=(dot_product2(vec3(ax-p2.x,ay-p2.y,az-p2.z))<((d1+d3)*(d1+d3)));
	if(i<N && j<N){
	  if(abs(i-j)>1 || from_i_to_chr[i]!=from_i_to_chr[j]){
	    t2=mult3(d2,ti[j]);
	    minDist2segments_KKT(P1,M1,vec3(p2.x+t2.x,p2.y+t2.y,p2.z+t2.z),vec3(p2.x-t2.x,p2.y-t2.y,p2.z-t2.z),false,0,min12);
	    new_nc[0]+=(dot_product2(min12[2])<((d1+d3)*(d1+d3)));
	  }
	}
      }
    }
  }
  printf("initial conformation: %i overlap(s)\n",new_nc[0]);
  // simulation while
  iteration=0;
  while(iteration<=iterations){
    b0=(CHROMOSOMES==1)?true:(ud1(e1)<.5);// no need of Crankshaft when the chain is free ?
    if(b0){
      // Pivot
      b1=(CHROMOSOMES==1)?true:(ud1(e1)<.5);// no need of reverse Pivot when the chain is free ?
      if(b1){
	// wrong initialization so we capture ...
	I1=0;
	I2=2*NPivot;
	while(abs(I1-I2)>NPivot){
	  I1=PPivot[(int)(l_PPivot*ud1(e1))];
	  chromosome=from_i_to_chr[I1];
	  I2=cend[chromosome];
	}
	I3=I1;
	I4=I2;
      }else{
	// wrong initialization so we capture ...
	I1=0;
	I2=2*NPivot;
	while(abs(I1-I2)>NPivot){
	  I1=MPivot[(int)(l_MPivot*ud1(e1))];
	  chromosome=from_i_to_chr[I1];
	  I2=cstart[chromosome];
	}
	I3=I2;
	I4=I1;
      }
      sens=(I1<=I2);
      theta=angle*(1.-2.*ud1(e1));
      if(0 && (I1>cstart[chromosome] && I1<=cend[chromosome])){// ???
	// sens: 1-1-0-0-0-0-0-0-0-0-0-0-0-1
	if(!pPivot[I1-1] && sens){
	  d0=arccos(dot_product(ti[I1-1],ti[I1]));
	  while((theta+d0)<=(.005*Pi) || (theta+d0)>=(.995*Pi)) theta=angle*(1.-2.*ud1(e1));
	  axe=normalize3(cross_product(ti[I1-1],ti[I1]));
	}
	// !sens: 1-0-0-0-0-0-0-0-0-0-0-0-1-1
	if(!mPivot[I1+1] && !sens){
	  d0=arccos(dot_product(ti[I1],ti[I1+1]));
	  while((theta+d0)<=(.005*Pi) || (theta+d0)>=(.995*Pi)) theta=angle*(1.-2.*ud1(e1));
	  axe=normalize3(cross_product(ti[I1],ti[I1+1]));
	}
	// sens: 1-1-0-0-0-0-0-0-0-0-0-0-0-1
	if(!pPivot[I1+1] && sens){
	  d0=arccos(dot_product(ti[I1-1],ti[I1]));
	  while((theta+d0)<=(.005*Pi) || (theta+d0)>=(.995*Pi)) theta=angle*(1.-2.*ud1(e1));
	  axe=normalize3(cross_product(ti[I1-1],ti[I1]));
	}
	// !sens: 1-0-0-0-0-0-0-0-0-0-0-0-1-1
	if(!mPivot[I1-1] && !sens){
	  d0=arccos(dot_product(ti[I1],ti[I1+1]));
	  while((theta+d0)<=(.005*Pi) || (theta+d0)>=(.995*Pi)) theta=angle*(1.-2.*ud1(e1));
	  axe=normalize3(cross_product(ti[I1],ti[I1+1]));
	}
      }else{
	unif1=2.*Pi*ud1(e1);
	unif2=arccos(2.*ud1(e1)-1.);
	axe=vec3(sin(unif2)*cos(unif1),sin(unif2)*sin(unif1),cos(unif2));
      }
      bPivot=true;
    }else{
      // Crankshaft
      // wrong initialization so we capture
      I1=0;
      I2=2*NCrankshaft;
      while(chromosome!=i2 || abs(I1-I2)>NCrankshaft){
	I1=ICrankshaft[min(l_ICrankshaft-1,(int)(l_ICrankshaft*ud1(e1)))];
	chromosome=from_i_to_chr[I1];
	I2=SCrankshaft[min(l_SCrankshaft-1,(int)(l_SCrankshaft*ud1(e1)))];
	i2=from_i_to_chr[I2];
      }
      if(I1>I2) swap(I1,I2);
      I3=I1;
      I4=I2;
      sens=true;
      axe=normalize3(sub3(add3(vec3(xi[I2]-xi[I1],yi[I2]-yi[I1],zi[I2]-zi[I1]),mult3(.5*l[I2],ti[I2])),mult3(-.5*l[I1],ti[I1])));
      theta=angle*(1.-2.*ud1(e1));
      bPivot=false;
    }
    // Pivot: bending and twisting before the move
    EbtzRg2[1]=EbtzRg2[0];
    if(I1>cstart[chromosome] && sens){
      u1=ui[I1-1];
      u2=ui[I1];
      v1=vi[I1-1];
      v2=vi[I1];
      ctheta=dot_product(ti[I1-1],ti[I1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I1-1];
      EbtzRg2[1]-=(gb[I1-1]*(1.-ctheta)+.5*gt[I1-1]*new_phi*new_phi);
    }
    if(I1<cend[chromosome] && !sens){
      u1=ui[I1];
      u2=ui[I1+1];
      v1=vi[I1];
      v2=vi[I1+1];
      ctheta=dot_product(ti[I1],ti[I1+1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I1];
      EbtzRg2[1]-=(gb[I1]*(1.-ctheta)+.5*gt[I1]*new_phi*new_phi);
    }
    // Crankshaft: bending and twisting before the move
    if(!bPivot && I2<cend[chromosome]){
      u1=ui[I2];
      u2=ui[I2+1];
      v1=vi[I2];
      v2=vi[I2+1];
      ctheta=dot_product(ti[I2],ti[I2+1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I2];
      EbtzRg2[1]-=(gb[I2]*(1.-ctheta)+.5*gt[I2]*new_phi*new_phi);
    }
    // SHLs: before the move
    if(0){
      for(int i=0;i<(nSHLs*NUCLEOSOMES);i++){
	i0=interaction[i].i;
	i1=interaction[i].j;
	// histones
	p0=vec3(xi[i0],yi[i0],zi[i0]);
	p0=add3(p0,mult3(proj_h[i].x,ui[i0]));
	p0=add3(p0,mult3(proj_h[i].y,vi[i0]));
	p0=add3(p0,mult3(proj_h[i].z,ti[i0]));
	// dna
	p1=vec3(xi[i1],yi[i1],zi[i1]);
	p1=add3(p1,mult3(proj_f[i].x,ui[i1]));
	p1=add3(p1,mult3(proj_f[i].y,vi[i1]));
	p1=add3(p1,mult3(proj_f[i].z,ti[i1]));
	expm=exp(-am*norm3(sub3(p0,p1)));
	EbtzRg2[1]-=Morse_SHL[i].x*(expm*expm-2.*expm);
      }
    }
    // remove energy contribution from gyration radius before the move
    if(!pbc) EbtzRg2[1]-=.5*ERg2*Rg2[0];
    // if(iteration>1000000) printf("ok %i\n",iteration);
    // new dna conformation
    P0=add3(vec3(xi[I1],yi[I1],zi[I1]),mult3((1-2*(sens))*.5*l[I1],ti[I1]));
    xINF=yINF=zINF=DBL_MAX;
    xSUP=ySUP=zSUP=DBL_MIN;
    inf_sup(xi,yi,zi,0,I3,xINF,yINF,zINF,xSUP,ySUP,zSUP);
    for(int i=I3;i<=I4;i++){
      uf[i]=rotation(axe,theta,ui[i]);
      vf[i]=rotation(axe,theta,vi[i]);
      tf[i]=rotation(axe,theta,ti[i]);
      t1=rotation(axe,theta,vec3(xi[i]-P0.x,yi[i]-P0.y,zi[i]-P0.z));
      ax=P0.x+t1.x;
      ay=P0.y+t1.y;
      az=P0.z+t1.z;
      xf[i]=ax;
      yf[i]=ay;
      zf[i]=az;
      xINF=fmin(xINF,ax);
      yINF=fmin(yINF,ay);
      zINF=fmin(zINF,az);
      xSUP=fmax(xSUP,ax);
      ySUP=fmax(ySUP,ay);
      zSUP=fmax(zSUP,az);
    }
    inf_sup(xi,yi,zi,I4+1,N,xINF,yINF,zINF,xSUP,ySUP,zSUP);
    // histones
    ih1=NH;
    ih2=N;
    nh1=NUCLEOSOMES;
    nh2=0;
    for(int n=nucleosome0[chromosome];n<=nucleosome1[chromosome];n++){
      nb=base[n];
      if(nb<I3 || nb>I4) inf_sup(xi,yi,zi,N+n*NHC_MC,N+n*NHC_MC+NHC_MC,xINF,yINF,zINF,xSUP,ySUP,zSUP);
      else{
	ih1=min(ih1,N+n*NHC_MC);
	ih2=max(ih2,N+n*NHC_MC+NHC_MC-1);
	if(ih1<N || ih2<N){
	  printf("histone selection is in DNA, exit.\n");
	  return 0;
	}
	nh1=min(nh1,n);
	nh2=max(nh2,n);
	for(int i=(n*NHC_MC);i<(n*NHC_MC+NHC_MC);i++){
	  tf[N+i]=rotation(axe,theta,ti[N+i]);
	  uf[N+i]=rotation(axe,theta,ui[N+i]);
	  vf[N+i]=rotation(axe,theta,vi[N+i]);
	  p0=rotation(axe,theta,vec3(xi[N+i]-P0.x,yi[N+i]-P0.y,zi[N+i]-P0.z));
	  ax=P0.x+p0.x;
	  ay=P0.y+p0.y;
	  az=P0.z+p0.z;
	  xINF=fmin(xINF,ax);
	  yINF=fmin(yINF,ay);
	  zINF=fmin(zINF,az);
	  xSUP=fmax(xSUP,ax);
	  ySUP=fmax(ySUP,ay);
	  zSUP=fmax(zSUP,az);
	  xf[N+i]=ax;
	  yf[N+i]=ay;
	  zf[N+i]=az;
	}
      }
    }
    // Pivot: bending and twisting energies (after the move)
    if(I1>cstart[chromosome] && sens){
      u1=ui[I1-1];
      u2=uf[I1];
      v1=vi[I1-1];
      v2=vf[I1];
      ctheta=dot_product(ti[I1-1],tf[I1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I1-1];
      EbtzRg2[1]+=(gb[I1-1]*(1.-ctheta)+.5*gt[I1-1]*new_phi*new_phi);
    }
    if(I1<cend[chromosome] && !sens){
      u1=ui[I1];
      u2=uf[I1+1];
      v1=vi[I1];
      v2=vf[I1+1];
      ctheta=dot_product(ti[I1],tf[I1+1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I1];
      EbtzRg2[1]+=(gb[I1]*(1.-ctheta)+.5*gt[I1]*new_phi*new_phi);
    }
    // Crankshaft: bending and twisting energies (after the move)
    if(!bPivot && I2<cend[chromosome]){
      u1=uf[I2];
      u2=ui[I2+1];
      v1=vf[I2];
      v2=vi[I2+1];
      ctheta=dot_product(tf[I2],ti[I2+1]);
      new_phi=sign((dot_product(u2,v1)-dot_product(v2,u1))/(1.+ctheta))*arccos((dot_product(u2,u1)+dot_product(v2,v1))/(1.+ctheta))-phi_e[I2];
      EbtzRg2[1]+=(gb[I2]*(1.-ctheta)+.5*gt[I2]*new_phi*new_phi);
    }
    // "stacking" energy after the move
    if(ES!=0.){
      for(int i=ih1;i<=ih2;i++){
	for(int j=N;j<ih1;j++){
	  d0=pow(xi[i]-xi[j],2.)+pow(yi[i]-yi[j],2.)+pow(zi[i]-zi[j],2.);
	  EbtzRg2[1]-=ES*(d0>=(ES_ci*ES_ci) && d0<=(ES_cs*ES_cs));
	  d0=pow(xf[i]-xi[j],2.)+pow(yf[i]-yi[j],2.)+pow(zf[i]-zi[j],2.);
	  EbtzRg2[1]+=ES*(d0>=(ES_ci*ES_ci) && d0<=(ES_cs*ES_cs));
	}
	for(int j=(ih2+1);j<NH;j++){
	  d0=pow(xi[i]-xi[j],2.)+pow(yi[i]-yi[j],2.)+pow(zi[i]-zi[j],2.);
	  EbtzRg2[1]-=ES*(d0>=(ES_ci*ES_ci) && d0<=(ES_cs*ES_cs));
	  d0=pow(xf[i]-xi[j],2.)+pow(yf[i]-yi[j],2.)+pow(zf[i]-zi[j],2.);
	  EbtzRg2[1]+=ES*(d0>=(ES_ci*ES_ci) && d0<=(ES_cs*ES_cs));
	}
      }
    }
    // SHLs: energy after the move
    if(0){
      for(int i=0;i<(nSHLs*NUCLEOSOMES);i++){
	i0=interaction[i].i;
	i1=interaction[i].j;
	// histones
	axe=proj_h[i];
	if(i0<ih1 || i0>ih2){
	  p0=vec3(xi[i0],yi[i0],zi[i0]);
	  p0=add3(p0,mult3(axe.x,ui[i0]));
	  p0=add3(p0,mult3(axe.y,vi[i0]));
	  p0=add3(p0,mult3(axe.z,ti[i0]));
	}else{
	  p0=vec3(xf[i0],yf[i0],zf[i0]);
	  p0=add3(p0,mult3(axe.x,uf[i0]));
	  p0=add3(p0,mult3(axe.y,vf[i0]));
	  p0=add3(p0,mult3(axe.z,tf[i0]));
	}
	// dna
	axe=proj_f[i];
	if(i1<I3 || i1>I4){
	  p1=vec3(xi[i1],yi[i1],zi[i1]);
	  p1=add3(p1,mult3(axe.x,ui[i1]));
	  p1=add3(p1,mult3(axe.y,vi[i1]));
	  p1=add3(p1,mult3(axe.z,ti[i1]));
	}else{
	  p1=vec3(xf[i1],yf[i1],zf[i1]);
	  p1=add3(p1,mult3(axe.x,uf[i1]));
	  p1=add3(p1,mult3(axe.y,vf[i1]));
	  p1=add3(p1,mult3(axe.z,tf[i1]));
	}
	expm=exp(-am*norm3(sub3(p0,p1)));
	EbtzRg2[1]+=Morse_SHL[i].x*(expm*expm-2.*expm);
      }
    }
    // compute gyration radius (just for dna)
    if(!pbc){
      cx=cy=cz=.0;
#pragma omp parallel sections num_threads(6)
      {
#pragma omp section
	{
	  for(int i=0;i<I3;i++) cx+=xi[i];
	  for(int i=I3;i<=I4;i++) cx+=xf[i];
	  for(int i=(I4+1);i<N;i++) cx+=xi[i];
	}
#pragma omp section
	{
	  for(int i=0;i<I3;i++) cy+=yi[i];
	  for(int i=I3;i<=I4;i++) cy+=yf[i];
	  for(int i=(I4+1);i<N;i++) cy+=yi[i];
	}
#pragma omp section
	{
	  for(int i=0;i<I3;i++) cz+=zi[i];
	  for(int i=I3;i<=I4;i++) cz+=zf[i];
	  for(int i=(I4+1);i<N;i++) cz+=zi[i];
	}
      }
      cx/=N;
      cy/=N;
      cz/=N;
      Rg2[1]=.0;
#pragma omp parallel sections num_threads(6)
      {
#pragma omp section
	{
	  Rg2xyz[0]=.0;
	  for(int i=0;i<I3;i++) Rg2xyz[0]+=pow(xi[i]-cx,2.);
	  for(int i=I3;i<=I4;i++) Rg2xyz[0]+=pow(xf[i]-cx,2.);
	  for(int i=(I4+1);i<N;i++) Rg2xyz[0]+=pow(xi[i]-cx,2.);
	}
#pragma omp section
	{
	  Rg2xyz[1]=.0;
	  for(int i=0;i<I3;i++) Rg2xyz[1]+=pow(yi[i]-cy,2.);
	  for(int i=I3;i<=I4;i++) Rg2xyz[1]+=pow(yf[i]-cy,2.);
	  for(int i=(I4+1);i<N;i++) Rg2xyz[1]+=pow(yi[i]-cy,2.);
	}
#pragma omp section
	{
	  Rg2xyz[2]=.0;
	  for(int i=0;i<I3;i++) Rg2xyz[2]+=pow(zi[i]-cz,2.);
	  for(int i=I3;i<=I4;i++) Rg2xyz[2]+=+pow(zf[i]-cz,2.);
	  for(int i=(I4+1);i<N;i++) Rg2xyz[2]+=pow(zi[i]-cz,2.);
	}
      }
      Rg2[1]=(Rg2xyz[0]+Rg2xyz[1]+Rg2xyz[2])/N;
      EbtzRg2[1]+=.5*ERg2*Rg2[1];
    }
    // acceptation ?
    AM=(EbtzRg2[1]<=EbtzRg2[0])?1:(int)(ud1(e1)<exp((EbtzRg2[0]-EbtzRg2[0])/kBT));
    if(AM==1){
      // histones that did not move: inf(x,y,z)
      inf_sup(xi,yi,zi,N,N+nucleosome0[chromosome]*NHC_MC,xINF,yINF,zINF,xSUP,ySUP,zSUP);
      inf_sup(xi,yi,zi,N+(nucleosome1[chromosome]+1)*NHC_MC,NH,xINF,yINF,zINF,xSUP,ySUP,zSUP);
      // new partition ?
      T0=clock();
      tbloc=cell0;
      inv_tbloc=1./tbloc;
      // box size to use with PBC
      if(pbc){
	xbloc=ybloc=zbloc=1;
	while(1){
	  tbloc=(R-xbox)/xbloc;
	  inv_tbloc=1./tbloc;
	  if(tbloc<(8.*cell0) && tbloc>cell0) break;
	  xbloc++;
	  ybloc++;
	  zbloc++;
	}
      }else{
	// add one block to the left
	xbox=xINF-cell0;
	ybox=yINF-cell0;
	zbox=zINF-cell0;
	// add one block to the right
	xSUP+=cell0;
	ySUP+=cell0;
	zSUP+=cell0;
	while(1){
	  xbloc=(int)((xSUP-xbox)*inv_tbloc)+1;
	  ybloc=(int)((ySUP-ybox)*inv_tbloc)+1;
	  zbloc=(int)((zSUP-zbox)*inv_tbloc)+1;
	  if((xbloc*ybloc*zbloc)<1000000) break;
	  tbloc*=1.01;
	  inv_tbloc=1./tbloc;
	}
      }
      // resize if nbloc is not big enough
      // resize if nbloc is too big
      if(nbloc<(xbloc*ybloc*zbloc) || (xbloc*ybloc*zbloc)<(int)(.5*nbloc)){
	nbloc=xbloc*ybloc*zbloc;
	lhitb.assign(nbloc,-1);
	hitb.assign(nbloc,-1);
	lsinb.assign(nbloc,0);
      }
      // wrap coordinates
      if(pbc){
#pragma omp parallel sections
	{
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wx[f]=xi[f]-L*rint(xi[f]/L);
	    for(int f=I3;f<=I4;f++) wx[f]=xf[f]-L*rint(xf[f]/L);
	    for(int f=(I4+1);f<ih1;f++) wx[f]=xi[f]-L*rint(xi[f]/L);
	    for(int f=ih1;f<=ih2;f++) wx[f]=xf[f]-L*rint(xf[f]/L);
	    for(int f=(ih2+1);f<NH;f++) wx[f]=xi[f]-L*rint(xi[f]/L);
	  }
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wy[f]=yi[f]-L*rint(yi[f]/L);
	    for(int f=I3;f<=I4;f++) wy[f]=yf[f]-L*rint(yf[f]/L);
	    for(int f=(I4+1);f<ih1;f++) wy[f]=yi[f]-L*rint(yi[f]/L);
	    for(int f=ih1;f<=ih2;f++) wy[f]=yf[f]-L*rint(yf[f]/L);
	    for(int f=(ih2+1);f<NH;f++) wy[f]=yi[f]-L*rint(yi[f]/L);
	  }
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wz[f]=zi[f]-L*rint(zi[f]/L);
	    for(int f=I3;f<=I4;f++) wz[f]=zf[f]-L*rint(zf[f]/L);
	    for(int f=(I4+1);f<ih1;f++) wz[f]=zi[f]-L*rint(zi[f]/L);
	    for(int f=ih1;f<=ih2;f++) wz[f]=zf[f]-L*rint(zf[f]/L);
	    for(int f=(ih2+1);f<NH;f++) wz[f]=zi[f]-L*rint(zi[f]/L);
	  }
	}
      }else{
	// if 'free' then unwrap coordinates
#pragma omp parallel sections
	{
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wx[f]=xi[f];
	    for(int f=I3;f<=I4;f++) wx[f]=xf[f];
	    for(int f=(I4+1);f<ih1;f++) wx[f]=xi[f];
	    for(int f=ih1;f<=ih2;f++) wx[f]=xf[f];
	    for(int f=(ih2+1);f<NH;f++) wx[f]=xi[f];
	  }
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wy[f]=yi[f];
	    for(int f=I3;f<=I4;f++) wy[f]=yf[f];
	    for(int f=(I4+1);f<ih1;f++) wy[f]=yi[f];
	    for(int f=ih1;f<=ih2;f++) wy[f]=yf[f];
	    for(int f=(ih2+1);f<NH;f++) wy[f]=yi[f];
	  }
#pragma omp section
	  {
	    for(int f=0;f<I3;f++) wz[f]=zi[f];
	    for(int f=I3;f<=I4;f++) wz[f]=zf[f];
	    for(int f=(I4+1);f<ih1;f++) wz[f]=zi[f];
	    for(int f=ih1;f<=ih2;f++) wz[f]=zf[f];
	    for(int f=(ih2+1);f<NH;f++) wz[f]=zi[f];
	  }
	}
      }
      // new segments per block
      nhitb=0;
      ngperb=1;
      for(int k=0;k<3;k++){
	switch(k){
	case 0:
	  fd=0;
	  hd=(bEV)*I3;
	  break;
	case 1:
	  fd=I4+1;
	  hd=(bEV)*N+(bEVh)*(ih1-N);
	  break;
	case 2:
	  fd=ih2+1;
	  hd=(bEVh)*NH;
	  break;
	default:
	  fd=0;
	  hd=-1;
	  break;
	}
	for(int i=fd;i<hd;i++){
	  i1=max(0,min(xbloc-1,(int)((wx[i]-xbox)*inv_tbloc)));
	  i2=max(0,min(ybloc-1,(int)((wy[i]-ybox)*inv_tbloc)));
	  i3=max(0,min(zbloc-1,(int)((wz[i]-zbox)*inv_tbloc)));
	  i0=ybloc*zbloc*i1+zbloc*i2+i3;
	  if(i0>=nbloc){
	    printf("block index %i is greater than the number of blocks %i.\n",i0,nbloc);
	    printf("%f %f %f\n",wx[i]-xbox,2*R,2*R0);
	    printf("%f %f %f\n",wy[i]-ybox,2*R,2*R0);
	    printf("%f %f %f\n",wz[i]-zbox,2*R,2*R0);
	    return 0;
	  }
	  if(i0<0){
	    printf("block index %i is lesser than zero.\n",i0);
	    printf("%f %f %f\n",wx[i]-xbox,2*R,2*R0);
	    printf("%f %f %f\n",wy[i]-ybox,2*R,2*R0);
	    printf("%f %f %f\n",wz[i]-zbox,2*R,2*R0);
	    return 0;
	  }
	  // hit block
	  if(nhitb>=nbloc){
	    printf("nhitb=%i nbloc=%i\n",nhitb,nbloc);
	    exit(EXIT_FAILURE);
	  }
	  if(hitb[i0]==-1){
	    lhitb[nhitb]=i0;
	    hitb[i0]=nhitb;  
	    lsinb[nhitb]=0;
	    i1=nhitb;
	    stob[i]=nhitb;
	    nhitb++;
	  }else{
	    i1=hitb[i0];
	    if(i1>=nbloc){
	      printf("i1=%i nbloc=%i\n",i1,nbloc);
	      exit(EXIT_FAILURE);
	    }
	    stob[i]=i1;
	    ngperb=max(ngperb,lsinb[i1]+1);// new partition
	  }
	  lsinb[i1]++;
	}
      }
      // compute segments per block (again)
      if((nhitb*ngperb)>Lsinb){
	sinb.resize(nhitb*ngperb);
	Lsinb=nhitb*ngperb;
	// if(iteration>1001000) printf("if ok0 %i %i %i\n",Lsinb,nhitb,ngperb);
      }
      lsinb.resize(nhitb);
      lsinb.assign(nhitb,0);
      // if(iteration>1001000) printf("ok0 %i %i %i (%i)\n",Lsinb,nhitb,ngperb,(int)bEV);
      if(bEV){
	// if(iteration>1001000) printf("after ok0 %i %i %i\n",(int)(stob.size()),(int)(sinb.size()),(int)(lsinb.size()));
	compute_sinb(0,I3,stob,sinb,lsinb,ngperb,nhitb,Lsinb);
	// if(iteration>1001000) printf("ok1\n");
	compute_sinb(I4+1,N,stob,sinb,lsinb,ngperb,nhitb,Lsinb);
      }
      // if(iteration>1001000) printf("ok2\n");
      if(bEVh){
	compute_sinb(N,ih1,stob,sinb,lsinb,ngperb,nhitb,Lsinb);
	compute_sinb(ih2+1,NH,stob,sinb,lsinb,ngperb,nhitb,Lsinb);
      }
      // if(iteration>1001000) printf("ok ok\n\n");
      TPartition+=(clock()-T0);
      T0=clock();
      // overlap(s) (histones, then dna)
      new_nc[0]=new_nc[1]=0;
      //#pragma omp parallel for private(fd,hd,ax,ay,az,d0,d1,t1,P1,M1,ii,jj,kk,ix,iy,iz,sx,sy,sz,i1,i2,i3,nb,nc,i5,p2,d3,t2,d2)
      for(int k=0;k<2;k++){
	if(k==0){
	  fd=ih1;
	  hd=(bEVh)*ih2;
	}else{
	  fd=I3;
	  hd=(bEV)*I4;
	}
	for(int i=fd;i<=(((new_nc[0]+new_nc[1])==0)*hd);i++){
	  ax=wx[i];
	  ay=wy[i];
	  az=wz[i];
	  d0=.5*l[i];
	  d1=r[i];
	  t1=mult3(d0,tf[i]);
	  P1=vec3(ax+t1.x,ay+t1.y,az+t1.z);
	  M1=vec3(ax-t1.x,ay-t1.y,az-t1.z);
	  // hit block
	  ii=max(0,min(xbloc-1,(int)((ax-xbox)*inv_tbloc)));
	  jj=max(0,min(ybloc-1,(int)((ay-ybox)*inv_tbloc)));
	  kk=max(0,min(zbloc-1,(int)((az-zbox)*inv_tbloc)));
	  // mid-block conditions
	  ix=iy=iz=sx=sy=sz=0;
	  if(!pbc){
	    if(tbloc>(2.*cell0)){
	      ix=((ax-d0-d1)>(xbox+(ii+.5)*tbloc))?1:0;
	      iy=((ay-d0-d1)>(ybox+(jj+.5)*tbloc))?1:0;
	      iz=((az-d0-d1)>(zbox+(kk+.5)*tbloc))?1:0;
	      sx=((ax+d0+d1)<(xbox+(ii+.5)*tbloc))?1:0;
	      sy=((ay+d0+d1)<(ybox+(jj+.5)*tbloc))?1:0;
	      sz=((az+d0+d1)<(zbox+(kk+.5)*tbloc))?1:0;
	    }
	    // if free do not look outside the partition
	    if(ii==0) ix=1;
	    if(jj==0) iy=1;
	    if(kk==0) iz=1;
	    if(ii==(xbloc-1)) sx=1;
	    if(jj==(ybloc-1)) sy=1;
	    if(kk==(zbloc-1)) sz=1;
	  }
	  for(int x=(ix-1);x<(2-sx);x++){
	    i1=ii+x;
	    i1=(pbc)?i1+((i1<0)-(i1>=xbloc))*xbloc:i1;
	    for(int y=(iy-1);y<(2-sy);y++){
	      i2=jj+y;
	      i2=(pbc)?i2+((i2<0)-(i2>=ybloc))*ybloc:i2;
	      for(int z=(iz-1);z<(2-sz);z++){
		i3=kk+z;
		i3=(pbc)?i3+((i3<0)-(i3>=zbloc))*zbloc:i3;
		nb=hitb[ybloc*zbloc*i1+zbloc*i2+i3];
		if(nb<nhitb){
		  nc=lsinb[nb];
		  // loop over the segments (current block)
		  for(int j=0;j<nc;j++){
		    i5=sinb[nb*ngperb+j];
		    p2=vec3(wx[i5],wy[i5],wz[i5]);
		    // shift current segment
		    if(pbc){
		      p2.x=ax+((p2.x-ax)-L*rint((p2.x-ax)/L));
		      p2.y=ay+((p2.y-ay)-L*rint((p2.y-ay)/L));
		      p2.z=az+((p2.z-az)-L*rint((p2.z-az)/L));
		    }
		    // overlap ?
		    d3=r[i5];
		    if(k==0){
		      // histone
		      if(i5>=N) new_nc[k]+=(dot_product2(vec3(ax-p2.x,ay-p2.y,az-p2.z))<((d1+d3)*(d1+d3)));
		      else{
			t2=mult3(.5*l[i5],ti[i5]);
			new_nc[k]+=(sqminDistPoint2Segment(sub3(p2,t2),add3(p2,t2),vec3(ax,ay,az))<((d1+d3)*(d1+d3)));
		      }
		    }else{
		      // dna
		      if(i5>=N) new_nc[k]+=(sqminDistPoint2Segment(M1,P1,p2)<((d1+d3)*(d1+d3)));
		      else{
			if(abs(i-i5)>1 || from_i_to_chr[i]!=from_i_to_chr[i5]){
			  d2=.5*l[i5];
			  if(dot_product2(vec3(p2.x-ax,p2.y-ay,p2.z-az))<=((d0+d1+d2+d3)*(d0+d1+d2+d3))){
			    t2=mult3(d2,ti[i5]);
			    minDist2segments_KKT(P1,M1,add3(p2,t2),sub3(p2,t2),false,0,min12);
			    new_nc[k]+=(dot_product2(min12[2])<((d1+d3)*(d1+d3)));
			  }
			}
		      }
		    }
		  }// Rof "j"
		}// Fi
	      }// Rof "z"
	    }// Rof "y"
	  }// Rof "x"
	}// Rof "i"
      }// Rof "k"
    }// Fi Metropolis acceptation
    // clean
    for(int i=0;i<nhitb;i++){
      lsinb[i]=0;
      hitb[lhitb[i]]=-1;
      lhitb[i]=-1;
    }
    // test overlaps "move" against "did not move"
    for(int i=0;i<((new_nc[0]==0 && new_nc[1]==0 && bEV && bEVh && !(I3==0 && I4==(N-1) && CHROMOSOMES==1))*20);i++){
      // wrong initialization so we capture
      i0=i1=0;
      while((i1>=ih1 && i1<=ih2) || (abs(i1-i0)<=1 && from_i_to_chr[i0]==from_i_to_chr[i1])){
	i0=min(I4,max(I3,(int)(I3+(I4-I3)*ud1(e1))));
	i1=(ud1(e1)<((I3-1)/(I3-1+NH-(i4+1))))?min(I3-1,(int)(I3*ud1(e1))):max(I4+1,I4+1+(int)((NH-(I4+1))*ud1(e1)));
      }
      ax=wx[i0];
      ay=wy[i0];
      az=wz[i0];
      t0=mult3(.5*l[i0],tf[i0]);
      P1=vec3(ax+t0.x,ay+t0.y,az+t0.z);
      M1=vec3(ax-t0.x,ay-t0.y,az-t0.z);
      p0=vec3(wx[i1],wy[i1],wz[i1]);
      if(pbc){
	p0.x=ax+((p0.x-ax)-L*rint((p0.x-ax)/L));
	p0.y=ay+((p0.y-ay)-L*rint((p0.y-ay)/L));
	p0.z=az+((p0.z-az)-L*rint((p0.z-az)/L));
      }
      if(i1>=N) d2=sqminDistPoint2Segment(M1,P1,p0);
      else{
	t0=mult3(.5*l[i1],ti[i1]);
	minDist2segments_KKT(P1,M1,add3(p0,t0),sub3(p0,t0),false,0,min12);
	d2=dot_product2(min12[2]);
      }
      if(d2<((r[i0]+r[i1])*(r[i0]+r[i1]))){
	retour=sprintf(sortie,"%s/error_n%i_%s.out",path_to,seed,title);
	fOut=fopen(sortie,"w");
	retour=fprintf(fOut,"step %i, true: %i(%i) and %i(%i)/%i %f\n",iteration,i0,from_i_to_chr[i0],i1,from_i_to_chr[i1],N,d2);
	fclose(fOut);
	printf("step %i, true: %i(%i) and %i(%i)/%i %f\n",iteration,i0,from_i_to_chr[i0],i1,from_i_to_chr[i1],N,d1);
	printf("between %i(%i) and %i(%i)\n",I3,from_i_to_chr[I3],I4,from_i_to_chr[I4]);
	printf("Pivot: %i Crankshaft: %i\n",(int)bPivot,(int)(!bPivot));
	return 0;
      }
    }
    TOverlaps+=(clock()-T0);
    // Metropolis
    if(new_nc[0]==0 && new_nc[1]==0 && AM==1){
      acc++;
#pragma omp parallel sections
      {
#pragma omp section
	{	  
	  for(int i=I3;i<=I4;i++){
	    ui[i]=uf[i];
	    xi[i]=xf[i];
	  }
	  for(int i=ih1;i<=ih2;i++){
	    ui[i]=uf[i];
	    xi[i]=xf[i];
	  }
	}
#pragma omp section
	{
	  for(int i=I3;i<=I4;i++){
	    vi[i]=vf[i];
	    yi[i]=yf[i];
	  }
	  for(int i=ih1;i<=ih2;i++){
	    vi[i]=vf[i];
	    yi[i]=yf[i];
	  }
	}
#pragma omp section
	{
	  for(int i=I3;i<=I4;i++){
	    ti[i]=tf[i];
	    zi[i]=zf[i];
	  }
	  for(int i=ih1;i<=ih2;i++){
	    ti[i]=tf[i];
	    zi[i]=zf[i];
	  }
	}
      }
      EbtzRg2[0]=EbtzRg2[1];
      Rg2[0]=Rg2[1];
      // box size to use with PBC
      // from 2*R to 2*R0
      if(pbc){
	xINF=yINF=zINF=DBL_MAX;
	xSUP=ySUP=zSUP=DBL_MIN;
#pragma omp parallel sections
	{
#pragma omp section
	  {
	    for(int f=0;f<NH;f++){
	      xINF=fmin(xINF,wx[f]);
	      xSUP=fmax(xSUP,wx[f]);
	    }
	  }
#pragma omp section
	  {
	    for(int f=0;f<NH;f++){
	      yINF=fmin(yINF,wy[f]);
	      ySUP=fmax(ySUP,wy[f]);
	    }
	  }
#pragma omp section
	  {
	    for(int f=0;f<NH;f++){
	      zINF=fmin(zINF,wz[f]);
	      zSUP=fmax(zSUP,wz[f]);
	    }
	  }
	}
	d0=fmax(fabs(xSUP),fmax(fabs(ySUP),fabs(zSUP)));
	d1=fmax(fabs(xINF),fmax(fabs(yINF),fabs(zINF)));
	d2=fmax(d0,d1);
	if(d2<R){
	  R=.01*R+.99*d2;
	  R=fmax(R,R0);
	  if(R==R0) angle=fmin(arccos((k_nm*nm/fL-lm)/(k_nm*nm/fL+lm)),sqrt(lm/lt));
	  L=2.*R;
	  // bottom left corner of the simulation box
	  xbox=ybox=zbox=-R;
	}
      }
    }
    // MSD: save com for each nucleosomes
    if(iteration>N3 && (iteration%T)==0){
      printf("store xyz to compute MSD %i.\n",3*CHROMOSOMES*M);
      for(int p=0;p<3;p++){
	cumul_nucleosomes=0;
	for(int c=0;c<CHROMOSOMES;c++){
	  i0=cumul_nucleosomes+(int)((1+p)*.25*nucleosome[c]);
	  axe=vec3();
	  for(int j=0;j<NHC_MC;j++) axe=add3(axe,vec3(xi[N+i0*NHC_MC+j],yi[N+i0*NHC_MC+j],zi[N+i0*NHC_MC+j]));
	  axe=mult3((fL/nm)/(double)NHC_MC,axe);
	  if((p*CHROMOSOMES*M+c*M+iteration/T)>=(3*CHROMOSOMES*M)){
	    printf("warning msd\n");
	    exit(EXIT_FAILURE);
	  }
	  tMSD[p*CHROMOSOMES*M+c*M+iteration/T]=iteration;
	  xMSD[p*CHROMOSOMES*M+c*M+iteration/T]=axe.x;
	  yMSD[p*CHROMOSOMES*M+c*M+iteration/T]=axe.y;
	  zMSD[p*CHROMOSOMES*M+c*M+iteration/T]=axe.z;
	  cumul_nucleosomes+=nucleosome[c];
	}
      }
    }
    // conformation and visualisation
    if((iteration%every)==0 || iteration==iterations){
      // save conformation for restart
      retour=sprintf(sortie,"%s/conformations/conformation_n%i_%s.out",path_to,seed,title);
      fConformation=fopen(sortie,"wb");
      retour=fwrite(&iteration,sizeof(int),1,fConformation);
      for(int f=0;f<NH;f++){
	retour+=fwrite(&xi[f],sizeof(double),1,fConformation);
	retour+=fwrite(&yi[f],sizeof(double),1,fConformation);
	retour+=fwrite(&zi[f],sizeof(double),1,fConformation);
	retour+=fwrite(&ui[f].x,sizeof(double),1,fConformation);
	retour+=fwrite(&ui[f].y,sizeof(double),1,fConformation);
	retour+=fwrite(&ui[f].z,sizeof(double),1,fConformation);
	retour+=fwrite(&vi[f].x,sizeof(double),1,fConformation);
	retour+=fwrite(&vi[f].y,sizeof(double),1,fConformation);
	retour+=fwrite(&vi[f].z,sizeof(double),1,fConformation);
	retour+=fwrite(&ti[f].x,sizeof(double),1,fConformation);
	retour+=fwrite(&ti[f].y,sizeof(double),1,fConformation);
	retour+=fwrite(&ti[f].z,sizeof(double),1,fConformation);
      }
      fclose(fConformation);
      if(retour!=(12*NH+1)) return 0;
      // visualisation
      // retour=sprintf(vname,"%s/visualisation/vn%i_%s_s%i.out",path_to,seed,title,iteration);
      retour=sprintf(vname,"%s/visualisation/vn%i_%s.out",path_to,seed,title);
      fOut=fopen(vname,"ab");
      for(int f=0;f<NH;f++){
	fwrite(&xi[f],sizeof(double),1,fOut);
	fwrite(&yi[f],sizeof(double),1,fOut);
	fwrite(&zi[f],sizeof(double),1,fOut);
	fwrite(&ti[f].x,sizeof(double),1,fOut);
	fwrite(&ti[f].y,sizeof(double),1,fOut);
	fwrite(&ti[f].z,sizeof(double),1,fOut);
      }
      fclose(fOut);
    }
    // # of contacts for 15 and 20 nm thresholds
    if(iteration==iterations){
      for(int c=0;c<2;c++){
	contacts=0;
	for(int f=0;f<NH;f++) colors[f]=(f<N)?0:1;
	for(int n=0;n<(NUCLEOSOMES-1);n++){
	  for(int o=(n+10);o<NUCLEOSOMES;o++){
	    if(norm3(vec3(xi[N+n*NHC_MC]-xi[N+o*NHC_MC],yi[N+n*NHC_MC]-yi[N+o*NHC_MC],zi[N+n*NHC_MC]-zi[N+o*NHC_MC]))<=(((c==0)*15.+(c==1)*20.)*nm/fL)){
	      contacts++;
	      colors[N+n*NHC_MC]=2;
	      colors[N+o*NHC_MC]=2;
	      for(int i=base[n];i<(base[n]+NNC/2);i++) colors[i]=2;
	      for(int i=base[n];i>=(base[n]-(NNC/2-1));i--) colors[i]=2;
	      for(int i=base[o];i<(base[o]+NNC/2);i++) colors[i]=2;
	      for(int i=base[o];i>=(base[o]-(NNC/2-1));i--) colors[i]=2;
	    }
	  }
	}
	retour=sprintf(vname,"%s/visualisation/sn%i_%s_s%i_%inm.out",path_to,seed,title,iteration,(c==0)*15+(c==1)*20);
	fOut=fopen(vname,"w");
	for(int f=0;f<NH;f++) retour=fprintf(fOut,"%i %f %f %i %i\n",(int)(f>=N),r[f],l[f],f,colors[f]);
	fclose(fOut);
	retour=sprintf(sortie,"%s/fibre_MC_n%i_%s.out",path_to,seed,title);
	fOut=fopen(sortie,"a");
	retour=fprintf(fOut,"%i, %i contacts(%i nm)\n",iteration,contacts,(c==0)*15+(c==1)*20);
	fclose(fOut);
      }
    }
    if((iteration%1000)==0){
      printf("iteration:%i(%i)\n",iteration,iterations);
      printf("     %i g per block,%i/%i hit block(s),pivot=%i(%i %i),acceptation=%f\n",ngperb,nhitb,nbloc,(int)bPivot,I1,I2,(double)acc/(double)iteration);
      // square end-to-end distance average
      d2=.0;
      for(int c=0;c<CHROMOSOMES;c++){
	i0=cstart[c];
	i1=cend[c];
	d2+=dot_product2(sub3(vec3(xi[i0],yi[i0],zi[i0]),vec3(xi[i1],yi[i1],zi[i1])));
      }
      printf("     sqrt(<Ree^2>)=%f nm\n",sqrt(d2/(double)CHROMOSOMES)*fL/nm);
      printf("     R=%f/%f nm\n",R*fL/nm,R0*fL/nm);
      if(!pbc) printf("     Rg^2/R^2=%f\n",Rg2[0]/(R0*R0));
      printf("     %f(%f,%f) minutes\n",((clock()-ComputeTime)/CLOCKS_PER_SEC)/60.,(TOverlaps/CLOCKS_PER_SEC)/60.,(TPartition/CLOCKS_PER_SEC)/60.);
      // if(iteration>1000000) printf("ok ...\n");
      retour=sprintf(sortie,"%s/fibre_MC_n%i_%s.out",path_to,seed,title);
      // if(iteration>1000000) printf("ok ok 1 (%i)...\n",(int)(fOut==NULL));
      fOut=fopen(sortie,"a");
      // if(iteration>1000000) printf("ok ok 1 (%i)...\n",(int)(fOut==NULL));
      retour=fprintf(fOut,"iteration:%i(%i)\n",iteration,iterations);
      // if(iteration>1000000) printf("ok ok 2 ...\n");
      retour=fprintf(fOut,"     %i g per block,%i/%i hit block(s),pivot=%i(%i %i),acceptation=%f\n",ngperb,nhitb,nbloc,(int)bPivot,I1,I2,(double)acc/(double)iteration);
      // if(iteration>1000000) printf("ok ok 3 ...\n");
      retour=fprintf(fOut,"     sqrt(<Ree^2>)=%f nm\n",sqrt(d2/(double)CHROMOSOMES)*fL/nm);
      // if(iteration>1000000) printf("ok ok 4 ...\n");
      retour=fprintf(fOut,"     R=%f/%f nm\n",R*fL/nm,R0*fL/nm);
      // if(iteration>1000000) printf("ok ok 5 ...\n");
      if(!pbc) retour=fprintf(fOut,"     Rg^2/R^2=%f\n",Rg2[0]/(R0*R0));
      // if(iteration>1000000) printf("ok ok 6 ...\n");
      retour=fprintf(fOut,"     %f(%f,%f) minutes\n",((clock()-ComputeTime)/CLOCKS_PER_SEC)/60.,(TOverlaps/CLOCKS_PER_SEC)/60.,(TPartition/CLOCKS_PER_SEC)/60.);
      // if(iteration>1000000) printf("ok ok 7 ...\n");
      fclose(fOut);
      // if(iteration>1000000) printf("ok ok ok ...\n\n");
    }
    iteration++;
  }// end simulation while
  // MSD: histones com
  for(int p=0;p<3;p++){
    retour=sprintf(nmsd,"%s/msd/rt_%.2f_n%i_%s.out",path_to,(1.+p)*.25,seed,title);
    fOut=fopen(nmsd,"a");
    for(int c=0;c<CHROMOSOMES;c++){
      for(int t=0;t<T;t++) retour=fprintf(fOut,"%i %f %f %f\n",tMSD[p*CHROMOSOMES*M+c*M+t],xMSD[p*CHROMOSOMES*M+c*M+t],yMSD[p*CHROMOSOMES*M+c*M+t],zMSD[p*CHROMOSOMES*M+c*M+t]);
    }
    fclose(fOut);
  }
  delete[] tMSD;
  delete[] xMSD;
  delete[] yMSD;
  delete[] zMSD;
  printf("msd\n");
  delete[] colors;
  delete[] cstart;
  delete[] cend;
  delete[] nucleosome;
  delete[] nucleosome0;
  delete[] nucleosome1;
  delete[] base;
  delete[] from_i_to_chr;
  printf("here\n");
  delete[] pi;
  delete[] xi;
  delete[] yi;
  delete[] zi;
  delete[] xf;
  delete[] yf;
  delete[] zf;
  printf("xyz\n");
  delete[] wx;
  delete[] wy;
  delete[] wz;
  printf("w\n");
  delete[] ui;
  delete[] vi;
  delete[] ti;
  delete[] uf;
  delete[] vf;
  delete[] tf;
  printf("uvt\n");
  printf("%i blocks and %i monomers per block\n",nbloc,ngperb);
  retour=sprintf(sortie,"%s/fibre_MC_n%i_%s.out",path_to,seed,title);
  fOut=fopen(sortie,"a");
  retour=fprintf(fOut,"computation time=%f minutes\n",(clock()-ComputeTime)/CLOCKS_PER_SEC/60.);
  retour=fprintf(fOut,"acceptation=%f\n",(double)acc/(double)iteration);
  retour=fprintf(fOut,"done\n");
  fclose(fOut);
  printf("fOut\n");
  return 0;
}
