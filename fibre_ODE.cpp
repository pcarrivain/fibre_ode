#include "functions.h"
#include "variables.h"
#include <filesystem>
#include <getopt.h>
#include <time.h>
#include <unistd.h>

using namespace Eigen;
using namespace std;

// parameters
double lh = 2.907 * nm / fL;
double rh = .85 * nm / fL;
double lp = .5 * lk / fL, lt = 86. * nm / fL;
const double vlim = .1;
const int is = 200000;
int dofs = 0, mcs = 1000, tmp, na, nc, new_nc, vl, fi, step0 = 0, mstep = 0,
    MSTEP = 0, rstep = 200000, reversome0, reversome1, nrl, NRL, by, nuci, nucj;
int ix, iy, iz, ibin, i0, i1, i2, i3, i4, i5, nacc[2], ntry, n0, n1;
bool bMC = false, locked, pivot, scaffold = false, debug = false;
double dLk, Tw, Tw0, Wr, Twt0, Wrt0, Et, cumE[2], zz, d0, d1, d2, avgdr,
    min_lbp, sum_abs_depth, new_sum_abs_depth, max_abs_depth, cos_tw2[2],
    dsolenoid[2];
double xtime, solvetime = .0, partitiontime = .0, langevintime = .0,
              cputime = .0, t0, t1, t2, t3;
const char *modes[2] = {"Lk", "T"};
char name[600], ename[200], chromatin_in[500], chromatin_out[100],
    fiber_type[100], cbuffer[200], extra[200], s0[10], s1[10], s2[10];
vec3 bn, bn1, bn2, ta, lv, av, tr, ur, vr, rij, p0, ev, evec1, evec2,
    evec3;
vec3 *old_ui, *old_vi, *old_ti, *old_pi;
vec3 *new_ui, *new_vi, *new_ti, *new_pi;
double oldE, newE;
double *gb = new double[1](), *gt = new double[1]();
double *b0 = new double[1]();
double *tw0 = new double[1]();
int *is_linker_nucleosome = new int[1]();
int *nucleosome_start = new int[1]();
FILE *fOut, *fbackup, *fcurrent;
int3p nc_test_Vl;
float f0;

double xmass = 1., xfriction = 1.;
bool same_mass = false;
// variables: monomers partition
int nbins = 1, lhit = 0, nperb = 10, nnperb = 10;
int *binn = NULL;
int *ninb = NULL;
int *bhit = NULL;
int *nhit = NULL;
int *lperb = NULL;
// variables: nucleosomes partition
int *nbinn = NULL;
int *nninb = NULL;
int *nbhit = NULL;
int *nnhit = NULL;
int *nlperb = NULL;

// main
int main(int argc, char **argv) {
  void (*my_nearCallback)(void *, dGeomID, dGeomID);
  my_nearCallback = &nearCallback;
  // default inputs
  double force = pN / fF;
  double torque = pN * nm / fE;
  double nLk = .0;
  int mode = 1;
  int seed = 1;
  int iterations = 1000000;
  int every = (int)(iterations / 100);
  int every_sqd = -1;
  bool noSM = false;
  double xghost = 1.01;
  int Vl_every = 1;
  bool test_Vl = false;
  int gyroscopic = 1;
  double bp_limit = .0;
  double BP_limit = 10.5;
  int simplified_nucleosome = 0;
  bool nucleosome_joints = true;
  int nucleosomes = 0;
  int reversomes = 0;
  int model = 0;
  bool random_fibre = false;
  char file_stacking[1000];
  sprintf(file_stacking, "%s", "");
  double prob_h1 = .0;
  double prob_no_nucleosome = .0;
  double prob_hexasome = .0;
  double prob_tetrasome = .0;
  bool accurate_step = false;
  bool start_at_zero = true;
  int step0 = 0;
  bool vreset = false;
  bool write_joint_feedback = false;
  bool solve_shls_forces = false;
  bool no_collisions = false;
  bool no_collisions_min = false;
  bool bstacking = false;
  bool bsolenoid = false;
  bool nn2 = false;
  double intrinsic_curvature = .0;
  bool bintrinsic_curvature = false;
  double intrinsic_twist = .0;
  bool bintrinsic_twist = false;
  double intrinsic_bending_torque = .0;
  bool bintrinsic_bending_torque = false;
  double intrinsic_torsion_torque = .0;
  bool bintrinsic_torsion_torque = false;
  double intrinsic_twisting_torque = .0;
  bool bintrinsic_twisting_torque = false;
  bool bdigestion = false;
  bool save_cg_nucleosome = false;
  bool use_std_thread = false;
  bool bphase_fluctuations = false;
  double kloop = .0;
  double sloop = 10. * nm / fL;
  int loop_every = 100;
  bool bloop_attraction = false;
  bool bnucleosome_dofs = false;
  bool bglobal = false;
  bool bdna_tail = false;
  double Ddna_tail = .0;
  bool use_Socol = false;
  char write_to[1000];
  int nargs = 0, arg_i;
  const option long_opts[] = {
      {"input_file", required_argument, nullptr, 'n'},
      {"mode", required_argument, nullptr, 'm'},
      {"force", required_argument, nullptr, 'f'},
      {"torque", required_argument, nullptr, 't'},
      {"nLk", required_argument, nullptr, 'd'},
      {"seed", required_argument, nullptr, 's'},
      {"iterations", required_argument, nullptr, 'i'},
      {"every", required_argument, nullptr, 'e'},
      {"every_sqd", required_argument, nullptr, 'E'},
      {"alone", required_argument, nullptr, 'a'},
      {"scale", required_argument, nullptr, 'X'},
      {"Vl_every", required_argument, nullptr, 'V'},
      {"test_Vl", required_argument, nullptr, 'T'},
      {"gyroscopic", required_argument, nullptr, 'g'},
      {"simplified_nucleosome", required_argument, nullptr, 'S'},
      {"no_nucleosome_joints", no_argument, nullptr, 'p'},
      {"nrl", required_argument, nullptr, 'l'},
      {"NRL", required_argument, nullptr, 'L'},
      {"bp_limit", required_argument, nullptr, 'b'},
      {"BP_limit", required_argument, nullptr, 'x'},
      {"model", required_argument, nullptr, 'M'},
      {"nucleosomes", required_argument, nullptr, 'N'},
      {"reversomes", required_argument, nullptr, 'r'},
      {"random_fibre", no_argument, nullptr, 'R'},
      {"stacking", required_argument, nullptr, 'k'},
      {"accurate_step", no_argument, nullptr, 'A'},
      {"step0", required_argument, nullptr, 'c'},
      {"vreset", no_argument, nullptr, 'v'},
      {"write_joint_feedback", no_argument, nullptr, 'F'},
      {"solve_shls_forces", no_argument, nullptr, 0},
      {"no_collisions", no_argument, nullptr, 1},
      {"no_collisions_min", no_argument, nullptr, 2},
      {"mstep", required_argument, nullptr, 3},
      {"digestion", no_argument, nullptr, 4},
      {"prob_h1", required_argument, nullptr, 5},
      {"prob_no_nucleosome", required_argument, nullptr, 6},
      {"prob_hexasome", required_argument, nullptr, 7},
      {"prob_tetrasome", required_argument, nullptr, 8},
      {"save_cg_nucleosome", no_argument, nullptr, 9},
      {"write_to", required_argument, nullptr, 'w'},
      {"solenoid", no_argument, nullptr, 10},
      {"intrinsic_curvature", required_argument, nullptr, 11},
      {"nn2", no_argument, nullptr, 12},
      {"intrinsic_bending_torque", required_argument, nullptr, 13},
      {"intrinsic_torsion_torque", required_argument, nullptr, 14},
      {"intrinsic_twisting_torque", required_argument, nullptr, 15},
      {"phase_fluctuations", no_argument, nullptr, 16},
      {"intrinsic_twist", required_argument, nullptr, 17},
      {"sloop", required_argument, nullptr, 18},
      {"loop_every", required_argument, nullptr, 19},
      {"nucleosome_dofs", no_argument, nullptr, 20},
      {"global", no_argument, nullptr, 21},
      {"xmass", required_argument, nullptr, 22},
      {"same_mass", no_argument, nullptr, 23},
      {"mcs", required_argument, nullptr, 24},
      {"xfriction", required_argument, nullptr, 25},
      {"dna_tail", required_argument, nullptr, 26},
      {"use_Socol", no_argument, nullptr, 27},
      {"help", optional_argument, nullptr, 'h'}};
  while ((arg_i = getopt_long(
              argc, argv,
              "n:m:f:t:d:s:i:e:E:a:X:V:T:g:S:pl:L:b:x:M:N:r:Rk:Ac:vFw:h",
              long_opts, nullptr)) != -1) {
    switch (arg_i) {
    case 'n':
      tmp = sprintf(chromatin_in, "%s", optarg);
      nargs++;
      break;
    case 'm':
      mode = (int)(atof(optarg));
      nargs++;
      break;
    case 'f':
      force = atof(optarg) * pN / fF;
      nargs++;
      break;
    case 't':
      torque = atof(optarg) * pN * nm / fE;
      nargs++;
      break;
    case 'd':
      nLk = (int)(atof(optarg));
      nargs++;
      break;
    case 's':
      seed = (int)(atof(optarg));
      nargs++;
      break;
    case 'i':
      iterations = (int)(atof(optarg));
      nargs++;
      break;
    case 'e':
      every = (int)(atof(optarg));
      nargs++;
      break;
    case 'E':
      every_sqd = (int)(atof(optarg));
      nargs++;
      break;
    case 'a':
      noSM = (strcmp(optarg, "yes") == 0);
      nargs++;
      break;
    case 'X':
      xghost = atof(optarg);
      nargs++;
      break;
    case 'V':
      Vl_every = (int)(atof(optarg));
      nargs++;
      break;
    case 'T':
      test_Vl = (strcmp(optarg, "yes") == 0);
      nargs++;
      break;
    case 'g':
      gyroscopic = (int)(atof(optarg));
      nargs++;
      break;
    case 'S':
      simplified_nucleosome = (int)(atof(optarg));
      nargs++;
      break;
    case 'p':
      nucleosome_joints = false;
      nargs++;
      break;
    case 'l':
      nrl = (int)(atof(optarg));
      nargs++;
      break;
    case 'L':
      NRL = (int)(atof(optarg));
      nargs++;
      break;
    case 'b':
      bp_limit = atof(optarg);
      nargs++;
      break;
    case 'x':
      BP_limit = atof(optarg);
      nargs++;
      break;
    case 'M':
      model = (int)(atof(optarg));
      nargs++;
      break;
    case 'N':
      nucleosomes = (int)(atof(optarg));
      nargs++;
      break;
    case 'r':
      reversomes = (int)(atof(optarg));
      nargs++;
      break;
    case 'R':
      random_fibre = true;
      nargs++;
      break;
    case 'k':
      sprintf(file_stacking, "%s", optarg);
      bsolenoid = false;
      // intrinsic_curvature=.0;
      // bintrinsic_curvature=false;
      nargs++;
      break;
    case 'A':
      accurate_step = true;
      nargs++;
      break;
    case 'c':
      step0 = (int)(atof(optarg));
      start_at_zero = false;
      nargs++;
      break;
    case 'v':
      vreset = true;
      nargs++;
      break;
    case 'F':
      write_joint_feedback = true;
      nargs++;
      break;
    case 'w':
      sprintf(write_to, "%s", optarg);
      nargs++;
      break;
    case 0:
      solve_shls_forces = true;
      nargs++;
      break;
    case 1:
      no_collisions = true;
      nargs++;
      break;
    case 2:
      no_collisions_min = true;
      nargs++;
      break;
    case 3:
      MSTEP = (int)atof(optarg);
      nargs++;
      break;
    case 4:
      bdigestion = true;
      nargs++;
      break;
    case 5:
      prob_h1 = atof(optarg);
      nargs++;
      break;
    case 6:
      prob_no_nucleosome = atof(optarg);
      nargs++;
      break;
    case 7:
      prob_hexasome = atof(optarg);
      nargs++;
      break;
    case 8:
      prob_tetrasome = atof(optarg);
      nargs++;
      break;
    case 9:
      save_cg_nucleosome = true;
      nargs++;
      break;
    case 10:
      bsolenoid = true;
      bstacking = false;
      // intrinsic_curvature=.0;
      // bintrinsic_curvature=false;
      nargs++;
      break;
    case 11:
      intrinsic_curvature = atof(optarg) * fL / nm;
      bintrinsic_curvature = true;
      // bsolenoid=false;
      // bstacking=false;
      nargs++;
      break;
    case 12:
      bsolenoid = true;
      nn2 = true;
      // bstacking=false;
      // bintrinsic_curvature=false;
      nargs++;
      break;
    case 13:
      intrinsic_bending_torque = atof(optarg) * pN * nm / fE;
      // intrinsic_bending_torque=fabs(atof(optarg))*pN*nm/fE;
      bintrinsic_bending_torque = true;
      nargs++;
      break;
    case 14:
      intrinsic_torsion_torque = atof(optarg) * pN * nm / fE;
      bintrinsic_torsion_torque = true;
      nargs++;
      break;
    case 15:
      intrinsic_twisting_torque = atof(optarg) * pN * nm / fE;
      bintrinsic_twisting_torque = true;
      nargs++;
      break;
    case 16:
      bphase_fluctuations = true;
      nargs++;
      break;
    case 17:
      intrinsic_twist = atof(optarg);
      bintrinsic_twist = true;
      nargs++;
      break;
    case 18:
      sloop = atof(optarg) * nm / fL;
      kloop = (3. * kB * temp / pow(atof(optarg) * nm, 2.)) / (fE / (fL * fL));
      bloop_attraction = true;
      nargs++;
      break;
    case 19:
      loop_every = abs((int)atof(optarg));
      bloop_attraction = true;
      nargs++;
      break;
    case 20:
      bnucleosome_dofs = true;
      nargs++;
      break;
    case 21:
      bglobal = true;
      nargs++;
      break;
    case 22:
      xmass = atof(optarg);
      nargs++;
      break;
    case 23:
      same_mass = true;
      nargs++;
      break;
    case 24:
      mcs = (int)atof(optarg);
      nargs++;
      break;
    case 25:
      xfriction = atof(optarg);
      nargs++;
      break;
    case 26:
      bdna_tail = true;
      Ddna_tail = atof(optarg) * kB * temp / fE;
      nargs++;
      break;
    case 27:
      use_Socol = true;
      nargs++;
      break;
    case 'h':
    default:
      printf("\nfibre_ODE, usage:\n");
      printf("      command to create chromatin fiber of 100 nucleosomes (147) "
             "with 21 bp between two nucleosomes.\n");
      printf("      the 21 bp is splitted in two segments of 10.5 bp each.\n");
      printf("      no broken shls, no broken docking domains and no H1:\n");
      printf(
          "      (for n in {1..100}; do echo 0 147 0 0 0 0 0; for l in {1..2}; "
          "do echo 0 10.5 -1 -1 0 0 0; done; done) >> chromatin.in\n");
      printf("      broken 6.5 shls, no broken docking domains and no H1:\n");
      printf(
          "      (for n in {1..100}; do echo 0 147 1 1 0 0 0; for l in {1..2}; "
          "do echo 0 10.5 -1 -1 0 0 0; done; done) >> chromatin.in\n\n");
      printf("      simulation of chromatin fiber:\n");
      printf("      example 1, chromatin fibre of 100 nucleosomes with a "
             "repeat of 168 bp, 21 bp linker cut into 2 monomers (gyroscopic "
             "effects on):\n");
      printf("      ./fibre_ODE -n chromatin.in -m 1 -f 0.0 -t 0.0 -s 1 -i "
             "100000 -e 10000 -a yes --scale 1.2 --Vl_every 10 -g 1 -S 0\n\n");
      printf("      example 2, reversome:\n");
      printf("      ./fibre_ODE -m 1 -f 5.0 -t 10.0 -s 1 -i 2000000 -e 2000 -a "
             "yes --scale 1.001 --Vl_every 1 -g 1 -S 0 -R -r 2 -M -1\n\n");
      printf("      input_file  (n): name of the file where to find the "
             "chromatin fibre description.\n");
      printf("                       it overwrites the number of nucleosomes "
             "given by -N argument.\n");
      printf("                       first column is the index of the chain "
             "(starting from 0).\n");
      printf("                       second column is the number of bp of the "
             "monomer. 147 tells the code to create a nucleosome.\n");
      printf("                       third and fourth columns are the number "
             "of broken SHLs.\n");
      printf("                       fifth and sixth columns are the number of "
             "broken docking domains.\n");
      printf("                       seventh column indicates if H1 (1) or not "
             "(0). It does not work for all the linker sizes.\n");
      printf("      mode        (m): if 0 the number of turns is fixed, "
             "otherwize the torque is constant.\n");
      printf("      force       (f): stretching force applied to the magnetic "
             "bead (in pN).\n");
      printf("      torque      (t): torque applied to the magnetic bead (in "
             "pN.nm).\n");
      printf("                       it is incompatible with 'mode=0'\n");
      printf("      nLk         (d): number of turns of the magnetic beads.\n");
      printf("                       it is incompatible with 'mode!=0'\n");
      printf("      nrl         (l): nucleosome repeat length in bp\n");
      printf("      NRL         (L): nucleosome repeat length in bp\n");
      printf("      bp_limit    (b):\n");
      printf("      BP_limit    (x):\n");
      printf("      seed        (s): seed for the pRNGs\n");
      printf("      iterations  (i): number of iterations\n");
      printf("      every       (e): save conformation every 'every' "
             "iterations\n");
      printf("                       stack conformation in the visualisation "
             "file\n");
      printf("      alone       (a): if yes, no bead and plane are used (no "
             "single-molecule manipulation)\n");
      printf("      scale       (X): scale the object in every dimension\n");
      printf("                       it is used for the within cut-off "
             "distance of the Verlet-list\n");
      printf("      Vl_every    (V): build the Verlet-list every this many "
             "time-steps\n");
      printf("      test_Vl     (T): test the Verlet-list implementation "
             "(--test_Vl yes)\n");
      printf("      gyroscopic  (g): 0 do not consider term dot(inertia) while "
             "1 does\n");
      printf(
          "      simplified_nucleosome (S): 0 consider all the SHLs, four "
          "helix-bundle and docking-domains, 1 and 2 are still experimental\n");
      printf("      nucleosomes (N): number of nucleosomes (see "
             "--random_fibre)\n");
      printf("      reversomes  (r): number of reversomes\n");
      printf("      model       (M): pre-defined model (starting from 0 to "
             "...)\n");
      printf("      random_fibre (R): create random fibre and run it (it "
             "overwrites the 'input_file' choice)\n");
      printf("      accurate_step (A): if -A use 'dWorldStep' instead of "
             "'dWorldQuickStep' function from 'Open-Dynamics-Engine'\n");
      printf("      step0         (c): start from conformation given by "
             "-c/--step0 ...\n");
      printf("      write_joint_feedback (F): write joint feedback (constraint "
             "from mechanicla joint or Morse potential)\n");
      printf("      stacking    (k): name of the file where to find NCP-NCP "
             "stacking parameters (it overwrites 'solenoid' argument)\n");
      printf("      MSTEP       (3): number of minimization steps (slowly "
             "increase forces amplitudes)\n");
      printf("      digestion   (4): compute digestion\n");
      printf("      prob_h1     (5): probability to add H1 (0.0 is the "
             "default)\n");
      printf("      prob_no_nucleosome (6): probability to add DNA linker "
             "instead of a nucleosome (0.0 is the default)\n");
      printf("      prob_hexasome (7): probability to add DNA linker instead "
             "of a nucleosome (0.0 is the default)\n");
      printf("      prob_tetrasome (8): probability to unwrap 1/4 of DNA on "
             "both sides of the nucleosome (0.0 is the default)\n");
      printf("      save_cg_nucleosome (9): save coarse-grained nucleosome to "
             "visualize with Blender visualisation module\n");
      printf("      solenoid    (10): build solenoid chromatin fiber with "
             "(n,n+1) interations\n");
      printf("                        if argument is equal to 1 or 2 it uses "
             "only one interaction point\n");
      printf("      intrinsic_curvature (11): add intrinsic curvature (in "
             "nm^-1) to each linkers\n");
      printf("      nn2         (12): build solenoid chromatin fiber with "
             "(n,n+2) interations\n");
      printf("      intrinsic_bending_torque (13): add bending torque > 0 (in "
             "pN.nm) to each linkers\n");
      printf("                                     if bending torque < 0 add "
             "torque along the cross-product between\n");
      printf("                                     minor-groove direction and "
             "tangent to the DNA\n");
      printf("      intrinsic_torsion_torque (14): add torsion torque (in "
             "pN.nm) to each linkers\n");
      printf("      intrinsic_twisting_torque (15): add twisting torque (in "
             "pN.nm) to each linkers\n");
      printf("      phase_fluctuations (16):\n");
      printf(
          "      intrinsic_twist (17): add intrinsic twist to each linkers\n");
      printf("      kloop (18): add attraction (spring constant in units of "
             "kB*T/nm^2 eq kg/s^2) between start and end of the loop/fiber\n");
      printf("      sloop (18): distance between two consecutive loop anchors "
             "(in nanometer)\n");
      printf("      loop_every (19): add loops of length this number of bp\n");
      printf("      global (21): use global thermostat\n");
      printf("      xmass  (22): multiply all the masses by this factor\n");
      printf("      same_xmass (23): use the same mass for all the objects\n");
      printf("      mcs    (24): number of Monte-Carlo steps divided by the "
             "number of DNA objects\n");
      printf("      xfriction (25): multiply all the friction coefficients by "
             "this factor\n");
      printf("      dna_tail  (26): intensity of the potential (in k_BT) "
             "between N-terminal tail and dna\n");
      printf("      use_Socol (27): use friction and viscosity from Socol & al, NAR 2019\n");
      printf("      Each time you run the code it writes a parameter file in "
             "the 'parameters' folder\n\n");
      printf("      For any kind of problems, please consider contact me "
             "p.carrivain_at_gmail.com.\n");
      return 0;
      break;
    }
  }

  if (bnucleosome_dofs) {
    printf("%li\n", compute_rank_of_J_nucleosome(0));
    return 0;
  }

  if (strcmp(chromatin_in, "") != 0 && random_fibre) {
    printf("random fibre and chromatin input file are incompatible.\n");
    return 0;
  }

  if (strcmp(chromatin_in, "") == 0 && model != 0 && !random_fibre) {
    printf("no chromatin input file, do nothing.\n");
    printf("please consider ./fibre_ODE -h command to see options.\n");
    return 0;
  }

  if (reversomes == 0)
    rstep = 0;

  if (step0 <= 0)
    start_at_zero = true;

  // variables: body and geom (ODE)
  dBodyID *fibre;
  dGeomID *gfibre, *ggfibre;
  dJointID *jfibre;
  dJointFeedback *fjfibre;
  int3p *dfibre;
  double *lf, *rf, *mf, *lbp, *sbp;

  // pRNG
  std::mt19937_64 mt0(seed), mt1(2 * seed), mt2(2 * seed + 1);
  std::uniform_real_distribution<> u01(.0, 1.);
  std::uniform_int_distribution<> uid(0, 1000000);

  // init ODE
  dInitODE();
  cputime = clock();

  // create the world and the spaces (ODE)
  dWorldID WorldODE = create_world_ODE(.0, wERP, wCFM, nSOR, wSOR);
  dSpaceID SpaceODE =
      hash_space((3.5721 * 1. / 10.5) * nm / fL,
                 ((noSM) ? 1. + 3.5721 + 1. : 2. * 50) * nm / fL);
  dSpaceID gSpaceODE =
      ghost_hash_space(((noSM) ? 1.01 * 3.5721 : 2. * 50) * nm / fL, xghost);
  // dSpaceID
  // SpaceODE=quadtree_space(vec3(.0,.0,.0),mult3(nucleosomes*10.*nm/fL,vec3(1.,1.,1.)),8);
  // dSpaceID
  // gSpaceODE=quadtree_space(vec3(.0,.0,.0),mult3(nucleosomes*10.*nm/fL,vec3(1.,1.,1.)),8);
  dJointGroupID contactgroup = dJointGroupCreate(0);

  // build sub-name of the files
  if (force != .0 || nLk != .0 || torque != .0)
    sprintf(ename, "F%.2f_%s%.2f_", force * fF / pN, modes[mode],
            (mode == 0) ? nLk : torque * fE / (pN * nm));
  else
    sprintf(ename, "%s", "");
  if (bdna_tail)
    sprintf(ename, "%sdnatail%fkBT_", ename, Ddna_tail * fE / (kB * temp));
  if (bintrinsic_curvature)
    sprintf(ename, "%skappa0%f_", ename, intrinsic_curvature * nm / fL);
  if (bintrinsic_twist)
    sprintf(ename, "%stw0%f_", ename, intrinsic_twist);
  if (bintrinsic_bending_torque)
    sprintf(ename, "%sbt%f_", ename, intrinsic_bending_torque * fE / (pN * nm));
  if (bintrinsic_torsion_torque)
    sprintf(ename, "%stt%f_", ename, intrinsic_torsion_torque * fE / (pN * nm));
  if (bintrinsic_twisting_torque)
    sprintf(ename, "%stwt%f_", ename,
            intrinsic_twisting_torque * fE / (pN * nm));
  if (bloop_attraction)
    sprintf(ename, "%sloop%ibp_anchor%fnm_", ename, loop_every,
            sloop * fL / nm);

  // char c1[1000],c2[500];
  // sprintf(c1,"%s","test1");
  // sprintf(c2,"%s","nexttest2");
  // unsigned int cl1=strlen(c1),stop1=0;
  // unsigned int cl2=strlen(c2),stop2=0;
  // for(unsigned int i=0;i<(cl1+1);i++){
  //   // printf("%i/%i %s\n",i,cl1,&c1[i]);
  //   if(strcmp(&c1[i],"")==0){
  //     stop1=i;
  //     break;
  //   }
  // }
  // for(unsigned int i=0;i<(cl2+1);i++){
  //   // printf("%i/%i %s\n",i,cl2,&c2[i]);
  //   if(strcmp(&c2[i],"")==0){
  //     stop2=i;
  //     break;
  //   }
  // }
  // printf("%s %s\n",c1,c2);
  // for(unsigned int i=0;i<stop2;i++){
  //   c1[stop1+i]=c2[i];
  //   // printf("%s(%i %i) %s(%i %i) ->
  //   %i\n\n",c1,stop1,cl1,c2,stop2,cl2,strlen(c1));
  // }
  // strncpy(&c1[stop1],c2,stop2);
  // printf("strncpy %s(%i %i) %s(%i %i) ->
  // %i\n\n",c1,stop1,cl1,c2,stop2,cl2,strlen(c1));
  // memcpy(&c1[stop1],&c2[0],stop2);
  // printf("memcpy %s(%i %i) %s(%i %i) ->
  // %i\n\n",c1,stop1,cl1,c2,stop2,cl2,strlen(c1)); return 0;

  double *angle_per_nrl = new double[207 - 147 + 1]();

  // create chromatin fibre
  vec3 *uvth = new vec3[1]();
  vec3 *uvth1 = new vec3[1]();
  int *h1_to_dna = new int[1]();
  int *starts = new int[3]();
  int *ends = new int[3]();
  double lp2r = (3.5721 + 2.) * nm / fL;
  int F = 0, J = 0, FH = 0, B = 0, nH1 = 0, nfail = 0, *chromatin_size = NULL;
  int *seeds = new int[nucleosomes + 1]();
  if (random_fibre) {
    if (model <= 0 && reversomes == 0) {
      printf("ask for reversome model but number of reversomes is 0\n");
      return 0;
    }
    if (no_collisions)
      tmp = sprintf(fiber_type, "%s", "phantom_chromatin");
    else
      tmp = sprintf(fiber_type, "%s", "chromatin");
    if (bsolenoid) {
      tmp = sprintf(fiber_type, "%s", "solenoidnn1");
      if (nn2)
        tmp = sprintf(fiber_type, "%s", "solenoidnn2");
    }
    // store seed of phase fluctuations pRNG (one per linker)
    for (int n = 0; n < (nucleosomes + 1); n++)
      seeds[n] = uid(mt0);
    min_lbp = 1.;
    F = 0;
    bool allnuc = (mcs > 0);
    while (1) {
      // try to add one nucleosome to the chromatin fibre
      if (model < 0) {
        // reversome nrl bp
        if (nucleosome_joints)
          sprintf(chromatin_in, "rev_%s%i_nrl%ibp_n%i.in", fiber_type,
                  nucleosomes + reversomes, nrl, seed);
        else {
          simplified_nucleosome = 0;
          sprintf(chromatin_in, "Mrev_%s%i_nrl%ibp_n%i.in", fiber_type,
                  nucleosomes + reversomes, nrl, seed);
        }
        create_random_fibre((allnuc) ? nucleosomes : 1,
                            nucleosomes + reversomes, nrl, nrl, bp_limit,
                            BP_limit, 0, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", prob_h1,
                            prob_no_nucleosome); // std::ref(mt0));
      }
      if (model == 0) {
        // reversome nrl bp
        if (nucleosome_joints)
          sprintf(chromatin_in, "frev%i_%s%i_nrl%ibp_n%i.in",
                  simplified_nucleosome, fiber_type, nucleosomes + reversomes,
                  nrl, seed);
        else {
          simplified_nucleosome = 0;
          sprintf(chromatin_in, "Mrev%i_%s%i_nrl%ibp_n%i.in",
                  simplified_nucleosome, fiber_type, nucleosomes + reversomes,
                  nrl, seed);
        }
        create_random_fibre(
            (allnuc) ? nucleosomes : 1, nucleosomes + reversomes, nrl, nrl,
            bp_limit, BP_limit, 0, 0, false, chromatin_in, mt0,
            (F == 0) ? "w" : "a", prob_h1, 0); // std::ref(mt0));
      }
      sprintf(extra, "%s", "_");
      if (prob_no_nucleosome != .0) {
        sprintf(cbuffer, "%spnn%f_", extra, prob_no_nucleosome);
        sprintf(extra, "%s", cbuffer);
      }
      if (prob_hexasome != .0) {
        sprintf(cbuffer, "%sphexasome%f_", extra, prob_hexasome);
        sprintf(extra, "%s", cbuffer);
      }
      if (prob_tetrasome != .0) {
        sprintf(cbuffer, "%sptetrasome%f_", extra, prob_tetrasome);
        sprintf(extra, "%s", cbuffer);
      }
      if (strcmp(ename, "") != 0) {
        sprintf(cbuffer, "%s%s", extra, ename);
        sprintf(extra, "%s", cbuffer);
      }
      if (model == 1) {
        // nrl to NRL
        sprintf(chromatin_in, "%s%i_nrl%ito%ibp_by%fto%fbp_h1%f%sn%i.in",
                fiber_type, nucleosomes, nrl, NRL, bp_limit, BP_limit, prob_h1,
                extra, seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, 0, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", prob_h1, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 2) {
        // nrl to NRL, break shls 6.5
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%sbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, nrl, NRL, bp_limit, BP_limit, extra, seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, 1, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 3) {
        // nrl to NRL, break shls 6.5 at random
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%srbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, nrl, NRL, bp_limit, BP_limit, extra, seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, -1, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 4) {
        // nrl to NRL, break shls 6.5 to 5.5
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%sbreakshls6.5to5.5_n%i.in",
                fiber_type, nucleosomes, nrl, NRL, bp_limit, BP_limit, extra,
                seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, 2, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 5) {
        // nrl to NRL, break shls 6.5 to 5.5 at random
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%srbreakshls6.5to5.5_n%i.in",
                fiber_type, nucleosomes, nrl, NRL, bp_limit, BP_limit, extra,
                seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, -2, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 6) {
        // nrl to NRL, break shls 6.5 to 4.5
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%sbreakshls6.5to4.5_n%i.in",
                fiber_type, nucleosomes, nrl, NRL, bp_limit, BP_limit, extra,
                seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, 3, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 7) {
        // nrl to NRL, break shls 6.5 to 4.5 at random
        sprintf(chromatin_in,
                "%s%i_nrl%ito%ibp_by%fto%fbp%sbreakshls6.5to4.5_n%i.in",
                fiber_type, nucleosomes, nrl, NRL, bp_limit, BP_limit, extra,
                seed);
        create_random_fibre((allnuc) ? nucleosomes : 1, nucleosomes, nrl, NRL,
                            bp_limit, BP_limit, -3, 0, false, chromatin_in, mt0,
                            (F == 0) ? "w" : "a", 0, prob_no_nucleosome,
                            prob_hexasome, prob_tetrasome); // std::ref(mt0));
      }
      if (model == 14) {
        // use nrl marginal density
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts14.out", 1);
        sprintf(chromatin_in, "%s%i_%s_n%i.in", fiber_type, nucleosomes,
                chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 0, 0,
                               false, prob_h1, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 141) {
        // use nrl marginal density, break shls 6.5
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts14.out", 1);
        sprintf(chromatin_in, "%s%i_%s_breakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 142) {
        // use nrl marginal density, break shls 6.5 at random
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts14.out", 1);
        sprintf(chromatin_in, "%s%i_%s_rbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, -1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 25) {
        // use nrl marginal density
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts25.out", 1);
        sprintf(chromatin_in, "%s%i_%s_n%i.in", fiber_type, nucleosomes,
                chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 0, 0,
                               false, prob_h1, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 251) {
        // use nrl marginal density, break shls 6.5
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts25.out", 1);
        sprintf(chromatin_in, "%s%i_%s_breakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 252) {
        // use nrl marginal density, break shls 6.5 at random
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts25.out", 1);
        sprintf(chromatin_in, "%s%i_%s_rbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, -1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 32) {
        // use nrl marginal density
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts32.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_n%i.in", fiber_type, nucleosomes,
                chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 0, 0,
                               false, prob_h1, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 321) {
        // use nrl marginal density, break shls 6.5
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts32.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_breakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 322) {
        // use nrl marginal density, break shls 6.5 at random
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts32.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_rbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, -1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 1425) {
        // use nrl marginal density
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts1425.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_n%i.in", fiber_type, nucleosomes,
                chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 0, 0,
                               false, prob_h1, chromatin_in, mt0,
                               (F == 0) ? "w" : "a", .0, .0, .0);
      }
      if (model == 14251) {
        // use nrl marginal density, break shls 6.5
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts1425.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_breakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 14252) {
        // use nrl marginal density, break shls 6.5 at random
        sprintf(chromatin_out, "best_nrl_density_seed%i_allN_linker_ts1425.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_rbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, -1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 142532) {
        // use nrl marginal density
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts142532.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_n%i.in", fiber_type, nucleosomes,
                chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 0, 0,
                               false, prob_h1, chromatin_in, mt0,
                               (F == 0) ? "w" : "a", .0, .0, .0);
      }
      if (model == 1425321) {
        // use nrl marginal density, break shls 6.5
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts142532.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_breakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, 1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      if (model == 1425322) {
        // use nrl marginal density, break shls 6.5 at random
        sprintf(chromatin_out, "best_nrl_density_seed%i_2N_linker_ts142532.out",
                1);
        sprintf(chromatin_in, "%s%i_%s_rbreakshls6.5_n%i.in", fiber_type,
                nucleosomes, chromatin_out, seed);
        create_fibre_from_data((allnuc) ? nucleosomes : 1, chromatin_out, -1, 0,
                               false, .0, chromatin_in, mt0,
                               (F == 0) ? "w" : "a");
      }
      // first try: check if collision(s)
      // second try: add DNA phase fluctuations
      i0 = 0;
      while (1) {
        for (int n = 0; n < (nucleosomes + 1); n++)
          if (0 || (n == 0 && nrl == NRL) || nrl != NRL)
            seeds[n] = uid(mt0);
          else
            seeds[n] = seeds[0];
        if (i0 == 0)
          chromatin_size = create_chromatin_fibre(
              chromatin_in, simplified_nucleosome, !noSM, &fibre, &gfibre,
              &ggfibre, dfibre, &jfibre, &fjfibre, WorldODE, SpaceODE,
              gSpaceODE, &lf, &rf, &mf, &lbp, &sbp, xghost, gyroscopic, dofs,
              true, .0, .0, seeds, intrinsic_twist, false);
        else
          chromatin_size = create_chromatin_fibre(
              chromatin_in, simplified_nucleosome, !noSM, &fibre, &gfibre,
              &ggfibre, dfibre, &jfibre, &fjfibre, WorldODE, SpaceODE,
              gSpaceODE, &lf, &rf, &mf, &lbp, &sbp, xghost, gyroscopic, dofs,
              true, d0, d1, seeds, intrinsic_twist, true);
        F = chromatin_size[0];
        FH = F + chromatin_size[3] * NHC;
        B = chromatin_size[1];
        J = chromatin_size[2];
        nH1 = chromatin_size[4];
        // initialize data
        initialize_body_data(&dfibre, B, 20);
        // compute min(lbp)
        // in the case of regular fiber do it only for the first iteration
        if ((i0 == 0 && nrl == NRL) || nrl != NRL) {
          min_lbp = DBL_MAX;
          for (int f = 0; f < F; f++)
            min_lbp = fmin(min_lbp, lbp[f]);
        }
        // compute start of each nucleosome
        // in the case of regular fiber do it only for the first iteration
        if ((i0 == 0 && nrl == NRL) || nrl != NRL) {
          delete[] nucleosome_start;
          nucleosome_start = NULL;
          nucleosome_start = dna_histone_starts(chromatin_in);
        }
        if (mcs > 0)
          break;
        // compute average angle between two consecutive nucleosomes
        if (1) {
          d0 = .0;
          na = 0;
          for (int i = 0; i < (nucleosomes - 1); i++) {
            bn1 = normalize3(cross_product(
                FrameODE(fibre[nucleosome_start[i]], 2),
                FrameODE(fibre[nucleosome_start[i] + NNC - 1], 2)));
            bn2 = normalize3(cross_product(
                FrameODE(fibre[nucleosome_start[i + 1]], 2),
                FrameODE(fibre[nucleosome_start[i + 1] + NNC - 1], 2)));
            bn = normalize3(cross_product(bn2, bn1));
            // number of monomers of the current linker
            na +=
                (nucleosome_start[i + 1] - 1 - (nucleosome_start[i] + NNC) + 1);
            // if(nrl!=NRL && i0==0)
            //   printf("angle=%f/%f nrl=%i+147
            //   (%f)\n",arccos(dot_product(bn2,bn1)),angle_per_nrl[(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC])],(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC]),dot_product(FrameODE(fibre[nucleosome_start[i]+NNC-1],2),bn));
            // if(nrl!=NRL && i0>0)
            //   printf("angle=%f/%f
            //   nrl=%i+147\n",arccos(dot_product(bn2,bn1)),angle_per_nrl[(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC])],(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC]));
            // if(nrl==NRL && i0>0)
            //   printf("angle=%f/%f
            //   nrl=%i+147\n",arccos(dot_product(bn2,bn1)),angle_per_nrl[(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC])],(int)rint(sbp[nucleosome_start[i+1]]-sbp[nucleosome_start[i]+NNC]));
            if (i0 == 0) {
              if (dot_product(FrameODE(fibre[nucleosome_start[i] + NNC - 1], 2),
                              bn) < .0)
                angle_per_nrl[(int)rint(sbp[nucleosome_start[i + 1]] -
                                        sbp[nucleosome_start[i] + NNC])] =
                    -arccos(dot_product(bn2, bn1));
              else
                angle_per_nrl[(int)rint(sbp[nucleosome_start[i + 1]] -
                                        sbp[nucleosome_start[i] + NNC])] =
                    arccos(dot_product(bn2, bn1));
            }
            d0 += arccos(dot_product(bn2, bn1));
          }
          d0 /= (double)na;
          d2 = .0;
          for (int i = 0; i < (nucleosomes - 1); i++) {
            bn1 = normalize3(cross_product(
                FrameODE(fibre[nucleosome_start[i]], 2),
                FrameODE(fibre[nucleosome_start[i] + NNC - 1], 2)));
            bn2 = normalize3(cross_product(
                FrameODE(fibre[nucleosome_start[i + 1]], 2),
                FrameODE(fibre[nucleosome_start[i + 1] + NNC - 1], 2)));
            d2 += (pow(arccos(dot_product(bn2, bn1)), 2.) - d0 * d0);
          }
          d2 /= (double)na;
        }
        if (i0 == 0) {
          // printf("%s angle=%f(+/-%f)\n",chromatin_in,d0,sqrt(d2));
          // d1=d0+sqrt(d2);
          // d0-=sqrt(d2);
          d0 = -.002 * pi;
          d1 = .002 * pi;
          // d0=d1=.0;
          // for(int i=0;i<61;i++)
          //   printf("angle_per_nrl[%i]=%f;\n",i,angle_per_nrl[i]);
          // return 0;
        }
        // if h1 do not collide against dGeomID
        starts[0] = 0;
        ends[0] = FH - 1;
        starts[1] = 0;
        ends[1] = -1;
        starts[2] = 0;
        ends[2] = -1;
        new_nc = partition_v0(fibre, gfibre, FH, starts, ends, WorldODE,
                              contactgroup, lp2r, sum_abs_depth, max_abs_depth,
                              true);
        // for segmentation between 1 and 3 bp per DNA segment, length is lesser
        // than radius therefore we remove collision(s) between segment 'i' and
        // segment 'i+x'
        if (min_lbp < 6.)
          remove_artificial_collisions(chromatin_size[3], F, sbp, lbp, rf,
                                       fibre, gfibre, new_nc, sum_abs_depth);
        if (contactgroup)
          dJointGroupEmpty(contactgroup);
        nc = (i0 == 0) ? new_nc : min(nc, new_nc);
        printf("%s: i=%i %i(min=%i) collision(s)\n", chromatin_in, i0, new_nc,
               nc);
        // do not need to use DNA phase fluctuations
        if (new_nc == 0 || mcs > 0)
          break;
        if (new_nc > 0 && mcs == 0) {
          // clean previous try
          clean_body_data(&dfibre, B);
          for (int f = 0; f < B; f++) {
            tmp = dBodyGetNumJoints(fibre[f]);
            for (int j = (tmp - 1); j >= 0; j--)
              dJointDestroy(dBodyGetJoint(fibre[f], j));
            dBodyDestroy(fibre[f]);
            dGeomDestroy(gfibre[f]);
            dGeomDestroy(ggfibre[f]);
          }
        }
        i0++;
      }
      if (mcs > 0)
        break;
      if (nfail > 0)
        printf("%s nc=%i sum(depth)=%f nm %i fail(s)\n\n", chromatin_in, nc,
               sum_abs_depth * fL / nm, nfail);
      // does it use MCS to remove overlap(s) ?
      if (mcs > 0)
        nc = 0;
      if (mcs == 0) {
        if (nc == 0) {
          // std::copy since C++ 17
          sprintf(name, "backup/backup%i_%s", chromatin_size[3], chromatin_in);
          // std::filesystem::copy(chromatin_in, name,
          // std::filesystem::copy_options::update_existing);
          fbackup = NULL;
          while (fbackup == NULL)
            fbackup = fopen(name, "w");
          fcurrent = NULL;
          while (fcurrent == NULL)
            fcurrent = fopen(chromatin_in, "r");
          while (!feof(fcurrent)) {
            fscanf(fcurrent, "%i %f %i %i %i %i %i\n", &i5, &f0, &i0, &i1, &i2,
                   &i3, &i4);
            fprintf(fbackup, "%i %f %i %i %i %i %i\n", i5, f0, i0, i1, i2, i3,
                    i4);
          }
          printf("before fclose\n");
          fclose(fbackup);
          fclose(fcurrent);
          seeds[chromatin_size[3]] = uid(mt0);
        } else {
          // back to valid conformation
          i0 = (nfail == 1) ? 2 : 1;
          sprintf(name, "backup/backup%i_%s", max(1, chromatin_size[3] - i0),
                  chromatin_in);
          nfail = (nfail == 1) ? 0 : nfail + 1;
          // restart from scratch or restart from backup
          if ((chromatin_size[3] - i0) < 1)
            F = 0;
          else
            seeds[chromatin_size[3] - i0 - (int)(nucleosome_start[0] == 0)] =
                uid(mt0);
          // turn on phase fluctuations
          if (!bphase_fluctuations) {
            printf("turn on phase fluctuations.\n");
            F = 0;
            bphase_fluctuations = true;
            nfail = 0;
          }
          if (F > 0) {
            fbackup = NULL;
            while (fbackup == NULL)
              fbackup = fopen(name, "r");
            fcurrent = NULL;
            while (fcurrent == NULL)
              fcurrent = fopen(chromatin_in, "w");
            while (!feof(fbackup)) {
              fscanf(fbackup, "%i %f %i %i %i %i %i\n", &i5, &f0, &i0, &i1, &i2,
                     &i3, &i4);
              fprintf(fcurrent, "%i %f %i %i %i %i %i\n", i5, f0, i0, i1, i2,
                      i3, i4);
            }
            fclose(fbackup);
            fclose(fcurrent);
          } else {
            // store seed of phase fluctuations pRNG
            for (int n = 0; n < (nucleosomes + 1); n++)
              seeds[n] = uid(mt0);
          }
        }
      }
      if (chromatin_size[3] < (nucleosomes + reversomes) ||
          (chromatin_size[3] == (nucleosomes + reversomes) && nc > 0)) {
        // clean previous try
        clean_body_data(&dfibre, B);
        for (int f = 0; f < B; f++) {
          tmp = dBodyGetNumJoints(fibre[f]);
          for (int j = (tmp - 1); j >= 0; j--)
            dJointDestroy(dBodyGetJoint(fibre[f], j));
          dBodyDestroy(fibre[f]);
          dGeomDestroy(gfibre[f]);
          dGeomDestroy(ggfibre[f]);
        }
      }
      if (chromatin_size[3] == (nucleosomes + reversomes) && nc == 0) {
        printf("%i/%i nucleosomes and %i collision.\n", chromatin_size[3],
               nucleosomes + reversomes, nc);
        break;
      }
    }
    if (nucleosomes == 0) {
      // only dna
      sprintf(chromatin_in, "dna%ibp_by_%fbp_n%i.in", nrl, bp_limit, seed);
      create_random_fibre(0, 0, nrl, nrl, bp_limit, BP_limit, 0, 0, false,
                          chromatin_in, mt0, "w"); // std::ref(mt0));
      chromatin_size = create_chromatin_fibre(
          chromatin_in, simplified_nucleosome, !noSM, &fibre, &gfibre, &ggfibre,
          dfibre, &jfibre, &fjfibre, WorldODE, SpaceODE, gSpaceODE, &lf, &rf,
          &mf, &lbp, &sbp, xghost, gyroscopic, dofs, true, .0, .0, NULL,
          intrinsic_twist);
      F = chromatin_size[0];
      FH = F + chromatin_size[3] * NHC;
      B = chromatin_size[1];
      J = chromatin_size[2];
      nH1 = chromatin_size[4];
      initialize_body_data(&dfibre, B, 20);
    }
    nucleosomes = chromatin_size[3];
  } else {
    // check linker segmentation of the chromatin input file
    check_chromatin_input_file(chromatin_in, 10.5);
    chromatin_size = create_chromatin_fibre(
        chromatin_in, simplified_nucleosome, !noSM, &fibre, &gfibre, &ggfibre,
        dfibre, &jfibre, &fjfibre, WorldODE, SpaceODE, gSpaceODE, &lf, &rf, &mf,
        &lbp, &sbp, xghost, gyroscopic, dofs, true, -.005 * pi, .005 * pi, NULL,
        intrinsic_twist, true);
    F = chromatin_size[0];
    FH = F + chromatin_size[3] * NHC;
    B = chromatin_size[1];
    J = chromatin_size[2];
    nucleosomes = chromatin_size[3];
    nH1 = chromatin_size[4];
    initialize_body_data(&dfibre, B, 20);
  }

  // compute min(lbp)
  // in the case of regular fiber do it only for the first iteration
  min_lbp = DBL_MAX;
  for (int f = 0; f < F; f++)
    min_lbp = fmin(min_lbp, lbp[f]);
  // compute start of each nucleosome
  // in the case of regular fiber do it only for the first iteration
  delete[] nucleosome_start;
  nucleosome_start = NULL;
  nucleosome_start = dna_histone_starts(chromatin_in);

  // compute loop anchors
  // first monomer is anchor
  int nanchors = 1;
  d0 = .0;
  for (int f = 0; f < F; f++) {
    d0 += lbp[f];
    if (d0 > loop_every) {
      d0 = .0;
      nanchors++;
    }
  }
  int *anchors = new int[nanchors]();
  anchors[0] = 0;
  i0 = 1;
  d0 = .0;
  for (int f = 0; f < F; f++) {
    d0 += lbp[f];
    if (d0 > loop_every) {
      anchors[i0] = f;
      d0 = .0;
      i0++;
    }
  }
  // last anchor corresponds to the end of the chromatin fiber
  anchors[nanchors - 1] = F - 1;

  // does a initial conformation exist ?
  dLk = .0;
  if (start_at_zero)
    tmp = 0;
  else {
    tmp = sprintf(name, "conformations/cn%i_%s_s%i.out", seed, chromatin_in,
                  step0);
    tmp = read_write_conformation(fibre, name, "read", 0, B);
  }
  if (tmp > 0)
    printf("initial conformation %s\n", name);
  else {
    start_at_zero = true;
    step0 = 0;
  }
  locked =
      (mode == 0 && ((nLk < 0. && dLk <= nLk) || (nLk >= 0. && dLk >= nLk)));

  // run MC steps to resolve overlap(s)
  old_ui = new vec3[B]();
  old_vi = new vec3[B]();
  old_ti = new vec3[B]();
  old_pi = new vec3[B]();
  new_ui = new vec3[B]();
  new_vi = new vec3[B]();
  new_ti = new vec3[B]();
  new_pi = new vec3[B]();
  for (int f = 0; f < B; f++) {
    old_ui[f] = FrameODE(fibre[f], 0);
    old_vi[f] = FrameODE(fibre[f], 1);
    old_ti[f] = FrameODE(fibre[f], 2);
    old_pi[f] = PositionBodyODE(fibre[f]);
  }
  // compute relative positions of the histones
  if (uvth == NULL)
    delete[] uvth;
  uvth = new vec3[nucleosomes * NHC]();
  for (int n = 0; n < nucleosomes; n++) {
    for (int h = (F + n * NHC); h < (F + (n + 1) * NHC); h++) {
      rij = sub3(PositionBodyODE(fibre[h]),
                 PositionBodyODE(fibre[nucleosome_start[n]]));
      uvth[h - F].x = dot_product(rij, FrameODE(fibre[nucleosome_start[n]], 0));
      uvth[h - F].y = dot_product(rij, FrameODE(fibre[nucleosome_start[n]], 1));
      uvth[h - F].z = dot_product(rij, FrameODE(fibre[nucleosome_start[n]], 2));
    }
  }
  // compute relative position of h1 writing wrt dna position
  if (h1_to_dna == NULL)
    delete[] h1_to_dna;
  h1_to_dna = new int[2 * nH1]();
  if (uvth1 == NULL)
    delete[] uvth1;
  uvth1 = new vec3[nH1]();
  for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
       h++) {
    // h1 is attached to two dna monomers
    i0 = 0;
    for (int f = 0; f < F; f++) {
      if (dAreConnected(fibre[h], fibre[f])) {
        h1_to_dna[2 * (h - F - nucleosomes * NHC) + i0] = f;
        i0++;
      }
      if (i0 == 2)
        break;
    }
    // compute relative position
    i2 = h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0];
    rij = sub3(PositionBodyODE(fibre[h]), PositionBodyODE(fibre[i2]));
    uvth1[h - F - nucleosomes * NHC].x =
        dot_product(rij, FrameODE(fibre[i2], 0));
    uvth1[h - F - nucleosomes * NHC].y =
        dot_product(rij, FrameODE(fibre[i2], 1));
    uvth1[h - F - nucleosomes * NHC].z =
        dot_product(rij, FrameODE(fibre[i2], 2));
  }
  // write size of the bodies
  // dna
  tmp = sprintf(name, "visualisation/sn%i_%s.out", seed, chromatin_in);
  fOut = fopen(name, "w");
  if (bloop_attraction) {
    for (int f = 0; f < F; f++) {
      i0 = 0;
      // use one color per loop
      for (int l = 0; l < (nanchors - 1); l++)
        if ((f >= anchors[l] && f < anchors[l + 1]) ||
            f == anchors[nanchors - 1])
          i0 = l + 1;
      i1 = 0;
      for (int l = 0; l < nanchors; l++)
        if (f == anchors[l])
          i1 = 1;
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", (i1 == 0) ? 0 : 1,
                    (i1 == 0) ? rf[f] : sloop, (i1 == 0) ? lf[f] : sloop, f,
                    (i1 == 0) ? i0 : 0, 0);
    }
    // histones
    for (int n = 0; n < nucleosomes; n++) {
      i0 = 0;
      for (int l = 0; l < (nanchors - 1); l++)
        if (nucleosome_start[n] >= anchors[l] &&
            nucleosome_start[n] < anchors[l + 1])
          i0 = l + 1;
      for (int i = 0; i < 4; i++)
        tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, F + n * NHC + i,
                      i0, 0);
    }
    // h1
    for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
         h++) {
      i0 = 0;
      for (int l = 0; l < (nanchors - 1); l++)
        if (h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0] >= anchors[l] &&
            h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0] < anchors[l + 1])
          i0 = l + 1;
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, rf[h], rf[h], h, i0, 0);
    }
  } else {
    for (int f = 0; f < F; f++)
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[f], lf[f], f, 0, 0);
    // histones
    for (int n = 0; n < nucleosomes; n++)
      for (int i = 0; i < 4; i++)
        tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, F + n * NHC + i,
                      11 * (i == 0 || i == 3) + 12 * (i == 1 || i == 2), 0);
    // h1
    for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
         h++)
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, rf[h], rf[h], h, 4, 0);
  }
  fclose(fOut);
  // compute min(lbp)
  min_lbp = DBL_MAX;
  for (int f = 0; f < F; f++)
    min_lbp = fmin(min_lbp, lbp[f]);
  // compute start of each nucleosome
  delete[] nucleosome_start;
  nucleosome_start = NULL;
  nucleosome_start = dna_histone_starts(chromatin_in);
  delete[] is_linker_nucleosome;
  is_linker_nucleosome = NULL;
  is_linker_nucleosome = is_linker_or_nucleosome(chromatin_in);
  // compute bending and twisting constants
  delete[] gb;
  gb = new double[J]();
  delete[] gt;
  gt = new double[J]();
  bending_twisting_constants(2. * lp, lt, lf, &gb, &gt, J, fE / (kB * temp),
                             true);
  // compute intrinsic twist
  delete[] b0;
  b0 = new double[F - 1]();
  delete[] tw0;
  tw0 = tw_e_chromatin_fibre(chromatin_in);
  // add intrinsic twist
  if (bintrinsic_twist && intrinsic_twist != .0)
    for (int f = 0; f < (F - 1); f++)
      if (is_linker_nucleosome[f] < 0 && is_linker_nucleosome[f + 1] < 0)
        tw0[f] += intrinsic_twist;
  // compute number of collision(s) before Monte-Carlo steps
  starts[0] = 0;
  ends[0] = FH - 1;
  starts[1] = 0;
  ends[1] = -1;
  starts[2] = 0;
  ends[2] = -1;
  nc = partition_v0(fibre, gfibre, FH, starts, ends, WorldODE, contactgroup,
                    lp2r, sum_abs_depth, max_abs_depth, true);
  // for segmentation between 1 and 3 bp per DNA segment, length is lesser than
  // radius therefore we remove collision(s) between segment 'i' and segment
  // 'i+x'
  if (min_lbp < 6.)
    remove_artificial_collisions(nucleosomes, F, sbp, lbp, rf, fibre, gfibre,
                                 nc, sum_abs_depth);
  if (contactgroup)
    dJointGroupEmpty(contactgroup);
  // compute elastic energy
  oldE = .0;
  for (int f = 0; f < (F - 1); f++)
    oldE +=
        gb[f] *
            (1. - cos(arccos(local_cosine_of_bending(fibre[f], fibre[f + 1])) -
                      b0[f])) +
        .5 * gt[f] * pow(local_twist(fibre[f], fibre[f + 1], tw0[f]), 2.);
  // compute loop energy
  if (bloop_attraction && kloop != .0)
    for (int n = 0; n < (nanchors - 1); n++)
      oldE += .5 * kloop *
              dot_product2(sub3(PositionBodyODE(fibre[anchors[n]]),
                                PositionBodyODE(fibre[anchors[n + 1]])));
  newE = oldE;
  // find conformation without overlaps
  dVector3 a1, a2;
  nacc[0] = nacc[1] = ntry = i0 = i1 = i2 = i3 = 0;
  bool *bscaffold = new bool[nanchors - 1]();
  // compute maximal number of bonds between
  // nucleosome 'n' and nucleosome 'n+m"
  switch (nucleosomes) {
  case 0:
    i3 = F;
    break;
  case 1:
    i3 = F;
    break;
  default:
    for (int i = 0; i < (nucleosomes - 1); i++)
      i3 = max(i3, nucleosome_start[i + 1] + NNC - nucleosome_start[i]);
    break;
  }
  // are Crankshaft, Pivot moves allowed ?
  int npfree_to_move = 0, nc1free_to_move = 0, nc2free_to_move = 0;
  int *c1free_to_move = new int[F]();
  int *c2free_to_move = new int[F]();
  int *pfree_to_move = new int[F]();
  for (int i = 0; i < F; i++) {
    if (is_linker_nucleosome[i] < 0) {
      // dna linker
      // Crankshaft move (first point)
      i0 = 1;
      for (int h = 0; h < nH1; h++)
        if (i > h1_to_dna[2 * h + 0] && i <= h1_to_dna[2 * h + 1])
          i0 = 0;
      if (i0 == 1) {
        c1free_to_move[nc1free_to_move] = i;
        nc1free_to_move++;
      }
      // Crankshaft move (second point)
      i0 = 1;
      for (int h = 0; h < nH1; h++)
        if (i >= h1_to_dna[2 * h + 0] && i < h1_to_dna[2 * h + 1])
          i0 = 0;
      if (i0 == 1) {
        c2free_to_move[nc2free_to_move] = i;
        nc2free_to_move++;
      }
      // Pivot move
      i0 = 1;
      for (int h = 0; h < nH1; h++)
        if (i > h1_to_dna[2 * h + 0] && i <= h1_to_dna[2 * h + 1])
          i0 = 0;
      if (i0 == 1) {
        pfree_to_move[npfree_to_move] = i;
        npfree_to_move++;
      }
    } else {
      if (i == nucleosome_start[is_linker_nucleosome[i] - 1]) {
        // fisrt nucleosomal dna monomer
        // Crankshaft move (first point)
        i0 = 1;
        for (int h = 0; h < nH1; h++)
          if (i > h1_to_dna[2 * h + 0] && i <= h1_to_dna[2 * h + 1])
            i0 = 0;
        if (i0 == 1) {
          c1free_to_move[nc1free_to_move] = i;
          nc1free_to_move++;
        }
        // Crankshaft move (second point)
        // Pivot move
        i0 = 1;
        for (int h = 0; h < nH1; h++)
          if (i > h1_to_dna[2 * h + 0] && i <= h1_to_dna[2 * h + 1])
            i0 = 0;
        if (i0 == 1) {
          pfree_to_move[npfree_to_move] = i;
          npfree_to_move++;
        }
      }
      if (i == nucleosome_start[is_linker_nucleosome[i] + NNC - 1]) {
        // fisrt nucleosomal dna monomer
        // Crankshaft move (first point)
        // Crankshaft move (second point)
        i0 = 0;
        for (int h = 0; h < nH1; h++)
          if (i >= h1_to_dna[2 * h + 0] && i < h1_to_dna[2 * h + 1])
            i0 = 0;
        if (i0 == 1) {
          c2free_to_move[nc2free_to_move] = i;
          nc2free_to_move++;
        }
        // Pivot move
      }
    }
  }
  while (start_at_zero) {
    t0 = clock();
    // Crankshaft or Pivot move
    pivot = u01(mt0) < (.5 - .4 * (scaffold));
    // ???
    for (int n = (nanchors - 2); n >= 0; n--) {
      if (!bscaffold[n]) {
        na = anchors[n];
        break;
      }
    }
    na = 0;
    // ???
    // pick one monomer at random
    // consider the case of 0 bp linker
    // it is possible to choose first nucleosomal DNA monomer
    if (pivot) {
      i0 = na - 1;
      while (i0 < na)
        i0 = pfree_to_move[(int)(npfree_to_move * u01(mt0))];
      // i0=pfree_to_move[(int)(npfree_to_move*u01(mt0))];
      i1 = F - 1;
    } else {
      i0 = i1 = 0;
      // do not choose nucleosome start for the Crankshaft end point
      while (i0 >= i1 || abs(i0 - i1) <= 1 || abs(i0 - i1) > i3 || i1 >= F) {
        i0 = na - 1;
        while (i0 < na) {
          i2 = (int)(nc1free_to_move * u01(mt0));
          i0 = c1free_to_move[i2];
        }
        i1 =
            c2free_to_move[min(i2 + (int)(i3 * u01(mt0)), nc2free_to_move - 1)];
      }
    }
    // look for the nucleosome
    if (i0 >= 0 && i0 <= nucleosome_start[0])
      nuci = 0;
    if (i0 > nucleosome_start[nucleosomes - 1])
      nuci = nucleosomes;
    if (i1 >= 0 && i1 <= nucleosome_start[0])
      nucj = 0;
    if (i1 > nucleosome_start[nucleosomes - 1])
      nucj = nucleosomes;
    for (int n = 0; n < (nucleosomes - 1); n++) {
      if (i0 > nucleosome_start[n] && i0 <= nucleosome_start[n + 1])
        nuci = n + 1;
      if (i1 > nucleosome_start[n] && i1 <= nucleosome_start[n + 1])
        nucj = n + 1;
    }
    // draw 3d random vector
    if (pivot)
      ta = random_vec3(mt0, u01);
    else
      ta = normalize3(
          sub3(PositionPosRelBodyODE(fibre[i1], .0, .0, .5 * lf[i1]),
               PositionPosRelBodyODE(fibre[i0], .0, .0, -.5 * lf[i0])));
    p0 = PositionPosRelBodyODE(fibre[i0], .0, .0, -.5 * lf[i0]);
    // random angle
    // R*dtheta<2 nm
    if (pivot) {
      p0 = PositionPosRelBodyODE(fibre[i0], .0, .0, -.5 * lf[i0]);
      d0 = .0;
      for (int i = i0; i < F; i++)
        d0 = fmax(
            d0, norm3(sub3(PositionPosRelBodyODE(fibre[i], .0, .0, .5 * lf[i]),
                           p0)));
    } else {
      d0 = .0;
      for (int i = i0; i < i1; i++) {
        d1 = dot_product(
            ta, sub3(PositionPosRelBodyODE(fibre[i], .0, .0, .5 * lf[i]), p0));
        d0 = fmax(
            d0, norm3(sub3(PositionPosRelBodyODE(fibre[i], .0, .0, .5 * lf[i]),
                           add3(p0, mult3(d1, ta)))));
      }
    }
    if (d0 == .0)
      continue;
    else
      d1 = fmin(.05 * pi, (2. * nm / fL) / d0);
    d0 = d1 * (1. - 2. * u01(mt0));
    // before to move the chain
    // compute elastic energy
    newE = oldE;
    if (i0 > 0)
      newE -= (gb[i0 - 1] * (1. - cos(arccos(local_cosine_of_bending(
                                          fibre[i0 - 1], fibre[i0])) -
                                      b0[i0 - 1])) +
               .5 * gt[i0 - 1] *
                   pow(local_twist(fibre[i0 - 1], fibre[i0], tw0[i0 - 1]), 2.));
    if (i1 < (F - 1))
      newE -= (gb[i1] * (1. - cos(arccos(local_cosine_of_bending(
                                      fibre[i1], fibre[i1 + 1])) -
                                  b0[i1])) +
               .5 * gt[i1] *
                   pow(local_twist(fibre[i1], fibre[i1 + 1], tw0[i1]), 2.));
    // compute loop energy
    if (bloop_attraction && kloop != .0)
      for (int n = 0; n < (nanchors - 1); n++)
        newE -= .5 * kloop *
                dot_product2(sub3(PositionBodyODE(fibre[anchors[n]]),
                                  PositionBodyODE(fibre[anchors[n + 1]])));
    // Crankshaft or Pivot move
    for (int f = i0; f <= i1; f++) {
      new_ui[f] = rotation(ta, d0, old_ui[f]);
      new_vi[f] = rotation(ta, d0, old_vi[f]);
      new_ti[f] = cross_product(new_ui[f], new_vi[f]);
      set_axes(fibre[f], new_ti[f], new_ui[f], new_vi[f]);
      new_pi[f] = add3(
          (f > 0) ? PositionPosRelBodyODE(fibre[f - 1], .0, .0, .5 * lf[f - 1])
                  : p0,
          mult3(.5 * lf[f], new_ti[f]));
      set_position(fibre[f], new_pi[f]);
    }
    // relative positions of the histones and h1 (sanity check)
    for (int n = nuci; n < nucj; n++) {
      for (int h = (F + n * NHC); h < (F + (n + 1) * NHC); h++) {
        i2 = nucleosome_start[n];
        new_ui[h] = rotation(ta, d0, old_ui[h]);
        new_vi[h] = rotation(ta, d0, old_vi[h]);
        new_ti[h] = cross_product(new_ui[h], new_vi[h]);
        set_axes(fibre[h], new_ti[h], new_ui[h], new_vi[h]);
        p0 = add3(PositionBodyODE(fibre[i2]),
                  mult3(uvth[h - F].x, FrameODE(fibre[i2], 0)));
        p0 = add3(p0, mult3(uvth[h - F].y, FrameODE(fibre[i2], 1)));
        p0 = add3(p0, mult3(uvth[h - F].z, FrameODE(fibre[i2], 2)));
        new_pi[h] = p0;
        set_position(fibre[h], p0);
      }
    }
    for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
         h++) {
      i2 = h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0];
      // does the current h1 move ?
      if (!(i2 >= i0 && i2 <= i1))
        continue;
      // orientation of the h1 dBodyID does matter
      new_ui[h] = rotation(ta, d0, old_ui[h]);
      new_vi[h] = rotation(ta, d0, old_vi[h]);
      new_ti[h] = cross_product(new_ui[h], new_vi[h]);
      set_axes(fibre[h], new_ti[h], new_ui[h], new_vi[h]);
      p0 = add3(
          PositionBodyODE(fibre[i2]),
          mult3(uvth1[h - F - nucleosomes * NHC].x, FrameODE(fibre[i2], 0)));
      p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].y,
                          FrameODE(fibre[i2], 1)));
      p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].z,
                          FrameODE(fibre[i2], 2)));
      new_pi[h] = p0;
      set_position(fibre[h], p0);
    }
    // compute new elastic energy
    if (i0 > 0)
      newE += gb[i0 - 1] * (1. - cos(arccos(local_cosine_of_bending(
                                         fibre[i0 - 1], fibre[i0])) -
                                     b0[i0 - 1])) +
              .5 * gt[i0 - 1] *
                  pow(local_twist(fibre[i0 - 1], fibre[i0], tw0[i0 - 1]), 2.);
    if (i1 < (F - 1))
      newE +=
          gb[i1] * (1. - cos(arccos(local_cosine_of_bending(fibre[i1],
                                                            fibre[i1 + 1])) -
                             b0[i1])) +
          .5 * gt[i1] * pow(local_twist(fibre[i1], fibre[i1 + 1], tw0[i1]), 2.);
    // compute new loop energy
    if (bloop_attraction && kloop != .0)
      for (int n = 0; n < (nanchors - 1); n++)
        newE += .5 * kloop *
                dot_product2(sub3(PositionBodyODE(fibre[anchors[n]]),
                                  PositionBodyODE(fibre[anchors[n + 1]])));
    t2 += (clock() - t0);
    bMC = (u01(mt0) < exp(-(newE - oldE) / (kB * temp / fE)));
    t0 = clock();
    if (bMC) {
      starts[0] = (nc == 0) ? i0 : 0;
      ends[0] = (nc == 0) ? i1 : FH - 1;
      starts[1] = (nc == 0) ? F + nuci * NHC : 0;
      ends[1] = (nc == 0) ? F + nucj * NHC - 1 : -1;
      starts[2] = 0;
      ends[2] = -1;
      new_nc =
          partition_v0(fibre, gfibre, FH, starts, ends, WorldODE, contactgroup,
                       lp2r, new_sum_abs_depth, max_abs_depth, true);
    }
    t1 += (t0 - clock());
    t0 = clock();
    if (bMC) {
      if (min_lbp < 6.) {
        // for segmentation between 1 and 3 bp per DNA segment, length is lesser
        // than radius therefore we remove collision(s) between segment 'i' and
        // segment 'i+x'
        remove_artificial_collisions(nucleosomes, F, sbp, lbp, rf, fibre,
                                     gfibre, new_nc, new_sum_abs_depth);
      }
      if (false && new_nc <= 15) {
        for (int f = 0; f < (B - 1); f++) {
          for (int g = (f + 1); g < B; g++) {
            if (dAreConnected(fibre[f], fibre[g]) == 1) {
              tmp = dBodyGetNumJoints(fibre[f]);
              for (int j = 0; j < tmp; j++)
                if (dJointGetType(dBodyGetJoint(fibre[f], j)) ==
                    dJointTypeContact)
                  if (dJointGetBody(dBodyGetJoint(fibre[f], j), 0) ==
                          fibre[g] ||
                      dJointGetBody(dBodyGetJoint(fibre[f], j), 1) == fibre[g])
                    printf("initial conformation: collision between %i/%i and "
                           "%i/%i\n",
                           f, F, g, F);
            }
          }
        }
      }
      if (contactgroup)
        dJointGroupEmpty(contactgroup);
    }
    // printf("partition/remove=%f partition/check=%f\n",t1/t2,t1/t3);
    // printf("%i -> %i\n",nc,new_nc);
    // accept first conformation
    // keep the conformation that corresponds to the smallest number of overlaps
    // if(new_nc<=nc &&
    // u01(mt0)<exp(-(newE-oldE)/(kB*temp/fE)-(new_sum_abs_depth-sum_abs_depth)/(.0001*nm/fL))){
    if (new_nc <= nc && new_sum_abs_depth <= ((1. + 1e-9) * sum_abs_depth) &&
        bMC) {
      if (1) {
        t0 = clock();
        // check anchors (connectivity)
        i2 = 0;
        for (int f = 0; f < FH; f++) {
          tmp = dBodyGetNumJoints(fibre[f]);
          for (int j = 0; j < tmp; j++) {
            dJointGetBallAnchor(dBodyGetJoint(fibre[f], j), a1);
            dJointGetBallAnchor2(dBodyGetJoint(fibre[f], j), a2);
            rij = vec3(a1[0] - a2[0], a1[1] - a2[1], a1[2] - a2[2]);
            if (dot_product2(rij) > (1e-6 * nm * nm / (fL * fL))) {
              printf("(%i %s) warning body=%i, joint=%i, danchors=%f nm (move "
                     "from %i to %i/%i) (pivot=%i)\n",
                     ntry, chromatin_in, f, j, norm3(rij) * fL / nm, i0, i1, F,
                     (int)pivot);
              printf("%i %i\n", is_linker_nucleosome[i0],
                     is_linker_nucleosome[i1]);
              for (int h = 0; h < nH1; h++) {
                if (h1_to_dna[2 * h + 0] == i0)
                  printf("%i %i(0) %i(1)\n", h, i0, h1_to_dna[2 * h + 1]);
                if (h1_to_dna[2 * h + 1] == i0)
                  printf("%i(1) %i\n", h, i0);
                if (h1_to_dna[2 * h + 0] == i1)
                  printf("%i(0) %i\n", h, i1);
                if (h1_to_dna[2 * h + 1] == i1)
                  printf("%i(1) %i\n", h, i1);
              }
              i2++;
            }
          }
        }
        if (i2 > 0)
          return 0;
        t3 += (t0 - clock());
      }
      // reset acceptation counter
      if (new_nc == 0 && nc > 0)
        ntry = nacc[0] = nacc[1] = 0;
      for (int f = i0; f <= i1; f++) {
        old_ui[f] = new_ui[f];
        old_vi[f] = new_vi[f];
        old_ti[f] = new_ti[f];
        old_pi[f] = new_pi[f];
      }
      for (int n = nuci; n < nucj; n++) {
	printf("warning\n");
        for (int h = (F + n * NHC); h < (F + (n + 1) * NHC); h++) {
          old_ui[h] = new_ui[h];
          old_vi[h] = new_vi[h];
          old_ti[h] = new_ti[h];
          old_pi[h] = new_pi[h];
        }
      }
      for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
           h++) {
	printf("warning\n");
        i2 = h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0];
        // does the current h1 move ?
        if (!(i2 >= i0 && i2 <= i1))
          continue;
        // orientation of the h1 dBodyID does matter
        old_ui[h] = new_ui[h];
        old_vi[h] = new_vi[h];
        old_ti[h] = new_ti[h];
        p0 = add3(old_pi[i2],
                  mult3(uvth1[h - F - nucleosomes * NHC].x, old_ui[i2]));
        p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].y, old_vi[i2]));
        p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].z, old_ti[i2]));
        old_pi[h] = p0;
      }
      nc = new_nc;
      sum_abs_depth = new_sum_abs_depth;
      oldE = newE;
      nacc[(pivot) ? 0 : 1]++;
      // check if anchor to consecutive anchor distance is within the range
      if (bloop_attraction && kloop != .0) {
        scaffold = true;
        tmp = 0;
        for (int n = 0; n < (nanchors - 1); n++) {
          bscaffold[n] = (bscaffold[n] ||
                          norm3(sub3(PositionBodyODE(fibre[anchors[n]]),
                                     PositionBodyODE(fibre[anchors[n + 1]]))) <
                              (3. * sloop));
          scaffold = (scaffold && bscaffold[n]);
          tmp += (int)bscaffold[n];
        }
      }
      // visualisation
      if (((nacc[0] + nacc[1]) % 10000) == 0 || (nacc[0] + nacc[1]) == 1) {
        // Save for visualisation
        tmp = sprintf(name, "visualisation/vn%i_%s.out", seed, chromatin_in);
        write_visualisation(fibre, name, 0, B,
                            ((nacc[0] + nacc[1]) == 1) ? "w" : "a");
        // Center-of-mass to 0,0,0
	reset_com(fibre, B);
        for (int i = 0; i < B; i++) {
          old_ui[i] = FrameODE(fibre[i], 0);
          old_vi[i] = FrameODE(fibre[i], 1);
          old_ti[i] = FrameODE(fibre[i], 2);
	  print3(FrameODE(fibre[i], 0));
        }
	// Rotate conformation if and only if number of anchors is > 0.
	// Rotation axis is determined by the position of the first and last anchors.
	if(nanchors > 1){
	  bn = normalize3(sub3(PositionBodyODE(fibre[anchors[nanchors - 1]]),
			       PositionBodyODE(fibre[anchors[0]])));
	  rotations(normalize3(cross_product(bn, vec3(.0, .0, 1.))),
		    arccos(dot_product(bn, vec3(.0, .0, 1.))), fibre, 0, B - 1,
		    vec3(.0, .0, .0));
	  // Rotation of the conformation
	  sprintf(name, "visualisation/fn%i_%s.out", seed, chromatin_in);
	  d0 = 2. * pi / 10.;
	  for (int k = 0; k <= 10; k++) {
	    rotations(vec3(.0, .0, 1.), d0, fibre, 0, B - 1, vec3(.0, .0, .0));
	    write_visualisation(fibre, name, 0, B, (k == 0) ? "w" : "a");
	  }
	  // Back to conformation before rotation
	  for (int i = 0; i < B; i++) {
	    set_position(fibre[i], old_pi[i]);
	    set_axes(fibre[i], old_ti[i], old_ui[i], old_vi[i]);
	  }
	}
      }
      // print log
      if (((nacc[0] + nacc[1]) % 1000) == 0) {
        printf("%s: %i overlaps sum=%f nm, Ree=%f nm, "
               "scaffold=%i/%i(na=%i), acc=%f(nacc=%i+%i)\n",
               chromatin_in, nc, sum_abs_depth * fL / nm,
               norm3(sub3(old_pi[0], old_pi[F - 1])) * fL / nm, tmp,
               nanchors - 1, na, (double)(nacc[0] + nacc[1]) / (double)ntry,
               nacc[0], nacc[1]);
        if (bloop_attraction && kloop != .0)
          for (int n = 0; n < (nanchors - 1); n++)
            printf("%i-%i %f nm\n", anchors[n], anchors[n + 1],
                   norm3(sub3(PositionBodyODE(fibre[anchors[n]]),
                              PositionBodyODE(fibre[anchors[n + 1]]))) *
                       fL / nm);
      }
      if (nc == 0 &&
          ((bloop_attraction && kloop != .0 &&
            (scaffold &&
             (nacc[0] + nacc[1]) > (mcs * (F - nucleosomes * NNC)))) ||
           (!bloop_attraction &&
            (nacc[0] + nacc[1]) > (mcs * (F - nucleosomes * NNC))))) {
        // if(nc==0){
        // save the conformation at the end of the MC
        sprintf(name, "conformations/cn%i_%s_s%i.out", seed, chromatin_in, -1);
        read_write_conformation(fibre, name, "write", 0, B);
        break;
      }
    } else {
      // come back to old conformation
      for (int f = i0; f <= i1; f++) {
        set_position(fibre[f], old_pi[f]);
        set_axes(fibre[f], old_ti[f], old_ui[f], old_vi[f]);
      }
      for (int n = nuci; n < nucj; n++) {
        for (int h = (F + n * NHC); h < (F + (n + 1) * NHC); h++) {
          set_position(fibre[h], old_pi[h]);
          set_axes(fibre[h], old_ti[h], old_ui[h], old_vi[h]);
        }
      }
      for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
           h++) {
        i2 = h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0];
        // does the current h1 move ?
        if (!(i2 >= i0 && i2 <= i1))
          continue;
        // orientation of the h1 dBodyID does matter
        set_axes(fibre[h], old_ti[h], old_ui[h], old_vi[h]);
        p0 = add3(
            PositionBodyODE(fibre[i2]),
            mult3(uvth1[h - F - nucleosomes * NHC].x, FrameODE(fibre[i2], 0)));
        p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].y,
                            FrameODE(fibre[i2], 1)));
        p0 = add3(p0, mult3(uvth1[h - F - nucleosomes * NHC].z,
                            FrameODE(fibre[i2], 2)));
        set_position(fibre[h], p0);
      }
    }
    ntry++;
    t2 += (t0 - clock());
    // printf("%f\n",t1/t2);
  }
  delete[] c1free_to_move;
  delete[] c2free_to_move;
  delete[] pfree_to_move;
  delete[] new_ui;
  delete[] new_vi;
  delete[] new_ti;
  delete[] new_pi;
  delete[] uvth;

  starts[0] = 0;
  ends[0] = FH - 1;
  starts[1] = 0;
  ends[1] = -1;
  starts[2] = 0;
  ends[2] = -1;

  int nconstraints = 0;
  for (int f = 0; f < B; f++)
    nconstraints += dBodyGetNumJoints(fibre[f]);
  nconstraints /= 2;

  // compute digestion
  if (bdigestion) {
    digestion(chromatin_in, .025, mt0);
    digestion(chromatin_in, .05, mt0);
    digestion(chromatin_in, .1, mt0);
    digestion(chromatin_in, .15, mt0);
    digestion(chromatin_in, .2, mt0);
    digestion(chromatin_in, .25, mt0);
    digestion(chromatin_in, .5, mt0);
    digestion(chromatin_in, .025, mt0, false);
    digestion(chromatin_in, .05, mt0, false);
    digestion(chromatin_in, .1, mt0, false);
    digestion(chromatin_in, .15, mt0, false);
    digestion(chromatin_in, .2, mt0, false);
    digestion(chromatin_in, .25, mt0, false);
    digestion(chromatin_in, .5, mt0, false);
  }

  // compute min(lbp)
  min_lbp = DBL_MAX;
  for (int f = 0; f < F; f++)
    min_lbp = fmin(min_lbp, lbp[f]);
  // compute start of each nucleosome
  delete[] nucleosome_start;
  nucleosome_start = NULL;
  nucleosome_start = dna_histone_starts(chromatin_in);
  delete[] is_linker_nucleosome;
  is_linker_nucleosome = NULL;
  is_linker_nucleosome = is_linker_or_nucleosome(chromatin_in);

  // add histone H1 joints
  int *h1s = nucleosome_with_h1(chromatin_in);
  dJointID *jh1 = new dJointID[nucleosomes]();
  if (prob_h1 > .0) {
    for (int n = 0; n < nucleosomes; n++) {
      if (h1s[n] == 0)
        continue;
      i0 = nucleosome_start[n] - 1;
      i1 = nucleosome_start[n] + NNC;
      if (i0 < 0 || i1 >= F)
        continue;
      rij = normalize3(
          sub3(PositionBodyODE(fibre[i0]), PositionBodyODE(fibre[i1])));
      jh1[n] =
          dball_joint(WorldODE, fibre[i0], fibre[i1],
                      sub3(PositionBodyODE(fibre[i0]), mult3(rf[i0], rij)),
                      add3(PositionBodyODE(fibre[i1]), mult3(rf[i1], rij)));
      dofs -= 3;
    }
  }

  // add feedback (only for reversome model)
  if (write_joint_feedback) {
    // do not ask for DNA-DNA joint feedback
    for (int j = J; j < (J + nucleosomes * ((NHC - 1) + nSHLs + nDOCKs)); j++)
      if (jfibre[j] != NULL)
        dJointSetFeedback(jfibre[j], &fjfibre[j]);
  }
  // remove com
  if (noSM)
    remove_com(fibre, 0, B - 1);

  // read minimal distance between dna/histone or histone/histone connected by a
  // joint minimal distance point on body 1 is r_1com=a_1*u_1+b_1*v_1+c_1*t_1
  // minimal distance point on body 2 is r_2com=a_2*u_2+b_2*v_2+c_2*t_2
  vec3 *uvtm1 = new vec3[NHC - 1 + nSHLs + nDOCKs]();
  vec3 *uvtm2 = new vec3[NHC - 1 + nSHLs + nDOCKs]();
  fOut = fopen("nucleosome_in/joint_relative_position.in", "r");
  for (int h = 0; h < (NHC - 1); h++) {
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm1[h].x, &uvtm1[h].y,
           &uvtm1[h].z);
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm2[h].x, &uvtm2[h].y,
           &uvtm2[h].z);
  }
  for (int s = 0; s < nSHLs; s++) {
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm1[NHC - 1 + s].x,
           &uvtm1[NHC - 1 + s].y, &uvtm1[NHC - 1 + s].z);
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm2[NHC - 1 + s].x,
           &uvtm2[NHC - 1 + s].y, &uvtm2[NHC - 1 + s].z);
    // printf("%f %f %f\n",uvtm1[NHC-1+s].x,uvtm1[NHC-1+s].y,uvtm1[NHC-1+s].z);
  }
  for (int d = 0; d < nDOCKs; d++) {
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm1[NHC - 1 + nSHLs + d].x,
           &uvtm1[NHC - 1 + nSHLs + d].y, &uvtm1[NHC - 1 + nSHLs + d].z);
    fscanf(fOut, "%s %s %lf %lf %lf\n", s0, s1, &uvtm2[NHC - 1 + nSHLs + d].x,
           &uvtm2[NHC - 1 + nSHLs + d].y, &uvtm2[NHC - 1 + nSHLs + d].z);
  }
  vec3 *rm1 = new vec3[NHC - 1 + nSHLs + nDOCKs]();
  vec3 *rm2 = new vec3[NHC - 1 + nSHLs + nDOCKs]();

  // variables: FHBs, SHLs and DDs Morse potentials
  double *Dm = new double[NHC - 1 + nSHLs + nDOCKs]();
  double *em = new double[NHC - 1 + nSHLs + nDOCKs]();
  double *am = new double[NHC - 1 + nSHLs + nDOCKs]();
  double *Em = new double[nucleosomes * (NHC - 1 + nSHLs + nDOCKs)]();
  vec3 *fm = new vec3[nucleosomes * (NHC - 1 + nSHLs + nDOCKs)]();
  vec3 *rm = new vec3[nucleosomes * (NHC - 1 + nSHLs + nDOCKs)]();
  for (int h = 0; h < (NHC - 1); h++) {
    // em[h]=2.*nm/fL;
    // am[h]=fL/(2.*nm);
    // Dm[h]=((h==0 || h==(NHC-2))?4.:4.)*kB*temp/fE;
    em[h] = -2. * nm / fL;        // ???
    am[h] = fL / (2. * nm);       // ???
    Dm[h] = 10. * kB * temp / fE; // ???
  }
  for (int s = 0; s < nSHLs; s++) {
    // em[NHC-1+s]=((s==0 || s==(nSHLs-1))?2.:2.)*nm/fL;
    // // DNA binding within the nucleosome core. Luger and Richmond Current,
    // Opinion in Structural Biology 1998 am[NHC-1+s]=fL/(((s==0 || s==2 ||
    // s==(nSHLs-1-2) || s==(nSHLs-1))?2.:1.)*nm);
    // // modulation of binding sites: 6.5<5.5~2.5~1.5<4.5<3.5<0.5
    // // shls 6.5
    // if(s==0 || s==(nSHLs-1))
    //   Dm[NHC-1+s]=1.*kB*temp/fE;
    // // shls 5.5
    // if(s==1 || s==(nSHLs-1-1))
    //   Dm[NHC-1+s]=4.*kB*temp/fE;
    // // shls 4.5
    // if(s==2 || s==(nSHLs-1-2))
    //   Dm[NHC-1+s]=6.*kB*temp/fE;
    // // shls 3.5
    // if(s==3 || s==(nSHLs-1-3))
    //   Dm[NHC-1+s]=7.*kB*temp/fE;
    // // shls 2.5
    // if(s==4 || s==(nSHLs-1-4))
    //   Dm[NHC-1+s]=4.*kB*temp/fE;
    // // shls 1.5
    // if(s==5 || s==(nSHLs-1-5))
    //   Dm[NHC-1+s]=4.*kB*temp/fE;
    // // shls 0.5
    // if(s==6 || s==(nSHLs-1-6))
    //   Dm[NHC-1+s]=8.*kB*temp/fE;
    // // if(s!=0 && s!=(nSHLs-1)) Dm[NHC-1+s]=20.*kB*temp/fE;
    em[NHC - 1 + s] = -2. * nm / fL;        // ???
    am[NHC - 1 + s] = fL / (2. * nm);       // ???
    Dm[NHC - 1 + s] = 10. * kB * temp / fE; // ???
  }
  for (int d = 0; d < nDOCKs; d++) {
    em[NHC - 1 + nSHLs + d] = -2. * nm / fL;        // ???
    am[NHC - 1 + nSHLs + d] = fL / (2. * nm);       // ???
    Dm[NHC - 1 + nSHLs + d] = 10. * kB * temp / fE; // ???
  }
  for (int j = 0; j < (NHC - 1 + nSHLs + nDOCKs); j++) { // ???
    em[j] = -2. * nm / fL;                               // ???
    am[j] = fL / (1. * nm);                              // ???
    Dm[j] = 20. * kB * temp / fE;                        // ???
  }                                                      // ???

  // variables: stacking
  double Ds = .0, es = .0, as = .0;
  if (strcmp(file_stacking, "") != 0) {
    fOut = fopen(file_stacking, "r");
    if (fOut != NULL) {
      bstacking = true;
      bsolenoid = false;
      nn2 = false;
      fprintf(fOut, "%s %s %s\n", s0, s1, s2);
      fprintf(fOut, "%lf %lf %lf\n", Ds, es, as);
      Ds *= kB * temp / fE;
      es *= nm / fL;
      as *= fL / nm;
      fclose(fOut);
    }
  }

  // partition: monomers
  delete[] binn;
  binn = new int[nbins]();
  std::fill_n(binn, nbins, -1);
  delete[] ninb;
  ninb = new int[B]();
  std::fill_n(ninb, B, -1);
  delete[] bhit;
  bhit = new int[B]();
  delete[] nhit;
  nhit = new int[B * nperb]();
  delete[] lperb;
  lperb = new int[B]();

  // bending and twisting strengths
  delete[] gb;
  gb = new double[J]();
  delete[] gt;
  gt = new double[J]();
  bending_twisting_constants(2. * lp, lt, lf, &gb, &gt, J, fE / (kB * temp),
                             true);
  // add intrinsic curvature
  delete[] b0;
  b0 = new double[F - 1]();
  if (bintrinsic_curvature && intrinsic_curvature != .0) {
    for (int f = 0; f < (F - 1); f++)
      if (is_linker_nucleosome[f] < 0 && is_linker_nucleosome[f + 1] < 0)
        b0[f] += arccos(
            1. - .5 * pow(.5 * (lf[f] + lf[f + 1]) * intrinsic_curvature, 2.));
  }
  // compute intrinsic twist
  delete[] tw0;
  tw0 = tw_e_chromatin_fibre(chromatin_in);
  // add intrinsic twist
  if (bintrinsic_twist && intrinsic_twist != .0) {
    for (int f = 0; f < (F - 1); f++)
      if (is_linker_nucleosome[f] < 0 && is_linker_nucleosome[f + 1] < 0)
        tw0[f] += intrinsic_twist;
  }

  // parameters: Langevin dynamics
  double tau = DBL_MAX, avgtau = .0;
  double *friction = new double[B]();
  double *sigmaT = new double[B]();
  double *sigmaR = new double[B]();
  for (int b = 0; b < B; b++) {
    if (use_Socol) {
      // DNA friction from Socol & al, NAR 2019
      friction[b] = 3. * pi * (viscosity_Socol_2019 / (fM / (fL * fT))) *
	lf[b] / log(lf[b] / (2. * rf[b]));
    } else
      friction[b] =
	xfriction * 6. * pi * (viscosity / (fM / (fL * fT))) * (.5 * lf[b]);
    sigmaT[b] = friction[b] / mf[b];
    sigmaR[b] = sigmaT[b];
    tau = fmin(tau, 1. / sigmaT[b]);
    avgtau += (1. / sigmaT[b]) / (double)B;
  }
  double tau_persistence = DBL_MAX, tau_solenoid = DBL_MAX,
         tau_torque = DBL_MAX;
  for (int f = 0; f < F; f++) {
    tau_persistence =
        fmin(tau_persistence, sqrt(inertia(fibre[f]).x / fmax(gb[f], gt[f])));
    tau_persistence =
        fmin(tau_persistence, sqrt(inertia(fibre[f]).y / fmax(gb[f], gt[f])));
    tau_persistence =
        fmin(tau_persistence, sqrt(inertia(fibre[f]).z / fmax(gb[f], gt[f])));
    if (f < (F - 1)) {
      tau_persistence = fmin(tau_persistence, sqrt(inertia(fibre[f]).x /
                                                   fmax(gb[f + 1], gt[f + 1])));
      tau_persistence = fmin(tau_persistence, sqrt(inertia(fibre[f]).y /
                                                   fmax(gb[f + 1], gt[f + 1])));
      tau_persistence = fmin(tau_persistence, sqrt(inertia(fibre[f]).z /
                                                   fmax(gb[f + 1], gt[f + 1])));
    }
    if (torque != .0)
      tau_torque =
          fmin(tau_torque,
               sqrt(fmin(inertia(fibre[f]).x,
                         fmin(inertia(fibre[f]).y, inertia(fibre[f]).z)) /
                    fabs(torque)));
    if (intrinsic_bending_torque != .0)
      tau_torque =
          fmin(tau_torque,
               sqrt(fmin(inertia(fibre[f]).x,
                         fmin(inertia(fibre[f]).y, inertia(fibre[f]).z)) /
                    fabs(intrinsic_bending_torque)));
    if (intrinsic_twisting_torque != .0)
      tau_torque =
          fmin(tau_torque,
               sqrt(fmin(inertia(fibre[f]).x,
                         fmin(inertia(fibre[f]).y, inertia(fibre[f]).z)) /
                    fabs(intrinsic_twisting_torque)));
  }
  double ksolenoid = 3. * (kB * temp / fE) / pow(1.25 * nm / fL, 2.);
  double estacking = 5. * nm / fL;
  if (bsolenoid) {
    for (int f = 0; f < B; f++) {
      tau_solenoid = fmin(tau_solenoid, 1. / sqrt(ksolenoid / mf[f]));
      // tau_solenoid=fmin(tau_solenoid,sqrt(inertia(fibre[f]).x/(ksolenoid*lf[f]*lf[f])));
      // tau_solenoid=fmin(tau_solenoid,sqrt(inertia(fibre[f]).y/(ksolenoid*lf[f]*lf[f])));
      // tau_solenoid=fmin(tau_solenoid,sqrt(inertia(fibre[f]).z/(ksolenoid*lf[f]*lf[f])));
    }
  }
  if (bloop_attraction && kloop != .0)
    for (int f = 0; f < B; f++)
      tau_solenoid = fmin(tau_solenoid, 1. / sqrt(kloop / mf[f]));
  double dt = fmin(.01 * tau,
                   fmin(sqrt(.1) * tau_persistence,
                        fmin(sqrt(.1) * tau_torque, sqrt(.01) * tau_solenoid)));
  if (!nucleosome_joints && nucleosomes > 0) {
    for (int i = 0; i < (NHC - 1 + nSHLs + nDOCKs); i++) {
      if (Dm[i] == .0)
        continue;
      // Morse potential (at x~e) ~ D*a^2*(x-e)^2
      // Harmonic potential with k=2*D*a^2 (time scale is sqrt(m/k))
      for (int n = 0; n < nucleosomes; n++)
        for (int b = nucleosome_start[n]; b < (nucleosome_start[n] + NNC); b++)
          dt = fmin(dt, sqrt(.01 * mf[b] / (2. * Dm[i] * am[i] * am[i])));
      for (int b = F; b < B; b++)
        dt = fmin(dt, sqrt(.01 * mf[b] / (2. * Dm[i] * am[i] * am[i])));
    }
  }
  double k_from_erp_cfm, mu_from_erp_cfm;
  k_and_mu_from_erp_cfm(dt, wERP, wCFM, k_from_erp_cfm, mu_from_erp_cfm);
  double mE = .5 * dofs * kB * temp / fE;
  printf("<E>/kBT=%f is approximative.\n", mE);
  printf("Indeed, we have to compute the rank of the constraints matrix to "
         "properly\n");
  printf(
      "determine the number of nucleosome unconstrained degrees of freedom.\n");
  printf("rank computation gives  %li\n",
         6 * (NNC + NHC) - compute_rank_of_J_nucleosome(simplified_nucleosome));
  printf("naive computation gives %i\n",
         6 * (NNC + NHC) - 3 * (NNC - 1) -
             3 * (nSHLs - ((simplified_nucleosome == 1) ? 4 : 0)) -
             3 * (NHC - 1) - 3 * nDOCKs);
  printf("                        %i/%i dofs\n", dofs,
         6 * B - 3 * (F - 1) -
             nucleosomes *
                 (3 * (NHC - 1) +
                  3 * (nSHLs - ((simplified_nucleosome == 1) ? 4 : 0)) +
                  3 * nDOCKs));
  // return 0;
  // dofs+=(nucleosomes*17-nucleosomes*12);
  // mE=.5*dofs*kB*temp/fE;
  // return 0;

  // write size of the bodies
  // dna
  tmp = sprintf(name, "visualisation/sn%i_%s.out", seed, chromatin_in);
  fOut = fopen(name, "w");
  if (bloop_attraction) {
    for (int f = 0; f < F; f++) {
      i0 = 0;
      // use one color per loop
      for (int l = 0; l < (nanchors - 1); l++)
        if ((f >= anchors[l] && f < anchors[l + 1]) ||
            f == anchors[nanchors - 1])
          i0 = l + 1;
      i1 = 0;
      for (int l = 0; l < nanchors; l++)
        if (f == anchors[l])
          i1 = 1;
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", (i1 == 0) ? 0 : 1,
                    (i1 == 0) ? rf[f] : sloop, (i1 == 0) ? lf[f] : sloop, f,
                    (i1 == 0) ? i0 : 0, 0);
    }
    // histones
    for (int n = 0; n < nucleosomes; n++) {
      i0 = 0;
      for (int l = 0; l < (nanchors - 1); l++)
        if (nucleosome_start[n] >= anchors[l] &&
            nucleosome_start[n] < anchors[l + 1])
          i0 = l + 1;
      for (int i = 0; i < 4; i++)
        tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, F + n * NHC + i,
                      i0, 0);
    }
    // h1
    for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
         h++) {
      i0 = 0;
      for (int l = 0; l < (nanchors - 1); l++)
        if (h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0] >= anchors[l] &&
            h1_to_dna[2 * (h - F - nucleosomes * NHC) + 0] < anchors[l + 1])
          i0 = l + 1;
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, rf[h], rf[h], h, i0, 0);
    }
  } else {
    for (int f = 0; f < F; f++)
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[f], lf[f], f, 0, 0);
    // histones
    for (int n = 0; n < nucleosomes; n++)
      for (int i = 0; i < 4; i++)
        tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, F + n * NHC + i,
                      11 * (i == 0 || i == 3) + 12 * (i == 1 || i == 2), 0);
    // h1
    for (int h = (F + nucleosomes * NHC); h < (F + nucleosomes * NHC + nH1);
         h++)
      tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, rf[h], rf[h], h, 4, 0);
  }
  if (!noSM)
    tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1,
                  dGeomSphereGetRadius(gfibre[B - 1]),
                  dGeomSphereGetRadius(gfibre[B - 1]), B - 1, 1, 0);
  fclose(fOut);

  // write nucleosome and joint anchors
  if (save_cg_nucleosome || (seed == 1 && nrl == 167 && NRL == 167)) {
    for (int d = 0; d < 2; d++) {
      tmp = sprintf(name, "%s",
                    (d == 0) ? "visualisation/snucleosome.out"
                             : "visualisation/sdinucleosome.out");
      fOut = fopen(name, "w");
      for (int n = 0; n < ((d == 0) ? 1 : 2); n++) {
        for (int f = nucleosome_start[0]; f < (nucleosome_start[0] + NNC); f++)
          tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[f], lf[f], f, 0, 0);
        for (int h = F; h < (F + NHC); h++)
          tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, 14 + h,
                        ((h - F) + 1) * ((h - F) < 3) + 9 * ((h - F) == 3), 0);
        // add joint anchors
        for (int f = nucleosome_start[0]; f < (nucleosome_start[0] + NNC); f++)
          for (int j = 0; j < dBodyGetNumJoints(fibre[f]); j++)
            tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, .3 * nm / fL,
                          .3 * nm / fL, 14 + 4, 10, 0);
        for (int h = F; h < (F + NHC); h++)
          for (int j = 0; j < dBodyGetNumJoints(fibre[h]); j++)
            tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, .3 * nm / fL,
                          .3 * nm / fL, 14 + 4, 10, 0);
        // add stacking anchors
        for (int h = 0; h < 2; h++) {
          i1 = (h == 0) ? F + 0 : F + 1;
          i0 = (h == 0) ? F + 2 : F + 3;
          tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, .3 * nm / fL,
                        .3 * nm / fL, 14 + 4, 11 + h, 0);
          tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1, .3 * nm / fL,
                        .3 * nm / fL, 14 + 4, 11 + h, 0);
        }
      }
      fclose(fOut);
      tmp = sprintf(name, "%s",
                    (d == 0) ? "visualisation/vnucleosome.out"
                             : "visualisation/vdinucleosome.out");
      fOut = fopen(name, "wb");
      for (int n = 0; n < ((d == 0) ? 1 : 2); n++) {
        if (n == 1) {
          bn = normalize3(
              cross_product(FrameODE(fibre[nucleosome_start[0]], 2),
                            FrameODE(fibre[nucleosome_start[0] + NNC - 1], 2)));
          rij = mult3(
              dot_product(
                  bn,
                  sub3(PositionPosRelBodyODE(
                           fibre[nucleosome_start[0] + NNC - 1], .0, .0,
                           .5 * lf[nucleosome_start[0] + NNC - 1]),
                       PositionPosRelBodyODE(fibre[nucleosome_start[0]], .0, .0,
                                             -.5 * lf[nucleosome_start[0]]))) +
                  2. * nm / fL,
              bn);
        } else
          rij = vec3(.0, .0, .0);
        for (int f = nucleosome_start[0]; f < (nucleosome_start[0] + NNC);
             f++) {
          bn = add3(PositionBodyODE(fibre[f]), rij);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          bn = FrameODE(fibre[f], 2);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
        }
        for (int h = F; h < (F + NHC); h++) {
          bn = add3(PositionBodyODE(fibre[h]), rij);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          bn = FrameODE(fibre[h], 2);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
        }
        for (int f = nucleosome_start[0]; f < (nucleosome_start[0] + NNC);
             f++) {
          for (int j = 0; j < dBodyGetNumJoints(fibre[f]); j++) {
            tmp += fwrite(get_joint_anchor(dBodyGetJoint(fibre[f], j), rij),
                          sizeof(double), 3, fOut);
            bn = vec3(.0, .0, 1.);
            tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
            tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
            tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          }
        }
        for (int h = F; h < (F + NHC); h++) {
          for (int j = 0; j < dBodyGetNumJoints(fibre[h]); j++) {
            tmp += fwrite(get_joint_anchor(dBodyGetJoint(fibre[h], j), rij),
                          sizeof(double), 3, fOut);
            bn = vec3(.0, .0, 1.);
            tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
            tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
            tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          }
        }
        for (int h = 0; h < 2; h++) {
          i1 = (h == 0) ? F + 0 : F + 1;
          i0 = (h == 0) ? F + 2 : F + 3;
          i3 = (h == 0) ? -1 : -1;
          i2 = (h == 0) ? 1 : 1;
          bn = add3(PositionPosRelBodyODE(fibre[i0], .0, .0, .5 * i2 * lf[i0]),
                    rij);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          bn = vec3(.0, .0, 1.);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          bn = add3(PositionPosRelBodyODE(fibre[i1], .0, .0, .5 * i3 * lf[i1]),
                    rij);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
          bn = vec3(.0, .0, 1.);
          tmp += fwrite(&(bn.x), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.y), sizeof(double), 1, fOut);
          tmp += fwrite(&(bn.z), sizeof(double), 1, fOut);
        }
      }
      fclose(fOut);
    }
  }

  // write parameters
  tmp = sprintf(name, "parameters/%s.out", chromatin_in);
  fOut = fopen(name, "w");
  tmp = fprintf(fOut, "seed %i\n", seed);
  tmp = fprintf(fOut, "iterations %i\n", iterations);
  tmp = fprintf(fOut, "every %i\n", every);
  tmp = fprintf(fOut, "length_unit %e\n", fL);
  tmp = fprintf(fOut, "time_unit %e\n", fT);
  tmp = fprintf(fOut, "mass_unit %e\n", fM);
  tmp = fprintf(fOut, "xmass %e\n", xmass);
  tmp = fprintf(fOut, "force_unit %e\n", fF);
  tmp = fprintf(fOut, "energy_unit %e\n", fE);
  tmp = fprintf(fOut, "kBT %e\n", kB * temp / fE);
  tmp = fprintf(fOut, "dt %e\n", dt);
  tmp = fprintf(fOut, "tau %e\n", tau);
  tmp = fprintf(fOut, "tau_persistence %e\n", tau_persistence);
  if (torque != .0)
    tmp = fprintf(fOut, "tau_torque %f\n", tau_torque);
  tmp = fprintf(fOut, "tau/dt %e\n", tau / dt);
  tmp = fprintf(fOut, "tau_persistence/dt %e\n", tau_persistence / dt);
  tmp = fprintf(fOut, "tau_solenoid/dt %e\n", tau_solenoid / dt);
  if (torque != .0)
    tmp = fprintf(fOut, "tau_torque/dt %e\n", tau_torque / dt);
  tmp = fprintf(fOut, "twisting_persistence_nm %f\n", lt * fL / nm);
  tmp = fprintf(fOut, "bending_persistence_nm %f\n", lp * fL / nm);
  if (nucleosomes > 0) {
    tmp = fprintf(fOut, "twisting_strength_kBT %f\n",
                  gt[nucleosome_start[0]] * fE / (kB * temp));
    tmp = fprintf(fOut, "bending_strength_kBT %f\n",
                  gb[nucleosome_start[0]] * fE / (kB * temp));
    tmp = fprintf(fOut, "twisting_strength_kBT %f\n",
                  gt[nucleosome_start[0] + NNC] * fE / (kB * temp));
    tmp = fprintf(fOut, "bending_strength_kBT %f\n",
                  gb[nucleosome_start[0] + NNC] * fE / (kB * temp));
  } else {
    tmp = fprintf(fOut, "twisting_strength_kBT %f\n", gt[0] * fE / (kB * temp));
    tmp = fprintf(fOut, "bending_strength_kBT %f\n", gb[0] * fE / (kB * temp));
  }
  tmp = fprintf(fOut, "mode %i\n", mode);
  tmp = fprintf(fOut, "force_pN %f\n", force * fF / pN);
  tmp = fprintf(fOut, "torque_pN_nm %f\n", torque * fE / (pN * nm));
  tmp = fprintf(fOut, "intrinsic_curvature_nm-1 %f\n",
                intrinsic_curvature * nm / fL);
  tmp = fprintf(fOut, "nucleosomes %i\n", nucleosomes);
  tmp = fprintf(fOut, "dna_monomers %i\n", F);
  tmp = fprintf(fOut, "histones %i\n", FH - F);
  tmp = fprintf(fOut, "h1 %i\n", nH1);
  tmp = fprintf(fOut, "dofs %i\n", dofs);
  tmp = fprintf(fOut, "nanchors %i\n", nanchors);
  tmp = fprintf(fOut, "magnetic_bead %i\n", (int)(!noSM));
  if (nucleosomes == 0) {
    tmp = fprintf(fOut, "crossover_Rouse_diffusive %e\n",
                  pow(sqrt(12. * pow(lk / fL, 2.) * (kB * temp / fE) /
                           (pi * friction[0])) /
                          (6. * (kB * temp / fE) / (friction[0] * B)),
                      2.));
    tmp = fprintf(fOut, "crossover_Rouse_diffusive %e\n",
                  pow(sqrt(16. * pow(lk / fL, 2.) * (kB * temp / fE) /
                           (3. * pi * friction[0])) /
                          (6. * (kB * temp / fE) / (friction[0] * B)),
                      2.));
    tmp =
        fprintf(fOut, "kBT_over_friction %e\n", (kB * temp / fE) / friction[0]);
    tmp = fprintf(fOut, "t_sqlk %e\n",
                  pow(lk / fL, 2.) / (6. * (kB * temp / fE) / friction[0]));
    tmp =
        fprintf(fOut, "t_sqlk %e\n",
                pow(lk / fL, 2.) /
                    (6. * (((double)dofs / (double)(6 * B)) * kB * temp / fE) /
                     friction[0]));
    tmp = fprintf(
        fOut, "tR_sqlk %e\n",
        pow(pow(lk / fL, 2.) / sqrt(16. * pow(lk / fL, 2.) * (kB * temp / fE) /
                                    (3. * pi * friction[0])),
            2.));
    tmp = fprintf(
        fOut, "tR_sqlk %e\n",
        pow(pow(lk / fL, 2.) /
                sqrt(16. * pow(lk / fL, 2.) *
                     (((double)dofs / (double)(6 * B)) * kB * temp / fE) /
                     (3. * pi * friction[0])),
            2.));
  }
  tmp = fprintf(fOut, "erp %e\n", wERP);
  tmp = fprintf(fOut, "cfm %e\n", wCFM);
  tmp = fprintf(fOut, "k_from_erp_cfm %e\n", k_from_erp_cfm);
  tmp = fprintf(fOut, "mu_from_erp_cfm %e\n", mu_from_erp_cfm);
  tmp = fprintf(fOut, "Verlet_list %i\n", Vl_every);
  fclose(fOut);

  // print the parameters
  printf("%s\n", dGetConfiguration());
  printf("%i geoms\n", dSpaceGetNumGeoms(SpaceODE));

  // initialize data
  initialize_body_data(&dfibre, B, 20);

  // print initial conformation collision(s)
  if (step0 == 0) {
    for (int f = 0; f < (B - 1); f++) {
      for (int g = (f + 1); g < B; g++) {
        if (dAreConnected(fibre[f], fibre[g]) == 1) {
          tmp = dBodyGetNumJoints(fibre[f]);
          for (int j = 0; j < tmp; j++)
            if (dJointGetType(dBodyGetJoint(fibre[f], j)) == dJointTypeContact)
              if (dJointGetBody(dBodyGetJoint(fibre[f], j), 0) == fibre[g] ||
                  dJointGetBody(dBodyGetJoint(fibre[f], j), 1) == fibre[g]) {
                printf(
                    "initial conformation: collision between %i/%i and %i/%i\n",
                    f, F, g, F);
                return 0;
              }
        }
      }
    }
  }

  // minimization of the collisions, bending and twisting energies
  if (!no_collisions_min) {
    nc = partition_v0(fibre, gfibre, FH, starts, ends, WorldODE, contactgroup,
                      lp2r, sum_abs_depth, max_abs_depth, false, true);
    // between 1 and 3 bp per DNA segment length is lesser than radius
    // therefore we remove collision(s) between segment 'i' and segment 'i+x'
    if (min_lbp < 6.)
      remove_artificial_collisions(nucleosomes, F, sbp, lbp, rf, fibre, gfibre,
                                   nc, sum_abs_depth);
    if (contactgroup)
      dJointGroupEmpty(contactgroup);
  } else
    nc = 0;
  // overwrite 'MSTEP' argument if collision(s) in the initial conformation
  if (step0 == 0 && nc > 0) {
    tmp = sprintf(name, "error_n%i_%s_nc%i.out", seed, chromatin_in, nc);
    fOut = fopen(name, "w");
    fclose(fOut);
    return 0;
    printf("overwrite 'MSTEP' argument because of %i collision(s) in the "
           "initial conformation.\n",
           nc);
    MSTEP = 500000;
  }
  // check local twist
  double *torsions = new double[F - 2]();
  double *tws = new double[F - 1]();
  chain_local_twist(fibre, F, 0, F - 1, tws);
  d0 = DBL_MIN;
  for (int f = 0; f < (F - 1); f++)
    if (is_linker_nucleosome[f] < 0 && is_linker_nucleosome[f + 1] < 0)
      d0 = fmax(d0, fabs(tws[f] - tw0[f] - 2. * pi * ((tws[f] - tw0[f]) > pi) +
                         2. * pi * ((tws[f] - tw0[f]) < (-pi))));
  // overwrite 'MSTEP' argument if tw-tw0 is not equal to zero
  if (false && fabs(d0) > (2. * pi * .5 / 10.5)) {
    printf("overwrite 'MSTEP' argument because max(|tw-tw0|)=%f is not equal "
           "to zero.\n",
           d0);
    MSTEP = 200000;
  }
  // overwrite 'MSTEP' argument if solenoid
  if (false && bsolenoid) {
    printf("overwrite 'MSTEP' argument if solenoid\n");
    MSTEP = max((nucleosomes + 1) * is,
                3 * (int)(((NRL - 147) * 3.5721 * nm / fL) / (vlim * dt)));
  }

  // writhe of the chromatin made of reversomes
  if (reversomes > 0) {
    reversome0 = (nucleosomes - reversomes) / 2;
    reversome1 = reversome0 + reversomes - 1;
    Twt0 = sum_of_local_twist(fibre, F, nucleosome_start[reversome0],
                              nucleosome_start[reversome1] + NNC - 1) /
           (2. * pi);
    Wrt0 = chain_writhe_ODE(fibre, lf, nucleosome_start[reversome0],
                            nucleosome_start[reversome1] + NNC - 1, true, false,
                            1, 16);
  }

  // re-initialize scaffold variables
  scaffold = false;
  for (int n = 0; n < (nanchors - 1); n++)
    bscaffold[n] = false;

  // simulation loop
  cumE[0] = cumE[1] = .0;
  for (int s = (step0 + (int)(step0 > 0)); s <= (step0 + iterations); s++) {
    // does it only run the MC algorithm ?
    if (iterations == 0)
      break;
    xtime = (mstep < MSTEP) ? fmin(1., 2. * (double)(mstep + 1) / (double)MSTEP)
                            : ((MSTEP == 0) ? fmin(1., 1e-3 * (s + 1)) : 1.);
    // kinetic energy
    if (s > 100 && (s % 100) == 0 && cumE[1] < 1000.) {
      cumE[0] += Ktr(fibre, 0, B - 1, false, 2);
      cumE[1]++;
    }
    // bending and twist
    bending_twist(fibre, gb, gt, xtime, b0, tw0, 0, F - 1, true, cos_tw2[0],
                  cos_tw2[1]);
    // bending dna-ground
    if (!noSM)
      ground_bending_twist(fibre[0], 32., 32.);
    // add force and torque to the bead
    if (!noSM)
      bead(fibre[B - 1], force, torque, locked);
    else {
      if (force != .0) {
        // add force +/- to both extremities
        addPosRelForceBodyODE(fibre[F - 1], vec3(.0, .0, .5 * lf[F - 1]),
                              mult3(xtime * force, FrameODE(fibre[F - 1], 2)));
        addPosRelForceBodyODE(fibre[0], vec3(.0, .0, -.5 * lf[0]),
                              mult3(-xtime * force, FrameODE(fibre[0], 2)));
      }
      if (torque != .0) {
        // add torque +/- to both extremities
        addTorqueBodyODE(fibre[F - 1],
                         mult3(xtime * torque, FrameODE(fibre[F - 1], 2)));
        addTorqueBodyODE(fibre[0],
                         mult3(-xtime * torque, FrameODE(fibre[0], 2)));
      }
    }

    // stacking, DNA-histone tail interaction
    // we model the N-terminal tail (length is 10 nm) with a gaussian and
    // a std that corresponds to the gyration radius of a sphere 3*R^2/5
    // where R is the radius of the sphere
    if (bdna_tail || bsolenoid || bstacking)
      partition_nucleosomes(fibre, rf, lf, nucleosomes, F,
                            (bsolenoid || bstacking) ? (xtime * ksolenoid) : .0,
                            estacking, (bdna_tail) ? (xtime * Ddna_tail) : .0,
                            5. * nm / fL);

    // intrinsic curvature
    if (false && bintrinsic_curvature && intrinsic_curvature > .0) {
      for (int n = 0; n < (nucleosomes - 1); n++) {
        if (false) {
          // d0=kB*temp*lp*intrinsic_curvature/fE;
          d0 = .0;
          for (int i = (nucleosome_start[n] + NNC); i < nucleosome_start[n + 1];
               i++)
            d0 += lf[i];
          // d0*=kB*temp*(d0/(nucleosome_start[n+1]-1-nucleosome_start[n]-NNC+1))*intrinsic_curvature/fE;
          // d0*=kB*temp*d0*intrinsic_curvature/fE;
          d0 *= kB * temp * lp * intrinsic_curvature / fE;
          // bn=normalize3(cross_product(FrameODE(fibre[nucleosome_start[n]+NNC],2),FrameODE(fibre[nucleosome_start[n+1]-1],2)));
          // addTorqueBodyODE(fibre[nucleosome_start[n]+NNC],mult3(-d0,bn));
          // addTorqueBodyODE(fibre[nucleosome_start[n+1]-1],mult3(d0,bn));
          //
          bn = normalize3(
              cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2),
                            FrameODE(fibre[nucleosome_start[n + 1]], 2)));
          addTorqueBodyODE(fibre[nucleosome_start[n] + NNC - 1],
                           mult3(-d0, bn));
          addTorqueBodyODE(fibre[nucleosome_start[n + 1]], mult3(d0, bn));
        } else {
          ta = normalize3(
              cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2),
                            FrameODE(fibre[nucleosome_start[n + 1]], 2)));
          for (int i = (nucleosome_start[n] + NNC - 1);
               i < nucleosome_start[n + 1]; i++) {
            d1 = dot_product(FrameODE(fibre[i], 2), FrameODE(fibre[i + 1], 2));
            if (fabs(d1) > .999)
              continue;
            // relationship between torque C and intrinsic curvature kappa0:
            // C=kB*T*lp*kappa0
            // -C*lb*kappa
            // where kappa=sqrt(2*(1-dot(t_1,t_2)))/lb
            // T=-cross(t_1,diff(-C*lb*kappa,t_1))
            //  =-C*cross(t_1,t_2)/sqrt(2*(1-dot(t_1,t_2)))
            d0 = -xtime * (kB * temp * lp * intrinsic_curvature / fE) /
                 sqrt(2. * (1. - d1));
            bn =
                cross_product(FrameODE(fibre[i], 2), FrameODE(fibre[i + 1], 2));
            // d1=(dot_product(ta,bn)<.0)?-1.:1.;
            // d0*=d1;
            addTorqueBodyODE(fibre[i], mult3(d0, bn));
            addTorqueBodyODE(fibre[i + 1], mult3(-d0, bn));
          }
        }
      }
    }

    // add bending torque to each linker
    if (bintrinsic_bending_torque && intrinsic_bending_torque != .0) {
      for (int n = 0; n < (nucleosomes - 1); n++) {
        if ((nucleosome_start[n] + NNC) == nucleosome_start[n + 1])
          continue;
        if (0) {
          bn = normalize3(
              cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2),
                            FrameODE(fibre[nucleosome_start[n + 1]], 2)));
          addTorqueBodyODE(fibre[nucleosome_start[n] + NNC - 1],
                           mult3(-xtime * intrinsic_bending_torque, bn));
          addTorqueBodyODE(fibre[nucleosome_start[n + 1]],
                           mult3(xtime * intrinsic_bending_torque, bn));
        }
        //
        if (0) {
          // choose
          // E=(kB*T*lp/2)*integrate kappa^2*ds from s=0 to L
          // let say kappa~cst along the chain
          // E=(kB*T*lp/2)*kappa0^2*L
          // kappa0=sqrt(2*C/(kB*T*lp*L))
          for (int i = (nucleosome_start[n] + NNC - 1);
               i < nucleosome_start[n + 1]; i++) {
            bn = normalize3(cross_product(FrameODE(fibre[i], 2),
                                          FrameODE(fibre[i + 1], 2)));
            d0 = -1.;
            d1 = 1.;
            d0 = d1 = .0;
            if (i == (nucleosome_start[n] + NNC - 1)) {
              ta = cross_product(
                  FrameODE(fibre[nucleosome_start[n] + NNC - 1], 0),
                  FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2));
              d0 = sign(dot_product(
                  ta,
                  bn)); //*(nucleosome_start[n+1]-(nucleosome_start[n]+NNC-1)+1);
              d1 = -d0;
            }
            if (i == (nucleosome_start[n + 1] - 1)) {
              ta = cross_product(FrameODE(fibre[nucleosome_start[n + 1]], 0),
                                 FrameODE(fibre[nucleosome_start[n + 1]], 2));
              d0 = sign(dot_product(
                  ta,
                  bn)); //*(nucleosome_start[n+1]-(nucleosome_start[n]+NNC-1)+1);
              d1 = -d0;
            }
            if (d0 != .0 && d1 != .0) {
              addTorqueBodyODE(
                  fibre[i], mult3(xtime * d0 * intrinsic_bending_torque, bn));
              addTorqueBodyODE(
                  fibre[i + 1],
                  mult3(xtime * d1 * intrinsic_bending_torque, bn));
            }
          }
        }
        //
        if (0) {
          bn = normalize3(
              cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2),
                            FrameODE(fibre[nucleosome_start[n + 1]], 2)));
          for (int i = (nucleosome_start[n] + NNC - 1);
               i < nucleosome_start[n + 1]; i++) {
            addTorqueBodyODE(fibre[i],
                             mult3(xtime * intrinsic_bending_torque, bn));
            addTorqueBodyODE(fibre[i + 1],
                             mult3(xtime * intrinsic_bending_torque, bn));
          }
        }
        //
        if (1) {
          if (intrinsic_bending_torque > .0) {
            // -C*lb*kappa
            // where kappa=sqrt(2*(1-dot(t_1,t_2)))/lb
            // T=-cross(t_1,diff(-C*lb*kappa,t_1))
            //  =-C*cross(t_1,t_2)/sqrt(2*(1-dot(t_1,t_2)))
            // add torque along the binormal
            for (int i = (nucleosome_start[n] + NNC - 1);
                 i < nucleosome_start[n + 1]; i++) {
              d1 =
                  dot_product(FrameODE(fibre[i], 2), FrameODE(fibre[i + 1], 2));
              if (fabs(d1) > .999)
                continue;
              d0 = -xtime * intrinsic_bending_torque / sqrt(2. * (1. - d1));
              bn = normalize3(cross_product(FrameODE(fibre[i], 2),
                                            FrameODE(fibre[i + 1], 2)));
              addTorqueBodyODE(fibre[i], mult3(d0, bn));
              addTorqueBodyODE(fibre[i + 1], mult3(-d0, bn));
            }
          } else {
            // add torque along the cross-product between
            // minor-groove direction and tangent to the DNA
            for (int i = (nucleosome_start[n] + NNC - 1);
                 i < nucleosome_start[n + 1]; i++) {
              bn = FrameODE(fibre[i], 1);
              addTorqueBodyODE(fibre[i],
                               mult3(-xtime * intrinsic_bending_torque, bn));
              addTorqueBodyODE(fibre[i + 1],
                               mult3(xtime * intrinsic_bending_torque, bn));
            }
          }
        }
        //
        if (0) {
          bn = cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 1], 0),
                             FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2));
          for (int i = (nucleosome_start[n] + NNC - 1);
               i < nucleosome_start[n + 1]; i++) {
            if (i == (nucleosome_start[n] + NNC - 1)) {
              addTorqueBodyODE(fibre[i],
                               mult3(xtime * intrinsic_bending_torque, bn));
              addTorqueBodyODE(fibre[i + 1],
                               mult3(-xtime * intrinsic_bending_torque, bn));
            } else {
              ta = normalize3(cross_product(FrameODE(fibre[i], 2),
                                            FrameODE(fibre[i + 1], 2)));
              d0 = arccos(dot_product(FrameODE(fibre[i], 2),
                                      FrameODE(fibre[i + 1], 2)));
              bn = rotation(ta, d0, bn);
              addTorqueBodyODE(fibre[i],
                               mult3(xtime * intrinsic_bending_torque, bn));
              addTorqueBodyODE(fibre[i + 1],
                               mult3(-xtime * intrinsic_bending_torque, bn));
            }
          }
        }
      }
    }

    // add torsion torque to each linker
    if (bintrinsic_torsion_torque && intrinsic_torsion_torque != .0) {
      for (int n = 0; n < (nucleosomes - 1); n++) {
        bn1 = normalize3(
            cross_product(FrameODE(fibre[nucleosome_start[n] + NNC - 2], 2),
                          FrameODE(fibre[nucleosome_start[n] + NNC - 1], 2)));
        bn2 = normalize3(
            cross_product(FrameODE(fibre[nucleosome_start[n + 1]], 2),
                          FrameODE(fibre[nucleosome_start[n + 1] + 1], 2)));
        bn = normalize3(cross_product(bn1, bn2));
        addTorqueBodyODE(fibre[nucleosome_start[n] + NNC - 1],
                         mult3(-xtime * intrinsic_torsion_torque, bn));
        addTorqueBodyODE(fibre[nucleosome_start[n + 1]],
                         mult3(xtime * intrinsic_torsion_torque, bn));
      }
    }

    // add twisting torque to each linker
    if (bintrinsic_twisting_torque && intrinsic_twisting_torque != .0) {
      for (int n = 0; n < (nucleosomes - 1); n++) {
        for (int i = (nucleosome_start[n] + NNC - 1);
             i < nucleosome_start[n + 1]; i++) {
          bn = mult3(1. / (1. + dot_product(FrameODE(fibre[i], 2),
                                            FrameODE(fibre[i + 1], 2))),
                     add3(FrameODE(fibre[i], 2), FrameODE(fibre[i + 1], 2)));
          addTorqueBodyODE(fibre[i],
                           mult3(-xtime * intrinsic_twisting_torque, bn));
          addTorqueBodyODE(fibre[i + 1],
                           mult3(xtime * intrinsic_twisting_torque, bn));
        }
      }
    }

    // add loop anchor every 'loop_every' nucleosomes
    if (bloop_attraction && kloop != .0) {
      for (int n = 0; n < (nanchors - 1); n++) {
        i0 = anchors[n];
        i1 = anchors[n + 1];
        rij = sub3(PositionBodyODE(fibre[i0]), PositionBodyODE(fibre[i1]));
        d0 = 3. * sloop;
        if (norm3(rij) > d0) {
          // too far, pull
          addForceBodyODE(fibre[i0],
                          mult3(-xtime * kloop * d0 / norm3(rij), rij));
          addForceBodyODE(fibre[i1],
                          mult3(xtime * kloop * d0 / norm3(rij), rij));
        } else {
          bscaffold[n] = true;
          // spring
          addForceBodyODE(fibre[i0], mult3(-xtime * kloop, rij));
          addForceBodyODE(fibre[i1], mult3(xtime * kloop, rij));
        }
      }
      scaffold = true;
      for (int n = 0; n < (nanchors - 1); n++)
        scaffold = (scaffold && bscaffold[n]);
    }

    // add H1
    if (prob_h1 > .0 && false) {
      if (s == 0) {
        for (int n = 0; n < nucleosomes; n++) {
          rij = sub3(PositionBodyODE(fibre[nucleosome_start[n] - 1]),
                     PositionBodyODE(fibre[nucleosome_start[n] + NNC]));
          if ((nucleosome_start[n] - 1) >= 0 && (nucleosome_start[n] + NNC) < F)
            print3(mult3(fL / nm, rij));
        }
      }
      for (int n = 0; n < nucleosomes; n++) {
        if (h1s[n] == 0)
          continue;
        if ((nucleosome_start[n] - 1) < 0 || (nucleosome_start[n] + NNC) >= F)
          continue;
        rij = sub3(PositionBodyODE(fibre[nucleosome_start[n] - 1]),
                   PositionBodyODE(fibre[nucleosome_start[n] + NNC]));
        addForceBodyODE(
            fibre[nucleosome_start[n] - 1],
            mult3(-3. * (kB * temp / fE) / pow(4.6 * nm / fL, 2.), rij));
        addForceBodyODE(
            fibre[nucleosome_start[n] + NNC],
            mult3(3. * (kB * temp / fE) / pow(4.6 * nm / fL, 2.), rij));
      }
    }

    // add force and torque to both extremity of the chromatin fibre
    if (reversomes > 0) {
      if (s == step0)
        zz = force * sign(PositionBodyODE(fibre[F - 1]).z -
                          PositionBodyODE(fibre[0]).z);
      // create barrier
      if ((s == rstep && step0 == 0) ||
          (s >= rstep && step0 > 0 && s == step0)) {
        printf("create barrier\n");
        ur = FrameODE(fibre[0], 0);
        vr = FrameODE(fibre[0], 1);
        tr = FrameODE(fibre[0], 2);
      }
      // disable SHLs 6.5, docking domains and four-helix-bundle joints
      // if 'model' is 0 do not disable and keep track of the joints feedback
      if ((s == rstep && step0 == 0) ||
          (s >= rstep && step0 > 0 && s == step0)) {
        printf("reversome\n");
        if (nucleosome_joints) {
          dofs += reversome_disable_joints(&jfibre, reversome0, reversome1, J);
          mE = .5 * dofs * kB * temp / fE;
        }
        // write size of the bodies
        tmp = sprintf(name, "visualisation/sn%i_%s.out", seed, chromatin_in);
        fOut = fopen(name, "w");
        for (int f = 0; f < F; f++) {
          if (nucleosome_start[reversome0] <= f &&
              f < (nucleosome_start[reversome1] + NNC))
            tmp =
                fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[f], lf[f], f, 11, 0);
          else
            tmp =
                fprintf(fOut, "%i %f %f %i %i %i\n", 0, rf[f], lf[f], f, 0, 0);
        }
        // for(int n=0;n<nucleosomes;n++) for(int i=0;i<4;i++)
        // tmp=fprintf(fOut,"%i %f %f %i %i
        // %i\n",0,rh,lh,F+n*NHC+i,(i+1)*(i<3)+9*(i==3),0);
        for (int n = 0; n < nucleosomes; n++)
          for (int i = 0; i < 4; i++)
            tmp =
                fprintf(fOut, "%i %f %f %i %i %i\n", 0, rh, lh, F + n * NHC + i,
                        11 * (i == 0 || i == 3) + 12 * (i == 1 || i == 2), 0);
        if (!noSM)
          tmp = fprintf(fOut, "%i %f %f %i %i %i\n", 1,
                        dGeomSphereGetRadius(gfibre[B - 1]),
                        dGeomSphereGetRadius(gfibre[B - 1]), B - 1, 1, 0);
        fclose(fOut);
      }
      // torque
      if (s >= rstep) {
        if (s == rstep)
          printf("reversome, apply torque to nucleosome %i\n", reversome1);
        nc = nucleosome_start[reversome1] + NNC;
        // nc=nucleosome_start[reversome1]+NNC-1;
        addTorqueBodyODE(fibre[nc], mult3(fmin(1., 1e-2 * (s - rstep)) * torque,
                                          FrameODE(fibre[nc], 2)));
        // addTorqueBodyODE(fibre[nc],mult3(torque,FrameODE(fibre[nc],2)));
      }
      // add friction to damp angular velocity around first and last DNA monomer
      // tangent
      if (1) {
        if (s >= rstep)
          set_axes(fibre[0], tr, ur, vr);
        av = AngVelBodyODE(fibre[0]);
        av = sub3(av, mult3(dot_product(av, FrameODE(fibre[0], 2)),
                            FrameODE(fibre[0], 2)));
        dBodySetAngularVel(fibre[0], av.x, av.y, av.z);
        // print3(mult3(dot_product(AngVelBodyODE(fibre[0]),FrameODE(fibre[0],2)),FrameODE(fibre[0],2)));
      }
      if (0) {
        av = AngVelBodyODE(fibre[F - 1]);
        av = sub3(av, mult3(dot_product(av, FrameODE(fibre[F - 1], 2)),
                            FrameODE(fibre[F - 1], 2)));
        dBodySetAngularVel(fibre[F - 1], av.x, av.y, av.z);
        // print3(mult3(dot_product(AngVelBodyODE(fibre[F-1]),FrameODE(fibre[F-1],2)),FrameODE(fibre[F-1],2)));
      }
    }

    if (solve_shls_forces) {
      solve_for_shls_forces(fibre, lf, uvtm1, .0, seed);
      return 0;
    }

    if (simplified_nucleosome == 0 && !nucleosome_joints) {
      // dofs+=replace_nucleosome_joints_by_potentials(&jfibre,0,nucleosomes-1,J,Dm,em,am,fm,Em,rm,uvtm1,uvtm2,"shls",true,false);
      dofs += replace_nucleosome_joints_by_potentials(
          &jfibre, 0, nucleosomes - 1, J, Dm, em, am, fm, Em, rm, uvtm1, uvtm2,
          "shls25811", true, false);
      // dofs+=replace_nucleosome_joints_by_potentials(&jfibre,0,nucleosomes-1,J,Dm,em,am,fm,Em,rm,uvtm1,uvtm2,"dds",true,false);
      dofs += replace_nucleosome_joints_by_potentials(
          &jfibre, 0, nucleosomes - 1, J, Dm, em, am, fm, Em, rm, uvtm1, uvtm2,
          "fhbs", true, false);
      if (s == (step0 + (int)(step0 > 0)))
        printf("new dofs=%i\n", dofs);
      mE = .5 * dofs * kB * temp / fE;
    }

    if (!no_collisions) {
      // ???
      // disable one over two 1 bp geometry
      // (Pi*1.0^2*3.5721*3.0/10.5)/(4.0*Pi*1.0^3/3)=0.76545
      // (Pi*1.0^2*3.5721*4.0/10.5)/(4.0*Pi*1.0^3/3)=1.0206
      if (true && min_lbp == 1.) {
        for (int n = 0; n < (nucleosomes - 1); n++) {
          for (int f = (nucleosome_start[n] + NNC); f < nucleosome_start[n + 1];
               f += 2) {
            if (lbp[f] > 1.)
              continue;
            dGeomDisable(gfibre[f]);
            dGeomDisable(ggfibre[f]);
          }
        }
        if (nucleosome_start[0] > 0) {
          for (int f = 1; f < nucleosome_start[0]; f += 2) {
            if (lbp[f] > 1.)
              continue;
            dGeomDisable(gfibre[f]);
            dGeomDisable(ggfibre[f]);
          }
        }
        if ((nucleosome_start[nucleosomes - 1] + NNC - 1) < (F - 1)) {
          for (int f = (nucleosome_start[nucleosomes - 1] + NNC); f < (F - 1);
               f += 2) {
            if (lbp[f] > 1.)
              continue;
            dGeomDisable(gfibre[f]);
            dGeomDisable(ggfibre[f]);
          }
        }
      }
      // ???
      // test Verlet-list implementation ?
      if (test_Vl) {
        nc_test_Vl = int3p(4, 0, 0);
        dSpaceCollide(SpaceODE, &nc_test_Vl, &test_nearCallback);
      }
      d0 = clock();
      if (1) {
        if (Vl_every > 1) {
          if ((s % Vl_every) == 0 || s == step0 || s == 1) {
            for (int i = 0; i < FH; i++)
              (dfibre[i].p)[0] = 2;
            if (debug)
              printf("partition %s %i\n", chromatin_in, s);
            nc = partition_v0(fibre, ggfibre, FH, starts, ends, WorldODE,
                              contactgroup,
                              2. * xghost * (.5 * 3.5721 + 1.) * nm / fL,
                              sum_abs_depth, max_abs_depth, true, true);
            if (debug)
              printf("ok partition %s %i\n\n", chromatin_in, s);
          }
          if (debug)
            printf("collide all %s %i\n", chromatin_in, s);
          nc = collide_all(WorldODE, gfibre, dfibre, FH, wERP, wCFM,
                           contactgroup, sum_abs_depth, max_abs_depth);
          if (debug)
            printf("ok collide all %s %i\n\n", chromatin_in, s);
        } else
          nc = partition_v0(fibre, gfibre, FH, starts, ends, WorldODE,
                            contactgroup, lp2r, sum_abs_depth, max_abs_depth,
                            false, false);
      } else {
        // do not collide h1 against dGeomID
        // build the Verlet-list
        vl = build_Verlet_list(gSpaceODE, s - step0, Vl_every, &dfibre, FH, 4,
                               my_nearCallback, false);
        // collide all in the normal space
        nc = collide_all(WorldODE, gfibre, dfibre, FH, wERP, wCFM, contactgroup,
                         sum_abs_depth, max_abs_depth);
        if (test_Vl && nc != nc_test_Vl.j) {
          // for(int n=0;n<chromatin_size[1];n++) for(int
          // v=2;v<(dfibre[n].p)[0];v++) printf("VL %i
          // %i\n",n,(dfibre[n].p)[v]);
          printf("s=%i ODE=%i VL=%i/%i\n", s, nc_test_Vl.j, nc, vl);
          return 0;
        }
      }
      partitiontime += (clock() - d0);
      // for segmentation between 1 and 3 bp per DNA segment, length is lesser
      // than radius therefore we remove collision(s) between segment 'i' and
      // segment 'i+x'
      if (min_lbp < 6.)
        remove_artificial_collisions(nucleosomes, F, sbp, lbp, rf, fibre,
                                     gfibre, nc, sum_abs_depth);
    } else
      nc = 0;

    if (vreset || mstep < MSTEP) {
      // decrease velocities
      for (int b = 0; b < B; b++) {
        lv = LinVelBodyODE(fibre[b]);
        av = AngVelBodyODE(fibre[b]);
        i0 = 0;
        if (norm3(lv) > vlim) {
          i0++;
          lv = mult3(vlim / norm3(lv), lv);
        }
        if (norm3(av) > vlim) {
          i0++;
          av = mult3(vlim / norm3(av), av);
        }
        if (i0 > 0)
          set3_velocities(fibre[b], lv, av);
      }
    } else {
      // Langevin-Euler (local thermostat)
      d0 = clock();
      if (use_std_thread) {
        std::thread first(langevin_euler, fibre, sigmaT, sigmaR, dt,
                          fE / (kB * temp), 0, (B - B % 2) / 2, std::ref(mt1));
        std::thread second(langevin_euler, fibre, sigmaT, sigmaR, dt,
                           fE / (kB * temp), (B - B % 2) / 2 + 1, B - 1,
                           std::ref(mt2));
        first.join();
        second.join();
      } else
          // if(1 || s<200 || scaffold || !bloop_attraction)
          //   langevin_euler(fibre,sigmaT,sigmaR,dt,fE/(kB*temp),0,B-1,std::ref(mt0));
          // else
          //   global_rescaling(fibre,0,B-1,dofs-3*nc,kB*temp/fE,1./avgtau,dt,std::ref(mt0));
          if (!bglobal || (bglobal && s < 100))
        langevin_euler(fibre, sigmaT, sigmaR, dt, fE / (kB * temp), 0, B - 1,
                       std::ref(mt0));
      else {
        global_rescaling(fibre, 0, B - 1,
                         (0 && cumE[1] >= 1000)
                             ? (int)floor(dofs * mE / (cumE[0] / cumE[1]))
                             : dofs,
                         kB * temp / fE, 1. / tau, dt, std::ref(mt0));
        if (0 && (s % 100) == 0)
          flying_ice_cube_correction(fibre, B);
      }
      // global_rescaling(fibre,0,B-1,dofs-3*nc,kB*temp/fE,1./avgtau,dt,std::ref(mt0));
      langevintime += (clock() - d0);
    }

    // save positions
    for (int i = 0; i < FH; i++)
      old_pi[i] = PositionBodyODE(fibre[i]);
    // step the dWorldID
    d0 = clock();
    if (debug)
      printf("step the world %s %i\n", chromatin_in, s);
    if (accurate_step)
      dWorldStep(WorldODE, dt);
    else
      dWorldQuickStep(WorldODE, dt);
    if (debug)
      printf("ok step the world %s %i\n\n", chromatin_in, s);
    if (contactgroup)
      dJointGroupEmpty(contactgroup);
    solvetime += (clock() - d0);
    // printf("%f\n",langevintime/solvetime);
    // print sqrt(<dr^2>)
    avgdr = .0;
    for (int i = 0; i < FH; i++)
      avgdr += dot_product2(sub3(PositionBodyODE(fibre[i]), old_pi[i]));
    avgdr = sqrt(avgdr / (double)FH);
    if ((s % every) == 0 && Vl_every > 1)
      printf("s=%i Vl_every*sqrt(<dr^2>)=%f v. x*radius-radius=%f nm (%i "
             "collision(s))\n",
             s, Vl_every * avgdr * fL / nm, (xghost * rf[0] - rf[0]) * fL / nm,
             nc);

    // dLk
    if (!noSM) {
      dLk += (AngVelBodyODE(fibre[B - 1]).z) * dt / (2. * pi);
      if (mode == 0 && ((nLk < 0. && dLk <= nLk) || (nLk >= 0. && dLk >= nLk)))
        locked = true;
    }

    // kinetic energy
    Et = Ktr(fibre, 0, B - 1, false, 2);

    if (every_sqd > 0 && (s % every_sqd) == 0 && mstep == MSTEP) {
      // // write square distances
      // for(int n=0;n<=3;n++){
      // 	tmp=sprintf(name,"sqdistances/dn%i/dn%i_%i_%s_s%i.out",n,n,seed,chromatin_in,s);
      // 	write_sqdistances(fibre,nucleosomes,F,name,"w",n);
      // }
      // write nucleosome-nucleosome square distances (between center-of-mass)
      tmp = sprintf(name, "sqdistances/nucnuc_%s.out", chromatin_in);
      write_nucnuc_sqdistances(fibre, nucleosomes, F, name, "a");
    }

    if ((s % every) == 0 && mstep == MSTEP) {
      n0 = max(0, (int)(.5 * nucleosomes) - 5);
      n1 = min(nucleosomes, (int)(.5 * nucleosomes) + 5);
      // write Ree per linker and Ree
      tmp = sprintf(name, "geometry/Ree_n%i_%s.out", seed, chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      tmp = fprintf(
          fOut, "%i %f\n", s,
          norm3(
              sub3(PositionPosRelBodyODE(fibre[F - 1], 0., 0., .5 * lf[F - 1]),
                   PositionPosRelBodyODE(fibre[0], 0., 0., -.5 * lf[0]))) *
              fL / nm);
      fclose(fOut);
      // write local curvature and local twist as-well-as local curvature and
      // local twist energies
      tmp = sprintf(name, "geometry/local_curvature_and_twist_n%i_%s.out", seed,
                    chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      for (int n = n0; n < (n1 - 1); n++) {
        // if((nucleosome_start[n]+NNC)>=(nucleosome_start[n+1]-1)) continue;
        for (int f = (nucleosome_start[n] + NNC - 1);
             f < nucleosome_start[n + 1]; f++)
          tmp = fprintf(
              fOut, "%i %i %f %f %f %f %f\n", s, n,
              local_curvature(fibre[f], fibre[f + 1], lf[f], lf[f + 1]) * nm /
                  fL,
              local_twist(fibre[f], fibre[f + 1]),
              local_twist(fibre[f], fibre[f + 1], tw0[f]),
              gb[f] *
                  (1. - dot_product(FrameODE(fibre[f], 2),
                                    FrameODE(fibre[f + 1], 2))) *
                  fE / (kB * temp),
              .5 * gt[f] *
                  pow(local_twist(fibre[f], fibre[f + 1], tw0[f]), 2.) * fE /
                  (kB * temp));
      }
      fclose(fOut);
      // write local torsion
      tmp = sprintf(name, "geometry/local_torsion_n%i_%s.out", seed,
                    chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      for (int n = n0; n < (n1 - 1); n++) {
        // if((nucleosome_start[n]+NNC)>=(nucleosome_start[n+1]-1)) continue;
        chain_local_torsion(fibre, lf, F, nucleosome_start[n] + NNC - 1,
                            nucleosome_start[n + 1], torsions);
        for (int f = (nucleosome_start[n] + NNC - 1);
             f < (nucleosome_start[n + 1] - 1); f++)
          tmp =
              fprintf(fOut, "%i %i %f\n", s, n,
                      torsions[f - (nucleosome_start[n] + NNC - 1)] * nm / fL);
      }
      fclose(fOut);
      // write frame u, v and t
      if (true) {
        tmp = sprintf(name, "geometry/uvt_n%i_%s.out", seed, chromatin_in);
        fOut = fopen(name, (s == 0) ? "w" : "a");
        for (int n = n0; n < (n1 - 1); n++) {
          for (int f = (nucleosome_start[n] + NNC); f < nucleosome_start[n + 1];
               f++)
            tmp = fprintf(fOut, "%i %i %f %f %f %f %f %f\n", s, n,
                          FrameODE(fibre[f], 0).x, FrameODE(fibre[f], 0).y,
                          FrameODE(fibre[f], 0).z, FrameODE(fibre[f], 2).x,
                          FrameODE(fibre[f], 2).y, FrameODE(fibre[f], 2).z);
        }
        fclose(fOut);
      }
      // write average curvature, Tw, Wr and end-to-end distance per linker
      tmp = sprintf(name, "geometry/avgcurvature_Tw_Wr_per_linker_n%i_%s.out",
                    seed, chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      for (int n = n0; n < (n1 - 1); n++) {
        if ((nucleosome_start[n] + NNC) >= (nucleosome_start[n + 1] - 1))
          continue;
        Tw = sum_of_local_twist(fibre, F, nucleosome_start[n] + NNC,
                                nucleosome_start[n + 1] - 1) /
             (2. * pi);
        Wr = chain_writhe_ODE(fibre, lf, nucleosome_start[n] + NNC,
                              nucleosome_start[n + 1] - 1, true, false, 1, 16);
        i0 = nucleosome_start[n] + NNC;
        i1 = nucleosome_start[n + 1] - 1;
        tmp = fprintf(
            fOut, "%i %i %f %f %f %f %f\n", s, n,
            sum_of_local_curvature(fibre, lf, F, nucleosome_start[n] + NNC,
                                   nucleosome_start[n + 1] - 1, true) *
                nm / fL,
            Tw,
            sum_of_local_twist(fibre, F, nucleosome_start[n] + NNC,
                               nucleosome_start[n + 1] - 1, tw0) /
                (2. * pi),
            Wr,
            norm3(
                sub3(PositionPosRelBodyODE(fibre[i1], 0., 0., .5 * lf[i1]),
                     PositionPosRelBodyODE(fibre[i0], 0., 0., -.5 * lf[i0]))) *
                fL / nm);
      }
      fclose(fOut);
      // write average curvature, Tw, Wr and end-to-end distance per nucleosome
      tmp =
          sprintf(name, "geometry/avgcurvature_Tw_Wr_per_nucleosome_n%i_%s.out",
                  seed, chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      for (int n = n0; n < n1; n++) {
        i0 = nucleosome_start[n];
        i1 = nucleosome_start[n] + NNC - 1;
        Tw = sum_of_local_twist(fibre, F, nucleosome_start[n],
                                nucleosome_start[n] + NNC - 1) /
             (2. * pi);
        Wr =
            chain_writhe_ODE(fibre, lf, nucleosome_start[n],
                             nucleosome_start[n] + NNC - 1, true, false, 1, 16);
        tmp = fprintf(
            fOut, "%i %i %f %f %f %f %f\n", s, n,
            sum_of_local_curvature(fibre, lf, F, nucleosome_start[n],
                                   nucleosome_start[n] + NNC - 1, true) *
                nm / fL,
            Tw,
            sum_of_local_twist(fibre, F, nucleosome_start[n],
                               nucleosome_start[n] + NNC - 1, tw0) /
                (2. * pi),
            Wr,
            norm3(
                sub3(PositionPosRelBodyODE(fibre[i1], 0., 0., .5 * lf[i1]),
                     PositionPosRelBodyODE(fibre[i0], 0., 0., -.5 * lf[i0]))) *
                fL / nm);
      }
      fclose(fOut);
      // write average curvature, Tw and Wr per nucleosome+linker+nucleosome
      if (false) {
        tmp = sprintf(name,
                      "geometry/avgcurvature_Tw_Wr_per_dinucleosome_n%i_%s.out",
                      seed, chromatin_in);
        fOut = fopen(name, (s == 0) ? "w" : "a");
        for (int n = n0; n < (n1 - 1); n++) {
          if ((nucleosome_start[n] + NNC) >= (nucleosome_start[n + 1] - 1))
            continue;
          Tw = sum_of_local_twist(fibre, F, nucleosome_start[n],
                                  nucleosome_start[n + 1] + NNC - 1) /
               (2. * pi);
          Wr = chain_writhe_ODE(fibre, lf, nucleosome_start[n],
                                nucleosome_start[n + 1] + NNC - 1, true, false,
                                1, 16);
          tmp = fprintf(
              fOut, "%i %i %f %f %f %f\n", s, n,
              sum_of_local_curvature(fibre, lf, F, nucleosome_start[n],
                                     nucleosome_start[n + 1] + NNC - 1, true) *
                  nm / fL,
              Tw,
              sum_of_local_twist(fibre, F, nucleosome_start[n],
                                 nucleosome_start[n + 1] + NNC - 1, tw0) /
                  (2. * pi),
              Wr);
        }
        fclose(fOut);
      }
    }

    if ((mstep % every) == 0 && mstep < MSTEP) {
      // save for visualisation
      tmp = sprintf(name, "visualisation/vmn%i_%s.out", seed, chromatin_in);
      write_visualisation(fibre, name, 0, B, (mstep == 0) ? "w" : "a");
    }
    if ((s % every) == 0 && mstep == MSTEP) {
      // save for visualisation
      tmp = sprintf(name, "visualisation/vn%i_%s.out", seed, chromatin_in);
      write_visualisation(fibre, name, 0, B,
                          (start_at_zero && s == 0) ? "w" : "a");
      // Center-of-mass to 0,0,0
      reset_com(fibre, B);
      // save frame, rotation around the com and along z-axis
      for (int i = 0; i < B; i++) {
        old_ui[i] = FrameODE(fibre[i], 0);
        old_vi[i] = FrameODE(fibre[i], 1);
        old_ti[i] = FrameODE(fibre[i], 2);
      }
      // compute gyration tensor and write radius of gyration
      gyration_tensor(fibre, (int)floor(.1 * F), (int)floor(.9 * F), ev, evec1,
                      evec2, evec3);
      tmp =
          sprintf(name, "geometry/Rg1_Rg2_Rg3_n%i_%s.out", seed, chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      tmp = fprintf(fOut, "%e %e %e\n", sqrt(ev.x) * fL / nm,
                    sqrt(ev.y) * fL / nm, sqrt(ev.z) * fL / nm);
      fclose(fOut);
      // Rotate conformation if and only if number of anchors is > 0.
      // Rotation axis is determined by the position of the first and last anchors.
      if (nanchors > 1){
	bn = normalize3(sub3(PositionBodyODE(fibre[anchors[nanchors - 1]]),
			     PositionBodyODE(fibre[anchors[0]])));
	rotations(normalize3(cross_product(bn, vec3(.0, .0, 1.))),
		  arccos(dot_product(bn, vec3(.0, .0, 1.))), fibre, 0, B - 1,
		  vec3(.0, .0, .0));
	// Rotation of the conformation
	tmp = sprintf(name, "visualisation/fn%i_%s.out", seed, chromatin_in);
	d0 = 2. * pi / 10.;
	for (int k = 0; k <= 10; k++) {
	  rotations(vec3(.0, .0, 1.), d0, fibre, 0, B - 1, vec3(.0, .0, .0));
	  write_visualisation(fibre, name, 0, B, (k == 0) ? "w" : "a");
	}
	// Back to conformation before rotation.
	for (int i = 0; i < B; i++) {
	  set_position(fibre[i], old_pi[i]);
	  set_axes(fibre[i], old_ti[i], old_ui[i], old_vi[i]);
	}
      }
      // save the conformation
      tmp =
          sprintf(name, "conformations/cn%i_%s_s%i.out", seed, chromatin_in, s);
      read_write_conformation(fibre, name, "write", 0, B);
      if (reversomes > 0) {
        // write Tw and Wr
        Tw = sum_of_local_twist(fibre, F, nucleosome_start[reversome0],
                                nucleosome_start[reversome1] + NNC - 1) /
             (2. * pi);
        Wr = chain_writhe_ODE(fibre, lf, nucleosome_start[reversome0],
                              nucleosome_start[reversome1] + NNC - 1, true,
                              false, 1, 16);
        tmp = sprintf(name, "writhe/n%i_%s.out", seed, chromatin_in);
        fOut = fopen(name, (s == 0) ? "w" : "a");
        tmp = fprintf(fOut, "%i %e %e\n", s, Tw, Wr);
        fclose(fOut);
      }
    }

    // print log
    if (((s % every) == 0 && mstep == MSTEP) ||
        ((mstep % every) == 0 && mstep < MSTEP)) {
      // Tw=sum_of_local_twist(fibre,F,0,F-1)/(2.*pi);
      // Wr=chain_writhe_ODE(fibre,lf,0,F-1,true,false,1,16);
      // printf("%i/%i done, z=%.4f nm dLk=%.4f Tw=%.4f Wr=%.4f E/<E>=%.4f %i
      // collision(s)
      // locked=%i\n",s,step0+iterations,fL*PositionPosRelBodyODE(fibre[J],0.,0.,.5*lf[J]).z/nm,Tw+Wr,Tw,Wr,Et/mE,nc,(int)locked);
      if (mstep < MSTEP)
        printf(
            "minimization %i/%i done, R=%.4f nm E/<E>=%.3f <E>_t/<E>=%.3f %i "
            "collision(s)\n",
            mstep, MSTEP,
            norm3(sub3(PositionPosRelBodyODE(fibre[J], 0., 0., .5 * lf[J]),
                       PositionPosRelBodyODE(fibre[0], 0., 0., -.5 * lf[0]))) *
                fL / nm,
            Et / mE, (cumE[0] / cumE[1]) / mE, nc);
      else
        printf(
            "%i/%i done, R=%.4f nm %i collision(s)\n", s, step0 + iterations,
            norm3(sub3(PositionPosRelBodyODE(fibre[J], 0., 0., .5 * lf[J]),
                       PositionPosRelBodyODE(fibre[0], 0., 0., -.5 * lf[0]))) *
                fL / nm,
            nc);
      printf("   E/<E>=%.3f <E>_t/<E>=%.3f\n", Et / mE,
             (cumE[0] / cumE[1]) / mE);
      printf("   K=%.3f R=%.3f\n", Ktr(fibre, 0, B - 1, false, 0),
             Ktr(fibre, 0, B - 1, false, 1));
      printf("   <cos(bending)>=%f sqrt(<tw^2>)=%f\n", cos_tw2[0],
             sqrt(cos_tw2[1]));
      if (bloop_attraction && kloop != .0) {
        for (int n = 0; n < (nanchors - 1); n++) {
          i0 = anchors[n];
          i1 = anchors[n + 1];
          rij = sub3(PositionBodyODE(fibre[i0]), PositionBodyODE(fibre[i1]));
          printf("   anchor(%i-%i)=%f nm\n", i0, i1, norm3(rij) * fL / nm);
        }
      }
      // check local twist
      chain_local_twist(fibre, F, 0, F - 1, tws);
      d0 = DBL_MAX;
      d1 = DBL_MIN;
      for (int f = 0; f < (F - 1); f++) {
        d0 = fmin(d0, tws[f] - tw0[f] - 2. * pi * ((tws[f] - tw0[f]) > pi) +
                          2. * pi * ((tws[f] - tw0[f]) < (-pi)));
        d1 = fmax(d1, tws[f] - tw0[f] - 2. * pi * ((tws[f] - tw0[f]) > pi) +
                          2. * pi * ((tws[f] - tw0[f]) < (-pi)));
      }
      printf("   min(tw-tw0)=%f max(tw-tw0)=%f\n", d0, d1);
      if (nucleosomes > 0) {
        printf(
            "   Wr(nucleosome)=%f\n",
            chain_writhe_ODE(
                fibre, lf,
                nucleosome_start[(nucleosomes - nucleosomes % 2) / 2],
                nucleosome_start[(nucleosomes - nucleosomes % 2) / 2] + NNC - 1,
                true, false, 1, 16));
        printf("   Tw(nucleosome)=%f\n",
               sum_of_local_twist(fibre, F, nucleosome_start[reversome0],
                                  nucleosome_start[reversome1] + NNC - 1) /
                   (2. * pi));
      }
      if (bsolenoid) {
        d2 = .0;
        for (int n = 0; n < (nucleosomes - 1); n++) {
          for (int f = 0; f < NHC; f++)
            for (int g = 0; g < NHC; g++)
              d2 += dot_product2(
                  sub3(PositionBodyODE(fibre[F + n * NHC + f]),
                       PositionBodyODE(fibre[F + (n + 1) * NHC + g])));
        }
        printf("   <dsolenoid>=%f nm\n",
               sqrt(d2 / ((nucleosomes - 1) * NHC * NHC)) * fL / nm);
      }
      if (reversomes > 0) {
        // write Tw and Wr
        Tw = sum_of_local_twist(fibre, F, nucleosome_start[reversome0],
                                nucleosome_start[reversome1] + NNC - 1) /
             (2. * pi);
        Wr = chain_writhe_ODE(fibre, lf, nucleosome_start[reversome0],
                              nucleosome_start[reversome1] + NNC - 1, true,
                              false, 1, 16);
        printf("   Tw(reversomes)=%.4f/%.4f Wr(reversomes)=%.4f/%.4f\n", Tw,
               Twt0, Wr, Wrt0);
      }
    }

    // check kinetic energy
    if (Et > (10. * mE)) {
      printf("s=%i mstep=%i xtime=%f E/<E>=%f is too big, exit.\n", s, mstep,
             xtime, Et / mE);
      tmp = sprintf(name, "error_n%i_%s.out", seed, chromatin_in);
      fOut = fopen(name, "w");
      tmp = fprintf(fOut, "s=%i mstep=%i xtime=%f E/<E>=%f is too big, exit.\n",
                    s, mstep, xtime, Et / mE);
      fclose(fOut);
      return 0;
    }

    if (write_joint_feedback && (s % every) == 0 && mstep == MSTEP) {
      // write joint feedback
      tmp = sprintf(name, "joint_feedback/n%i_%s.out", seed, chromatin_in);
      fOut = fopen(name, (s == 0) ? "w" : "a");
      if (s == 0)
        tmp = fprintf(fOut, "f_pN E_kBT r_nm\n");
      if (simplified_nucleosome == 0 && !nucleosome_joints) {
        for (int n = 0; n < nucleosomes; n++) {
          for (int f = 0; f < (NHC - 1); f++) {
            fi = n * (NHC - 1 + nSHLs + nDOCKs) + f;
            tmp = fprintf(fOut, "%i FHB%i %i %f %f %f\n", n, f, s,
                          norm3(fm[fi]) * fF / pN, Em[fi] * fE / (kB * temp),
                          norm3(rm[fi]) * fL / nm);
          }
          for (int f = 0; f < nSHLs; f++) {
            fi = n * (NHC - 1 + nSHLs + nDOCKs) + NHC - 1 + f;
            tmp = fprintf(fOut, "%i SHL%i %i %f %f %f\n", n, f, s,
                          norm3(fm[fi]) * fF / pN, Em[fi] * fE / (kB * temp),
                          norm3(rm[fi]) * fL / nm);
          }
          for (int f = 0; f < nDOCKs; f++) {
            fi = n * (NHC - 1 + nSHLs + nDOCKs) + NHC - 1 + nSHLs + f;
            tmp = fprintf(fOut, "%i docking_domain%i %i %f %f %f\n", n, f, s,
                          norm3(fm[fi]) * fF / pN, Em[fi] * fE / (kB * temp),
                          norm3(rm[fi]) * fL / nm);
          }
        }
      } else {
        for (int n = 0; n < nucleosomes; n++) {
          for (int f = 0; f < (NHC - 1); f++) {
            fi = J + n * (NHC - 1 + nSHLs + nDOCKs) + f;
            if (dJointIsEnabled(jfibre[fi]))
              tmp = fprintf(fOut, "%i FHB%i %i %f %f %f %f\n", n, f, s,
                            norm3(vec3((fjfibre[fi].f1)[0], (fjfibre[fi].f1)[1],
                                       (fjfibre[fi].f1)[2])) *
                                fF / pN,
                            norm3(vec3((fjfibre[fi].f2)[0], (fjfibre[fi].f2)[1],
                                       (fjfibre[fi].f2)[2])) *
                                fF / pN,
                            norm3(vec3((fjfibre[fi].t1)[0], (fjfibre[fi].t1)[1],
                                       (fjfibre[fi].t1)[2])) *
                                fE / (pN * nm),
                            norm3(vec3((fjfibre[fi].t2)[0], (fjfibre[fi].t2)[1],
                                       (fjfibre[fi].t2)[2])) *
                                fE / (pN * nm));
            else
              tmp = fprintf(fOut, "%i FHB%i %i %f %f %f %f\n", n, f, s, .0, .0,
                            .0, .0);
          }
          for (int f = 0; f < nSHLs; f++) {
            fi = J + n * (NHC - 1 + nSHLs + nDOCKs) + NHC - 1 + f;
            if (dJointIsEnabled(jfibre[fi]))
              tmp = fprintf(fOut, "%i SHL%i %i %f %f %f %f\n", n, f, s,
                            norm3(vec3((fjfibre[fi].f1)[0], (fjfibre[fi].f1)[1],
                                       (fjfibre[fi].f1)[2])) *
                                fF / pN,
                            norm3(vec3((fjfibre[fi].f2)[0], (fjfibre[fi].f2)[1],
                                       (fjfibre[fi].f2)[2])) *
                                fF / pN,
                            norm3(vec3((fjfibre[fi].t1)[0], (fjfibre[fi].t1)[1],
                                       (fjfibre[fi].t1)[2])) *
                                fE / (pN * nm),
                            norm3(vec3((fjfibre[fi].t2)[0], (fjfibre[fi].t2)[1],
                                       (fjfibre[fi].t2)[2])) *
                                fE / (pN * nm));
            else
              tmp = fprintf(fOut, "%i SHL%i %i %f %f %f %f\n", n, f, s, .0, .0,
                            .0, .0);
          }
          for (int f = 0; f < nDOCKs; f++) {
            fi = J + n * (NHC - 1 + nSHLs + nDOCKs) + NHC - 1 + nSHLs + f;
            if (dJointIsEnabled(jfibre[fi]))
              tmp =
                  fprintf(fOut, "%i docking_domain%i %i %f %f %f %f\n", n, f, s,
                          norm3(vec3((fjfibre[fi].f1)[0], (fjfibre[fi].f1)[1],
                                     (fjfibre[fi].f1)[2])) *
                              fF / pN,
                          norm3(vec3((fjfibre[fi].f2)[0], (fjfibre[fi].f2)[1],
                                     (fjfibre[fi].f2)[2])) *
                              fF / pN,
                          norm3(vec3((fjfibre[fi].t1)[0], (fjfibre[fi].t1)[1],
                                     (fjfibre[fi].t1)[2])) *
                              fE / (pN * nm),
                          norm3(vec3((fjfibre[fi].t2)[0], (fjfibre[fi].t2)[1],
                                     (fjfibre[fi].t2)[2])) *
                              fE / (pN * nm));
            else
              tmp = fprintf(fOut, "%i docking_domain%i %i %f %f %f %f\n", n, f,
                            s, .0, .0, .0, .0);
          }
        }
      }
      fclose(fOut);
    }

    if (mstep < MSTEP) {
      remove_com(fibre, 0, B - 1);
      s--;
      mstep++;
    } // else return 0;
  }   // end simulation loop

  // clean ODE objects (dJointID, dBodyID and dGeomID)
  for (int f = 0; f < B; f++) {
    tmp = dBodyGetNumJoints(fibre[f]);
    for (int j = (tmp - 1); j >= 0; j--)
      dJointDestroy(dBodyGetJoint(fibre[f], j));
    dBodyDestroy(fibre[f]);
    dGeomDestroy(gfibre[f]);
    dGeomDestroy(ggfibre[f]);
  }
  if (contactgroup)
    dJointGroupDestroy(contactgroup);
  dWorldDestroy(WorldODE);
  dSpaceDestroy(SpaceODE);
  dSpaceDestroy(gSpaceODE);
  dCloseODE();
  // clean pointers
  delete[] fibre;
  delete[] gfibre;
  delete[] ggfibre;
  delete[] dfibre;
  delete[] jfibre;
  delete[] fjfibre;
  delete[] jh1;
  delete[] gb;
  delete[] gt;
  delete[] lf;
  delete[] rf;
  delete[] mf;
  delete[] lbp;
  delete[] sbp;
  delete[] friction;
  delete[] sigmaT;
  delete[] sigmaR;
  delete[] chromatin_size;
  delete[] nucleosome_start;
  delete[] is_linker_nucleosome;
  delete[] Dm;
  delete[] em;
  delete[] am;
  delete[] rm;
  delete[] rm1;
  delete[] rm2;
  delete[] binn;
  delete[] ninb;
  delete[] bhit;
  delete[] torsions;
  delete[] b0;
  delete[] tw0;
  delete[] tws;
  delete[] old_ui;
  delete[] old_vi;
  delete[] old_ti;
  delete[] old_pi;
  delete[] anchors;
  delete[] h1_to_dna;
  delete[] uvth1;
  delete[] angle_per_nrl;
  printf("computation time: %f minutes\n",
         (clock() - cputime) / CLOCKS_PER_SEC / 60.);
  tmp = sprintf(name, "end_n%i_%s.out", seed, chromatin_in);
  fOut = fopen(name, "w");
  tmp = fprintf(fOut, "computation time: %f minutes\n",
                (clock() - cputime) / CLOCKS_PER_SEC / 60.);
  fclose(fOut);
  return 0;
}
