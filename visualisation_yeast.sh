#!/bin/bash
visualisation=/scratch/pcarriva/simulations/fibre_ODE/visualisation
output=/scratch/pcarriva/simulations/fibre_ODE
path_to_blender=/scratch/pcarriva/blender-2.79b-linux-glibc219-x86_64
vfolder=/scratch/pcarriva/simulations/visualisation_with_blender

while getopts hs:v:B:p:o:f: option
do
    case "${option}"
    in
        s) structure=${OPTARG};;
	v) visualisation=${OPTARG};;
	B) path_to_blender=${OPTARG};;
        p) path_to_data=${OPTARG};;
        o) output=${OPTARG};;
	f) frames=${OPTARG};;
	h) echo "usage:"
	   echo "     -s <structure>"
	   echo "     -v <visualisation name>"
	   echo "     -B <path to your blender>"
	   echo "     -p <path to the data>"
	   echo "     -o <where to write the output>"
	   echo "examples:"
	   echo "for c in yeast_n1; do ./visualisation_yeast.sh -s 1 -s ${c}.out -v ${c}.out -B /scratch/pcarriva/blender-2.79b-linux-glibc219-x86_64 -p /scratch/pcarriva/simulations/fibre_ODE/visualisation -o /scratch/pcarriva/simulations/fibre_ODE; done"
	   exit 0
	   ;;
    esac;
done;

# ${path_to_blender}/blender -b -P ${vfolder}/visualisation.py -- -c ensemble -v $visualisation -s $structure --chr_colors -p $path_to_data -w $output -x -75.0 -y 0.0 -z 0.0 --nvertices=12 --make_movie --save;
${path_to_blender}/blender -b -P ${vfolder}/visualisation.py -- -c ensemble -v $visualisation -s $structure --chr_colors -p $path_to_data -w $output -x -75.0 -y 0.0 -z 0.0 --nvertices=12 --make_movie;
