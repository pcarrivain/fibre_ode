The repository provides a set of functions to model DNA and chromatin fiber.
We use rigid-body-dynamics to model DNA and chromatin fiber as a mechanical articulated system.
In particular, we use the work of [Carrivain, Barbi and Victor](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003456).


# Verlet list for ODE

The rigid-body-dynamics is useful for mechanical articulated system.
In addition to that the tool allows the user to simulate complex shape and resolve excluded volume constraint.
It is used in the industry of video games to accurately reproduce physics.
However, a software like *Open-Dynamics-Engine* compute pairwize overlap check every time-step.
The engine starts with a partition of the space and then loop over all the blocks of partition.
For each blocks it runs a nested loops to check the overlaps between the objects inside the block.


The module implements external functions that can be used to compute [Verlet-list](https://en.wikipedia.org/wiki/Verlet_list).
Therefore, the user does not call the pairwize overlap check every time-step.
He only needs to loop over the Verlet-list with the pairwize of objects within a given cut-off distance.
However, the Verlet-list has to be updated according to the displacement length of the objects.

The module can be used to speed-up the *Open-Dynamics-Engine* simulation of polymers and complex objects system.


We test the module with many examples: chromatin fiber, bacteria, yeast genome.
It is an assembly of DNA wrapped around nucleosomes (histones core) that compact the genome.
We model the DNA at the scale of 10 base-pair as an articulated system.
In particular, we use the work of [Carrivain, Barbi and Victor](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003456).
The nucleosomes is built with complex shape. We run a Langevin dynamics and check
that our Verlet-list implementation gives the same results *Open-Dynamics-Engine* would give.
The name of the executable followed by a -h explains how to use the example:
```bash
./bacteria -h
./fibre_ODE -h
./patches -h
./yeast -h
```

1. The chromatin fibre example is the file [fibre_ODE.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/fibre_ODE.cpp?ref_type=heads).
2. The bacteria example is the file [bacteria.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/bacteria.cpp).
3. The patches example (WIP) is the file [patches.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/patches.cpp).
   It is an example on how to add patches interactions at the geometry surface.
4. The yeast genome example is the file [yeast.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/yeast.cpp).
5. The Verlet-list for ODE functions are described in the file [functions.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/functions.cpp).
6. *Open-Dynamics-Engine* (ODE) can be found [here](https://www.ode.org/).
7. The file [read_nucleosome.cpp](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/read_nucleosome.cpp) read the structure of the nucleosome.
8. The structure of the nucleosome can be found [here](https://gitlab.com/pcarrivain/fibre_ode/-/tree/master/nucleosome_in?ref_type=heads).

-----

# Installation

You have to clone the repository and name it *fibre_ODE*.

Then, you have to compile [Open-Dynamics-Engine](https://www.ode.org/).

Eventually, you have to compile the examples.

-----

# *Open-Dynamics-Engine*

I work with [0.16 ODE version](https://bitbucket.org/odedevs/ode/downloads/).
The installation steps are:
```bash
git clone https://gitlab.com/pcarrivain/fibre_ode.git
cd <path to fibre_ODE>
wget https://bitbucket.org/odedevs/ode/downloads/ode-0.16.tar.gz
tar -zxvf ode-0.16.tar.gz
cd ode-0.16
mkdir myBuild_static
./configure --prefix="<path to fibre_ODE>/ode-0.16/myBuild_static" --enable-double-precision --enable-libccd
make install
make
```
I provide a [Makefile](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/Makefile) to compile the examples.
You can edit it to change the path to your ODE build.

-----

# Eigen C++

Download [Eigen C++](https://eigen.tuxfamily.org/index.php?title=Main_Page) version=3.4 in the current folder.
Rename it to *eigen_340*.

-----

# Compilation

You can clean and then compile with:
```bash
make mrproper
make
```
A [Makefile](gitlab.com/pcarrivain/fibre_ode/-/blob/master/Makefile) is provided to compile all the examples.

-----

# Output and print

The executable prints the kinetic energy over the theoritical average.
Theoritical average is approximative because we have to compute the rank of the constraints matrix
to properly get the number of degrees of freedom.

Local curvature, local twist, bending and twisting local energies are written in the geometry folder.

The code writes conformations of the chromatin fiber in the `conformations` folder thanks to the function `read_write_conformation`.
You must pass to the function a list of `dBodyID`, the name of the output file, what to do (``"read"`` or ``"write"``), the first `dBodyID` and the last one to save (not included).
Output file format is binary.
The three first objects to be written are the position of the first `dBodyID`.
Then, the function writes the four coordinates of the quaternion, the three coordinates of the linear velocity and eventually the three coordinates of the angular velocity.


-----

# Yeast

Yeast genome is made of 16 chromosomes.
Initial conformation is a Rabl like conformation.
Chromosomes are slowly confined to a sphere of $`R=1000`$ nm (minimization steps).
Each chromosome is divided into bond of length $`l=60`$ nm and radius $`r=10`$ nm.
rDNA is made of spheres of radius $`75`$ nm.
Run the following bash command to check if the code properly works with default parameters:
```bash
./yeast -s 1 -i 10000 -e 1000 --scale 1.01 Vl_every 1 -g 1 -M 400000 -Z
```
Random number seed is 1.
The number of minimization steps is 400000.
After the minimization, simulation runs for 10000 steps.
A file "yeast_n1.in" is written to the parameters folder.
A conformation file is written to the conformation folder every 1000 steps.
Visualisation file is written to the visualisation folder (see Visualisation with Blender section).
Please wait for the example completion before to run the following command to make a movie (do not forget to change path):
```bash
./visualisation_yeast.sh -s 1 -s syeast_n1.out -v yeast_n1.out -B /scratch/pcarriva/blender-2.79b-linux-glibc219-x86_64 -p /scratch/pcarriva/simulations/fibre_ODE/visualisation -o /scratch/pcarriva/simulations/fibre_ODE
```
Please consider running the following bash command to read more about command-line arguments:
```bash
./yeast -h
```

-----

# Run chromatin fiber models

The file "fibre_ODE.cpp" is an example on how to run chromatin fiber model.
Please consider running the following bash command to read more about command-line arguments:
```bash
./fibre_ODE -h
```
Main functions *create_random_fibre* and *create_chromatin_fibre*.
The first one writes a file that describes the chromatin fiber.
The second one creates the chromatin fiber from the file.
Then, you have to run *dna_histone_starts* function to get the start of each nucleosome.
You can also run *is_linker_or_nucleosome* function that returns an array of size the number of monomers.
If the monomer is part of a nucleosome, the index is strictly positive and corresponds to the nucleosome index.
If the monomer is part of a DNA linker, the index is strictly negative and corresponds to the linker index.

-----

# Chromatin input file

In order to run the code, you need an input file like
([example](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/chromatin.in))
to describe the system:

```bash
...
0 10.5 -1 -1 0 0 0
0 147 0 0 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
0 147 1 1 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
0 10.5 -1 -1 0 0 0
1 147 0 1 0 0 0
1 10.5 -1 -1 0 0 0
1 10.5 -1 -1 0 0 0
1 10.5 -1 -1 0 0 0
1 10.5 -1 -1 0 0 0
1 147 1 0 1 1 0
1 10.5 -1 -1 0 0 0
...
```

First column is the index of the chain (starting from 0).
Second column is the number of bp of the monomer. 147 tells the code to create a nucleosome.
Third and fourth columns are the number of broken SHLs.
Fifth and sixth columns are the number of broken docking domains.
Seventh column indicates if H1 (1) or not (0).
It does not work for all the linker sizes.

-----

# Zig-zag and solenoid models

To run zig-zag model consider using the following command line:
```bash
./fibre_ODE -n no_solenoid2_nrl177_by_10bp.in -m 1 -f 0.0 -t 0.0 -s ${seed} -i 12000000 -e 4000 -a yes --scale 1.001 --Vl_every 1 -g 1 -S 0 --mstep 0
```

To run solenoid model consider using the following command line:
```bash
./fibre_ODE -n solenoid2_nrl177_by_10bp.in -m 1 -f 0.0 -t 0.0 -s ${seed} -i 12000000 -e 4000 -a yes --scale 1.001 --Vl_every 1 -g 1 -S 0 --solenoid --mstep 0
```

It runs dinucleosome model with an nrl of 177 bp for 12000000 steps and saves conformation every 4000 steps.

-----

# Build Verlet list for ODE

Here, the protocol to build Verlet-list for ODE is described:

1. Create the normal *dBodyID* and *dGeomID* from ODE API
2. Create kind of a ghost geometry with *create_ghost_geometry* function
3. Set data to the *dBodyID* using *struct int3p*
4. The member 'p' of *struct int3p* does not need work from your side
    1. first element is the number of collisions made by object *i*
    2. second element is the maximal number of collisions
    3. next elements are the objects *j* that are potentially overlapping object *i*
5. However, 'j' member of *struct int3p* needs to be filled with the index of 'dBodyID'
6. Initialize the 'dBodyID' data with *initialize_body_data* function
7. Build the Verlet-list with *build_Verlet_list* function
8. Collide the 'dGeomID' with *collide_all* function

The ghost geometry is the normal geometry that has been extruded in all dimensions.
You can choose to bound your geometry with a sphere of radius x-times her biggest dimension.
For example, the bounding sphere of a capsule of radius $`r`$ and cylinder length $`l`$ has a radius $`x(l+r)`$.
For example, the bounding sphere of a cylinder of radius $`r`$ and length $`l`$ has a radius $`xl`$.

Do not forget to write your own callback function. It is well described by ODE manual.
We provide a example that has to be copy-paste.
In this example, we do not consider collision between two connected bodies
as-well-as between two bodies of the same nucleosomes (chromatin fibre example).
This is the only part of the function you can change.
The other is needed to correctly build the Verlet-list.
```c++
void nearCallback(void *data,dGeomID o1,dGeomID o2){
  assert(o1);
  assert(o2);
  bool order_12;
  dBodyID b1=dGeomGetBody(o1);
  dBodyID b2=dGeomGetBody(o2);
  // no collision between two linked bodies
  if(b1 && b2 && dAreConnected(b1,b2)) return;
  int3p *c1=(int3p*)(dBodyGetData(b1));
  int3p *c2=(int3p*)(dBodyGetData(b2));
  // no need to look for contacts inside nucleosome
  if((c1->k)==(c2->k) && (c1->k)>=0) return;
  // build contacts
  const int dcontact_size=*((const int *)data);
  dContact *contact=new dContact[dcontact_size];
  int nc_VL,nc_max;
  int n=dCollide(o1,o2,dcontact_size,&(contact[0].geom),sizeof(dContact));
  // add to the Verlet-list
  if(n>0){
    // sort pairwize according to the index of each geometry
    order_12=(bool)((c1->j)<=(c2->j));
    if(order_12){
      nc_VL=(c1->p)[0];
      nc_max=(c1->p)[1];
    }else{
      nc_VL=(c2->p)[0];
      nc_max=(c2->p)[1];
    }
    // reallocation ?
    if(nc_VL>=nc_max){
      printf("reallocation %i/%i\n",nc_VL,nc_max);
      nc_max=nc_VL+1;
      if(order_12){
	int_new_copy(&(((int3p*)(dBodyGetData(b1)))->p),nc_VL,nc_max);
	(((int3p*)(dBodyGetData(b1)))->p)[1]=nc_max;
      }else{
	int_new_copy(&(((int3p*)(dBodyGetData(b2)))->p),nc_VL,nc_max);
	(((int3p*)(dBodyGetData(b2)))->p)[1]=nc_max;
      }
    }
    // add the geometry to the Verlet-List
    // increment the length of the Verlet-List
    if(order_12){
      (((int3p*)(dBodyGetData(b1)))->p)[nc_VL]=c2.j;
      (((int3p*)(dBodyGetData(b1)))->p)[0]=nc_VL+1;
    }else{
      (((int3p*)(dBodyGetData(b2)))->p)[nc_VL]=c1.j;
      (((int3p*)(dBodyGetData(b2)))->p)[0]=nc_VL+1;
    }
  }// Fi "n>0"
  c1=NULL;
  c2=NULL;
  delete[] contact;
  contact=NULL;
}
```

I declare the function *my_nearCallback* like:
```c++
void (*my_nearCallback)(void*,dGeomID,dGeomID);
  my_nearCallback=&nearCallback;
```

-----

# Resolve collisions first for *Open-Dynamics-Engine*

The idea is to first resolve the excluded volume constraints only and then apply the
feedback forces to the the articulated system we eventually ask ODE to solve.

We need to build the articulated system (ring chain see [example](gitlab.com/pcarrivain/fibre_ode/-/blob/master/bacteria.cpp)).
```c++
dBodyID *dna=new dBodyID[N];
create_ring_chain(WorldODE,SpaceODE,gSpaceODE,N,&dna,ddna,&gdna,&ggdna,&jdna,l,l_bp,radius,mass,x_ghost,overtwist,gyroscopic);
```

We need to build another system with no mechanical joints to get the feedback forces
from collision response only.
```c++
dBodyID *copy_dna=new dBodyID[N];
copy_system_with_no_joints(WorldODE,dna,&copy_dna,N);
```

Every time-step we copy the state of the system:
```c++
success=copy_state_body(&dna,&copy_dna,N,1);
```

Once, we get the feedback forces from collisions response:
```c++
step_the_world_contact_joints(WorldODE,&dna,&copy_dna,N,dt,1);
```

we solve the articulated system with the help of world step functions from *Open-Dynamics-Engine*:
```c++
dWorldQuickStep(WorldODE,dt);
```

-----

# Check Verlet-list for *Open-Dynamics-Engine*

You can test the Verlet-list with the option:
```bash
./fibre_ODE --test yes
./bacteria --test yes
```
The test compares the number of collisions built from Verlet-list with the number
of collisions built from normal ODE usage. If the two numbers are not equal, please
consider to change the scale factor of ghost geometries and/or the number of steps
between two builds of the Verlet-list. If there is still problems please contact me.

-----

# Monte-Carlo

I provide a [code](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/fibre_MC.cpp) to run
chromatin fibre simulation with Monte-Carlo method.
The method uses the Crankshaft and Pivot moves to sample chromatin conformations.
To compile simply uses the available [Makefile](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/Makefile).
In order to run the code, you need an input file like
([example](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/chromatin_MC.in))
to describe the system:

```bash
...
0 10.5 -1 -1 0 0
0 147 0 0 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
0 147 1 1 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
0 10.5 -1 -1 0 0
1 147 0 1 0 0
1 10.5 -1 -1 0 0
1 10.5 -1 -1 0 0
1 10.5 -1 -1 0 0
1 10.5 -1 -1 0 0
1 147 1 0 1 1
1 10.5 -1 -1 0 0
...
```

where the first column is the index of the chain, the second column is 147 for a nucleosome or
something else for simple DNA monomer.
The total number of base-pair $`G`$ is simply the sum over the second column.
The third and fourth columns are the number of SHLs to break on both side of the nucleosome.
Possible values are 0, 1, 2, 3, 4, 5, 6 and 7.
Negative value means you break SHLs at random.
The fifth and sixth columns are the number of docking domains on both side of the nucleosome.
For simple DNA monomer there is nothing to break.
For more details you can have a look to the description of the function:
```c++
void create_random_fibre(int nucleosomes,int nrl,int NRL,int break_shl,int break_dock,bool symmetric_break,char *fname,std::mt19937_64 &mt);
```
and to the folder [fibres](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/fibres).
The ([example](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/chromatin_MC.in)) build 25 chains
made of 100 nucleosomes each and 42 base-pair between two nucleosomes.
The chains are located on a square lattice in a cubic box of edge given
by number of base-pair per cubic nanometer.

Then, you need another [input file](https://gitlab.com/pcarrivain/fibre_ode/-/blob/master/inputs.in) with simulation parameters:

```bash
in chromatin_MC.in
twist yes
excluded_volume yes
excluded_volume_histones yes
k_nm 100.0
k_bp 300.0
bp_per_nm3 0.01
pbc yes
ES 0.0
steps 100
path_to /scratch/pcarriva/simulations/fibre_ODE
```
where you can choose if you consider excluded volume constraints as-well-as twisting energy contribution.
*k_nm* and *k_bp* is the Kuhn length of DNA in nanometer and base-pair.
*bp_per_nm3* is the number of base-pair per cubic nanometer $`\gamma`$.
We build an effective radius:
```math
R=\sqrt[^3]{\frac{3G}{4\pi\gamma}}
```
It is used to compute a potential on the gyration radius of the system:
```math
E=\frac{3k_BT}{2R^2}R_g^2
```
If you choose the parameter *pbc* to be *yes* then the code uses *Periodic-Boundary-Conditions*
instead of gyration radius potential.
The simulation box size is:
```math
L=\sqrt[^3]{\frac{G}{\gamma}}
```
The parameters *ES* is *work-in-progress* (stacking energy between nucleosomes).
The amplitude of angle for the Pivot and Crankshaft moves is estimated from the bond length,
bending and twisting persistence lengths.
The bending and twisting contribution to the total energy is computed
following [Carrivain, Barbi and Victor](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003456).
The maximal size (in number of bonds) of sub-chain that can be picked at random is half of the chain length.
The parameter *steps* gives The total number of steps divided by the total number of nucleosomes.

Eventually, you can run the simulation (seed is 1 here) with the following command line:
```bash
./fibre_MC /scratch/pcarriva/simulations/fibre_ODE inputs.in 1
```

-----

# Visualisation with [Blender](www.blender.org)

There is two scripts [bash](gitlab.com/pcarrivain/fibre_ode/-/blob/master/visualisation.sh) and [python](gitlab.com/pcarrivain/fibre_ode/-/blob/master/visualisation.py) for the visualisation with blender.
This is still work-in-progress.
I provide [data](gitlab.com/pcarrivain/fibre_ode/-/blob/master/data) to test the visualisation script.
There is two files. One of them describes the structure of the chromation fibre:
```C
// write size of the bodies
sprintf(nom,"visualisation/sn%i_%s_F%.2f_%s%.1f.out",seed,chromatin_in,force*fF/pN,texte,arg_rot);
fStructure=fopen(nom,"w");
for(int f=0;f<F;f++) retour=fprintf(fStructure,"%i %f %f %i %i %i\n",0,r_f[f],l_f[f],f,0,0);
for(int n=0;n<nucleosomes;n++) for(int i=0;i<4;i++) retour=fprintf(fStructure,"%i %f %f %i %i %i\n",0,r_h,l_h,F+n*NHC+i,(i+1)*(i<3)+9*(i==3),0);
if(!noSM) retour=fprintf(fStructure,"%i %f %f %i %i %i\n",1,dGeomSphereGetRadius(gfibre[B-1]),dGeomSphereGetRadius(gfibre[B-1]),B-1,1,0);
fclose(fStructure);
```
and the other one is a stack of conformations (binary format):
```C
sprintf(nom,"visualisation/vn%i_%s_F%.2f_%s%.1f.out",seed,chromatin_in,force*fF/pN,texte,arg_rot);
write_visualisation(fibre,nom,0,B,"a");
```
The previous lines of code comes from [fibre_ODE.cpp](gitlab.com/pcarrivain/fibre_ode/-/blob/master/fibre_ODE.cpp).
The version of [Blender](www.blender.org) I used is [2.79b](https://www.blender.org/download/previous-versions/).
The bash script is an example of how to use the python script to create png of a given trajectory.
A simple:
```bash
python3.7 visualisation.py -h
```
prints basic usage.
If you git clone the project in */scratch* folder you can copy-paste the following
command lines in a bash script and run it:
```bash
output=/scratch/fibre_ODE
structure=sn1_chromatin.in_F0.00_T0.0.out
visualisation=vn1_chromatin.in_F0.00_T0.0.out
path_to_data=/scratch/fibre_ODE/data
/scratch/blender-2.79b-linux-glibc219-x86_64/blender -b -P ${output}/visualisation.py -- -c ensemble -v $visualisation -s $structure --epi_colors -p $path_to_data -w $output -x 0.0 -y -50.0 -z 0.0 --e1=1.57 --e2=0 --e3=0 --nvertices=32 --make_movie;
```
to make a movie from [data](gitlab.com/pcarrivain/fibre_ode/-/blob/master/data).

-----

# Data analysis (WiP)

I provide a
[python module](https://gitlab.com/pcarrivain/fibre_ODE/-/blob/master/data_analysis.py)
and
[example](https://gitlab.com/pcarrivain/fibre_ODE/-/blob/master/analysis_fibre.py)
to compute observable like radius of gyration, shape ...
You need to install [Numpy](numpy.org), HPC software [Dask](https://dask.org) and [Numba](http://numba.pydata.org).
[Numba](http://numba.pydata.org) is used to speed-up loop and nested loop computation
while Dask is used to compute different quantities on multi-core machine.
Of note, copy chromatin description file to the folder "fibres".

# Contour length (WiP)

To find WLC trajectories that perfectly match pixels hit by a DNA linker we use Monte-Carlo simulation.
The end points of the WLC are fixed and perfectly match the first and last pixels.
To sample WLC conformations we use Crankshaft moves.