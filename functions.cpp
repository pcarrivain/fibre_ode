/*********************************************************************************
 *                                                                               *
 * This file includes some functions that aim to speed-up computation time *
 * when using Open-Dynamics-Engine software to solve articulated system. * This
 *code has been developped by Pascal Carrivain (p.carrivain_at_gmail.com). * It
 *is distributed under MIT licence.                                          *
 *                                                                               *
 ********************************************************************************/

#include "functions.h"
#include "read_nucleosome.h"
#include "variables.h"

#define SMALL 1e-10

using namespace Eigen;
using namespace std;

void k_and_mu_from_erp_cfm(double dt, double erp, double cfm,
                           double &k_from_erp_cfm, double &mu_from_erp_cfm) {
  k_from_erp_cfm = erp / (dt * cfm);
  mu_from_erp_cfm = 1. / cfm - dt * k_from_erp_cfm;
}

/*! @brief compute partition of space and overlap between list of bodies and
  partition. partition is a collection of cubic bins. attach contact joint if
  collision between two bodies. return the number of contacts.
  @param bodies list of dBodyID
  @param geoms list of dGeomID
  @param N number of dGeomID
  @param start consider dBodyID from ...
  @param end ... to this index
  @param world dWorldID (see ODE documentation)
  @param contactgroup contact group from Open-Dynamics-Engine
  @param gbin size of the bin has to be greater than the maximal geometry
  dimension
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param KKT use KKT conditions to compute overlap between two capsules
 */
int partition(const dBodyID *bodies, const dGeomID *geoms, int N,
              const int *starts, const int *ends, dWorldID world,
              dJointGroupID &contactgroup, double gbin, double &sum_abs_depth,
              double &max_abs_depth, bool KKT) {
  sum_abs_depth = max_abs_depth = .0;
  bool debug = false;
  int noverlaps = 0;
  int i0, ibin, ibinn, ig, ilperb, newnbins, ijk[3], nijk[3], bijk[3], oijk[3],
      xyz[3];
  vec3 com = vec3(.0, .0, .0), ri, ti;
  double d0, d1, lx, rx, ly, ry, lz, rz;
  double gbinx, gbiny, gbinz;
  double invgbinx, invgbiny, invgbinz;
  double length1, radius1, length2, radius2;
  dBodyID body, body2;
  dGeomID geom, geom2;
  double t0, t1 = .0, t2 = .0;
  // compute center of mass and bottom
  // left corner of the partition
  lx = ly = lz = DBL_MAX;
  rx = ry = rz = DBL_MIN;
  for (int i = 0; i < N; i++) {
    ri = PositionBodyODE(bodies[i]);
    lx = fmin(lx, ri.x);
    rx = fmax(rx, ri.x);
    ly = fmin(ly, ri.y);
    ry = fmax(ry, ri.y);
    lz = fmin(lz, ri.z);
    rz = fmax(rz, ri.z);
    com = add3(com, ri);
  }
  com = mult3(1. / (double)N, com);
  lx -= com.x;
  rx -= com.x;
  ly -= com.y;
  ry -= com.y;
  lz -= com.z;
  rz -= com.z;
  // compute new bin size to fit in memory
  i0 = 0;
  gbinx = gbiny = gbinz = gbin;
  invgbinx = invgbiny = invgbinz = 1. / gbin;
  while (1) {
    nijk[0] = bijk[0] = (int)floor((rx - lx) * invgbinx) + 2;
    if (nijk[0] <= 200)
      break;
    else {
      i0 = 1;
      gbinx *= 1.05;
      invgbinx = 1. / gbinx;
    }
  }
  while (1) {
    nijk[1] = bijk[1] = (int)floor((ry - ly) * invgbiny) + 2;
    if (nijk[1] <= 200)
      break;
    else {
      i0 = 1;
      gbiny *= 1.05;
      invgbiny = 1. / gbiny;
    }
  }
  while (1) {
    nijk[2] = bijk[2] = (int)floor((rz - lz) * invgbinz) + 2;
    if (nijk[2] <= 200)
      break;
    else {
      i0 = 1;
      gbinz *= 1.05;
      invgbinz = 1. / gbinz;
    }
  }
  if (i0 == 1)
    printf("new bin size %fx%fx%f/%f\n", gbinx, gbiny, gbinz, gbin);
  newnbins = nijk[0] * nijk[1] * nijk[2];
  oijk[0] = 0;
  oijk[1] = 1;
  oijk[2] = 2;
  bool sorted = false;
  while (1) {
    sorted = true;
    for (int i = 0; i < 2; i++) {
      if (bijk[i] > bijk[i + 1]) {
        sorted = false;
        swap(bijk[i], bijk[i + 1]);
        swap(oijk[i], oijk[i + 1]);
      }
    }
    if (sorted)
      break;
  }
  // reallocation ?
  if (newnbins > nbins) {
    printf("partition reallocation, number of bins=%i.\n", newnbins);
    nbins = newnbins;
    delete[] binn;
    binn = new int[nbins]();
    std::fill_n(binn, nbins, -1);
    // if(bhit==NULL)
    //   bhit=new int[N]();
    // else{
    //   delete[] bhit;
    //   bhit=new int[N]();
    // }
  }
  if (bhit == NULL)
    bhit = new int[N]();
  if (ninb == NULL)
    ninb = new int[N]();
  if (nhit == NULL) {
    printf("nhit reallocation\n");
    nhit = new int[N * nperb]();
  }
  if (lperb == NULL)
    lperb = new int[N]();
  // compute body-partition overlap
  lhit = 0;
  // left corner per dimension
  double eijk[3];
  eijk[0] = lx;
  eijk[1] = ly;
  eijk[2] = lz;
  // compute monomer/partition overlap
  int iterations = 0, counts = 0;
  int3p datai, dataj;
  vec3 r0, rl;
  int x0, y0, z0, x1, y1, z1, ireset = 0;
  bool reset = true, is_in_range = true;
  while (reset) {
    reset = false;
    iterations = counts = lhit = 0;
    for (int i = 0; i < N; i++) {
      if (debug)
        printf("ok\n");
      ri = sub3(PositionBodyODE(bodies[i]), com);
      ijk[0] = (int)((ri.x - eijk[0]) * invgbinx);
      ijk[1] = (int)((ri.y - eijk[1]) * invgbiny);
      ijk[2] = (int)((ri.z - eijk[2]) * invgbinz);
      ibin = ijk[oijk[0]] * nijk[oijk[1]] * nijk[oijk[2]] +
             ijk[oijk[1]] * nijk[oijk[2]] + ijk[oijk[2]];
      // printf("%li %i\n",(long int)ibin,lhit);
      // printf("%i %i %i\n",ijk[0],ijk[1],ijk[2]);
      if (debug)
        printf("ibin=%i %i,%i,%i %i,%i,%i (ireset=%i nbins=%i)\n", ibin, ijk[0],
               ijk[1], ijk[2], nijk[0], nijk[1], nijk[2], ireset, nbins);
      ninb[i] = ibin;
      if (binn[ibin] == -1) {
        if (debug)
          printf("(1)\n");
        binn[ibin] = lhit;
        bhit[lhit] = ibin;
        // check value of lperb
        if (lperb[lhit] != 0) {
          printf("lperb %i!=0 (%i/%i), ireset=%i, exit.\n", lperb[lhit], lhit,
                 i, ireset);
          exit(EXIT_FAILURE);
        }
        nhit[lhit * nperb + lperb[lhit]] = i;
        lperb[lhit]++;
        lhit++;
        if (debug)
          printf("(1)\n");
      } else {
        if (debug)
          printf("(2)\n");
        // reallocation ?
        ibinn = binn[ibin];
        if (lperb[ibinn] >= nperb) {
          // new number of dGeomID per bin
          printf("new number of dGeomID per bin %i.\n", nperb + 5);
          // exit(EXIT_FAILURE);
          nperb += 10;
          delete[] nhit;
          nhit = new int[N * nperb]();
          // clean partition
          printf("clean partition lhit=%i i=%i ireset=%i, and retry.\n", lhit,
                 i, ireset);
          for (int n = 0; n < lhit; n++)
            binn[bhit[n]] = -1;
          for (int n = 0; n <= i; n++) {
            ninb[n] = -1;
            lperb[n] = 0;
          }
          if (contactgroup)
            dJointGroupEmpty(contactgroup);
          reset = true;
          i = -1;
          lhit = 0;
          break;
        } else {
          // printf("%i/%i\n",lperb[binn[ibin]],nperb);
          nhit[ibinn * nperb + lperb[ibinn]] = i;
          lperb[ibinn]++;
        }
        if (debug)
          printf("(2)\n");
      }
      if (debug)
        printf("(3)\n");
      // check if 'i' is in range
      is_in_range = false;
      for (int p = 0; p < 3; p++)
        if (i >= starts[p] && i <= ends[p])
          is_in_range = true;
      if (!is_in_range)
        continue;
      datai = *(int3p *)(dBodyGetData(bodies[i]));
      body = bodies[i];
      geom = geoms[i];
      // capsule or sphere
      if (dGeomGetClass(geom) == dCapsuleClass)
        dGeomCapsuleGetParams(geom, &radius1, &length1);
      else {
        if (dGeomGetClass(geom) == dSphereClass) {
          radius1 = dGeomSphereGetRadius(geom);
          length1 = .0;
        }
      }
      ti = FrameODE(body, 2);
      ri = sub3(PositionBodyODE(body), com);
      r0 = sub3(ri, mult3(radius1 + .5 * length1, ti));
      rl = add3(ri, mult3(radius1 + .5 * length1, ti));
      ijk[0] = (int)((ri.x - eijk[0]) * invgbinx);
      ijk[1] = (int)((ri.y - eijk[1]) * invgbiny);
      ijk[2] = (int)((ri.z - eijk[2]) * invgbinz);
      d0 = (ijk[0] + .5) * gbinx + eijk[0];
      x0 = (ijk[0] == 0 || (r0.x > d0 && rl.x > d0)) ? 0 : -1;
      x1 = ((ijk[0] - 1) == nijk[0] || (r0.x < d0 && rl.x < d0)) ? 1 : 2;
      d0 = (ijk[1] + .5) * gbiny + eijk[1];
      y0 = (ijk[1] == 0 || (r0.y > d0 && rl.y > d0)) ? 0 : -1;
      y1 = ((ijk[1] - 1) == nijk[1] || (r0.y < d0 && rl.y < d0)) ? 1 : 2;
      d0 = (ijk[2] + .5) * gbinz + eijk[2];
      z0 = (ijk[2] == 0 || (r0.z > d0 && rl.z > d0)) ? 0 : -1;
      z1 = ((ijk[2] - 1) == nijk[2] || (r0.z < d0 && rl.z < d0)) ? 1 : 2;
      for (int x = x0; x < x1; x++) {
        for (int y = y0; y < y1; y++) {
          for (int z = z0; z < z1; z++) {
            xyz[0] = x;
            xyz[1] = y;
            xyz[2] = z;
            ibin =
                (ijk[oijk[0]] + xyz[oijk[0]]) * nijk[oijk[1]] * nijk[oijk[2]] +
                (ijk[oijk[1]] + xyz[oijk[1]]) * nijk[oijk[2]] +
                (ijk[oijk[2]] + xyz[oijk[2]]);
            // does the bin has at least one dBodyID ?
            ibinn = binn[ibin];
            if (ibinn == -1)
              continue;
            ilperb = lperb[ibinn];
            for (int m = 0; m < ilperb; m++) {
              ig = nhit[ibinn * nperb + m];
              if (i == ig) // || dAreConnected(body,body2))
                continue;
              iterations++;
              body2 = bodies[ig];
              geom2 = geoms[ig];
              dataj = *(int3p *)(dBodyGetData(body2));
              // do not collide two geometries of the same nucleosome
              if (datai.k == dataj.k && datai.k > 0)
                continue;
              // capsule or sphere
              if (dGeomGetClass(geom2) == dCapsuleClass)
                dGeomCapsuleGetParams(geom2, &radius2, &length2);
              else {
                if (dGeomGetClass(geom2) == dSphereClass) {
                  radius2 = dGeomSphereGetRadius(geom2);
                  length2 = .0;
                }
              }
              d0 = .5 * length1 + radius1;
              d1 = .5 * length2 + radius2;
              if (norm3(sub3(ri, sub3(PositionBodyODE(body2), com))) >
                  sqrt(d0 * d0 +
                       2. * d0 * d1 *
                           fabs(dot_product(ti, FrameODE(body2, 2))) +
                       d1 * d1))
                continue;
              // printf("%i(%i,%i,%i,%i/%i,lhit=%i): %i
              // %i\n",s,x,y,z,m,lperb[ibinn],lhit,i,ig); for each block index
              // of the geometry follows ascending order example: consider
              // dGeomID 28 and block 1 12 23 25 36 101 do not collide if
              // distance between two center of mass exceed dimension of the
              // capsule
              if (KKT) {
                // t0=clock();
                // collide_g1_g2(world,geom,geom2,wERP,wCFM,contactgroup,d0,d1,false);//
                // ??? t1+=(clock()-t0); if(contactgroup)
                // 	dJointGroupEmpty(contactgroup);
                t0 = clock();
                noverlaps += collide_c1_c2(world, geom, geom2, wERP, wCFM,
                                           contactgroup, d0, d1, false);
                t2 += (clock() - t0);
                // printf("collide/KKT=%f\n",t1/t2);
              } else
                noverlaps += collide_g1_g2(world, geom, geom2, wERP, wCFM,
                                           contactgroup, d0, d1, false);
              sum_abs_depth += d0;
              max_abs_depth = fmax(max_abs_depth, d1);
              counts++;
            }
          }
        }
      }
      if (debug)
        printf("(3)\n");
    }
    ireset++;
    if (debug)
      printf("okok\n\n");
  }
  // clean partition
  for (int n = 0; n < lhit; n++)
    binn[bhit[n]] = -1;
  for (int i = 0; i < N; i++) {
    ninb[i] = -1;
    lperb[i] = 0;
  }
  return noverlaps;
}

/*! @brief compute partition of space and overlap between list of bodies and
  partition. partition is a collection of cubic bins. attach contact joint if
  collision between two bodies. return the number of contacts.
  @param bodies list of dBodyID
  @param geoms list of dGeomID
  @param N number of dGeomID
  @param start consider dBodyID from ...
  @param end ... to this index
  @param world dWorldID (see ODE documentation)
  @param contactgroup contact group from Open-Dynamics-Engine
  @param gbin size of the bin has to be greater than the maximal geometry
  dimension
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param KKT use KKT conditions to compute overlap between two capsules
  @param verlet_list build Verlet list
 */
int partition_v0(const dBodyID *bodies, const dGeomID *geoms, int N,
                 const int *starts, const int *ends, dWorldID world,
                 dJointGroupID &contactgroup, double gbin,
                 double &sum_abs_depth, double &max_abs_depth, bool KKT,
                 bool verlet_list) {
  if (N <= 0) {
    printf("N<=0, do nothing.\n");
    return 0;
  }
  sum_abs_depth = max_abs_depth = .0;
  int noverlaps = 0, i0, ibin, ibinn, ig, ilperb, newnbins;
  int mijk = (int)floor(pow(ULONG_MAX, 1. / 3.)), ijk[3], nijk[3], bijk[3],
      oijk[3], xyz[3];
  vec3 ri, ti;
  double d0, d1, lx, rx, ly, ry, lz, rz;
  double gbinx, gbiny, gbinz;
  double invgbinx, invgbiny, invgbinz;
  double length1, radius1, length2, radius2;
  dBodyID body, body2;
  dGeomID geom, geom2;
  double t0, t1 = .0, t2 = .0;
  unsigned long int ul0, ul1, ul2;
  // left corner of the partition
  lx = ly = lz = DBL_MAX;
  rx = ry = rz = DBL_MIN;
  for (int i = 0; i < N; i++) {
    ri = PositionBodyODE(bodies[i]);
    lx = fmin(lx, ri.x);
    rx = fmax(rx, ri.x);
    ly = fmin(ly, ri.y);
    ry = fmax(ry, ri.y);
    lz = fmin(lz, ri.z);
    rz = fmax(rz, ri.z);
  }
  // compute new bin size to fit in memory
  i0 = 0;
  gbinx = gbiny = gbinz = gbin;
  invgbinx = invgbiny = invgbinz = 1. / gbin;
  if (0) {
    while (1) {
      nijk[0] = bijk[0] = (int)floor((rx - lx) * invgbinx) + 2;
      if (nijk[0] <= 400)
        break;
      else {
        i0 = 1;
        gbinx *= 1.05;
        invgbinx = 1. / gbinx;
      }
    }
    while (1) {
      nijk[1] = bijk[1] = (int)floor((ry - ly) * invgbiny) + 2;
      if (nijk[1] <= 400)
        break;
      else {
        i0 = 1;
        gbiny *= 1.05;
        invgbiny = 1. / gbiny;
      }
    }
    while (1) {
      nijk[2] = bijk[2] = (int)floor((rz - lz) * invgbinz) + 2;
      if (nijk[2] <= 400)
        break;
      else {
        i0 = 1;
        gbinz *= 1.05;
        invgbinz = 1. / gbinz;
      }
    }
    // if(i0==1)
    //   printf("new bin size %fx%fx%f/%f, %i geom(s) per
    //   bin,%i\n",gbinx,gbiny,gbinz,gbin,nperb,N*nperb);
    newnbins = nijk[0] * nijk[1] * nijk[2];
  } else {
    while (1) {
      nijk[0] = bijk[0] = (int)floor((rx - lx) * invgbinx) + 2;
      if (nijk[0] <= mijk)
        break;
      else {
        i0 = 1;
        gbinx *= 1.05;
        invgbinx = 1. / gbinx;
      }
    }
    while (1) {
      nijk[1] = bijk[1] = (int)floor((ry - ly) * invgbiny) + 2;
      if (nijk[1] <= mijk)
        break;
      else {
        i0 = 1;
        gbiny *= 1.05;
        invgbiny = 1. / gbiny;
      }
    }
    while (1) {
      nijk[2] = bijk[2] = (int)floor((rz - lz) * invgbinz) + 2;
      if (nijk[2] <= mijk)
        break;
      else {
        i0 = 1;
        gbinz *= 1.05;
        invgbinz = 1. / gbinz;
      }
    }
    newnbins = N;
  }
  oijk[0] = 0;
  oijk[1] = 1;
  oijk[2] = 2;
  bool sorted = false;
  while (!sorted) {
    sorted = true;
    for (int i = 0; i < 2; i++) {
      if (bijk[i] > bijk[i + 1]) {
        sorted = false;
        swap(bijk[i], bijk[i + 1]);
        swap(oijk[i], oijk[i + 1]);
      }
    }
  }
  // reallocation ?
  if (newnbins > nbins) {
    printf("partition reallocation, number of bins=%i.\n", newnbins);
    nbins = newnbins;
    delete[] binn;
    binn = new int[nbins]();
    std::fill_n(binn, nbins, -1);
  }
  if (bhit == NULL)
    bhit = new int[N]();
  if (ninb == NULL)
    ninb = new int[N]();
  if (nhit == NULL)
    nhit = new int[N * nperb]();
  if (lperb == NULL)
    lperb = new int[N]();
  // left corner per dimension
  double eijk[3];
  eijk[0] = lx;
  eijk[1] = ly;
  eijk[2] = lz;
  // compute monomer/partition overlap
  lhit = 0;
  for (int i = 0; i < N; i++) {
    // // we collide [start,end] against [0,start[ and against ]end,N[
    // i0=1;
    // for(int p=0;p<3;p++)
    //   if(i>=starts[p] && i<=ends[p])
    // 	i0=0;
    // if(i0==0)
    //   continue;
    // compute bin index
    ri = PositionBodyODE(bodies[i]);
    ijk[0] = (int)((ri.x - eijk[0]) * invgbinx);
    ijk[1] = (int)((ri.y - eijk[1]) * invgbiny);
    ijk[2] = (int)((ri.z - eijk[2]) * invgbinz);
    // ibin=ijk[oijk[0]]*nijk[oijk[1]]*nijk[oijk[2]]+ijk[oijk[1]]*nijk[oijk[2]]+ijk[oijk[2]];
    ul0 = ijk[oijk[0]] * nijk[oijk[1]] * nijk[oijk[2]];
    ul1 = ijk[oijk[1]] * nijk[oijk[2]];
    ul2 = ijk[oijk[2]];
    // it is ok because N is an unsigned integer
    ibin = (int)((ul0 + ul1 + ul2) % (unsigned long int)N); // ???
    ninb[i] = ibin;
    if (binn[ibin] == -1) {
      binn[ibin] = lhit;
      bhit[lhit] = ibin;
      // check value of lperb
      if (lperb[lhit] != 0) {
        printf("lperb!=0, exit.\n");
        exit(EXIT_FAILURE);
      }
      nhit[lhit * nperb] = i;
      lperb[lhit]++;
      lhit++;
    } else {
      // reallocation ?
      ibinn = binn[ibin];
      if (lperb[ibinn] >= nperb) {
        // new number of dGeomID per bin
        nperb += 10;
        // printf("new number of dGeomID per bin %i/%i %fx%fx%f bin=%i lperb=%i
        // i=%i lhit=%i.\n",nperb,N,gbinx,gbiny,gbinz,ibin,lperb[ibinn],i,lhit);
        // printf("   %i %i %i/%i %i
        // %i\n",ijk[0],ijk[1],ijk[2],nijk[0],nijk[1],nijk[2]);
        delete[] nhit;
        nhit = new int[N * nperb]();
        // clean partition
        for (int n = 0; n < lhit; n++)
          binn[bhit[n]] = -1;
        for (int n = 0; n < N; n++) {
          ninb[n] = -1;
          lperb[n] = 0;
        }
        lhit = 0;
        i = -1;
      } else {
        // printf("%i/%i\n",lperb[binn[ibin]],nperb);
        nhit[ibinn * nperb + lperb[ibinn]] = i;
        lperb[ibinn]++;
      }
    }
  }
  int iterations = 0, counts = 0;
  int3p datai, dataj;
  vec3 r0, rl;
  int x0, y0, z0, x1, y1, z1;
  for (int p = 0; p < 3; p++) {
    for (int i = starts[p]; i <= ends[p]; i++) {
      datai = *(int3p *)(dBodyGetData(bodies[i]));
      body = bodies[i];
      geom = geoms[i];
      if (dGeomGetClass(geom) == dCapsuleClass)
        dGeomCapsuleGetParams(geom, &radius1, &length1);
      else {
        if (dGeomGetClass(geom) == dSphereClass) {
          radius1 = dGeomSphereGetRadius(geom);
          length1 = .0;
        }
      }
      ti = FrameODE(body, 2);
      ri = PositionBodyODE(body);
      r0 = sub3(ri, mult3(radius1 + .5 * length1, ti));
      rl = add3(ri, mult3(radius1 + .5 * length1, ti));
      ijk[0] = (int)((ri.x - eijk[0]) * invgbinx);
      ijk[1] = (int)((ri.y - eijk[1]) * invgbiny);
      ijk[2] = (int)((ri.z - eijk[2]) * invgbinz);
      d0 = (ijk[0] + .5) * gbinx + eijk[0] - radius1;
      d1 = (ijk[0] + .5) * gbinx + eijk[0] + radius1;
      x0 = (ijk[0] == 0 || (r0.x > d1 && rl.x > d1)) ? 0 : -1;
      x1 = ((ijk[0] - 1) == nijk[0] || (r0.x < d0 && rl.x < d0)) ? 1 : 2;
      d0 = (ijk[1] + .5) * gbiny + eijk[1] - radius1;
      d1 = (ijk[1] + .5) * gbiny + eijk[1] + radius1;
      y0 = (ijk[1] == 0 || (r0.y > d1 && rl.y > d1)) ? 0 : -1;
      y1 = ((ijk[1] - 1) == nijk[1] || (r0.y < d0 && rl.y < d0)) ? 1 : 2;
      d0 = (ijk[2] + .5) * gbinz + eijk[2] - radius1;
      d1 = (ijk[2] + .5) * gbinz + eijk[2] + radius1;
      z0 = (ijk[2] == 0 || (r0.z > d1 && rl.z > d1)) ? 0 : -1;
      z1 = ((ijk[2] - 1) == nijk[2] || (r0.z < d0 && rl.z < d0)) ? 1 : 2;
      for (int x = x0; x < x1; x++) {
        for (int y = y0; y < y1; y++) {
          for (int z = z0; z < z1; z++) {
            xyz[0] = x;
            xyz[1] = y;
            xyz[2] = z;
            ul0 = (ijk[oijk[0]] + xyz[oijk[0]]) * nijk[oijk[1]] * nijk[oijk[2]];
            ul1 = (ijk[oijk[1]] + xyz[oijk[1]]) * nijk[oijk[2]];
            ul2 = ijk[oijk[2]] + xyz[oijk[2]];
            // it is ok because N is an unsigned integer
            ibin = (int)((ul0 + ul1 + ul2) % (unsigned long int)N); // ???
            // does the bin has at least one dBodyID ?
            ibinn = binn[ibin];
            if (ibinn == -1)
              continue;
            ilperb = lperb[ibinn];
            for (int m = 0; m < ilperb; m++) {
              ig = nhit[ibinn * nperb + m];
              iterations++;
              body2 = bodies[ig];
              if (i == ig || dAreConnected(body, body2))
                continue;
              dataj = *(int3p *)(dBodyGetData(body2));
              // do not collide two geometries of the same nucleosome
              if (datai.k == dataj.k && datai.k > 0)
                continue;
              geom2 = geoms[ig];
              if (dGeomGetClass(geom2) == dCapsuleClass)
                dGeomCapsuleGetParams(geom2, &radius2, &length2);
              else {
                if (dGeomGetClass(geom2) == dSphereClass) {
                  radius2 = dGeomSphereGetRadius(geom2);
                  length2 = .0;
                }
              }
              d0 = .5 * length1 + radius1;
              d1 = .5 * length2 + radius2;
              // if (dot_product2(sub3(ri, PositionBodyODE(body2))) >
              //     (d0 * d0 +
              //      2. * d0 * d1 * fabs(dot_product(ti, FrameODE(body2, 2))) +
              //      d1 * d1))
              //   continue;
	      if (norm3(sub3(ri, PositionBodyODE(body2))) > (d0 + d1))
                continue;
              // for each block index of the geometry follows ascending order
              // example: consider dGeomID 28 and block 1 12 23 25 36 101
              // do not collide if distance between two center of mass
              // exceed dimension of the capsule
              if (KKT) {
                // t0=clock();
                // collide_g1_g2(world,geom,geom2,wERP,wCFM,contactgroup,d0,d1,false);//
                // ??? t1+=(clock()-t0); if(contactgroup)
                // 	dJointGroupEmpty(contactgroup);
                t0 = clock();
                if (verlet_list)
                  nearCallback(NULL, geom, geom2);
                else
                  noverlaps += collide_c1_c2(world, geom, geom2, wERP, wCFM,
                                             contactgroup, d0, d1, false);
                t2 += (clock() - t0);
                // printf("collide/KKT=%f\n",t1/t2);
              } else
                noverlaps += collide_g1_g2(world, geom, geom2, wERP, wCFM,
                                           contactgroup, d0, d1, false);
              sum_abs_depth += d0;
              max_abs_depth = fmax(max_abs_depth, d1);
              counts++;
            }
          }
        }
      }
    }
  }
  // clean partition
  if (1) {
    for (int i = 0; i < lhit; i++) {
      binn[bhit[i]] = -1;
      lperb[i] = 0;
    }
  } else {
    for (int n = 0; n < lhit; n++)
      binn[bhit[n]] = -1;
    for (int i = 0; i < N; i++) {
      ninb[i] = -1;
      lperb[i] = 0;
    }
  }
  return noverlaps;
}

/*! @brief is n prime ?
  @param n number
 */
bool is_prime(int n) {
  bool is_prime = true;
  int mmax = (int)rint(sqrt(n)) + 1;
  if ((n % 2) == 0)
    return false;
  for (int m = 3; m <= mmax; m++) {
    is_prime = (is_prime && (n % m) != 0);
    if (!is_prime)
      break;
  }
  return is_prime;
}

/*! @brief
  @param bodies list of dBodyID objects
  @param rb list of radius of each object
  @param lb list of lengths of each object
  @param nucleosomes number of nucleosomes
  @param F number of DNA monomers
  @param kspring spring constant
  @param espring equilibrium spring length
  @param Dtail_dna potential intensity
  @param tail_size std of the gaussian
 */
void partition_nucleosomes(const dBodyID *bodies, const double *rb,
                           const double *lb, int nucleosomes, int F,
                           double kspring, double espring, double Dtail_dna,
                           double tail_size) {
  // gaussian sigma
  double dna_sigma = 1. * nm / fL;
  double sqdna_sigma = dna_sigma * dna_sigma;
  double sqtail_size = tail_size * tail_size;
  double sqcutoff = 9. * (sqdna_sigma + sqtail_size);
  // gaussian overlap potential is like d*a*exp(-b*r^2)
  // where a unit is inverse of volume unit
  // where d units are kBT units times volume unit
  // argument Dtail_dna is equal to d*a
  // therefore we have to compute d from Dtail_dna=d*a
  // normalize tail-dna gaussian overlap potential intensity
  // from kBT units to kBT units times volume unit
  double dtail_dna =
      Dtail_dna /
      (27. / pow(2. * pi * (3. * sqtail_size + 3. * sqdna_sigma), 1.5));
  double fnorm =
      81. / (sqrt(2.) * pow(pi, 1.5) * (2. * sqtail_size + 2. * sqdna_sigma) *
             pow(3. * sqtail_size + 3. * sqdna_sigma, 1.5));
  double xsqd = -3. / (2. * sqtail_size + 2. * sqdna_sigma);
  // dna-tail N-terminal index (see Luger 1998)
  int hpatches[8];
  hpatches[0] = 0;
  hpatches[1] = 2;
  hpatches[2] = 6;
  hpatches[3] = 7;
  hpatches[4] = 13 - 7;
  hpatches[5] = 13 - 6;
  hpatches[6] = 13 - 2;
  hpatches[7] = 13;
  // dna-tail N-terminal location relative to nucleosomal dna monomer (see Luger
  // 1998)
  vec3 uvw[8];
  uvw[0] = uvw[7] = vec3(-1., .0, .0);
  uvw[1] = uvw[6] = vec3(-1., .0, .0);
  uvw[2] = uvw[5] = vec3(-1., .0, .0);
  uvw[3] = uvw[4] = vec3(-1., .0, .0);
  int ix, iy, iz, i0, i1, i2, i3, nc, ibin, ibinn;
  vec3 bn, com, rij;
  double grid_bin = 1.1 * (sqrt(sqcutoff) + 11. * nm / fL), foverlap, sqd;
  bool isnucleosome, restart;
  // compute bottom left corner of the partition
  double d0 = DBL_MAX;
  double d1 = DBL_MIN;
  for (int f = 0; f < (F + nucleosomes * NHC); f++) {
    com = PositionBodyODE(bodies[f]);
    d0 = fmin(d0, fmin(fmin(com.x, com.y), com.z));
    d1 = fmax(d1, fmax(fmax(com.x, com.y), com.z));
  }
  double extent = d0 - .001 * d0;
  // number of bins along one dimension
  int nbins1d = (int)ceil((d1 - extent) / grid_bin) + 2;
  // number of bins, use modulo operator
  // therefore index is between 0 and NP (not included)
  int NP = F + nucleosomes * NHC;
  while (!is_prime(NP))
    NP++;
  // printf("%i %i\n",F+nucleosomes*NHC,NP);
  // reallocation ?
  // first call of the 'partition_nucleosome'
  if (nbinn == NULL) {
    printf("tail-dna interaction=%f kB*T*nm^3\n",
           (dtail_dna * fE * fL * fL * fL) / (kB * temp * nm * nm * nm));
    // dtail_dna=Dtail_dna/(27./pow(2.*pi*(3.*sqtail_size+3.*sqdna_sigma),1.5));
    delete[] nbinn;
    nbinn = new int[NP]();
    std::fill_n(nbinn, NP, -1);
  }
  if (nbhit == NULL) {
    delete[] nbhit;
    nbhit = new int[NP]();
  }
  if (nninb == NULL) {
    delete[] nninb;
    nninb = new int[NP]();
    std::fill_n(nninb, NP, -1);
  }
  if (nnhit == NULL) {
    delete[] nnhit;
    nnhit = new int[NP * nnperb]();
  }
  if (nlperb == NULL) {
    delete[] nlperb;
    nlperb = new int[NP]();
  }
  // compute nucleosome-partition overlap
  // compute linker-partition overlap
  while (1) {
    lhit = 0;
    restart = false;
    // compute nucleosome-partition overlap
    for (int n = 0; n < nucleosomes; n++) {
      com = vec3();
      for (int h = 0; h < NHC; h++)
        com = add3(com, PositionBodyODE(bodies[F + n * NHC + h]));
      com = mult3(1. / NHC, com);
      ix = (int)((com.x - extent) / grid_bin);
      iy = (int)((com.y - extent) / grid_bin);
      iz = (int)((com.z - extent) / grid_bin);
      ibin = (ix * nbins1d * nbins1d + iy * nbins1d + iz) % NP;
      nninb[n] = ibin;
      ibinn = nbinn[ibin];
      if (ibinn == -1) {
        // first time the bin is hit
        nbinn[ibin] = lhit;
        nbhit[lhit] = ibin;
        // strictly positive index
	if ((ibinn * nnperb + nlperb[lhit]) >= (NP * nnperb))
	    printf("warning\n");
        nnhit[lhit * nnperb + nlperb[lhit]] = n + 1;
        nlperb[lhit]++;
        lhit++;
      } else {
        // new number of objects per bin
        if (nlperb[ibinn] >= nnperb) {
          nnperb++;
          // clean partition
          for (int i = 0; i < lhit; i++)
            nbinn[nbhit[i]] = -1;
          for (int i = 0; i < NP; i++) {
            nninb[i] = -1;
            nlperb[i] = 0;
          }
          // reallocation
          delete[] nnhit;
          nnhit = new int[NP * nnperb]();
          // restart computation from zero
          restart = true;
          break;
        }
        // strictly positive index
	if ((ibinn * nnperb + nlperb[ibinn]) >= (NP * nnperb))
	    printf("warning\n");
        nnhit[ibinn * nnperb + nlperb[ibinn]] = n + 1;
        nlperb[ibinn]++;
      }
    }
    // compute linker-partition overlap
    for (int n = -1; n < nucleosomes; n++) {
      if (restart)
        continue;
      if (n == -1 && nucleosome_start[0] == 0)
        continue;
      else {
        i0 = 0;
        i1 = nucleosome_start[0] - 1;
      }
      if (n == (nucleosomes - 1) &&
          (nucleosome_start[nucleosomes - 1] + NNC) == F)
        continue;
      else {
        i0 = nucleosome_start[nucleosomes - 1] + NNC;
        i1 = F - 1;
      }
      if (n > -1 && n < (nucleosomes - 1)) {
        i0 = nucleosome_start[n] + NNC;
        i1 = nucleosome_start[n + 1] - 1;
      }
      for (int i = i0; i <= i1; i++) {
        com = PositionBodyODE(bodies[i]);
        ix = (int)((com.x - extent) / grid_bin);
        iy = (int)((com.y - extent) / grid_bin);
        iz = (int)((com.z - extent) / grid_bin);
        ibin = (ix * nbins1d * nbins1d + iy * nbins1d + iz) % NP;
	// because 'n' starts at -1
        nninb[nucleosomes + n + 1] = ibin;
        ibinn = nbinn[ibin];
        if (ibinn == -1) {
          // first time the bin is hit
          nbinn[ibin] = lhit;
          nbhit[lhit] = ibin;
          // strictly negative index
	  if ((ibinn * nnperb + nlperb[lhit]) >= (NP * nnperb))
	    printf("warning\n");
          nnhit[lhit * nnperb + nlperb[lhit]] = -(i + 1);
          nlperb[lhit]++;
          lhit++;
        } else {
          // new number of objects per bin
          if (nlperb[nbinn[ibin]] >= nnperb) {
            nnperb++;
            // clean partition
            for (int j = 0; j < lhit; j++)
              nbinn[nbhit[j]] = -1;
            for (int j = 0; j < NP; j++) {
              nninb[j] = -1;
              nlperb[j] = 0;
            }
            // reallocation
            delete[] nnhit;
            nnhit = new int[NP * nnperb]();
            // restart computation from zero
            restart = true;
            break;
          }
          // strictly negative index
	  if ((ibinn * nnperb + nlperb[ibinn]) >= (NP * nnperb))
	    printf("warning\n");
          nnhit[ibinn * nnperb + nlperb[ibinn]] = -(i + 1);
          nlperb[ibinn]++;
        }
      }
    }
    if (!restart)
      break;
  }
  for (int n = 0; n < nucleosomes; n++) {
    com = vec3();
    for (int h = 0; h < NHC; h++)
      com = add3(com, PositionBodyODE(bodies[F + n * NHC + h]));
    com = mult3(1. / NHC, com);
    ix = (int)((com.x - extent) / grid_bin);
    iy = (int)((com.y - extent) / grid_bin);
    iz = (int)((com.z - extent) / grid_bin);
    for (int x = -1; x < 2; x++) {
      if ((ix + x) < 0 || (ix + x) >= nbins1d)
        continue;
      for (int y = -1; y < 2; y++) {
        if ((iy + y) < 0 || (iy + y) >= nbins1d)
          continue;
        for (int z = -1; z < 2; z++) {
          if ((iz + z) < 0 || (iz + z) >= nbins1d)
            continue;
          ibin =
              ((ix + x) * nbins1d * nbins1d + (iy + y) * nbins1d + (iz + z)) %
              NP;
          ibinn = nbinn[ibin];
          // does the bin has at least one nucleosome ?
          if (ibinn == -1)
            continue;
          for (int m = 0; m < nlperb[ibinn]; m++) {
            // -1 is because of strictly negative and/or positive index
            if (nnhit[ibinn * nnperb + m] > 0) {
              isnucleosome = true;
              nc = nnhit[ibinn * nnperb + m] - 1;
            } else {
              isnucleosome = false;
              nc = -nnhit[ibinn * nnperb + m] - 1;
            }
            // add stacking
            if (n < nc && kspring > .0 && isnucleosome) {
              for (int h = 0; h < 2; h++) {
                if (h == 0) {
                  i1 = F + nc * NHC + 0;
                  i0 = F + n * NHC + 2;
                  i3 = -1;
                  i2 = 1;
                }
                if (h == 1) {
                  i1 = F + nc * NHC + 1;
                  i0 = F + n * NHC + 3;
                  i3 = -1;
                  i2 = 1;
                }
                bn = sub3(
                    PositionPosRelBodyODE(bodies[i0], .0, .0, .5 * i2 * lb[i0]),
                    PositionPosRelBodyODE(bodies[i1], .0, .0,
                                          .5 * i3 * lb[i1]));
                // bn=force_from_Morse_potential(4.*kB*temp/fE,1.5*nm/fL,1./(1.25*nm/fL),bn);
                d0 = kspring * (norm3(bn) - espring) / norm3(bn);
                addPosRelForceBodyODE(
                    bodies[i0], vec3(.0, .0, .5 * i2 * lb[i0]), mult3(-d0, bn));
                addPosRelForceBodyODE(
                    bodies[i1], vec3(.0, .0, .5 * i3 * lb[i1]), mult3(d0, bn));
                // if((s%every)==0)
                //   printf("stacking distance=%f nm\n",norm3(bn)*fL/nm);
              }
            }
            // add tail-dna interactions
            if ((!isnucleosome || (isnucleosome && n < nc)) &&
                Dtail_dna != .0) {
              if (isnucleosome) {
                for (int p = 0; p < 8; p++) {
                  // nucleosome 'n' tail with dna of nucleosome 'nc'
                  i0 = nucleosome_start[n] + hpatches[p];
                  for (int d = 0; d < NNC; d++) {
                    i1 = nucleosome_start[nc] + d;
                    rij =
                        sub3(PositionPosRelBodyODE(bodies[i0], -rb[i0], .0, .0),
                             PositionBodyODE(bodies[i1]));
                    sqd = dot_product(rij, rij);
                    if (sqd > sqcutoff)
                      continue;
                    // 27.*exp(-3.*sqd/(2.*sqtail_size+2.*sqdna_sigma))/pow(2.*pi*(3.*sqtail_size+3.*sqdna_sigma),1.5);
                    foverlap = dtail_dna * exp(xsqd * sqd) * fnorm;
                    addPosRelForceBodyODE(bodies[i0], vec3(-rb[i0], .0, .0),
                                          mult3(foverlap, rij));
                    addForceBodyODE(bodies[i1], mult3(-foverlap, rij));
                  }
                  // nucleosome 'nc' tail with dna of nucleosome 'n'
                  i0 = nucleosome_start[nc] + hpatches[p];
                  for (int d = 0; d < NNC; d++) {
                    i1 = nucleosome_start[n] + d;
                    rij =
                        sub3(PositionPosRelBodyODE(bodies[i1], -rb[i1], .0, .0),
                             PositionBodyODE(bodies[i0]));
                    sqd = dot_product(rij, rij);
                    if (sqd > sqcutoff)
                      continue;
                    // 27.*exp(-3.*sqd/(2.*sqtail_size+2.*sqdna_sigma))/pow(2.*pi*(3.*sqtail_size+3.*sqdna_sigma),1.5);
                    foverlap = dtail_dna * exp(xsqd * sqd) * fnorm;
                    addPosRelForceBodyODE(bodies[i1], vec3(-rb[i1], .0, .0),
                                          mult3(foverlap, rij));
                    addForceBodyODE(bodies[i0], mult3(-foverlap, rij));
                  }
                }
              } else {
                // nucleosome 'n' tail with linker dna monomer 'nc'
                for (int p = 0; p < 8; p++) {
                  i0 = nucleosome_start[n] + hpatches[p];
                  i1 = nc;
                  rij = sub3(PositionPosRelBodyODE(bodies[i0], -rb[i0], .0, .0),
                             PositionBodyODE(bodies[i1]));
                  sqd = dot_product(rij, rij);
                  if (sqd > sqcutoff)
                    continue;
                  // 27.*exp(-3.*sqd/(2.*sqtail_size+2.*sqdna_sigma))/pow(2.*pi*(3.*sqtail_size+3.*sqdna_sigma),1.5);
                  foverlap = dtail_dna * exp(xsqd * sqd) * fnorm;
                  addPosRelForceBodyODE(bodies[i0], vec3(-rb[i0], .0, .0),
                                        mult3(foverlap, rij));
                  addForceBodyODE(bodies[i1], mult3(-foverlap, rij));
                }
              }
            }
          }
        }
      }
    }
  }
  // clean partition
  for (int n = 0; n < lhit; n++)
    nbinn[nbhit[n]] = -1;
  for (int n = 0; n < NP; n++) {
    nninb[n] = -1;
    nlperb[n] = 0;
  }
}

/*! @brief callback for collision between two geometries.
  it is designed to test the Verlet-list implementation.
  @param data see Open-Dynamics-Engine definition of nearCallback
  @param o1 first geometry
  @param o2 second geometry
*/
void test_nearCallback(void *data, dGeomID o1, dGeomID o2) {
  assert(o1);
  assert(o2);
  dBodyID b1 = dGeomGetBody(o1);
  dBodyID b2 = dGeomGetBody(o2);
  // no collision between two linked bodies
  if (b1 && b2 && dAreConnected(b1, b2))
    return;
  int3p *c1 = (int3p *)dBodyGetData(b1);
  int3p *c2 = (int3p *)dBodyGetData(b2);
  // no need to look for contacts inside nucleosome
  if ((c1->k) == (c2->k) && (c1->k) > 0)
    return;
  // build contacts
  const int dcontact_size = (*((int3p *)data)).i;
  dContact *contact = new dContact[dcontact_size];
  int n = dCollide(o1, o2, dcontact_size, &(contact[0].geom), sizeof(dContact));
  if (n > 0)
    ((*((int3p *)data)).j)++;
  c1 = NULL;
  c2 = NULL;
  delete[] contact;
  contact = NULL;
}

/*! @brief callback for collision between two geometries.
  @param data see Open-Dynamics-Engine definition of nearCallback
              here, data is the contact size (int)
  @param o1 first geometry
  @param o2 second geometry
*/
void nearCallback(void *data, dGeomID o1, dGeomID o2) {
  assert(o1);
  assert(o2);
  bool order_12;
  dBodyID b1 = dGeomGetBody(o1);
  dBodyID b2 = dGeomGetBody(o2);
  // no collision between two linked bodies
  if (b1 && b2 && dAreConnected(b1, b2))
    return;
  int3p c1 = *(int3p *)(dBodyGetData(b1));
  int3p c2 = *(int3p *)(dBodyGetData(b2));
  // no need to look for contacts inside nucleosome
  if ((c1.k) == (c2.k) && (c1.k) > 0)
    return;
  // ???
  // double radius1,length1,radius2,length2;
  // if(dGeomGetClass(o1)==dCapsuleClass && dGeomGetClass(o2)==dCapsuleClass){
  //   dGeomCapsuleGetParams(o1,&radius1,&length1);
  //   dGeomCapsuleGetParams(o2,&radius2,&length2);
  //   if(norm3(sub3(PositionBodyODE(b1),PositionBodyODE(b2)))>(.5*(length1+length2)+radius1+radius2))
  //     return;
  // }
  // ???
  // build contacts
  int nc_VL, nc_max, dcontact_size;
  if (data != NULL)
    dcontact_size = *((const int *)data);
  else
    dcontact_size = contact_array_size;
  dContact *contact = new dContact[dcontact_size];
  int n = dCollide(o1, o2, dcontact_size, &(contact[0].geom), sizeof(dContact));
  // add to the Verlet-list
  if (n > 0) {
    // sort pairwize according to the index of each geometry
    order_12 = (bool)((c1.j) <= (c2.j));
    if (order_12) {
      nc_VL = (c1.p)[0];
      nc_max = (c1.p)[1];
    } else {
      nc_VL = (c2.p)[0];
      nc_max = (c2.p)[1];
    }
    // reallocation ?
    if (nc_VL >= nc_max) {
      // printf("reallocation %i/%i (%i,%i)\n",nc_VL,nc_max,c1.j,c2.j);
      nc_max = nc_VL + 1;
      if (order_12) {
        int_new_copy(&(((int3p *)(dBodyGetData(b1)))->p), nc_VL, nc_max);
        (((int3p *)(dBodyGetData(b1)))->p)[1] = nc_max;
      } else {
        int_new_copy(&(((int3p *)(dBodyGetData(b2)))->p), nc_VL, nc_max);
        (((int3p *)(dBodyGetData(b2)))->p)[1] = nc_max;
      }
      // printf("reallocation done %i/%i (%i,%i)\n",nc_VL,nc_max,c1.j,c2.j);
    }
    // add the geometry to the Verlet-List
    // increment the length of the Verlet-List
    if (order_12) {
      (((int3p *)(dBodyGetData(b1)))->p)[nc_VL] = c2.j;
      (((int3p *)(dBodyGetData(b1)))->p)[0] = nc_VL + 1;
    } else {
      (((int3p *)(dBodyGetData(b2)))->p)[nc_VL] = c1.j;
      (((int3p *)(dBodyGetData(b2)))->p)[0] = nc_VL + 1;
    }
  } // Fi "n>0"
  delete[] contact;
  contact = NULL;
}

/*! return a ghost hash space.
  https://ode.org/wiki/index.php?title=Manual
  all the geometries in the ghost space have to be extruded.
  it is kind of a Verlet-list.
  @param max_dim largest dimension
  @param xfactor scale factor to apply
*/
dSpaceID ghost_hash_space(double max_dim, double xfactor) {
  dSpaceID ghost_space = dHashSpaceCreate(0);
  dHashSpaceSetLevels(ghost_space, 0,
                      (int)(log(xfactor * max_dim) / log(2.)) + 1);
  return ghost_space;
}

/*! return an hash space.
  https://ode.org/wiki/index.php?title=Manual
  @param min_dim dimension to determine hash space level
  @param max_dim dimension to determine hash space level
*/
dSpaceID hash_space(double min_dim, double max_dim) {
  // 2^n>max_dim
  int p2 = 1, n2 = 0;
  while (p2 < max_dim) {
    p2 *= 2;
    n2++;
  }
  // printf("%f %f/%i\n",max_dim,pow(2.,n2),(int)(log(max_dim)/log(2.))+1);
  dSpaceID space = dHashSpaceCreate(0);
  dHashSpaceSetLevels(space, (int)(log(min_dim) / log(2.)) - 1, n2);
  return space;
}

/*! return an quad-tree space.
  https://ode.org/wiki/index.php?title=Manual
  @param center see ODE documentation
  @param extents see ODE documentation
  @param depth see ODE documentation
*/
dSpaceID quadtree_space(vec3 center, vec3 extents, int depth) {
  dVector3 arg1, arg2;
  arg1[0] = center.x;
  arg1[1] = center.y;
  arg1[2] = center.z;
  arg2[0] = extents.x;
  arg2[1] = extents.y;
  arg2[2] = extents.z;
  return dQuadTreeSpaceCreate(0, arg1, arg2, depth);
}

/*! @brief creates and returns ghost geometry.
  @param space 'dSpaceID' where to create the ghost geometry
  @param b 'dBodyID' object to set to the ghost geometry
  @param g1 type/name of the geometry (sphere, capsule, cylinder)
  @param g2 type/name of the ghost geometry (sphere, capsule, cylinder)
  @param xdim multiply the geometry size by this factor
  @param dim1 size 1 of the geometry (radius for sphere, capsule and cylinder)
  @param dim2 size 2 of the geometry (radius for sphere, capsule and cylinder)
  @param dim3 size 3 of the geometry (radius for sphere, length for capsule and
  cylinder)
*/
dGeomID create_ghost_geometry(dSpaceID space, dBodyID b, const char *g1,
                              const char *g2, double xdim, double dim1,
                              double dim2, double dim3) {
  dGeomID g = NULL;
  if (strcmp(g1, "box") == 0)
    g = dCreateBox(space, xdim * dim1, xdim * dim2, xdim * dim3);
  // from capsule to ...
  if (strcmp(g1, "capsule") == 0) {
    // ... ghost sphere or ghost capsule
    if (strcmp(g2, "sphere") == 0)
      g = dCreateSphere(space, xdim * dim1 + .5 * dim3);
    else
      g = dCreateCapsule(space, xdim * dim1, dim3);
  }
  // from cylinder to ...
  if (strcmp(g1, "cylinder") == 0) {
    // ... ghost sphere or ghost cylinder
    if (strcmp(g2, "sphere") == 0)
      g = dCreateSphere(space, xdim * (dim1 + .5 * dim3));
    else
      g = dCreateCylinder(space, xdim * dim1, xdim * dim3);
  }
  // from sphere to ghost sphere
  if (strcmp(g1, "sphere") == 0)
    g = dCreateSphere(space, xdim * dim1);
  if (g != NULL)
    dGeomSetBody(g, b);
  return g;
}

/*! @brief initialize the 'dBodyID' data.
  it is used to store the geometries that potentially overlap.
  @param ds list of data per 'dBodyID'
  @param N number of 'dBodyID' in the ODE world
  @param operg number of pairwize per geometry
*/
void initialize_body_data(int3p **ds, int N, int operg) {
  for (int n = 0; n < N; n++) {
    ((*ds)[n].p) = new int[operg]();
    for (int i = 0; i < operg; i++)
      ((*ds)[n].p)[i] = -1;
    ((*ds)[n].p)[0] = 2;
    ((*ds)[n].p)[1] = operg;
  }
}

/*! @brief clean 'dBodyID' data.
  @param ds list of data per 'dBodyID'
  @param N number of 'dBodyID' in the ODE world
*/
void clean_body_data(int3p **ds, int N) {
  for (int n = 0; n < N; n++) {
    if ((*ds)[n].p != NULL) {
      delete[](*ds)[n].p;
      (*ds)[n].p = NULL;
    }
  }
  delete[] * ds;
  *ds = NULL;
}

/*! @brief collide capsule o1 with capsule o2 (add feedback to the contact
  joint).
  @param w 'dWorldID' where to find the bodies
  @param g1 first geometry
  @param g2 second geometry
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup contact group from Open-Dynamics-Engine
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param do_not_attach if true do not create contact joint
*/
int collide_c1_c2(dWorldID w, dGeomID g1, dGeomID g2, double wERP, double wCFM,
                  dJointGroupID &contactgroup, double &sum_abs_depth,
                  double &max_abs_depth, bool do_not_attach) {
  sum_abs_depth = max_abs_depth = .0;
  dBodyID b1 = dGeomGetBody(g1);
  dBodyID b2 = dGeomGetBody(g2);
  double radius1, length1, radius2, length2, d1;
  vector<vec3> mindist(3);
  if (!b1 || !b2)
    return 0;
  if (dAreConnected(b1, b2))
    return 0;
  if (dGeomGetClass(g1) == dCapsuleClass)
    dGeomCapsuleGetParams(g1, &radius1, &length1);
  else {
    if (dGeomGetClass(g1) == dSphereClass) {
      radius1 = dGeomSphereGetRadius(g1);
      length1 = .0;
    }
  }
  if (dGeomGetClass(g2) == dCapsuleClass)
    dGeomCapsuleGetParams(g2, &radius2, &length2);
  else {
    if (dGeomGetClass(g2) == dSphereClass) {
      radius2 = dGeomSphereGetRadius(g2);
      length2 = .0;
    }
  }
  // d1=.5*(length1+length2)+radius1+radius2;
  // if(dot_product2(sub3(PositionBodyODE(b1),PositionBodyODE(b2)))>(d1*d1))
  //   return 0;
  dContact contact[1];
  minDist2segments_KKT(PositionPosRelBodyODE(b1, .0, .0, -.5 * length1),
                       PositionPosRelBodyODE(b1, .0, .0, .5 * length1),
                       PositionPosRelBodyODE(b2, .0, .0, -.5 * length2),
                       PositionPosRelBodyODE(b2, .0, .0, .5 * length2), false,
                       0, mindist);
  d1 = norm3(sub3(mindist[0], mindist[1]));
  if (d1 < (radius1 + radius2)) {
    sum_abs_depth += (d1 < (radius1 + radius2)) ? radius1 + radius2 - d1 : .0;
    max_abs_depth =
        fmax(max_abs_depth,
             (d1 < (radius1 + radius2)) ? radius1 + radius2 - d1 : .0);
    if (!do_not_attach) {
      contact[0].surface.mode =
          dContactSoftERP | dContactSoftCFM | dContactBounce | dContactMu2;
      contact[0].surface.mu = 0.;
      contact[0].surface.mu2 = 0.;
      contact[0].surface.bounce_vel = 0.;
      contact[0].surface.bounce = 1.;
      contact[0].surface.soft_erp = wERP;
      contact[0].surface.soft_cfm = wCFM;
      dJointID c = dJointCreateContact(w, contactgroup, &contact[0]);
      dJointAttach(c, b1, b2);
      // dJointFeedback fc;
      // dJointSetFeedback(c,&fc);
    }
    return 1;
  } else
    return 0;
}

/*! @brief collide geom o1 with geom o2 (add feedback to the contact joint).
  @param w 'dWorldID' where to find the bodies
  @param g1 first geometry
  @param g2 second geometry
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup contact group from Open-Dynamics-Engine
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
  @param do_not_attach if true do not create contact joint
*/
int collide_g1_g2(dWorldID w, dGeomID g1, dGeomID g2, double wERP, double wCFM,
                  dJointGroupID &contactgroup, double &sum_abs_depth,
                  double &max_abs_depth, bool do_not_attach) {
  sum_abs_depth = max_abs_depth = .0;
  dBodyID b1 = dGeomGetBody(g1);
  dBodyID b2 = dGeomGetBody(g2);
  if (b1 && b2 && dAreConnected(b1, b2))
    return 0;
  int n = 0;
  dContact contact[contact_array_size];
  n = dCollide(g1, g2, contact_array_size, &(contact[0].geom),
               sizeof(dContact));
  if (n >= contact_array_size)
    printf("contact array size is too small.\n");
  if (n > 0) {
    for (int i = 0; i < min(n, contact_array_size); i++) {
      sum_abs_depth += fabs(contact[i].geom.depth);
      max_abs_depth = fmax(max_abs_depth, fabs(contact[i].geom.depth));
      if (!do_not_attach) {
        contact[i].surface.mode =
            dContactSoftERP | dContactSoftCFM | dContactBounce | dContactMu2;
        contact[i].surface.mu = 0.;
        contact[i].surface.mu2 = 0.;
        contact[i].surface.bounce_vel = 0.;
        contact[i].surface.bounce = 1.;
        contact[i].surface.soft_erp = wERP;
        contact[i].surface.soft_cfm = wCFM;
        dJointID c = dJointCreateContact(w, contactgroup, &contact[i]);
        dJointAttach(c, b1, b2);
        // dJointFeedback fc;
        // dJointSetFeedback(c,&fc);
      }
    }
  }
  return (int)(n > 0);
}

/*! @brief collide all the pairwize (from Verlet-list) in the normal space and
  return the number of collisions (add feedback to the contact joint).
  @param w 'dWorldID' where to find the bodies
  @param gs list of geometries (one per 'dBodyID') passed by reference
  @param ds list of data (one per 'dBodyID') passed by reference
  @param N number of 'dBodyID' in the ODE world
  @param wERP error-reduction-parameter from Open-Dynamics-Engine
  @param wCFM constraint-force-mixing from Open-Dynamics-Engine
  @param contactgroup
  @param stop_if return 1 if one collision occurs and if stop_if is equal to
  true
  @param do_not_attach if true do not create contact joint
  @param sum_abs_depth cumulative of the absolute value of the contact depth
  @param max_abs_depth maximum of the absolute value of the contact depth
*/
int collide_all(dWorldID w, dGeomID *&gs, int3p *ds, int N, double wERP,
                double wCFM, dJointGroupID &contactgroup, double &sum_abs_depth,
                double &max_abs_depth, bool stop_if, bool do_not_attach) {
  int nc = 0, nc_VL = 0, cs, i0;
  dBodyID b1, b2;
  dContact contact[contact_array_size];
  double radius1, length1, radius2, length2, cut;
  max_abs_depth = sum_abs_depth = .0;
  if (contactgroup)
    dJointGroupEmpty(contactgroup);
  for (int n = 0; n < N; n++) {
    nc_VL = (ds[n].p)[0];
    b1 = dGeomGetBody(gs[n]);
    if (dGeomGetClass(gs[n]) == dCapsuleClass)
      dGeomCapsuleGetParams(gs[n], &radius1, &length1);
    for (int v = 2; v < nc_VL; v++) {
      i0 = (ds[n].p)[v];
      b2 = dGeomGetBody(gs[i0]);
      if (!b1 || !b2)
        continue;
      if (dAreConnected(b1, b2))
        continue;
      // do not collide if distance between two center of mass
      // exceed dimension of the capsule
      if (dGeomGetClass(gs[n]) == dCapsuleClass &&
          dGeomGetClass(gs[i0]) == dCapsuleClass) {
        dGeomCapsuleGetParams(gs[i0], &radius2, &length2);
        cut = .5 * (length1 + length2) + radius1 + radius2;
        if (dot_product2(sub3(PositionBodyODE(b1), PositionBodyODE(b2))) >
            (cut * cut))
          continue;
      }
      cs = dCollide(gs[n], gs[i0], contact_array_size, &(contact[0].geom),
                    sizeof(dContact));
      // if(cs==0){// ???
      //   printf("(%i/%i) %i-%i\n",v,nc_VL,n,i0);
      //   // dGeomCapsuleGetParams(gs[n],&r,&l);
      //   // printf("%f %f\n",r,l);
      //   // dGeomCapsuleGetParams(gs[i0],&r,&l);
      //   // printf("%f %f\n",r,l);
      //   exit(EXIT_FAILURE);
      // }
      if (cs >= contact_array_size)
        printf("contact array size is too small.\n");
      if (cs > 0) {
        for (int i = 0; i < min(cs, contact_array_size); i++) {
          sum_abs_depth += fabs(contact[i].geom.depth);
          max_abs_depth = fmax(max_abs_depth, fabs(contact[i].geom.depth));
          if (!do_not_attach) {
            contact[i].surface.mode = dContactSoftERP | dContactSoftCFM |
                                      dContactBounce | dContactMu2;
            contact[i].surface.mu = 0.;
            contact[i].surface.mu2 = 0.;
            contact[i].surface.bounce_vel = 0.;
            contact[i].surface.bounce = 1.;
            contact[i].surface.soft_erp = wERP;
            contact[i].surface.soft_cfm = wCFM;
            dJointID c = dJointCreateContact(w, contactgroup, &contact[i]);
            dJointAttach(c, b1, b2);
            // dJointFeedback fc;
            // dJointSetFeedback(c,&fc);
          }
        }
        nc++;
        if (stop_if)
          return nc;
      }
    }
  }
  return nc;
}

/*! @brief build kind of Verlet-list and return the potentially number of
  collisions. geometries have been extruded in every dimensions. they are used
  to build the Verlet-list.
  @param space ghost space
  @param step current step
  @param every build Verlet-list 'every' steps
  @param ds list of data per 'dBodyID' passed by reference
  @param N number of 'dBodyID' in the ODE world
  @param contact_size size of 'dContact' (see Open-Dynamics-Engine for more
  details)
  @param nearCallback callback function (see Open-Dynamics-Engine for more
  details)
  @param return_vl if true return the number of ghost collisions, otherwize
  return -1 (default)
*/
int build_Verlet_list(dSpaceID &space, int step, int every, int3p **ds, int N,
                      int contact_size,
                      void (*nearCallback)(void *, dGeomID, dGeomID),
                      bool return_vl) {
  if ((step % every) == 0 || step == 0 || step == 1) {
    for (int n = 0; n < N; n++)
      ((*ds)[n].p)[0] = 2;
    dSpaceCollide(space, &contact_size, nearCallback);
  }
  // return the potentially number of collisions
  int nc = 0;
  if (return_vl)
    for (int n = 0; n < N; n++)
      nc += ((*ds)[n].p)[0] - 2;
  else
    nc = -1;
  return nc;
}

/*! @brief increase the size of a list to 'b' while preserving the 'a' elements
  @param p int* to increase size
  @param a old size
  @param b new size
*/
void int_new_copy(int **p, int a, int b) {
  int *buffer = new int[a];
  for (int i = 0; i < a; i++)
    buffer[i] = (*p)[i];
  delete[] * p;
  // p=NULL;
  *p = new int[b];
  for (int i = 0; i < a; i++)
    (*p)[i] = buffer[i];
  delete[] buffer;
  buffer = NULL;
}

/*! @brief increase the size of a list to 'b' while preserving the 'a' elements
  @param p double* to increase size
  @param a old size
  @param b new size
*/
void double_new_copy(double **p, int a, int b) {
  double *buffer = new double[a];
  for (int i = 0; i < a; i++)
    buffer[i] = (*p)[i];
  delete[] * p;
  // p=NULL;
  *p = new double[b];
  for (int i = 0; i < a; i++)
    (*p)[i] = buffer[i];
  delete[] buffer;
  buffer = NULL;
}

// dichotomy
double dichotomy(double (*f)(double), double y, double a, double b, double p) {
  double fa, fb, m, fm;
  fa = f(a) - y;
  fb = f(b) - y;
  m = .5 * (a + b);
  fm = f(m) - y;
  // invalid interval
  if (fa * fb >= 0 || a >= b) {
    printf("invalid interval\n");
    printf("a=%e b=%e\n", a, b);
    printf("f(a)=%e f(b)=%e y=%e\n", f(a), f(b), y);
    exit(EXIT_FAILURE);
  }
  // precision ?
  while (fabs(a - b) > p) {
    m = .5 * (a + b);
    fm = f(m) - y;
    fa = f(a) - y;
    if (fm == 0.)
      break; // zero ?
    if (fa * fm > 0.)
      a = m;
    else
      b = m;
  }
  return m;
}

/*! @brief return the Langevin function at 'x'.
  @param x point where to evaluate Langevin function
*/
double flangevin(double x) { return (x == .0) ? .0 : 1. / tanh(x) - 1. / x; }

/*! @brief print 'vec3 u' and its norm.
  @param u vec3 to print
*/
void print3(vec3 u) { printf("%e,%e,%e->%e\n", u.x, u.y, u.z, norm3(u)); }

/*! @brief return random vector (isotropic distribution)
 */
vec3 random_vec3(std::mt19937_64 &mt, std::uniform_real_distribution<> u01) {
  double unif1 = 2. * arccos(-1.) * u01(mt);
  double unif2 = arccos(2. * u01(mt) - 1.);
  return vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
}

/*! @brief add force from Morse potential at point
  com(i)+u_i*u(i)+v_i*v(i)+t_i*t(i)
  @param D strength
  @param re equilibrium distance
  @param a inverse of range
  @param b1 first dBodyID object
  @param u1 position of the point (wrt com) along u
  @param v1 position of the point (wrt com) along v
  @param t1 position of the point (wrt com) along t
  @param b2 second dBodyID object
  @param u2 position of the point (wrt com) along u
  @param v2 position of the point (wrt com) along v
  @param t2 position of the point (wrt com) along t
*/
void ForceMorse(double D, double re, double a, dBodyID b1, double u1, double v1,
                double t1, dBodyID b2, double u2, double v2, double t2) {
  vec3 ri =
      add3(PositionBodyODE(b1),
           add3(mult3(u1, FrameODE(b1, 0)),
                add3(mult3(v1, FrameODE(b1, 1)), mult3(t1, FrameODE(b1, 2)))));
  vec3 rj =
      add3(PositionBodyODE(b2),
           add3(mult3(u2, FrameODE(b2, 0)),
                add3(mult3(v2, FrameODE(b2, 1)), mult3(t2, FrameODE(b2, 2)))));
  vec3 rij = sub3(ri, rj);
  double n_rij = norm3(rij);
  double exp_m = exp(-a * (n_rij - re));
  vec3 FM = (n_rij == .0)
                ? vec3(.0, .0, .0)
                : mult3(-2. * D * a * exp_m * (1. - exp_m) / n_rij, rij);
  dBodyAddForceAtRelPos(b1, FM.x, FM.y, FM.z, u1, v1, t1);
  dBodyAddForceAtRelPos(b2, -FM.x, -FM.y, -FM.z, u2, v2, t2);
}

/*! @brief return force derived from Morse potential (apply force to 'i' and
  -force to 'j')
  @param Dm strength
  @param em equilibrium distance
  @param am inverse of range
  @param rij distance vector ri-rj between 'i' and 'j'
*/
vec3 force_from_Morse_potential(double Dm, double em, double am, vec3 rij) {
  double nrij = norm3(rij);
  double expm = exp(-am * (nrij - em));
  return (nrij == .0) ? vec3()
                      : mult3(-2. * Dm * am * expm * (1. - expm) / nrij, rij);
}

/*! @brief return Morse potential
  @param Dm strength
  @param em equilibrium distance
  @param am inverse of range
  @param rij distance vector ri-rj between 'i' and 'j'
*/
double Morse_potential(double Dm, double em, double am, vec3 rij) {
  double nrij = norm3(rij);
  double expm = exp(-am * (nrij - em));
  return Dm * (1. - expm) * (1. - expm);
}

/*! @brief return the 3d cross product.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 cross_product(vec3 u, vec3 v) {
  vec3 pv;
  pv.x = u.y * v.z - u.z * v.y;
  pv.y = u.z * v.x - u.x * v.z;
  pv.z = u.x * v.y - u.y * v.x;
  return pv;
}

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
  @param seed init rPNGs
*/
vec3 solve_cross_a_b_equal_c_v1(vec3 a, vec3 c, int seed) {
  std::mt19937_64 mt(seed);
  std::uniform_real_distribution<> u01(.0, 1.);
  // u=a/||a||
  // b=x*u+y*v+z*t
  double na = 1. / norm3(a);
  vec3 u = normalize3(a);
  // random vector rv
  double unif1 = 2. * arccos(-1.) * u01(mt);
  double unif2 = arccos(2. * u01(mt) - 1.);
  vec3 rv = vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
  while (fabs(dot_product(u, rv)) > .99) {
    unif1 = 2. * arccos(-1.) * u01(mt);
    unif2 = arccos(2. * u01(mt) - 1.);
    rv = vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
  }
  // build t=cross(u,rv)
  vec3 t = normalize3(cross_product(u, rv));
  // build v=cross(t,u)
  vec3 v = normalize3(cross_product(t, u));
  // ||a||*cross(u,x*u+y*v+z*t)=c
  // ||a||*cross(u,y*v+z*t)=c
  // ||a||*(y*t-z*v)=c
  return add3(mult3(dot_product(t, c) * na, v),
              mult3(-dot_product(v, c) * na, t));
}

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
  @param mt Mersenne-Twister from std
*/
vec3 solve_cross_a_b_equal_c_v2(vec3 a, vec3 c, std::mt19937_64 &mt) {
  std::uniform_real_distribution<> u01(.0, 1.);
  // u=a/||a||
  // b=x*u+y*v+z*t
  double na = 1. / norm3(a);
  vec3 u = normalize3(a);
  // random vector rv
  double unif1 = 2. * arccos(-1.) * u01(mt);
  double unif2 = arccos(2. * u01(mt) - 1.);
  vec3 rv = vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
  while (fabs(dot_product(u, rv)) > .99) {
    unif1 = 2. * arccos(-1.) * u01(mt);
    unif2 = arccos(2. * u01(mt) - 1.);
    rv = vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
  }
  // build t=cross(u,rv)
  vec3 t = normalize3(cross_product(u, rv));
  // build v=cross(t,u)
  vec3 v = normalize3(cross_product(t, u));
  // ||a||*cross(u,x*u+y*v+z*t)=c
  // ||a||*cross(u,y*v+z*t)=c
  // ||a||*(y*t-z*v)=c
  return add3(mult3(dot_product(t, c) * na, v),
              mult3(-dot_product(v, c) * na, t));
}

/*! @brief solve cross(a,b)=c and return solution for 'b'.
  Of note, there is an infinite number of solutions along vector 'a'.
  Build random frame (u=a/||a||,v,t).
  @param a lhs vector
  @param c rhs vector
*/
vec3 solve_cross_a_b_equal_c_v3(vec3 a, vec3 c) {
  // u=a/||a||
  // b=x*u+y*v+z*t
  double na = 1. / norm3(a);
  vec3 u = normalize3(a);
  // random vector rv
  vec3 rv = vec3(1., 1., 1.);
  if (fabs(dot_product(u, rv)) > .99)
    rv.x += 1.;
  if (fabs(dot_product(u, rv)) > .99) {
    rv.x = 1.;
    rv.y += 1.;
  }
  if (fabs(dot_product(u, rv)) > .99) {
    rv.x = 1.;
    rv.y = 1.;
    rv.z += 1.;
  }
  // build t=cross(u,rv)
  vec3 t = normalize3(cross_product(u, rv));
  // build v=cross(t,u)
  vec3 v = normalize3(cross_product(t, u));
  // ||a||*cross(u,x*u+y*v+z*t)=c
  // ||a||*cross(u,y*v+z*t)=c
  // ||a||*(y*t-z*v)=c
  return add3(mult3(dot_product(t, c) * na, v),
              mult3(-dot_product(v, c) * na, t));
}

/*! @brief return 'vec3 u' multiplied by a scalar 'a'.
  @param a scalar
  @param u vector
*/
vec3 mult3(double a, vec3 u) { return vec3(a * u.x, a * u.y, a * u.z); }

/*! @brief return the addition of vec3 'u' with vec3 'v'.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 add3(vec3 u, vec3 v) { return vec3(u.x + v.x, u.y + v.y, u.z + v.z); }

/*! @brief return the subtraction of vec3 'u' with vec3 'v'.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 sub3(vec3 u, vec3 v) { return vec3(u.x - v.x, u.y - v.y, u.z - v.z); }

/*! @brief return the subtraction of vec3 'u' with vec3 'v'.
  @param u first 'vec3'
  @param v second 'vec3'
*/
vec3 normalize3(vec3 u) {
  double n = sqrt(u.x * u.x + u.y * u.y + u.z * u.z);
  return (n != .0) ? mult3(1. / n, u) : u;
}

/*! @brief return the norm of 'vec3 u'.
  @param u 3d vector
*/
double norm3(vec3 u) { return sqrt(u.x * u.x + u.y * u.y + u.z * u.z); }

/*! @brief return the tranpose of 'mat33 m'.
  @param m the 'mat33' to transpose
*/
mat33 mat3T(mat33 m) {
  double a = m.c1.y;
  m.c1.y = m.c2.x;
  m.c2.x = a;
  a = m.c1.z;
  m.c1.z = m.c3.x;
  m.c3.x = a;
  a = m.c2.z;
  m.c2.z = m.c3.y;
  m.c3.y = a;
  return m;
}

/*! @brief return the matricial element-wize addition.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 add33(mat33 m1, mat33 m2) {
  m1.c1 = add3(m1.c1, m2.c1);
  m1.c2 = add3(m1.c2, m2.c2);
  m1.c3 = add3(m1.c3, m2.c3);
  return m1;
}

/*! @brief return the matricial element-wize subtract.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 sub33(mat33 m1, mat33 m2) {
  m1.c1 = sub3(m1.c1, m2.c1);
  m1.c2 = sub3(m1.c2, m2.c2);
  m1.c3 = sub3(m1.c3, m2.c3);
  return m1;
}

/*! @brief return the matricial product.
  @param m1 first matrix 'mat33'
  @param m2 second matrix 'mat33'
*/
mat33 mult33(mat33 m1, mat33 m2) {
  double a = m1.c1.x, b = m1.c2.x, c = m1.c3.x;
  m1.c1.x = a * m2.c1.x + b * m2.c1.y + c * m2.c1.z;
  m1.c2.x = a * m2.c2.x + b * m2.c2.y + c * m2.c2.z;
  m1.c3.x = a * m2.c3.x + b * m2.c3.y + c * m2.c3.z;
  a = m1.c1.y;
  b = m1.c2.y;
  c = m1.c3.y;
  m1.c1.y = a * m2.c1.x + b * m2.c1.y + c * m2.c1.z;
  m1.c2.y = a * m2.c2.x + b * m2.c2.y + c * m2.c2.z;
  m1.c3.y = a * m2.c3.x + b * m2.c3.y + c * m2.c3.z;
  a = m1.c1.z;
  b = m1.c2.z;
  c = m1.c3.z;
  m1.c1.z = a * m2.c1.x + b * m2.c1.y + c * m2.c1.z;
  m1.c2.z = a * m2.c2.x + b * m2.c2.y + c * m2.c2.z;
  m1.c3.z = a * m2.c3.x + b * m2.c3.y + c * m2.c3.z;
  return m1;
}

/*! @brief return the product of mat33 'm' with vec3 'v'.
  @param m mat33
  @param v vec3
*/
vec3 mult_33_3(mat33 m, vec3 v) {
  double a = v.x, b = v.y;
  v.x = m.c1.x * a + m.c2.x * b + m.c3.x * v.z;
  v.y = m.c1.y * a + m.c2.y * b + m.c3.y * v.z;
  v.z = m.c1.z * a + m.c2.z * b + m.c3.z * v.z;
  return v;

} /*! @brief return the skew mat33 'm' from vec3 'v'.
   @param v vec3
 */
mat33 skew_from_vec3(vec3 v) {
  mat33 m;
  m.c1 = vec3(.0, v.z, -v.y);
  m.c2 = vec3(-v.z, .0, v.x);
  m.c3 = vec3(v.y, -v.x, .0);
  return m;
}

/*! @brief return 'dWorldID'.
  @param g gravity
  @param wERP error-reduction-parameter
  @param wCFM constraint-force-mixing
  @param nSOR number of successive over-iterations
  @param wSOR over-relaxation value
*/
dWorldID create_world_ODE(double g, double wERP, double wCFM, int nSOR,
                          double wSOR) {
  dWorldID WorldODE = dWorldCreate();
  dWorldSetGravity(WorldODE, 0., 0., g);
  dWorldSetERP(WorldODE, wERP);
  dWorldSetCFM(WorldODE, wCFM);
  dWorldSetQuickStepNumIterations(WorldODE, nSOR);
  dWorldSetQuickStepW(WorldODE, wSOR);
  return WorldODE;
}

/*! @brief return first, second or third principal axis of 'dBodyID b'.
  @param b 'dBodyID' object
  @param a index of the principal axis
*/
vec3 FrameODE(dBodyID b, int a) {
  const dReal *o = dBodyGetRotation(b);
  return vec3(o[0 + a], o[4 + a], o[8 + a]);
}

/*! @brief return rotation matrix of 'dBodyID b'.
  @param b 'dBodyID' object
*/
mat33 RotationBodyODE(dBodyID b) {
  const dReal *R = dBodyGetRotation(b);
  vec3 c1 = vec3(R[0], R[4], R[8]);
  vec3 c2 = vec3(R[1], R[5], R[9]);
  vec3 c3 = vec3(R[2], R[6], R[10]);
  return mat33(c1, c2, c3);
}

/*! @brief return position of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 PositionBodyODE(dBodyID b) {
  const dReal *r = dBodyGetPosition(b);
  return vec3(r[0], r[1], r[2]);
}

/*! @brief return position of 'dGeomID b'.
  @param g 'dGeomID' object
*/
vec3 PositionGeomODE(dGeomID g) {
  const dReal *r = dGeomGetPosition(g);
  return vec3(r[0], r[1], r[2]);
}

/*! @brief return the position (with respect to the frame world) of a point
  writing with respect to the body frame
  @param b 'dBodyID' object
  @param x x-coordinate writing with respect to the body frame
  @param y y-coordinate writing with respect to the body frame
  @param z z-coordinate writing with respect to the body frame
*/
vec3 PositionPosRelBodyODE(dBodyID b, double x, double y, double z) {
  vec3 r = PositionBodyODE(b);
  r = add3(r, mult3(x, FrameODE(b, 0)));
  r = add3(r, mult3(y, FrameODE(b, 1)));
  r = add3(r, mult3(z, FrameODE(b, 2)));
  return r;
}

/*! @brief return linear velocity of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 LinVelBodyODE(dBodyID b) {
  const dReal *v = dBodyGetLinearVel(b);
  return vec3(v[0], v[1], v[2]);
}

/*! @brief return angular velocity of 'dBodyID b'.
  @param b 'dBodyID' object
*/
vec3 AngVelBodyODE(dBodyID b) {
  const dReal *w = dBodyGetAngularVel(b);
  return vec3(w[0], w[1], w[2]);
}

/*! @brief return angular velocity of 'dBodyID b' with respect to the body
  frame.
  @param b 'dBodyID' object
*/
vec3 AngVel2BodyODE(dBodyID b) {
  const dReal *w = dBodyGetAngularVel(b);
  dVector3 o;
  dBodyVectorFromWorld(b, w[0], w[1], w[2], o);
  w = NULL;
  return vec3(o[0], o[1], o[2]);
}

/*! @brief return the velocity (with respect to the frame world) of a point
  writing with respect to the body frame.
  @param b 'dBodyID' object
  @param x x-coordinate writing with respect to the body frame
  @param y y-coordinate writing with respect to the body frame
  @param z z-coordinate writing with respect to the body frame
*/
vec3 VelPosRelBodyODE(dBodyID b, double x, double y, double z) {
  vec3 t, v;
  t = vec3(0., 0., 0.);
  t = add3(t, mult3(x, FrameODE(b, 0)));
  t = add3(t, mult3(y, FrameODE(b, 1)));
  t = add3(t, mult3(z, FrameODE(b, 2)));
  v = LinVelBodyODE(b);
  // Varignon : v + w x (x*u + y*v +z*t)
  v = add3(v, cross_product(AngVelBodyODE(b), t));
  return v;
}

/*! @brief zero to the force and torque of dBodyID 'b'.
  @param b 'dBodyID' object
*/
void ZeroForceTorqueBodyODE(dBodyID b) {
  dBodySetForce(b, 0., 0., 0.);
  dBodySetTorque(b, 0., 0., 0.);
}

/*! @brief add 'vec3' force f to dBodyID 'b'.
  @param b 'dBodyID' object
  @param f force to add
*/
void addForceBodyODE(dBodyID b, vec3 f) { dBodyAddForce(b, f.x, f.y, f.z); }

/*! @brief add 'vec3' force 'f' to dBodyID 'b' (x,y,z) coordinates with respect
  to the frame u,v,t
  @param b 'dBodyID' object
  @param r point where to add the force
  @param f force to add
*/
void addPosRelForceBodyODE(dBodyID b, vec3 r, vec3 f) {
  dBodyAddForceAtRelPos(b, f.x, f.y, f.z, r.x, r.y, r.z);
}

/*! @brief add vec3 force 'f' to dBodyID 'b' (x,y,z) coordinates with respect to
  the world frame
  @param b 'dBodyID' object
  @param r point where to add the force
  @param f force to add
*/
void addPosForceBodyODE(dBodyID b, vec3 r, vec3 f) {
  dBodyAddForceAtPos(b, f.x, f.y, f.z, r.x, r.y, r.z);
}

/*! @brief return the force applied to a body.
  @param b 'dBodyID' object
*/
vec3 ForceBodyODE(dBodyID b) {
  const dReal *f = dBodyGetForce(b);
  return vec3(f[0], f[1], f[2]);
}

/*! @brief add 'vec3' torque t to dBodyID 'b'.
  @param b 'dBodyID' object
  @param t torque to add
*/
void addTorqueBodyODE(dBodyID b, vec3 t) { dBodyAddTorque(b, t.x, t.y, t.z); }

/*! @brief return the torque applied to a body.
  @param b 'dBodyID' object
*/
vec3 TorqueBodyODE(dBodyID b) {
  const dReal *c = dBodyGetTorque(b);
  return vec3(c[0], c[1], c[2]);
}

/*! @brief rotation of 'vec3 b' around 'vec3 a' and angle 'r'.
  @param a axe
  @param r angle
  @param b 3d vector to rotate
*/
vec3 rotation(vec3 a, double r, vec3 b) {
  double c = cos(r);
  double s = sin(r);
  if (a.x == b.x && a.y == b.y && a.z == b.z)
    return b;
  else
    return add3(add3(mult3(c, b), mult3((1. - c) * dot_product(b, a), a)),
                mult3(s, cross_product(a, b)));
  // return
  // mult3(n,normalize3(add3(add3(mult3(c,b),mult3((1.-c)*dot_product(b,a),a)),mult3(s,cross_product(a,b)))));
}

/*! @brief Reset center-of-mass.
  @param bodies list of dBodyID
  @param B number of dBodyID
*/
void reset_com(const dBodyID *bodies, int B) {
  vec3 com = vec3(.0, .0, .0);
  for (int i = 0; i < B; i++)
    com = add3(com, PositionBodyODE(bodies[i]));
  com = mult3(1. / (double)B, com);
  for (int i = 0; i < B; i++)
    set_position(bodies[i], sub3(PositionBodyODE(bodies[i]), com));
}

/*! @brief rotation of the frames of a list of dBodyID.
  @param a Rotation axis.
  @param r Rotation angle.
  @param bodies Rotation of the frame of each of the body.
  @param start From body ...
  @param end ... to body.
  @param p0 Set center-of-mass at p0.
*/
void rotations(vec3 a, double r, const dBodyID *bodies, int start, int end,
               vec3 p0) {
  double c = cos(r);
  double s = sin(r);
  vec3 p, u, v;
  for (int i = start; i <= end; i++) {
    u = FrameODE(bodies[i], 0);
    u = add3(add3(mult3(c, u), mult3((1. - c) * dot_product(u, a), a)),
             mult3(s, cross_product(a, u)));
    v = FrameODE(bodies[i], 1);
    v = add3(add3(mult3(c, v), mult3((1. - c) * dot_product(v, a), a)),
             mult3(s, cross_product(a, v)));
    set_axes(bodies[i], cross_product(u, v), u, v);
    p = sub3(PositionBodyODE(bodies[i]), p0);
    set_position(
        bodies[i],
        add3(p0, add3(add3(mult3(c, p), mult3((1. - c) * dot_product(p, a), a)),
                      mult3(s, cross_product(a, p)))));
  }
}

/*! @brief intrinsic twist angle along the chromatin fibre.
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
double *tw_e_chromatin_fibre(char *chromatin) {
  // read the structure of the chromatin from input file
  int F = 0, nucleosomes = 0, retour = 0, s0, s1, dock0, dock1, h1, chain;
  float f0;
  FILE *chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosomes += 1;
      F += 14;
    } else
      F++;
  }
  fclose(chromatin_in);
  // bp per monomer
  float *lbp = new float[F]();
  F = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      for (int i = 0; i < 14; i++)
        lbp[F + i] = 10.5;
      F += 14;
    } else {
      lbp[F] = f0;
      F++;
    }
  }
  fclose(chromatin_in);
  // calculate the intrinsic twist angle between monomer "f-1" and monomer "f"
  double *twe = new double[F - 1]();
  for (int f = 1; f < F; f++) {
    twe[f - 1] = sign_phase *
                 ((2. * pi / 10.5) * fmod(.5 * (lbp[f] + lbp[f - 1]), 10.5));
    // local twist is in [-pi,pi]
    if (twe[f - 1] > pi)
      twe[f - 1] -= 2. * pi;
    if (twe[f - 1] < (-pi))
      twe[f - 1] += 2. * pi;
  }
  delete[] lbp;
  lbp = NULL;
  return twe;
}

/*! @brief check if linker segmentation is greater than 'BP_limit' argument.
  if yes, create a new file such that linker segmentation is not greater than
  'BP_limit'. copy the old one to the backup folder.
  @param fname name of the chromatin fiber input file
  @param BP_limit max linker segmentation in bp
 */
void check_chromatin_input_file(const char *fname, double BP_limit) {
  int i0, i1, i2, i3, i4, segmentation = 1, chain;
  float f0;
  double d0;
  FILE *fin = NULL, *fout = NULL;
  char bname[1000];
  // copy to backup folder
  sprintf(bname, "backup/%s", fname);
  fin = fopen(fname, "r");
  if (fin != NULL) {
    fout = fopen(bname, "w");
    while (!feof(fin)) {
      fscanf(fin, "%i %f %i %i %i %i %i\n", &chain, &f0, &i0, &i1, &i2, &i3,
             &i4);
      fprintf(fout, "%i %f %i %i %i %i %i\n", chain, f0, i0, i1, i2, i3, i4);
    }
    fclose(fin);
    fclose(fout);
  }
  // check if linker segmentation is greater than 'BP_limit' argument
  fin = fopen(bname, "r");
  if (fin != NULL) {
    fout = fopen(fname, "w");
    while (!feof(fin)) {
      fscanf(fin, "%i %f %i %i %i %i %i\n", &chain, &f0, &i0, &i1, &i2, &i3,
             &i4);
      // do not consider 0 bp linker
      if (f0 == .0)
        continue;
      if (f0 == 147.)
        fprintf(fout, "%i %f %i %i %i %i %i\n", chain, f0, i0, i1, i2, i3, i4);
      else {
        if (f0 > BP_limit) {
          segmentation = 1;
          while ((f0 / (double)segmentation) > BP_limit)
            segmentation++;
          d0 = f0 / (double)segmentation;
          for (int i = 0; i < segmentation; i++)
            fprintf(fout, "%i %f %i %i %i %i %i\n", chain, d0, i0, i1, i2, i3,
                    i4);
        } else
          fprintf(fout, "%i %f %i %i %i %i %i\n", chain, f0, i0, i1, i2, i3,
                  i4);
      }
    }
    fclose(fin);
    fclose(fout);
  }
}

/*! @brief write a file that describe a random chromatin fibre, nrls are drawn
  from uniform distribution between 'nrl' and 'NRL'
  @param nucleosomes number of nucleosomes to add
  @param NUCLEOSOMES number of nucleosomes (target)
  @param nrl 147 (only if nucleosomes>0) + size of the linker in bp (min)
  @param NRL 147 (only if nucleosomes>0) + size of the linker in bp (max)
  @param bp_limit linker (only if nucleosomes>0) or DNA (only if nucleosomes=0)
  segmentation lower limit
  @param BP_limit linker (only if nucleosomes>0) or DNA (only if nucleosomes=0)
  segmentation upper limit
  @param break_shl number of shls to break (|...|<= 7 because we have 7 shls on
  both side, negative means random)
  @param break_dock number of docking domains to break (-1, 0 or 1, negative
  means random)
  @param symmetric_break if true break the same number of shls on both side
  (work only if 'break_shl'<0 and 'break_dock'<0)
  @param fname name of the file where to write the chromatin fibre
  @param mt random number from std::mt19937_64
  @param what_to_do write a new file "w" or append to an existing file "a"
  @param prob_h1 probability to add H1
  @param prob_no_nucleosome probability of no nucleosome (add a DNA linker
  instead of a nucleosome) if 'prob_no_nucleosome' is an negative integer then
  no nucleosome every this number
  @param prob_hexasome probability to add an hexasome instead of a nucleosome
         if 'prob_hexasome' is an negative integer then hexasome every this
  number
  @param prob_tetrasome probability to add a tetrasome instead of a nucleosome
         if 'prob_tetrasome' is an negative integer then tetrasome every this
  number
 */
void create_random_fibre(int nucleosomes, int NUCLEOSOMES, int nrl, int NRL,
                         double bp_limit, double BP_limit, int break_shl,
                         int break_dock, bool symmetric_break, char *fname,
                         std::mt19937_64 &mt, const char *what_to_do,
                         double prob_h1, double prob_no_nucleosome,
                         double prob_hexasome, double prob_tetrasome) {
  std::uniform_real_distribution<> ud(.0,
                                      (double)max(0, min(1, abs(break_dock))));
  std::uniform_int_distribution<> ul(nrl, NRL);
  std::uniform_int_distribution<> us(0, min(abs(break_shl), 7));
  std::uniform_real_distribution<> u01(.0, 1.);
  int linker, segmentation, i0, i1, i2, i3, i4, i5, dock0, dock1, nn = 0, chain;
  float f0;
  double bp_per_monomer;
  FILE *fout = NULL;
  // count number of nucleosomes in the file 'fname'
  if (strcmp(what_to_do, "a") == 0) {
    fout = fopen(fname, "r");
    while (!feof(fout)) {
      fscanf(fout, "%i %f %i %i %i %i %i\n", &i5, &f0, &i0, &i1, &i2, &i3, &i4);
      if ((int)f0 == 147)
        nn++;
    }
    fclose(fout);
  }
  fout = fopen(fname, what_to_do);
  // first linker (in bp)
  if (strcmp(what_to_do, "w") == 0) {
    if (nucleosomes > 0) {
      linker = ul(mt) - 147;
      if (bp_limit == BP_limit) {
        bp_per_monomer = bp_limit;
        if (fmod(linker, bp_limit) != .0) {
          printf("linker length in bp is not a multiple of 'bp_limit' "
                 "argument, exit.\n");
          exit(EXIT_FAILURE);
        }
        segmentation = linker / bp_limit;
      } else {
        segmentation = 1;
        // do not consider linker monomer segmentation<bp_limit
        // do not consider linker monomer segmentation>BP_limit
        while (((double)linker / (double)segmentation) < bp_limit ||
               ((double)linker / (double)segmentation) > BP_limit)
          segmentation++;
        bp_per_monomer = (double)linker / (double)segmentation;
      }
      if (linker == 0)
        segmentation = 0;
    } else {
      bp_per_monomer = bp_limit;
      segmentation = (int)((double)nrl / bp_per_monomer);
    }
    for (int l = 0; l < segmentation; l++)
      fprintf(fout, "%i %f %i %i %i %i %i\n", 0, bp_per_monomer, -1, -1, 0, 0,
              0);
  }
  // loop over the nucleosomes
  for (int n = 0; n < nucleosomes; n++) {
    // new nucleosome
    if (break_dock < 0) {
      // randomly break docking domains
      if (symmetric_break)
        dock0 = dock1 = (int)(ud(mt) > .5);
      else {
        dock0 = (int)(ud(mt) > .5);
        dock1 = (int)(ud(mt) > .5);
      }
    } else
      dock0 = dock1 = break_dock;
    if (break_shl < 0) {
      // randomly break SHLs
      if (symmetric_break)
        i0 = i1 = us(mt);
      else {
        i0 = us(mt);
        i1 = us(mt);
      }
    } else
      i0 = i1 = min(break_shl, 7);
    // hexasome instead of a nucleosome ?
    if (prob_hexasome > .0) {
      if (u01(mt) < prob_hexasome) {
        if (u01(mt) < .5) {
          i0 = 4;
          i1 = 0;
        } else {
          i0 = 0;
          i1 = 4;
        }
      }
    }
    // tetrasome instead of a nucleosome ?
    if (prob_tetrasome > .0) {
      if (u01(mt) < prob_tetrasome) {
        i0 = i1 = 4;
      }
    }
    // no nucleosome ?
    if (prob_no_nucleosome > .0) {
      if (u01(mt) < prob_no_nucleosome)
        i0 = i1 = 7;
    }
    if (prob_hexasome < .0 && ((nn + n) % (int)(-prob_hexasome)) == 0 &&
        (nn + n) > 0 && (nn + n) < (NUCLEOSOMES - 1)) {
      // hexasome at the center of the fiber
      i0 = 4;
      i1 = 0;
    }
    if (prob_tetrasome < .0 && ((nn + n) % (int)(-prob_tetrasome)) == 0 &&
        (nn + n) > 0 && (nn + n) < (NUCLEOSOMES - 1)) {
      // tetrasome at the center of the fiber
      i0 = i1 = 4;
    }
    // printf("%i %i\n",(int)(-prob_no_nucleosome),(nn+n));
    if (prob_no_nucleosome < .0 &&
        ((nn + n) % (int)(-prob_no_nucleosome)) == 0 && (nn + n) > 0 &&
        (nn + n) < (NUCLEOSOMES - 1)) {
      // no nucleosome at the center of the fiber
      i0 = i1 = 7;
    }
    fprintf(fout, "%i %f %i %i %i %i %i\n", 0, 147., i0, i1, dock0, dock1,
            (int)(u01(mt) < prob_h1));
    // new linker(s)
    linker = ul(mt) - 147;
    // linker=0;
    // // while(1){
    // for(int l=0;l<2;l++){
    //   linker+=ul(mt)-147;
    //   if(prob_no_nucleosome<=.0) break;
    //   else if(u01(mt)>prob_no_nucleosome) break;
    // }
    if (bp_limit == BP_limit) {
      bp_per_monomer = bp_limit;
      if (fmod(linker, bp_limit) != .0) {
        printf("linker length in bp is not a multiple of 'bp_limit' argument, "
               "exit.\n");
        exit(EXIT_FAILURE);
      }
      segmentation = linker / bp_limit;
    } else {
      segmentation = 1;
      // do not consider linker monomer segmentation<bp_limit
      // do not consider linker monomer segmentation>BP_limit
      while (((double)linker / (double)segmentation) < bp_limit ||
             ((double)linker / (double)segmentation) > BP_limit)
        segmentation++;
      bp_per_monomer = (double)linker / (double)segmentation;
    }
    if (linker == 0)
      segmentation = 0;
    // // choose linker monomer (in bp) such as is closed to bp_limit
    // if(fabs((double)linker/(double)segmentation-bp_limit)<fabs(bp_per_monomer-bp_limit)){
    //   segmentation--;
    //   bp_per_monomer=(double)linker/(double)segmentation;
    // }
    for (int l = 0; l < segmentation; l++)
      fprintf(fout, "%i %f %i %i %i %i %i\n", 0,
              (double)linker / (double)segmentation, -1, -1, 0, 0, 0);
  }
  fclose(fout);
}

/*! @brief write a file that describe a chromatin fibre (use nrl density from
  data file)
  @param nucleosomes number of nucleosomes
  @param ndata file name where to find nrl>=147 density (1 bp bin)
         header for nrl distribution density file (two columns file) has to be
  'nrl p(nrl)' nrl is integer and p(nrl) is double
  @param break_shl number of shls to break (|...|<= 7 because we have 7 shls on
  both side, negative means random)
  @param break_dock number of docking domains to break (-1, 0 or 1, negative
  means random)
  @param symmetric_break if true break the same number of shls on both side
  (work only if 'break_shl'<0 and 'break_dock'<0)
  @param prob_h1 probability to add H1
  @param fname name of the file where to write the chromatin fibre
  @param mt random number from std::mt19937_64
  @param what_to_do write a new file "w" or append to an existing file "a"
  @param gmean add normal distribution (gmean>147)
  @param gstd normal distribution standard deviation
  @param prob_no_nucleosome probability of no nucleosome
  draw a random number, if it is less than 'prob_no_nucleosome' add a new linker
  instead of a nucleosome
 */
void create_fibre_from_data(int nucleosomes, const char *ndata, int break_shl,
                            int break_dock, bool symmetric_break,
                            double prob_h1, char *fname, std::mt19937_64 &mt,
                            const char *what_to_do, double gmean, double gstd,
                            double prob_no_nucleosome) {
  std::uniform_real_distribution<> ud01(.0, 1.);
  std::uniform_real_distribution<> ud(0.,
                                      (double)max(0, min(1, abs(break_dock))));
  std::uniform_int_distribution<> us(0, min(abs(break_shl), 7));
  std::normal_distribution<> nd(gmean, gstd);
  std::uniform_real_distribution<> uh1(.0, 1.);
  int linker, linker1, linker2, segmentation, i0, i1, dock0, dock1, nbins = 0,
                                                                    nb = 0;
  double bp_per_monomer, nentries = .0;
  // read nrl distribution density
  char s0[4], s1[7];
  float f0;
  FILE *fin = fopen(ndata, "r");
  fscanf(fin, "%s %s\n", s0, s1);
  if (strcmp(s0, "nrl") != 0 || strcmp(s1, "p(nrl)") != 0) {
    printf("header for nrl distribution density file has to be 'nrl p(nrl)', "
           "exit.\n");
    fclose(fin);
    return;
  }
  while (!feof(fin)) {
    fscanf(fin, "%i %f\n", &i0, &f0);
    nbins = max(nbins, i0 - 147 + 1);
  }
  fclose(fin);
  if (nbins == 0) {
    printf("number of bins is zero, exit\n");
    return;
  }
  if (gmean > 147. && gstd > .0)
    nbins = max(nbins, (int)(gmean + 10. * gstd) - 147 + 1);
  // read nrl density
  int *bins = new int[nbins]();
  double *dbins = new double[nbins]();
  double *gbins = new double[nbins]();
  fin = fopen(ndata, "r");
  fscanf(fin, "%s %s\n", s0, s1);
  while (!feof(fin)) {
    fscanf(fin, "%i %f\n", &i0, &f0);
    bins[nb] = i0;
    dbins[nb] = f0;
    nentries += f0;
    nb++;
  }
  fclose(fin);
  for (int b = 1; b < nbins; b++)
    if ((bins[b] - bins[b - 1]) > 1)
      return;
  for (int b = 0; b < nbins; b++)
    bins[b] = 147 + b;
  // check first nrl value
  if (bins[0] < 147) {
    printf("nrl value is < 147, exit\n");
    return;
  }
  // normalize
  for (int b = 0; b < nbins; b++)
    dbins[b] /= nentries;
  // add normal distribution to the nrl density
  if (gmean > 147. && gstd > .0) {
    for (int n = 0; n < 10000; n++) {
      nb = (int)floor(nd(mt)) - 147;
      while (nb <= 0 || nb >= nbins)
        nb = (int)floor(nd(mt)) - 147;
      gbins[nb]++;
    }
    nentries = .0;
    for (int b = 0; b < nbins; b++)
      nentries += gbins[b];
    for (int b = 0; b < nbins; b++)
      gbins[b] /= nentries;
    for (int b = 0; b < nbins; b++)
      dbins[b] += gbins[b];
  }
  // for(int b=0;b<nbins;b++) printf("%i/%i %f %f\n",b,nbins,dbins[b],gbins[b]);
  // write chromatin fiber
  std::uniform_int_distribution<> ul(0, nbins - 1);
  FILE *fout = fopen(fname, what_to_do);
  if (strcmp(what_to_do, "w") == 0) {
    // first linker in bp (only if new file)
    // indeed, existing file already has first/last linker
    linker1 = ul(mt);
    linker2 = ul(mt);
    while (dbins[linker1] == .0 && dbins[linker2] == .0) {
      linker1 = ul(mt);
      linker2 = ul(mt);
    }
    linker = (dbins[linker1] < dbins[linker2]) ? bins[linker2] : bins[linker1];
    linker -= 147;
    segmentation = 1;
    // do not consider linker monomer with more than 11 bp
    while (((double)linker / segmentation) > 11.)
      segmentation++;
    bp_per_monomer = (double)linker / segmentation;
    // // choose linker monomer (in bp) such as it is closed to 11 bp
    // if(fabs((double)linker/(segmentation-1)-11.)<fabs(bp_per_monomer-11.)){
    //   segmentation--;
    //   bp_per_monomer=(double)linker/segmentation;
    // }
    for (int l = 0; l < segmentation; l++)
      fprintf(fout, "%i %f %i %i %i %i %i\n", 0, bp_per_monomer, -1, -1, 0, 0,
              0);
  }
  // loop over the nucleosomes
  for (int n = 0; n < nucleosomes; n++) {
    // new nucleosome
    if (break_dock < 0) {
      // randomly break docking domains
      if (symmetric_break)
        dock0 = dock1 = (int)(ud(mt) > .5);
      else {
        dock0 = (int)(ud(mt) > .5);
        dock1 = (int)(ud(mt) > .5);
      }
    } else
      dock0 = dock1 = break_dock;
    if (break_shl < 0) {
      // randomly break SHLs
      if (symmetric_break)
        i0 = i1 = us(mt);
      else {
        i0 = us(mt);
        i1 = us(mt);
      }
    } else
      i0 = i1 = min(break_shl, 7);
    fprintf(fout, "%i %f %i %i %i %i %i\n", 0, 147., i0, i1, dock0, dock1,
            (int)(uh1(mt) < prob_h1));
    // new linker
    linker = 0;
    while (1) {
      linker1 = ul(mt);
      linker2 = ul(mt);
      while (dbins[linker1] == .0 && dbins[linker2] == .0) {
        linker1 = ul(mt);
        linker2 = ul(mt);
      }
      linker +=
          (dbins[linker1] < dbins[linker2]) ? bins[linker2] : bins[linker1];
      linker -= 147;
      if (prob_no_nucleosome <= .0)
        break;
      else if (ud01(mt) > prob_no_nucleosome)
        break;
    }
    segmentation = 1;
    // do not consider linker monomer with more than 11 bp
    while (((double)linker / segmentation) > 11.)
      segmentation++;
    bp_per_monomer = (double)linker / segmentation;
    // // choose linker monomer (in bp) such as is closed to 11 bp
    // if(fabs((double)linker/(segmentation-1)-11.)<fabs(bp_per_monomer-11.)){
    //   segmentation--;
    //   bp_per_monomer=(double)linker/segmentation;
    // }
    for (int l = 0; l < segmentation; l++)
      fprintf(fout, "%i %f %i %i %i %i %i\n", 0, (double)linker / segmentation,
              -1, -1, 0, 0, 0);
  }
  fclose(fout);
  delete[] bins;
  bins = NULL;
  delete[] dbins;
  dbins = NULL;
  delete[] gbins;
  gbins = NULL;
}

/*! @brief create Yeast genome
  @param dna array of dBodyID
  @param gdna array of dGeomID
  @param ggdna array of ghost dGeomID
  @param ddna
  @param jdna array of dJointID
 */
int create_yeast(dBodyID **dna, dGeomID **gdna, dGeomID **ggdna, int3p *&ddna,
                 dJointID **jdna, dWorldID world, dSpaceID space,
                 dSpaceID gspace, int **gf, double **lf, double **rf,
                 double **mf, double xghost, int gyroscopic, int &dofs,
                 int *&segments, int *&centromeres, int *&chrC, int *&chrT,
                 int *&Lchr, int *&Gchr, std::mt19937_64 &mt) {
  std::uniform_real_distribution<> ud(.0, 1.);
  int nchr = 16, N = 0, nb = 0, nc, nj, O = 2, xrdna = 1;
  int ordre_chr[16];
  double R = 1100. * nm / yL, rdna = 75. * nm / yL, zz, ZZ;
  vec3 rn, com = vec3(), ti, ui, vi, bn;
  delete[] segments;
  segments = new int[nchr]();
  delete[] centromeres;
  centromeres = new int[nchr]();
  delete[] Lchr;
  Lchr = new int[nchr]();
  delete[] Gchr;
  Gchr = new int[nchr]();
  delete[] chrC;
  chrC = new int[nchr]();
  delete[] chrT;
  chrT = new int[2 * nchr]();
  vec3 *rc = new vec3[nchr]();
  for (int n = 0; n < nchr; n++) {
    ordre_chr[n] = n;
    Lchr[n] = 0.;
    Gchr[n] = 0;
    if (n == 0)
      segments[n] = 46;
    if (n == 1)
      segments[n] = 163;
    if (n == 2)
      segments[n] = 63;
    if (n == 3)
      segments[n] = 306;
    if (n == 4)
      segments[n] = 115;
    if (n == 5)
      segments[n] = 54;
    if (n == 6)
      segments[n] = 218;
    if (n == 7)
      segments[n] = 113;
    if (n == 8)
      segments[n] = 88;
    if (n == 9)
      segments[n] = 149;
    if (n == 10)
      segments[n] = 133;
    // 5000 * N + x * 150 * 1000 = G(chr 12) 1078177 bp
    if (n == 11)
      segments[n] = 186 + xrdna * 150;
    if (n == 12)
      segments[n] = 185;
    if (n == 13)
      segments[n] = 157;
    if (n == 14)
      segments[n] = 218;
    if (n == 15)
      segments[n] = 190;
    if (n == 0)
      centromeres[n] = 29;
    if (n == 1)
      centromeres[n] = 47;
    if (n == 2)
      centromeres[n] = 21;
    if (n == 3)
      centromeres[n] = 89;
    if (n == 4)
      centromeres[n] = 29;
    if (n == 5)
      centromeres[n] = 29;
    if (n == 6)
      centromeres[n] = 99;
    if (n == 7)
      centromeres[n] = 20;
    if (n == 8)
      centromeres[n] = 70;
    if (n == 9)
      centromeres[n] = 87;
    if (n == 10)
      centromeres[n] = 87;
    if (n == 11)
      centromeres[n] = 29;
    if (n == 12)
      centromeres[n] = 52;
    if (n == 13)
      centromeres[n] = 125;
    if (n == 14)
      centromeres[n] = 64;
    if (n == 15)
      centromeres[n] = 110;
    N += segments[n];
  }
  // telomeres
  nb = 0;
  for (int n = 0; n < nchr; n++) {
    for (int i = 0; i < segments[n]; i++) {
      if (i == 0)
        chrT[2 * n] = nb;
      if (i == (segments[n] - 1))
        chrT[2 * n + 1] = nb;
      nb++;
    }
  }
  // centromeres random order
  for (int i = 0; i < nchr; i++)
    ordre_chr[i] = i;
  for (int i = 0; i < 1000; i++) {
    nb = (int)(nchr * ud(mt));
    nc = (int)(nchr * ud(mt));
    swap(ordre_chr[nb], ordre_chr[nc]);
  }
  // centromeres positions
  nb = 0;
  for (int n = 0; n < nchr; n++) {
    rc[n] = vec3(R * cos(2. * pi * ordre_chr[n] / nchr),
                 R * sin(2. * pi * ordre_chr[n] / nchr), R);
    for (int i = 0; i < segments[n]; i++) {
      if (i == centromeres[n])
        chrC[n] = nb;
      nb++;
    }
  }
  // create bodies, geoms and joints
  nb = nj = dofs = 0;
  (*dna) = new dBodyID[N]();
  (*gdna) = new dGeomID[N]();
  (*ggdna) = new dGeomID[N]();
  (*jdna) = new dJointID[N]();
  (*gf) = new int[N]();
  (*rf) = new double[N]();
  (*lf) = new double[N]();
  (*mf) = new double[N]();
  ddna = new int3p[N]();
  for (int n = 0; n < nchr; n++) {
    for (int i = 0; i < segments[n]; i++) {
      // bodies and geoms
      if (n == 11 && i >= 90 && i < (90 + xrdna * 150)) {
        // rdna
        (*gf)[nb] = 5000; // 1000;
        (*rf)[nb] = xrdna * rdna;
        (*lf)[nb] = 2. * (*rf)[nb];
        (*mf)[nb] = xmass * (*gf)[nb] * m1bp / yM;
        (*dna)[nb] = b_sphere(world, space, (*gdna)[nb], (*rf)[nb], (*mf)[nb]);
        (*ggdna)[nb] =
            create_ghost_geometry(gspace, (*dna)[nb], "sphere", "sphere",
                                  xghost, (*rf)[nb], (*rf)[nb], (*rf)[nb]);
      } else {
        // dna
        (*gf)[nb] = 5000;
        (*rf)[nb] = 10. * nm / yL;
        (*lf)[nb] = 60. * nm / yL;
        (*mf)[nb] = xmass * (*gf)[nb] * m1bp / yM;
        (*dna)[nb] = b_capsule(world, space, (*gdna)[nb], (*rf)[nb], (*lf)[nb],
                               (*mf)[nb], gyroscopic);
        (*ggdna)[nb] =
            create_ghost_geometry(gspace, (*dna)[nb], "capsule", "sphere",
                                  xghost, (*rf)[nb], (*rf)[nb], (*lf)[nb]);
      }
      ddna[nb] = int3p(0, nb, n);
      Lchr[n] += (*lf)[nb];
      Gchr[n] += (*gf)[nb];
      O = 4;
      if (i == 0) {
        // first bond (left) towards z-direction
        set_axes((*dna)[nb], vec3(.0, .0, 1.), vec3(1., .0, .0),
                 vec3(.0, 1., .0));
        set_position((*dna)[nb], vec3());
      } else {
        // left towards z-direction
        if (nb < (chrC[n] - (O - 1)))
          set_axes((*dna)[nb], vec3(.0, .0, 1.), vec3(1., .0, .0),
                   vec3(.0, 1., .0));
        else {
          // centromere lies in the xOy plan
          // right towards -z-direction
          if (abs(nb - chrC[n]) < O)
            set_axes((*dna)[nb], vec3(.0, 1., .0), vec3(1., .0, .0),
                     vec3(.0, .0, -1.));
          else
            set_axes((*dna)[nb], vec3(.0, .0, -1.), vec3(1., .0, .0),
                     vec3(.0, -1., .0));
        }
        set_position((*dna)[nb],
                     add3(PositionPosRelBodyODE((*dna)[nb - 1], 0., 0.,
                                                .5 * (*lf)[nb - 1]),
                          mult3(.5 * (*lf)[nb], FrameODE((*dna)[nb], 2))));
      }
      // dofs
      dofs += 6;
      // create joints
      if (i > 0) {
        (*jdna)[nj] = ball_joint(
            world, (*dna)[nb - 1], (*dna)[nb],
            PositionPosRelBodyODE((*dna)[nb - 1], .0, .0, .5 * (*lf)[nb - 1]));
        nj++;
        dofs -= 3;
      }
      nb++;
    }
  }
  // no longer a straight line
  // if(abs(nb-chrC[n])<O)
  // set_axes((*dna)[nb],vec3(.0,1.,.0),vec3(1.,.0,.0),vec3(.0,.0,-1.));
  double A1 = .25 * pi, A2 = asin(1.) / (2. * pi / A1 - 1.);
  for (int n = 0; n < nchr; n++) {
    for (int i = (chrC[n] + O); i <= chrT[2 * n + 1]; i++) {
      // A2=asin(2.*(*rf)[i]/(*lf)[i])/(2.*pi/A1-1.);
      ui = rotation(vec3(.0, .0, -1.), A1 * (i - (chrC[n] + 2) + 1),
                    FrameODE((*dna)[chrC[n]], 0));
      ti = rotation(vec3(.0, .0, -1.), A1 * (i - (chrC[n] + 2) + 1),
                    FrameODE((*dna)[chrC[n]], 2));
      bn = normalize3(cross_product(ti, vec3(.0, .0, -1.)));
      ui = rotation(bn, A2, ui);
      ti = rotation(bn, A2, ti);
      if (dot_product(ti, vec3(.0, .0, -1.)) < .0)
        return -1;
      // printf("%f\n",dot_product(ti,vec3(.0,.0,-1.)));
      set_axes((*dna)[i], ti, ui, normalize3(cross_product(ti, ui)));
      set_position((*dna)[i], add3(PositionPosRelBodyODE((*dna)[i - 1], 0., 0.,
                                                         .5 * (*lf)[i - 1]),
                                   mult3(.5 * (*lf)[i], ti)));
    }
    for (int i = (chrC[n] - O); i >= chrT[2 * n]; i--) {
      // A2=asin(2.*(*rf)[i]/(*lf)[i])/(2.*pi/A1-1.);
      ui = rotation(vec3(.0, .0, 1.), A1 * ((chrC[n] - 2) - i + 1),
                    FrameODE((*dna)[chrC[n]], 0));
      ti = rotation(vec3(.0, .0, 1.), A1 * ((chrC[n] - 2) - i + 1),
                    FrameODE((*dna)[chrC[n]], 2));
      bn = normalize3(cross_product(ti, vec3(.0, .0, 1.)));
      ui = rotation(bn, A2, ui);
      ti = rotation(bn, A2, ti);
      if (dot_product(ti, vec3(.0, .0, 1.)) < .0)
        return -1;
      // printf("%f\n",dot_product(ti,vec3(.0,.0,1.)));
      set_axes((*dna)[i], ti, ui, normalize3(cross_product(ti, ui)));
      set_position((*dna)[i], sub3(PositionPosRelBodyODE((*dna)[i + 1], 0., 0.,
                                                         -.5 * (*lf)[i + 1]),
                                   mult3(.5 * (*lf)[i], ti)));
    }
  }
  // check connectivity
  nb = 0;
  for (int n = 0; n < nchr; n++) {
    for (int i = 0; i < segments[n]; i++) {
      if (i > 0) {
        if (norm3(sub3(PositionPosRelBodyODE((*dna)[nb - 1], 0., 0.,
                                             .5 * (*lf)[nb - 1]),
                       PositionPosRelBodyODE((*dna)[nb], 0., 0.,
                                             -.5 * (*lf)[nb]))) > 1e-3) {
          printf("connectivity warning\n");
          return -1;
        }
      }
      nb++;
    }
  }
  // moves centromeres
  nb = 0;
  for (int n = 0; n < nchr; n++) {
    rn = sub3(rc[n], PositionBodyODE((*dna)[chrC[n]]));
    for (int i = 0; i < segments[n]; i++) {
      set_position((*dna)[nb], add3(PositionBodyODE((*dna)[nb]), rn));
      nb++;
    }
  }
  // remove com
  for (int n = 0; n < N; n++)
    com = add3(com, PositionBodyODE((*dna)[n]));
  com = mult3(1. / N, com);
  for (int n = 0; n < N; n++)
    set_position((*dna)[n], sub3(PositionBodyODE((*dna)[n]), com));
  //
  if (0) {
    zz = DBL_MAX;
    ZZ = .0;
    for (int n = 0; n < N; n++) {
      rn = PositionBodyODE((*dna)[n]);
      zz = fmin(zz, rn.z);
      ZZ = fmax(ZZ, rn.z);
    }
    for (int n = 0; n < N; n++)
      set_position((*dna)[n], sub3(PositionBodyODE((*dna)[n]),
                                   vec3(.0, .0, .5 * (zz + ZZ))));
  }
  // set data
  for (int n = 0; n < N; n++)
    dBodySetData((*dna)[n], &ddna[n]);
  // set velocities to zero
  for (int n = 0; n < N; n++)
    set_velocities((*dna)[n], .0, .0, .0, .0, .0, .0);
  return N;
}

/*! @brief compute constraint matrix J of the nucleosome.
  compute its rank and return the number of degres of freedom of the object.
  @param simplified_nucleosome 0 for full description of the histones core
*/
long int compute_rank_of_J_nucleosome(int simplified_nucleosome) {
  vec3 u1, v1, t1, u2, v2, t2, p0, bn;
  int s0, s1, dock0, dock1, i0, i1;
  double theta;
  // read the structure of the nucleosome from input files
  vector<vec3> nucl_pos;
  nucl_pos = read_dna_nucl_pos("nucleosome_in/nposition.in", fL);
  vector<vec3> hist_pos;
  hist_pos = read_hist_pos("nucleosome_in/hposition.in", fL);
  vector<vec3> hist_joints;
  hist_joints = read_hist_joint_pos("nucleosome_in/hist_joints.in", fL);
  vector<vector<vec3>> nucl_rot(NNC, vector<vec3>(3));
  nucl_rot = read_dna_rot("nucleosome_in/nrotation.in");
  vector<vector<vec3>> hist_rot(NHC, vector<vec3>(3));
  hist_rot = read_hist_rot("nucleosome_in/hrotation.in");
  vector<vec3> SHLs_pos(nSHLs);
  SHLs_pos = read_shls_pos("nucleosome_in/SHLs.in", fL);
  vector<vec3> dockings_pos(nDOCKs);
  dockings_pos = read_dockings_pos("nucleosome_in/dockings.in", fL);
  vector<vector<int>> SHLs_bodies(nSHLs, vector<int>(2));
  SHLs_bodies = read_shls_bodies("nucleosome_in/SHLs.in");
  vector<vector<int>> dockings_bodies(nDOCKs, vector<int>(2));
  dockings_bodies = read_dockings_bodies("nucleosome_in/dockings.in");
  // nucleosome
  s0 = s1 = dock0 = dock1 = 0;
  mat33 skew33;
  int nrows = (simplified_nucleosome == 1)
                  ? 3 * (14 - 1) + 3 * (NHC - 1) + 3 * (nSHLs - 4) + 3 * nDOCKs
                  : 3 * (14 - 1) + 3 * (NHC - 1) + 3 * nSHLs + 3 * nDOCKs;
  MatrixXd Jij(nrows, 6 * (NNC + NHC));
  int nconstraints = 0;
  // create the nucleosomal dna
  for (int c = 0; c < (NNC - 1); c++) {
    // third entry is the nucleosome index (start from 1)
    t2 = nucl_rot[c][2];
    u2 = nucl_rot[c][0];
    v2 = nucl_rot[c][1];
    // do not break shls: do not add nucleosomal DNA twist
    if (0 && c >= s0 && c <= (NNC - 1 - s1)) {
      t1 = nucl_rot[c - 1][2];
      u1 = nucl_rot[c - 1][0];
      v1 = nucl_rot[c - 1][1];
      bn = normalize3(cross_product(t1, t2));
      theta = arccos(dot_product(t1, t2));
      u2 = rotation(bn, theta, u1);
      v2 = rotation(bn, theta, v1);
    }
    // body
    Jij(3 * nconstraints + 0, 6 * c + 0) = 1.;
    Jij(3 * nconstraints + 1, 6 * c + 1) = 1.;
    Jij(3 * nconstraints + 2, 6 * c + 2) = 1.;
    skew33 = skew_from_vec3(mult3(.5 * 3.5721 * nm / fL, nucl_rot[c][2]));
    Jij(3 * nconstraints + 0, 6 * c + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * c + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * c + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * c + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * c + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * c + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * c + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * c + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * c + 5) = skew33.c3.z;
    // next body
    Jij(3 * nconstraints + 0, 6 * (c + 1) + 0) = -1.;
    Jij(3 * nconstraints + 1, 6 * (c + 1) + 1) = -1.;
    Jij(3 * nconstraints + 2, 6 * (c + 1) + 2) = -1.;
    skew33 = skew_from_vec3(mult3(-.5 * 3.5721 * nm / fL, nucl_rot[c + 1][2]));
    Jij(3 * nconstraints + 0, 6 * (c + 1) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (c + 1) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (c + 1) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (c + 1) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (c + 1) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (c + 1) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (c + 1) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (c + 1) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (c + 1) + 5) = skew33.c3.z;
    nconstraints++;
  }
  // create the histones ball joints
  for (int h = 0; h < (NHC - 1); h++) {
    // histone
    Jij(3 * nconstraints + 0, 6 * (NNC + h) + 0) = 1.;
    Jij(3 * nconstraints + 1, 6 * (NNC + h) + 1) = 1.;
    Jij(3 * nconstraints + 2, 6 * (NNC + h) + 2) = 1.;
    skew33 = skew_from_vec3(sub3(hist_joints[h], hist_pos[h]));
    Jij(3 * nconstraints + 0, 6 * (NNC + h) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + h) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + h) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (NNC + h) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + h) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + h) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (NNC + h) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + h) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + h) + 5) = skew33.c3.z;
    // next histone
    Jij(3 * nconstraints + 0, 6 * (NNC + h + 1) + 0) = -1.;
    Jij(3 * nconstraints + 1, 6 * (NNC + h + 1) + 1) = -1.;
    Jij(3 * nconstraints + 2, 6 * (NNC + h + 1) + 2) = -1.;
    skew33 = skew_from_vec3(sub3(hist_joints[h], hist_pos[h + 1]));
    Jij(3 * nconstraints + 0, 6 * (NNC + h + 1) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + h + 1) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + h + 1) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (NNC + h + 1) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + h + 1) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + h + 1) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (NNC + h + 1) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + h + 1) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + h + 1) + 5) = skew33.c3.z;
    nconstraints++;
  }
  // shls
  for (int s = s0; s < (nSHLs - s1); s++) {
    // One histone capsule is attached to three DNA capsules
    // remove the second (middle) joint for each histone
    // keep shls +/- 6.5 between first/last DNA capsules and last/first H3-H4
    // capsules
    if (simplified_nucleosome == 1 &&
        !(s == 0 || s == 1 || s == 3 || s == 4 || s == 6 || s == 7 || s == 9 ||
          s == 10 || s == 12 || s == 13))
      continue;
    i0 = SHLs_bodies[s][0];
    i1 = SHLs_bodies[s][1];
    // histone
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 0) = 1.;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 1) = 1.;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 2) = 1.;
    skew33 = skew_from_vec3(sub3(SHLs_pos[s], hist_pos[i0]));
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 5) = skew33.c3.z;
    // dna
    Jij(3 * nconstraints + 0, 6 * i1 + 0) = -1.;
    Jij(3 * nconstraints + 1, 6 * i1 + 1) = -1.;
    Jij(3 * nconstraints + 2, 6 * i1 + 2) = -1.;
    skew33 = skew_from_vec3(sub3(SHLs_pos[s], nucl_pos[i1]));
    Jij(3 * nconstraints + 0, 6 * i1 + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * i1 + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * i1 + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * i1 + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * i1 + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * i1 + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * i1 + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * i1 + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * i1 + 5) = skew33.c3.z;
    nconstraints++;
  }
  // create the docking domains ball joints
  for (int d = 0; d < nDOCKs; d++) {
    i0 = dockings_bodies[d][0];
    i1 = dockings_bodies[d][1];
    // histone
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 0) = 1.;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 1) = 1.;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 2) = 1.;
    skew33 = skew_from_vec3(sub3(dockings_pos[d], hist_pos[i0]));
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i0) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i0) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i0) + 5) = skew33.c3.z;
    // next histone
    Jij(3 * nconstraints + 0, 6 * (NNC + i1) + 0) = -1.;
    Jij(3 * nconstraints + 1, 6 * (NNC + i1) + 1) = -1.;
    Jij(3 * nconstraints + 2, 6 * (NNC + i1) + 2) = -1.;
    skew33 = skew_from_vec3(sub3(dockings_pos[d], hist_pos[i1]));
    Jij(3 * nconstraints + 0, 6 * (NNC + i1) + 3) = skew33.c1.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i1) + 4) = skew33.c2.x;
    Jij(3 * nconstraints + 0, 6 * (NNC + i1) + 5) = skew33.c3.x;
    Jij(3 * nconstraints + 1, 6 * (NNC + i1) + 3) = skew33.c1.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i1) + 4) = skew33.c2.y;
    Jij(3 * nconstraints + 1, 6 * (NNC + i1) + 5) = skew33.c3.y;
    Jij(3 * nconstraints + 2, 6 * (NNC + i1) + 3) = skew33.c1.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i1) + 4) = skew33.c2.z;
    Jij(3 * nconstraints + 2, 6 * (NNC + i1) + 5) = skew33.c3.z;
    nconstraints++;
  }
  long int rank = Jij.fullPivLu().rank();
  printf("%i objects\n", NNC + NHC);
  printf("%i constraints\n",
         3 * (NNC - 1) + 3 * (NHC - 1) + 3 * nSHLs + 3 * nDOCKs);
  printf("rank=%li nrows=%li ncols=%li\n", rank, Jij.rows(), Jij.cols());
  return rank;
}

/*! @brief create chromatin fibre and return number of DNA monomers,
  DNA+histones monomers, DNA joints and number of nucleosomes. intrinsic twist
  is zero; see 'tw_e_chromatin_fibre' function to get intrinsic twist between
  two consecutive frames. use the result as an argument of the
  'bending_twisting' function. 'fibre_ODE.cpp' source code file gives an example
  how to minimize bending and twisting energies.
  @param chromatin name of the file where to find the chromatin fibre
  description
  @param simplified_nucleosome 0 for full description of the histones core
  @param mbead attached magnetic bead to the chromatin fibre
  @param fibre array of DNA dBodyID object
  @param gfibre array of DNA dGeomID object
  @param ggfibre array of DNA ghost dGeomID object
  @param dfibre
  @param jfibre array of DNA dJointID object
  @param fjfibre
  @param world dWorldID (see Open-Dynamics-Engine)
  @param space dSpaceID (see Open-Dynamics-Engine)
  @param gspace dSpaceID for ghost geometries
  @param lf array of bond length
  @param rf array of bond radius
  @param mf array of bond mass
  @param lbp number of bp per DNA segment
  @param sbp genomic position
  @param xghost
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
  @param dofs
  @param phase1 fluctuations around DNA phase in [phase1,phase2]
  @param phase2 fluctuations around DNA phase in [phase1,phase2]
  @param seed seed phase fluctuations pRNG (array length has to be equal to the
  number of DNA linkers)
  @param intrinsic_twist add intrinsic twist to between two consecutive monomer
  of DNA linker
*/
int *create_chromatin_fibre(
    char *chromatin, int simplified_nucleosome, bool mbead, dBodyID **fibre,
    dGeomID **gfibre, dGeomID **ggfibre, int3p *&dfibre, dJointID **jfibre,
    dJointFeedback **fjfibre, dWorldID world, dSpaceID space, dSpaceID gspace,
    double **lf, double **rf, double **mf, double **lbp, double **sbp,
    double xghost, int gyroscopic, int &dofs, bool phase, double phase1,
    double phase2, const int *seeds, double intrinsic_twist, bool use_table) {
  double *angle_per_nrl = new double[207 - 147 + 1]();
  if (use_table) {
    // angle_per_nrl[20]=0.334627;
    // angle_per_nrl[21]=0.263772;
    // angle_per_nrl[22]=0.862170;
    // angle_per_nrl[23]=1.460569;
    // angle_per_nrl[24]=2.058968;
    // angle_per_nrl[25]=2.657366;
    // angle_per_nrl[26]=3.027421;
    // angle_per_nrl[27]=2.429022;
    // angle_per_nrl[28]=1.830624;
    // angle_per_nrl[29]=1.232224;
    // angle_per_nrl[30]=0.633826;
    // angle_per_nrl[31]=0.035426;
    // angle_per_nrl[32]=0.562970;
    // angle_per_nrl[33]=1.161370;
    // angle_per_nrl[34]=1.759769;
    // angle_per_nrl[35]=2.358166;
    // angle_per_nrl[36]=2.956566;
    // angle_per_nrl[37]=2.728222;
    // angle_per_nrl[38]=2.129823;
    // angle_per_nrl[39]=1.531423;
    // angle_per_nrl[40]=0.933025;
    // angle_per_nrl[41]=0.334627;
    // angle_per_nrl[42]=0.263772;
    // angle_per_nrl[43]=0.862170;
    // angle_per_nrl[44]=1.460569;
    // angle_per_nrl[45]=2.058968;
    // angle_per_nrl[46]=2.657366;
    // angle_per_nrl[47]=3.027420;
    // angle_per_nrl[48]=2.429022;
    // angle_per_nrl[49]=1.830625;
    // angle_per_nrl[50]=1.232223;
    // angle_per_nrl[51]=0.633824;
    // angle_per_nrl[52]=0.035425;
    // angle_per_nrl[53]=0.562971;
    // angle_per_nrl[54]=1.161370;
    // angle_per_nrl[55]=1.759768;
    // angle_per_nrl[56]=2.358166;
    // angle_per_nrl[57]=2.956564;
    // angle_per_nrl[58]=2.728220;
    // angle_per_nrl[59]=2.129822;
    // angle_per_nrl[60]=1.531424;
    angle_per_nrl[20] = 0.334627;
    angle_per_nrl[21] = -0.263772;
    angle_per_nrl[22] = -0.862170;
    angle_per_nrl[23] = -1.460569;
    angle_per_nrl[24] = -2.058968;
    angle_per_nrl[25] = -2.657366;
    angle_per_nrl[26] = 3.027421;
    angle_per_nrl[27] = 2.429022;
    angle_per_nrl[28] = 1.830624;
    angle_per_nrl[29] = 1.232224;
    angle_per_nrl[30] = 0.633826;
    angle_per_nrl[31] = 0.035426;
    angle_per_nrl[32] = -0.562970;
    angle_per_nrl[33] = -1.161370;
    angle_per_nrl[34] = -1.759769;
    angle_per_nrl[35] = -2.358166;
    angle_per_nrl[36] = -2.956566;
    angle_per_nrl[37] = 2.728222;
    angle_per_nrl[38] = 2.129823;
    angle_per_nrl[39] = 1.531423;
    angle_per_nrl[40] = 0.933025;
    angle_per_nrl[41] = 0.334627;
    angle_per_nrl[42] = -0.263772;
    angle_per_nrl[43] = -0.862170;
    angle_per_nrl[44] = -1.460569;
    angle_per_nrl[45] = -2.058968;
    angle_per_nrl[46] = -2.657366;
    angle_per_nrl[47] = 3.027420;
    angle_per_nrl[48] = 2.429022;
    angle_per_nrl[49] = 1.830625;
    angle_per_nrl[50] = 1.232223;
    angle_per_nrl[51] = 0.633824;
    angle_per_nrl[52] = 0.035425;
    angle_per_nrl[53] = -0.562971;
    angle_per_nrl[54] = -1.161370;
    angle_per_nrl[55] = -1.759768;
    angle_per_nrl[56] = -2.358166;
    angle_per_nrl[57] = -2.956564;
    angle_per_nrl[58] = 2.728220;
    angle_per_nrl[59] = 2.129822;
    angle_per_nrl[60] = 1.531424;
  }
  int i0 = 1, chain;
  int *chromatin_size = new int[5];
  vec3 u1, v1, t1, u2, v2, t2, p0, dp, bn, r1m, r2m;
  vec3 u0 = vec3(0., 0., 1.);
  vec3 v0 = vec3(0., 1., 0.);
  vec3 t0 = vec3(-1., 0., 0.);
  int start, fcount = 0, linkers = 0, nucleosomes = 0, start_with_linker = 0,
             F = 0, nH1 = 0, iH1, retour, s0, s1, dock0, dock1, h1, j1, j2;
  double theta;
  // phase fluctuations pRNG
  std::random_device rd;
  std::mt19937_64 mt(rd());
  std::uniform_real_distribution<> uphase(phase1, phase2);
  // read the structure of the nucleosome from input files
  vector<vec3> nucl_pos;
  nucl_pos = read_dna_nucl_pos("nucleosome_in/nposition.in", fL);
  vector<vec3> hist_pos;
  hist_pos = read_hist_pos("nucleosome_in/hposition.in", fL);
  vector<vec3> hist_joints;
  hist_joints = read_hist_joint_pos("nucleosome_in/hist_joints.in", fL);
  vector<vector<vec3>> nucl_rot(NNC, vector<vec3>(3));
  nucl_rot = read_dna_rot("nucleosome_in/nrotation.in");
  vector<vector<vec3>> hist_rot(NHC, vector<vec3>(3));
  hist_rot = read_hist_rot("nucleosome_in/hrotation.in");
  vector<vec3> SHLs_pos(nSHLs);
  SHLs_pos = read_shls_pos("nucleosome_in/SHLs.in", fL);
  vector<vec3> dockings_pos(nDOCKs);
  dockings_pos = read_dockings_pos("nucleosome_in/dockings.in", fL);
  vector<vector<int>> SHLs_bodies(nSHLs, vector<int>(2));
  SHLs_bodies = read_shls_bodies("nucleosome_in/SHLs.in");
  vector<vector<int>> dockings_bodies(nDOCKs, vector<int>(2));
  dockings_bodies = read_dockings_bodies("nucleosome_in/dockings.in");
  vector<vec3> mindist(3);
  double lh = 2.907 * nm / fL;
  double rh = .85 * nm / fL;
  double rH1 = .5 * nm / fL;
  double offset_H1 = .0 * rH1;
  char fname[200];
  FILE *fOut;
  // read the structure of the chromatin from input file
  bool bcylinder = false, bfeedback = false;
  float f0;
  double lbp_min = 147.;
  // compute number of nucleosomes and number of linkers
  fcount = 0;
  FILE *chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosomes++;
      nH1 += (int)(h1 == 1);
      lbp_min = fmin(lbp_min, 10.5);
      F += 14;
    } else {
      if (((f0 / 10.5) * 3.5721) < 2.)
        bcylinder = true;
      lbp_min = fmin(lbp_min, f0);
      F++;
    }
    fcount++;
  }
  fclose(chromatin_in);
  bcylinder = false; // ???
  // store bp per line
  if ((*lbp) != NULL)
    delete[] * lbp;
  (*lbp) = new double[fcount]();
  fcount = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    (*lbp)[fcount] = f0;
    fcount++;
  }
  fclose(chromatin_in);
  // compute bp per linker
  linkers = 0;
  for (int i = 0; i < (fcount - 1); i++) {
    if ((*lbp)[i] != 147. && i == 0)
      linkers++;
    if ((*lbp)[i] == 147. && (*lbp)[i + 1] != 147.)
      linkers++;
  }
  printf("%s: %i nucleosome(s), %i linker(s), min(bp)=%f\n", chromatin,
         nucleosomes, linkers, lbp_min);
  double *bp_per_linker = new double[linkers]();
  int *n_per_linker = new int[linkers]();
  linkers = 0;
  for (int i = 0; i < fcount; i++) {
    if ((*lbp)[i] != 147.) {
      bp_per_linker[linkers] += (*lbp)[i];
      n_per_linker[linkers]++;
    } else
      linkers++;
  }
  // number of objects
  int N = nucleosomes;
  int B = F + nucleosomes * NHC + nH1 + (int)mbead;
  // number of joints (DNA)
  int J = F - 1;
  // number of joints made with SHLs, FHBs and DDs
  int HJ = NHC - 1 + nSHLs + nDOCKs;
  // number of DNA monomers
  chromatin_size[0] = F;
  // number of objects
  chromatin_size[1] = B;
  // number of joints
  chromatin_size[2] = J;
  // number of nucleosomes
  chromatin_size[3] = N;
  // number of H1 histones
  chromatin_size[4] = nH1;
  // variables declaration
  if ((*fibre) != NULL)
    delete[] * fibre;
  (*fibre) = new dBodyID[B]();
  if ((*gfibre) != NULL)
    delete[] * gfibre;
  (*gfibre) = new dGeomID[B]();
  if ((*ggfibre) != NULL)
    delete[] * ggfibre;
  (*ggfibre) = new dGeomID[B]();
  if (dfibre != NULL)
    delete[] dfibre;
  dfibre = new int3p[B]();
  if ((*lf) != NULL)
    delete[] * lf;
  (*lf) = new double[B]();
  if ((*rf) != NULL)
    delete[] * rf;
  (*rf) = new double[B]();
  if ((*mf) != NULL)
    delete[] * mf;
  (*mf) = new double[B]();
  if ((*lbp) != NULL)
    delete[] * lbp;
  (*lbp) = new double[B]();
  if ((*sbp) != NULL)
    delete[] * sbp;
  (*sbp) = new double[F]();
  // dofs
  dofs = 6 * B;
  // joints
  if ((*jfibre) != NULL)
    delete[] * jfibre;
  (*jfibre) = new dJointID[J + nucleosomes * (NHC - 1) + nucleosomes * nSHLs +
                           nucleosomes * nDOCKs + 2];
  if (bfeedback) {
    if ((*fjfibre) != NULL)
      delete[] * fjfibre;
    (*fjfibre) = new dJointFeedback[J + nucleosomes * (NHC - 1) +
                                    nucleosomes * nSHLs + nucleosomes * nDOCKs];
  }
  // nucleosome start
  int *nstart = new int[nucleosomes]();
  // H1
  int *h1s = new int[nucleosomes]();
  // chromatin fiber
  fcount = linkers = nucleosomes = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // chromatin fiber starts with DNA linker
    // always seed with linker index (useful for DNA phase fluctuations)
    if (fcount == 0 && ((int)f0) != 147 && phase1 != .0 && phase2 != .0 &&
        seeds != NULL) {
      mt.seed(abs(seeds[0]));
      start_with_linker = 1;
    }
    // check if input line corresponds to a nucleosome
    if (((int)f0) == 147 && retour == 7) {
      // always seed with linker index (useful for DNA phase fluctuations)
      if (phase1 != .0 && phase2 != .0 && seeds != NULL) {
        // printf("%i\n",seeds[nucleosomes+start_with_linker]);
        mt.seed(abs(seeds[nucleosomes + start_with_linker]));
        // printf("nucleosome=%i
        // seed=%i\n",nucleosomes,seeds[nucleosomes+start_with_linker]);
      }
      // nucleosome
      start = fcount;
      nstart[nucleosomes] = start;
      h1s[nucleosomes] = (int)(h1 == 1);
      // create the nucleosomal dna
      if (nucleosomes == 0) {
        retour = sprintf(fname, "nucleosome_twist/%s", chromatin);
        fOut = fopen(fname, "w");
        fprintf(fOut, "cos(bending) twist\n");
      }
      for (int c = 0; c < NNC; c++) {
        (*lf)[fcount] = 3.5721 * nm / fL;
        (*lbp)[fcount] = 10.5;
        (*rf)[fcount] = nm / fL;
        (*mf)[fcount] = xmass * 10.5 * m1bp / fM;
        // use cylinder for the last and first monomers of the nucleosome
        if (bcylinder && (c == 0 || c == (NNC - 1))) {
          (*fibre)[fcount] =
              b_cylinder(world, space, (*gfibre)[fcount], (*rf)[fcount],
                         (*lf)[fcount], (*mf)[fcount], gyroscopic);
          (*ggfibre)[fcount] = create_ghost_geometry(
              gspace, (*fibre)[fcount], "cylinder", "sphere", xghost,
              (*rf)[fcount], (*rf)[fcount], (*lf)[fcount]);
        } else {
          (*fibre)[fcount] =
              b_capsule(world, space, (*gfibre)[fcount], (*rf)[fcount],
                        (*lf)[fcount], (*mf)[fcount], gyroscopic);
          (*ggfibre)[fcount] = create_ghost_geometry(
              gspace, (*fibre)[fcount], "capsule", "sphere", xghost,
              (*rf)[fcount], (*rf)[fcount], (*lf)[fcount]);
          // (*ggfibre)[fcount]=create_ghost_geometry(gspace,(*fibre)[fcount],"capsule","capsule",xghost,(*rf)[fcount],(*rf)[fcount],(*lf)[fcount]);
        }
        // third entry is the nucleosome index (start from 1)
        dfibre[fcount] = int3p(0, fcount, nucleosomes + 1);
        t2 = nucl_rot[c][2];
        u2 = nucl_rot[c][0];
        v2 = nucl_rot[c][1];
        if (dot_product(cross_product(u2, v2), t2) < .99)
          exit(EXIT_FAILURE);
        // do not break shls: do not add nucleosomal DNA twist
        if (0 && c >= s0 && c <= (NNC - 1 - s1)) {
          t1 = FrameODE((*fibre)[fcount - 1], 2);
          u1 = FrameODE((*fibre)[fcount - 1], 0);
          v1 = FrameODE((*fibre)[fcount - 1], 1);
          bn = normalize3(cross_product(t1, t2));
          theta = arccos(dot_product(t1, t2));
          u2 = rotation(bn, theta, u1);
          v2 = rotation(bn, theta, v1);
        }
        if (nucleosomes == 0)
          fprintf(fOut, "%f %f\n", dot_product(t1, t2),
                  sign(dot_product(u2, v1) - dot_product(v2, u1)) *
                      arccos((dot_product(u2, u1) + dot_product(v2, v1)) /
                             (1. + dot_product(t1, t2))));
        set_axes((*fibre)[fcount], t2, u2, v2);
        set_position((*fibre)[fcount], nucl_pos[c]);
        fcount++;
      }
      if (nucleosomes == 0)
        fclose(fOut);
      // create the histones
      for (int h = 0; h < NHC; h++) {
        (*lf)[F + nucleosomes * NHC + h] = lh;
        (*rf)[F + nucleosomes * NHC + h] = rh;
        (*mf)[F + nucleosomes * NHC + h] = xmass * 10.5 * m1bp / fM;
        (*fibre)[F + nucleosomes * NHC + h] = b_capsule(
            world, space, (*gfibre)[F + nucleosomes * NHC + h], rh,
            lh - 2. * rh, (*mf)[F + nucleosomes * NHC + h], gyroscopic);
        (*ggfibre)[F + nucleosomes * NHC + h] =
            create_ghost_geometry(gspace, (*fibre)[F + nucleosomes * NHC + h],
                                  "capsule", "sphere", xghost, rh, rh, lh);
        // (*ggfibre)[F+nucleosomes*NHC+h]=create_ghost_geometry(gspace,(*fibre)[F+nucleosomes*NHC+h],"capsule","capsule",xghost,rh,rh,lh);
        // if complete unwrapping of the nucleosomal DNA disable histones
        // geometries
        if (s0 == 7 && s1 == 7) {
          dGeomDisable((*gfibre)[F + nucleosomes * NHC + h]);
          dGeomDisable((*ggfibre)[F + nucleosomes * NHC + h]);
        }
        // body data
        dfibre[F + nucleosomes * NHC + h] =
            int3p(1, F + nucleosomes * NHC + h, nucleosomes);
        set_axes((*fibre)[F + nucleosomes * NHC + h], hist_rot[h][2],
                 hist_rot[h][0], hist_rot[h][1]);
        set_position((*fibre)[F + nucleosomes * NHC + h], hist_pos[h]);
      }
      // compute and write minimal distance between dna/histone and
      // histone/histone connected by a joint
      if (nucleosomes == 0) {
        retour =
            sprintf(fname, "nucleosome_joint_relative_position/%s", chromatin);
        fOut = fopen(fname, "w");
        fprintf(fOut, "object1 object2 dot(joint-com,u) dot(joint-com,v) "
                      "dot(joint-com,t)\n");
        // FHBs
        for (int h = 0; h < (NHC - 1); h++) {
          j1 = F + nucleosomes * NHC + h;
          u1 = FrameODE((*fibre)[j1], 0);
          v1 = FrameODE((*fibre)[j1], 1);
          t1 = FrameODE((*fibre)[j1], 2);
          j2 = F + nucleosomes * NHC + h + 1;
          u2 = FrameODE((*fibre)[j2], 0);
          v2 = FrameODE((*fibre)[j2], 1);
          t2 = FrameODE((*fibre)[j2], 2);
          minDist2segments_KKT(
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, -.5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, .5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, -.5 * (*lf)[j2]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, .5 * (*lf)[j2]),
              false, 0, mindist);
          mindist[0] = hist_joints[h];
          mindist[1] = hist_joints[h];
          // r1m=sub3(sub3(mult3((*rf)[j1],mindist[2]),mindist[0]),PositionBodyODE((*fibre)[j1]));
          // r2m=sub3(add3(mult3((*rf)[j2],mindist[2]),mindist[1]),PositionBodyODE((*fibre)[j2]));
          r1m = sub3(mindist[0], PositionBodyODE((*fibre)[j1]));
          r2m = sub3(mindist[1], PositionBodyODE((*fibre)[j2]));
          fprintf(fOut, "FHB%i histone%i %f %f %f\n", h, h,
                  dot_product(FrameODE((*fibre)[j1], 0), r1m),
                  dot_product(FrameODE((*fibre)[j1], 1), r1m),
                  dot_product(FrameODE((*fibre)[j1], 2), r1m));
          fprintf(fOut, "FHB%i histone%i %f %f %f\n", h, h + 1,
                  dot_product(FrameODE((*fibre)[j2], 0), r2m),
                  dot_product(FrameODE((*fibre)[j2], 1), r2m),
                  dot_product(FrameODE((*fibre)[j2], 2), r2m));
        }
        // SHLs
        for (int s = 0; s < nSHLs; s++) {
          j1 = F + nucleosomes * NHC + SHLs_bodies[s][0];
          u1 = FrameODE((*fibre)[j1], 0);
          v1 = FrameODE((*fibre)[j1], 1);
          t1 = FrameODE((*fibre)[j1], 2);
          j2 = start + SHLs_bodies[s][1];
          u2 = FrameODE((*fibre)[j2], 0);
          v2 = FrameODE((*fibre)[j2], 1);
          t2 = FrameODE((*fibre)[j2], 2);
          minDist2segments_KKT(
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, -.5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, .5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, -.5 * (*lf)[j2]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, .5 * (*lf)[j2]),
              false, 0, mindist);
          mindist[0] = SHLs_pos[s];
          mindist[1] = SHLs_pos[s];
          // r1m=sub3(sub3(mult3((*rf)[j1],mindist[2]),mindist[0]),PositionBodyODE((*fibre)[j1]));
          // r2m=sub3(add3(mult3((*rf)[j2],mindist[2]),mindist[1]),PositionBodyODE((*fibre)[j2]));
          r1m = sub3(mindist[0], PositionBodyODE((*fibre)[j1]));
          r2m = sub3(mindist[1], PositionBodyODE((*fibre)[j2]));
          fprintf(fOut, "SHL%i histone%i %f %f %f\n", s, SHLs_bodies[s][0],
                  dot_product(FrameODE((*fibre)[j1], 0), r1m),
                  dot_product(FrameODE((*fibre)[j1], 1), r1m),
                  dot_product(FrameODE((*fibre)[j1], 2), r1m));
          fprintf(fOut, "SHL%i dna%i %f %f %f\n", s, SHLs_bodies[s][1],
                  dot_product(FrameODE((*fibre)[j2], 0), r2m),
                  dot_product(FrameODE((*fibre)[j2], 1), r2m),
                  dot_product(FrameODE((*fibre)[j2], 2), r2m));
        }
        // DDs
        for (int d = 0; d < nDOCKs; d++) {
          j1 = F + nucleosomes * NHC + dockings_bodies[d][0];
          u1 = FrameODE((*fibre)[j1], 0);
          v1 = FrameODE((*fibre)[j1], 1);
          t1 = FrameODE((*fibre)[j1], 2);
          j2 = F + nucleosomes * NHC + dockings_bodies[d][1];
          u2 = FrameODE((*fibre)[j2], 0);
          v2 = FrameODE((*fibre)[j2], 1);
          t2 = FrameODE((*fibre)[j2], 2);
          minDist2segments_KKT(
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, -.5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j1], .0, .0, .5 * (*lf)[j1]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, -.5 * (*lf)[j2]),
              PositionPosRelBodyODE((*fibre)[j2], .0, .0, .5 * (*lf)[j2]),
              false, 0, mindist);
          mindist[0] = dockings_pos[d];
          mindist[1] = dockings_pos[d];
          // r1m=sub3(sub3(mult3((*rf)[j1],mindist[2]),mindist[0]),PositionBodyODE((*fibre)[j1]));
          // r2m=sub3(add3(mult3((*rf)[j2],mindist[2]),mindist[1]),PositionBodyODE((*fibre)[j2]));
          r1m = sub3(mindist[0], PositionBodyODE((*fibre)[j1]));
          r2m = sub3(mindist[1], PositionBodyODE((*fibre)[j2]));
          fprintf(fOut, "DD%i histone%i %f %f %f\n", d, dockings_bodies[d][0],
                  dot_product(FrameODE((*fibre)[j1], 0), r1m),
                  dot_product(FrameODE((*fibre)[j1], 1), r1m),
                  dot_product(FrameODE((*fibre)[j1], 2), r1m));
          fprintf(fOut, "DD%i histone%i %f %f %f\n", d, dockings_bodies[d][1],
                  dot_product(FrameODE((*fibre)[j2], 0), r2m),
                  dot_product(FrameODE((*fibre)[j2], 1), r2m),
                  dot_product(FrameODE((*fibre)[j2], 2), r2m));
        }
        fclose(fOut);
      }
      // create the histones ball joints
      for (int h = 0; h < (NHC - 1); h++) {
        (*jfibre)[J + nucleosomes * HJ + h] =
            ball_joint(world, (*fibre)[F + nucleosomes * NHC + h],
                       (*fibre)[F + nucleosomes * NHC + h + 1], hist_joints[h]);
        dofs -= 3;
      }
      // create the SHLs ball joints
      switch (simplified_nucleosome) {
      case 0:
        // no simple nucleosome model
        for (int s = s0; s < (nSHLs - s1); s++) {
          (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
              world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
              (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
          dofs -= 3;
        }
        break;
      case 1:
        // one histone capsule is attached to three DNA capsules
        // remove the second (middle) joint for each histone
        // keep shls +/- 6.5 between first/last DNA capsules and last/first
        // H3-H4 capsules
        for (int s = s0; s < (nSHLs - s1); s++) {
          if (s == 0 || s == 1 || s == 3 || s == 4 || s == 6 || s == 7 ||
              s == 9 || s == 10 || s == 12 || s == 13) {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dofs -= 3;
          } else {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dJointDisable((*jfibre)[J + nucleosomes * HJ + NHC - 1 + s]);
          }
        }
        break;
      case 2:
        // one histone capsule is attached to three DNA capsules
        // remove the first and third joints for each histone
        // keep shls +/- 6.5 between first/last DNA capsules and last/first
        // H3-H4 capsules
        for (int s = s0; s < (nSHLs - s1); s++) {
          if (s == 0 || s == 2 || s == 5 || s == 8 || s == 11 || s == 13) {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dofs -= 3;
          } else {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dJointDisable((*jfibre)[J + nucleosomes * HJ + NHC - 1 + s]);
          }
        }
        break;
      case 3:
        // one histone capsule is attached to three DNA capsules
        // keep only the middle joint
        // keep shls +/- 6.5 between first/last DNA capsules and last/first
        // H3-H4 capsules
        for (int s = s0; s < (nSHLs - s1); s++) {
          if (s == 0 || s == 2 || s == 11 || s == 13) {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dofs -= 3;
          } else {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
                (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
            dJointDisable((*jfibre)[J + nucleosomes * HJ + NHC - 1 + s]);
          }
        }
        break;
      default:
        // no simple nucleosome model (default)
        for (int s = s0; s < (nSHLs - s1); s++) {
          (*jfibre)[J + nucleosomes * HJ + NHC - 1 + s] = ball_joint(
              world, (*fibre)[F + nucleosomes * NHC + SHLs_bodies[s][0]],
              (*fibre)[start + SHLs_bodies[s][1]], SHLs_pos[s]);
          dofs -= 3;
        }
        break;
      }
      // create the docking domains ball joints
      if (simplified_nucleosome <= 2) {
        for (int d = 0; d < nDOCKs; d++) {
          if ((dock0 == 0 && d == 0) || (dock1 == 0 && d == 1)) {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + nSHLs + d] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + dockings_bodies[d][0]],
                (*fibre)[F + nucleosomes * NHC + dockings_bodies[d][1]],
                dockings_pos[d]);
            dofs -= 3;
          } else {
            (*jfibre)[J + nucleosomes * HJ + NHC - 1 + nSHLs + d] = ball_joint(
                world, (*fibre)[F + nucleosomes * NHC + dockings_bodies[d][0]],
                (*fibre)[F + nucleosomes * NHC + dockings_bodies[d][1]],
                dockings_pos[d]);
            dJointDisable(
                (*jfibre)[J + nucleosomes * HJ + NHC - 1 + nSHLs + d]);
          }
        }
      }
      // connectivity between current nucleosome and previous linker
      p0 = (start == 0) ? vec3(.0, .0, .0)
                        : PositionPosRelBodyODE((*fibre)[start - 1], .0, .0,
                                                .5 * (*lf)[start - 1]);
      dp = (start == 0)
               ? vec3(.0, .0, .0)
               : sub3(p0, PositionPosRelBodyODE((*fibre)[start], .0, .0,
                                                -.5 * (*lf)[start]));
      for (int c = 0; c < NNC; c++)
        set_position((*fibre)[start + c],
                     add3(PositionBodyODE((*fibre)[start + c]), dp));
      for (int h = 0; h < NHC; h++)
        set_position(
            (*fibre)[F + nucleosomes * NHC + h],
            add3(PositionBodyODE((*fibre)[F + nucleosomes * NHC + h]), dp));
      // nucleosome dna orientation along the tangent
      t1 = (start == 0) ? t0 : FrameODE((*fibre)[start - 1], 2);
      t2 = FrameODE((*fibre)[start], 2);
      bn = normalize3(cross_product(t2, t1));
      theta = arccos(dot_product(t2, t1));
      rotations(bn, theta, *fibre, start, start + NNC - 1, p0);
      // octamer orientation along the tangent
      rotations(bn, theta, *fibre, F + nucleosomes * NHC,
                F + nucleosomes * NHC + NHC - 1, p0);
      // nucleosome dna orientation along the normal
      u1 = (start == 0) ? u0 : FrameODE((*fibre)[start - 1], 0);
      u2 = FrameODE((*fibre)[start], 0);
      bn = normalize3(cross_product(u2, u1));
      theta = arccos(dot_product(u2, u1));
      rotations(bn, theta, *fibre, start, start + NNC - 1, p0);
      // octamer orientation along the normal
      rotations(bn, theta, *fibre, F + nucleosomes * NHC,
                F + nucleosomes * NHC + NHC - 1, p0);
      // phase
      if (phase && start > 0) {
        if ((bool)i0 && use_table)
          theta = sign_phase *
                  ((2. * pi / 10.5) * .5 * ((*lbp)[start - 1] + (*lbp)[start]) +
                   intrinsic_twist +
                   angle_per_nrl[(int)rint(bp_per_linker[nucleosomes])] /
                       (n_per_linker[nucleosomes] + 1));
        else
          theta = sign_phase *
                  ((2. * pi / 10.5) * .5 * ((*lbp)[start - 1] + (*lbp)[start]) +
                   intrinsic_twist);
        if ((phase1 > -pi && phase2 < pi && phase1 < phase2))
          theta += sign_phase * uphase(mt);
        bn = FrameODE((*fibre)[start], 2);
        // dna
        rotations(bn, theta, *fibre, start, start + NNC - 1, p0);
        // octamer
        rotations(bn, theta, *fibre, F + nucleosomes * NHC,
                  F + nucleosomes * NHC + NHC - 1, p0);
      }
      nucleosomes++;
    } else {
      // not a nucleosome
      (*lf)[fcount] = (f0 / 10.5) * 3.5721 * nm / fL;
      (*lbp)[fcount] = f0;
      (*rf)[fcount] = nm / fL;
      (*mf)[fcount] =
          (same_mass) ? xmass * 10.5 * m1bp / fM : xmass * f0 * m1bp / fM;
      // length and capsule radius condition
      if (bcylinder) {
        (*fibre)[fcount] =
            b_cylinder(world, space, (*gfibre)[fcount], (*rf)[fcount],
                       (*lf)[fcount], (*mf)[fcount], gyroscopic);
        (*ggfibre)[fcount] = create_ghost_geometry(
            gspace, (*fibre)[fcount], "cylinder", "sphere", xghost,
            (*rf)[fcount], (*rf)[fcount], (*lf)[fcount]);
      } else {
        (*fibre)[fcount] =
            b_capsule(world, space, (*gfibre)[fcount], (*rf)[fcount],
                      (*lf)[fcount], (*mf)[fcount], gyroscopic);
        (*ggfibre)[fcount] = create_ghost_geometry(
            gspace, (*fibre)[fcount], "capsule", "sphere", xghost,
            (*rf)[fcount], (*rf)[fcount], (*lf)[fcount]);
        // (*ggfibre)[fcount]=create_ghost_geometry(gspace,(*fibre)[fcount],"capsule","capsule",xghost,(*rf)[fcount],(*rf)[fcount],(*lf)[fcount]);
      }
      // third entry is the linker index (start from -1)
      if (fcount == 0)
        linkers++;
      if (fcount > 0)
        if (dfibre[fcount - 1].k > 0)
          linkers++;
      // if monomer is inside linker its index is < 0
      // if monomer is inside nucleosome its index is > 0
      dfibre[fcount] = int3p(0, fcount, -linkers);
      // first monomer of the chromatin fibre ?
      if (fcount == 0) {
        set_axes((*fibre)[fcount], t0, u0, v0);
        set_position((*fibre)[fcount], mult3(.5 * (*lf)[fcount], t0));
      } else {
        u1 = FrameODE((*fibre)[fcount - 1], 0);
        v1 = FrameODE((*fibre)[fcount - 1], 1);
        t1 = FrameODE((*fibre)[fcount - 1], 2);
        if (phase) {
          theta = sign_phase * ((2. * pi / 10.5) * .5 *
                                    ((*lbp)[fcount - 1] + (*lbp)[fcount]) +
                                intrinsic_twist);
          // use table (irregular fiber)
          if ((bool)i0 && use_table)
            theta += sign_phase *
                     angle_per_nrl[(int)rint(bp_per_linker[linkers - 1])] /
                     (n_per_linker[linkers - 1] + 1);
          // phase fluctuation
          if ((phase1 > -pi && phase2 < pi &&
               phase1 < phase2)) // && dfibre[fcount-1].k<0)
            theta += sign_phase * uphase(mt);
          u2 = rotation(t1, theta, u1);
          v2 = rotation(t1, theta, v1);
        } else {
          u2 = u1;
          v2 = v1;
        }
        t2 = normalize3(cross_product(u2, v2));
        set_axes((*fibre)[fcount], t2, u2, v2);
        set_position((*fibre)[fcount],
                     add3(PositionPosRelBodyODE((*fibre)[fcount - 1], 0., 0.,
                                                .5 * (*lf)[fcount - 1]),
                          mult3(.5 * (*lf)[fcount], t2)));
      }
      fcount++;
    }
  }
  fclose(chromatin_in);
  // create H1
  nH1 = 0;
  for (int n = 0; n < N; n++) {
    if (h1s[n] == 1 && (nstart[n] - 1) > 0 && (nstart[n] + NNC - 1 + 1) < F) {
      iH1 = F + N * NHC + nH1;
      (*lf)[iH1] = 2. * rH1;
      (*rf)[iH1] = rH1;
      (*mf)[iH1] = xmass * 10.5 * m1bp / fM;
      (*fibre)[iH1] =
          b_sphere(world, space, (*gfibre)[iH1], (*rf)[iH1], (*mf)[iH1]);
      (*ggfibre)[iH1] = create_ghost_geometry(gspace, (*fibre)[iH1], "sphere",
                                              "sphere", xghost, 2. * (*rf)[iH1],
                                              2. * (*rf)[iH1], 2. * (*rf)[iH1]);
      // do not collide h1 dGeomID against other dGeomID
      // (*lf)[iH1]=.0;
      // (*rf)[iH1]=.0;
      dGeomSphereSetRadius((*gfibre)[iH1], .0);
      dGeomSphereSetRadius((*ggfibre)[iH1], .0);
      // data
      dfibre[iH1] = int3p(-1, iH1, -1);
      // axes
      set_axes((*fibre)[iH1], FrameODE((*fibre)[iH1], 2),
               FrameODE((*fibre)[iH1], 0), FrameODE((*fibre)[iH1], 1));
      // position
      j1 = nstart[n] - 1;
      j2 = nstart[n] + NNC - 1 + 1;
      set_position(
          (*fibre)[iH1],
          mult3(.5, add3(PositionPosRelBodyODE((*fibre)[j1], .0, .0,
                                               -.5 * (*lf)[j1] + offset_H1),
                         PositionPosRelBodyODE((*fibre)[j2], .0, .0,
                                               .5 * (*lf)[j2] - offset_H1))));
      // for(int z=0;z<20;z++)
      // 	printf("n=%i z=%f
      // %f/%f\n",n,z/20.,norm3(sub3(PositionPosRelBodyODE((*fibre)[j1],.0,.0,-.5*(*lf)[j1]+z*rH1/20.),PositionPosRelBodyODE((*fibre)[j2],.0,.0,.5*(*lf)[j2]-z*rH1/20.))),2.*rH1+(*rf)[j1]+(*rf)[j2]);
      // create H1/DNA joints
      ball_joint(
          world, (*fibre)[iH1], (*fibre)[j1],
          mult3(.5, add3(PositionBodyODE((*fibre)[iH1]),
                         PositionPosRelBodyODE((*fibre)[j1], .0, .0,
                                               -.5 * (*lf)[j1] + offset_H1))));
      dofs -= 3;
      ball_joint(
          world, (*fibre)[iH1], (*fibre)[j2],
          mult3(.5, add3(PositionBodyODE((*fibre)[iH1]),
                         PositionPosRelBodyODE((*fibre)[j2], .0, .0,
                                               .5 * (*lf)[j2] - offset_H1))));
      dofs -= 3;
      nH1++;
    }
  }
  // magnetic bead ?
  if (mbead) {
    (*lf)[B - 1] = 2. * 50. * nm / fL;
    (*rf)[B - 1] = 50. * nm / fL;
    (*mf)[B - 1] =
        (same_mass) ? xmass * 10.5 * m1bp / fM : xmass * 2. * 10.5 * m1bp / fM;
    (*fibre)[B - 1] =
        b_sphere(world, space, (*gfibre)[B - 1], (*rf)[B - 1], (*mf)[B - 1]);
    (*ggfibre)[B - 1] = create_ghost_geometry(
        gspace, (*fibre)[B - 1], "sphere", "sphere", xghost, 2. * (*rf)[B - 1],
        2. * (*rf)[B - 1], 2. * (*rf)[B - 1]);
    dfibre[B - 1] = int3p(-1, B - 1, -1);
    set_axes((*fibre)[B - 1], FrameODE((*fibre)[J], 2),
             FrameODE((*fibre)[J], 0), FrameODE((*fibre)[J], 1));
    set_position((*fibre)[B - 1],
                 PositionPosRelBodyODE((*fibre)[J], 0., 0., .5 * (*lf)[J]));
    // create the ground
    dGeomID ground = dCreatePlane(space, 0., 0., 1., -(*rf)[0] - .01 * nm / fL);
    if (ground != NULL) {
      // attach the fiber to the ground
      (*jfibre)[J + N * (NHC - 1) + N * nSHLs + N * nDOCKs + 1] = ball_joint(
          world, 0, (*fibre)[0],
          PositionPosRelBodyODE((*fibre)[0], 0., 0., -.5 * (*lf)[0]));
      dofs -= 3;
    }
    // attach the extremity of the fiber to the magnetic bead
    (*jfibre)[J + N * (NHC - 1) + N * nSHLs + N * nDOCKs + 2] =
        ball_joint(world, (*fibre)[J], (*fibre)[B - 1],
                   PositionPosRelBodyODE((*fibre)[J], 0., 0., .5 * (*lf)[J]));
    dofs -= 3;
  }
  // set data
  for (int b = 0; b < B; b++)
    dBodySetData((*fibre)[b], &dfibre[b]);
  // create the ball joints inside the chromatin fiber
  for (int f = 0; f < J; f++) {
    (*jfibre)[f] =
        ball_joint(world, (*fibre)[f], (*fibre)[f + 1],
                   PositionPosRelBodyODE((*fibre)[f], 0., 0., .5 * (*lf)[f]));
    dofs -= 3;
  }
  // add feedback to all joints
  if (bfeedback) {
    for (int j = 0; j < (J + N * HJ); j++) {
      // check if joint is not NULL (broken SHL/docking domain ?)
      if ((*jfibre)[j] != NULL)
        dJointSetFeedback((*jfibre)[j], fjfibre[j]);
    }
  }
  // compute genomic position
  (*sbp)[0] = .0;
  for (int f = 1; f < F; f++)
    (*sbp)[f] = (*sbp)[f - 1] + (*lbp)[f - 1];
  // clean
  delete[] bp_per_linker;
  bp_per_linker = NULL;
  delete[] n_per_linker;
  n_per_linker = NULL;
  delete[] nstart;
  nstart = NULL;
  delete[] h1s;
  h1s = NULL;
  delete[] angle_per_nrl;
  angle_per_nrl = NULL;
  return chromatin_size;
}

/*! @brief disable joints for reversome simulation
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
*/
int reversome_disable_joints(dJointID **jfibre, int n1, int n2, int J) {
  int add_dofs = 0, HJ = NHC - 1 + nSHLs + nDOCKs;
  for (int n = n1; n <= n2; n++) {
    // break Four-Helix-Bundle
    for (int h = 0; h < (NHC - 1); h++) {
      dJointDisable((*jfibre)[J + n * HJ + h]);
      add_dofs += 3;
    }
    // break SHLs 6.5
    dJointDisable((*jfibre)[J + n * HJ + (NHC - 1) + 0]);
    add_dofs += 3;
    dJointDisable((*jfibre)[J + n * HJ + (NHC - 1) + nSHLs - 1]);
    add_dofs += 3;
    // break docking domains
    dJointDisable((*jfibre)[J + n * HJ + (NHC - 1) + nSHLs + 0]);
    add_dofs += 3;
    dJointDisable((*jfibre)[J + n * HJ + (NHC - 1) + nSHLs + nDOCKs - 1]);
    add_dofs += 3;
  }
  return add_dofs;
}

/*! @brief disable "Four-Helix-Bundle" joints
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
*/
int disable_FHB_joints(dJointID **jfibre, int n1, int n2, int J) {
  int add_dofs = 0, HJ = NHC - 1 + nSHLs + nDOCKs;
  for (int n = n1; n <= n2; n++) {
    // break Four-Helix-Bundle
    for (int h = 0; h < (NHC - 1); h++) {
      dJointDisable((*jfibre)[J + n * HJ + h]);
      add_dofs += 3;
    }
  }
  return add_dofs;
}

/*! @brief disable nucleosome joints
  and return number of dofs added to the system.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
*/
int disable_nucleosome_joints(dJointID **jfibre, int n1, int n2, int J) {
  int add_dofs = 0, HJ = NHC - 1 + nSHLs + nDOCKs;
  for (int n = n1; n <= n2; n++) {
    // break Four-Helix-Bundles
    for (int h = 0; h < (NHC - 1); h++) {
      dJointDisable((*jfibre)[J + n * HJ + h]);
      add_dofs += 3;
    }
    // break SHLs
    for (int s = 0; s < nSHLs; s++) {
      dJointDisable((*jfibre)[J + n * HJ + NHC - 1 + s]);
      add_dofs += 3;
    }
    // break docking-domains
    for (int d = 0; d < nDOCKs; d++) {
      dJointDisable((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d]);
      add_dofs += 3;
    }
  }
  return add_dofs;
}

/*! @brief apply FHBs, SHLs and DDs forces (use 'disable_nucleosome_joints'
  function before) and return number of dofs added to the system. it also store
  forces and energies from Morse potential.
  @param jfibre array of dJointID (see 'create_chromatin_fibre' function)
  @param n1 from nucleosome 'n1'
  @param n2 to nucleosome 'n2'
  @param J number of DNA joints
  @param Dm Morse potential strength
  @param em Morse potential equilibrium length
  @param am Morse potential range (inverse of length unit)
  @param fm to store forces (size has to be nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param Em to store energies (size has to be nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param rm to store vector distances (size has to be
  nucleosomes*(NHC-1+nSHLs+nDOCKs)
  @param replace replace shls, fhbs, dds or all joints by Morse potentials
  @param use_min_dist if true use minimal distance
  @param use_omp if 'true' use pragma omp parallel for
*/
int replace_nucleosome_joints_by_potentials(dJointID **jfibre, int n1, int n2,
                                            int J, double *&Dm, double *&em,
                                            double *&am, vec3 *&fm, double *&Em,
                                            vec3 *&rm, vec3 *&uvtm1,
                                            vec3 *&uvtm2, const char *replace,
                                            bool use_mindist, bool use_omp) {
  int add_dofs = 0, HJ = NHC - 1 + nSHLs + nDOCKs;
  dVector3 r1, r2;
  vec3 r12, force, R1, R2;
  dBodyID b1, b2;
  double fsign = 1.;
  int *add_dofs_per_nucleosome = new int[(use_omp) ? n2 - n1 + 1 : 1]();
#pragma omp parallel for private(r12, force, R1, R2) if (use_omp)
  for (int n = n1; n <= n2; n++) {
    // break Four-Helix-Bundles
    if (strcmp(replace, "fhbs") == 0 || strcmp(replace, "all") == 0) {
      for (int h = 0; h < (NHC - 1); h++) {
        if (dJointIsEnabled((*jfibre)[J + n * HJ + h])) {
          dJointDisable((*jfibre)[J + n * HJ + h]);
          if (use_omp)
            add_dofs_per_nucleosome[n - n1] += 3;
          else
            add_dofs += 3;
        }
        if (!use_mindist) {
          dJointGetBallAnchor((*jfibre)[J + n * HJ + h], r1);
          dJointGetBallAnchor2((*jfibre)[J + n * HJ + h], r2);
          R1 = vec3(r1[0], r1[1], r1[2]);
          R2 = vec3(r2[0], r2[1], r2[2]);
        } else {
          b1 = dJointGetBody((*jfibre)[J + n * HJ + h], 0);
          R1 = add3(PositionBodyODE(b1), mult3(uvtm1[h].x, FrameODE(b1, 0)));
          R1 = add3(R1, mult3(uvtm1[h].y, FrameODE(b1, 1)));
          R1 = add3(R1, mult3(uvtm1[h].z, FrameODE(b1, 2)));
          b2 = dJointGetBody((*jfibre)[J + n * HJ + h], 1);
          R2 = add3(PositionBodyODE(b2), mult3(uvtm2[h].x, FrameODE(b2, 0)));
          R2 = add3(R2, mult3(uvtm2[h].y, FrameODE(b2, 1)));
          R2 = add3(R2, mult3(uvtm2[h].z, FrameODE(b2, 2)));
        }
        r12 = sub3(R1, R2);
        rm[n * HJ + h] = r12;
        // if(n==n1)
        // 	print3(r12);
        force = force_from_Morse_potential(Dm[h], em[h], am[h], r12);
        fm[n * HJ + h].x = force.x;
        fm[n * HJ + h].y = force.y;
        fm[n * HJ + h].z = force.z;
        Em[n * HJ + h] = Morse_potential(Dm[h], em[h], am[h], r12);
        addPosForceBodyODE(dJointGetBody((*jfibre)[J + n * HJ + h], 0), R1,
                           mult3(fsign, force));
        addPosForceBodyODE(dJointGetBody((*jfibre)[J + n * HJ + h], 1), R2,
                           mult3(-fsign, force));
      }
    }
    // break SHLs
    if (strcmp(replace, "shls") == 0 || strcmp(replace, "shls25811") == 0 ||
        strcmp(replace, "all") == 0) {
      for (int s = 0; s < nSHLs; s++) {
        if (strcmp(replace, "shls25811") == 0 &&
            (s == 0 || s == 1 || s == 3 || s == 4 || s == 6 || s == 7 ||
             s == 9 || s == 10 || s == 12 || s == 13))
          continue;
        if (dJointIsEnabled((*jfibre)[J + n * HJ + NHC - 1 + s])) {
          dJointDisable((*jfibre)[J + n * HJ + NHC - 1 + s]);
          if (use_omp)
            add_dofs_per_nucleosome[n - n1] += 3;
          else
            add_dofs += 3;
        }
        if (!use_mindist) {
          dJointGetBallAnchor((*jfibre)[J + n * HJ + NHC - 1 + s], r1);
          dJointGetBallAnchor2((*jfibre)[J + n * HJ + NHC - 1 + s], r2);
          R1 = vec3(r1[0], r1[1], r1[2]);
          R2 = vec3(r2[0], r2[1], r2[2]);
        } else {
          b1 = dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + s], 0);
          R1 = add3(PositionBodyODE(b1),
                    mult3(uvtm1[NHC - 1 + s].x, FrameODE(b1, 0)));
          R1 = add3(R1, mult3(uvtm1[NHC - 1 + s].y, FrameODE(b1, 1)));
          R1 = add3(R1, mult3(uvtm1[NHC - 1 + s].z, FrameODE(b1, 2)));
          b2 = dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + s], 1);
          R2 = add3(PositionBodyODE(b2),
                    mult3(uvtm2[NHC - 1 + s].x, FrameODE(b2, 0)));
          R2 = add3(R2, mult3(uvtm2[NHC - 1 + s].y, FrameODE(b2, 1)));
          R2 = add3(R2, mult3(uvtm2[NHC - 1 + s].z, FrameODE(b2, 2)));
        }
        r12 = sub3(R1, R2);
        rm[n * HJ + NHC - 1 + s] = r12;
        force = force_from_Morse_potential(Dm[NHC - 1 + s], em[NHC - 1 + s],
                                           am[NHC - 1 + s], r12);
        fm[n * HJ + NHC - 1 + s].x = force.x;
        fm[n * HJ + NHC - 1 + s].y = force.y;
        fm[n * HJ + NHC - 1 + s].z = force.z;
        Em[n * HJ + NHC - 1 + s] = Morse_potential(
            Dm[NHC - 1 + s], em[NHC - 1 + s], am[NHC - 1 + s], r12);
        addPosForceBodyODE(
            dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + s], 0), R1,
            mult3(fsign, force));
        addPosForceBodyODE(
            dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + s], 1), R2,
            mult3(-fsign, force));
      }
    }
    // break docking-domains
    if (strcmp(replace, "dds") == 0 || strcmp(replace, "all") == 0) {
      for (int d = 0; d < nDOCKs; d++) {
        if (dJointIsEnabled((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d])) {
          dJointDisable((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d]);
          if (use_omp)
            add_dofs_per_nucleosome[n - n1] += 3;
          else
            add_dofs += 3;
        }
        if (!use_mindist) {
          dJointGetBallAnchor((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], r1);
          dJointGetBallAnchor2((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], r2);
          R1 = vec3(r1[0], r1[1], r1[2]);
          R2 = vec3(r2[0], r2[1], r2[2]);
        } else {
          b1 = dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], 0);
          R1 = add3(PositionBodyODE(b1),
                    mult3(uvtm1[NHC - 1 + nSHLs + d].x, FrameODE(b1, 0)));
          R1 = add3(R1, mult3(uvtm1[NHC - 1 + nSHLs + d].y, FrameODE(b1, 1)));
          R1 = add3(R1, mult3(uvtm1[NHC - 1 + nSHLs + d].z, FrameODE(b1, 2)));
          b2 = dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], 1);
          R2 = add3(PositionBodyODE(b2),
                    mult3(uvtm2[NHC - 1 + nSHLs + d].x, FrameODE(b2, 0)));
          R2 = add3(R2, mult3(uvtm2[NHC - 1 + nSHLs + d].y, FrameODE(b2, 1)));
          R2 = add3(R2, mult3(uvtm2[NHC - 1 + nSHLs + d].z, FrameODE(b2, 2)));
        }
        r12 = sub3(R1, R2);
        rm[n * HJ + NHC - 1 + nSHLs + d] = r12;
        force = force_from_Morse_potential(Dm[NHC - 1 + nSHLs + d],
                                           em[NHC - 1 + nSHLs + d],
                                           am[NHC - 1 + nSHLs + d], r12);
        fm[n * HJ + NHC - 1 + nSHLs + d].x = force.x;
        fm[n * HJ + NHC - 1 + nSHLs + d].y = force.y;
        fm[n * HJ + NHC - 1 + nSHLs + d].z = force.z;
        Em[n * HJ + NHC - 1 + nSHLs + d] =
            Morse_potential(Dm[NHC - 1 + nSHLs + d], em[NHC - 1 + nSHLs + d],
                            am[NHC - 1 + nSHLs + d], r12);
        addPosForceBodyODE(
            dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], 0), R1,
            mult3(fsign, force));
        addPosForceBodyODE(
            dJointGetBody((*jfibre)[J + n * HJ + NHC - 1 + nSHLs + d], 1), R2,
            mult3(-fsign, force));
      }
    }
  }
  if (use_omp)
    for (int n = n1; n <= n2; n++)
      add_dofs += add_dofs_per_nucleosome[n - n1];
  delete[] add_dofs_per_nucleosome;
  add_dofs_per_nucleosome = NULL;
  return add_dofs;
}

/*! @brief digest chromatin fiber and write fragment size in bp
  @param chromatin_name of the file where to find the chromatin fibre
  description
  @param prob_to_cut probability to cut a linker
  @param mt Mersenne-Twister from std
  @param full if true linkers are fully digested
*/
void digestion(char *chromatin, double prob_to_cut, std::mt19937_64 &mt,
               bool full) {
  int nucleosomes = 0, F = 0, retour, s0, s1, dock0, dock1, h1, chain;
  // read the structure of the chromatin from input file
  float f0;
  FILE *chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosomes++;
      F += NNC;
    } else
      F++;
  }
  fclose(chromatin_in);
  double *lbp = new double[F]();
  bool *is_nucleosome_dna = new bool[F]();
  // read the structure of the chromatin from input file and fill 'lbp'
  F = nucleosomes = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      for (int i = 0; i < NNC; i++) {
        lbp[F + i] = 10.5;
        is_nucleosome_dna[F + i] = true;
      }
      F += 14;
      nucleosomes++;
    } else {
      lbp[F] = f0;
      is_nucleosome_dna[F] = false;
      F++;
    }
  }
  fclose(chromatin_in);
  // cut linker
  std::uniform_real_distribution<> ul(.0, 1.);
  int *cut = new int[(full) ? nucleosomes : F]();
  int ncut = 0;
  if (full) {
    // full digestion of the linker
    for (int n = 0; n < nucleosomes; n++) {
      if (n == 0 || ul(mt) < prob_to_cut) {
        cut[ncut] = n;
        ncut++;
      }
    }
  } else {
    for (int n = 0; n < (nucleosomes - 1); n++) {
      if (n == 0 || ul(mt) < prob_to_cut) {
        cut[ncut] =
            nucleosome_start[n] + NNC +
            (int)((nucleosome_start[n + 1] - (nucleosome_start[n] + NNC)) *
                  ul(mt));
        ncut++;
      }
    }
    // for(int f=0;f<F;f++){
    //   if(ul(mt)<prob_to_cut && !is_nucleosome_dna[f]){
    // 	cut[ncut]=f;
    // 	ncut++;
    //   }
    // }
  }
  // compute number of bp between two cuts
  double *bp_per_cut = new double[ncut - 1]();
  int n0, n1;
  if (full) {
    // full digestion of the linker
    for (int n = 0; n < (ncut - 1); n++) {
      bp_per_cut[n] = .0;
      // first cut
      n0 = nucleosome_start[cut[n]];
      // second cut
      n1 = nucleosome_start[cut[n + 1]];
      for (int i = n0; i < n1; i++)
        bp_per_cut[n] += lbp[i];
    }
  } else {
    for (int n = 0; n < (ncut - 1); n++) {
      bp_per_cut[n] = .0;
      for (int i = cut[n]; i < cut[n + 1]; i++)
        bp_per_cut[n] += lbp[i];
    }
  }
  // write number of bp between two cuts
  char ndigestion[1000];
  if (full)
    sprintf(ndigestion, "digestion/full_digestion%f_%s", prob_to_cut,
            chromatin);
  else
    sprintf(ndigestion, "digestion/digestion%f_%s", prob_to_cut, chromatin);
  FILE *fdigestion = fopen(ndigestion, "w");
  for (int n = 0; n < (ncut - 1); n++) {
    // Genome-wide measurement of local nucleosome array regularity and spacing
    // by nanopore sequencing read length 'x' follow exponential distribution
    // a*exp(-a*x) max of frequency is for x=300 bp solve a*exp(-a*300)=0.00225
    // for a read length (bp) - frequency 200              - 0.0005 300 -
    // 0.00225 > 300            - decrease from 0.00225
    if (bp_per_cut[n] < 300.)
      if (ul(mt) > (.025 * bp_per_cut[n] / 300.))
        continue;
    // if(bp_per_cut[n]<300.)
    // if(ul(mt)>((0.0005+(bp_per_cut[n]/300.)*(0.00225-0.005))/0.00225))
    // continue;
    fprintf(fdigestion, "%.1f\n", bp_per_cut[n]);
  }
  fclose(fdigestion);
  // clean
  delete[] lbp;
  lbp = NULL;
  delete[] is_nucleosome_dna;
  is_nucleosome_dna = NULL;
  delete[] cut;
  cut = NULL;
  delete[] bp_per_cut;
  bp_per_cut = NULL;
}

/*! @brief fill an array with negative integer for linkers and positive integer
  for nuclesomes
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
int *is_linker_or_nucleosome(char *chromatin) {
  int nucleosomes = 0, linkers = 0, F = 0, FF, retour, s0, s1, dock0, dock1, h1,
      chain;
  // read the structure of the chromatin from input file
  float f0;
  FILE *chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosomes++;
      F += NNC;
    } else
      F++;
  }
  fclose(chromatin_in);
  int *is_linker_nucleosome = new int[F + nucleosomes * NHC]();
  // read the structure of the chromatin from input file and fill
  // 'is_linker_nucleosome'
  FF = F;
  F = nucleosomes = linkers = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      // dna
      for (int i = 0; i < NNC; i++)
        is_linker_nucleosome[F + i] = nucleosomes + 1;
      // histones
      for (int i = 0; i < NHC; i++)
        is_linker_nucleosome[FF + nucleosomes * NHC + i] = nucleosomes + 1;
      F += 14;
      nucleosomes++;
    } else {
      is_linker_nucleosome[F] = -(linkers + 1);
      F++;
      linkers++;
    }
  }
  fclose(chromatin_in);
  return is_linker_nucleosome;
}

/*! @brief fill an array with 0 if no H1 otherwize 1 for all the nuclesomes
  @param chromatin name of the file where to find the chromatin fibre
  description
*/
int *nucleosome_with_h1(char *chromatin) {
  int nucleosomes = 0, retour, s0, s1, dock0, dock1, h1, chain;
  // read the structure of the chromatin from input file
  float f0;
  FILE *chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7)
      nucleosomes++;
  }
  fclose(chromatin_in);
  int *h1s = new int[nucleosomes]();
  // read the structure of the chromatin from input file and fill 'h1s'
  nucleosomes = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7)
      h1s[nucleosomes] = (h1 != 0) ? 1 : 0;
  }
  fclose(chromatin_in);
  return h1s;
}

/*! @brief fill an array with the DNA/histone monomer start of each nucleosome.
  if the fiber is made of N nucleosomes the array will be of size 2*N
  the first N are the start of DNA nucleosomes and the last N are the start of
  histone cores. First, run 'create_chromatin_fibre' function and then run
  'dna_histone_starts' function.
  @param chromatin_name of the file where to find the chromatin fibre
  description
*/
int *dna_histone_starts(char *chromatin) {
  int nucleosomes = 0, id, inucl, F = 0, retour, s0, s1, dock0, dock1, h1,
      chain;
  // read the structure of the chromatin from input file
  float f0;
  FILE *chromatin_in = fopen(chromatin, "r");
  // F is the number of DNA monomers
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosomes++;
      F += NNC;
    } else
      F++;
  }
  fclose(chromatin_in);
  printf("dna_histone_starts function found %i nucleosome(s)\n", nucleosomes);
  // compute DNA/histone start
  delete[] nucleosome_start;
  nucleosome_start = new int[2 * nucleosomes]();
  id = inucl = 0;
  chromatin_in = fopen(chromatin, "r");
  while (!feof(chromatin_in)) {
    retour = fscanf(chromatin_in, "%i %f %i %i %i %i %i\n", &chain, &f0, &s0,
                    &s1, &dock0, &dock1, &h1);
    // do not consider 0 bp linker
    if (f0 == .0)
      continue;
    if (((int)f0) == 147 && retour == 7) {
      nucleosome_start[inucl] = id;
      nucleosome_start[nucleosomes + inucl] = F + inucl * NHC;
      inucl++;
      id += NNC;
    } else
      id++;
  }
  fclose(chromatin_in);
  return nucleosome_start;
}

/*! @brief create a linear chain with overtwist.
  @param world dWorlID where to create the ring chain
  @param space dSpaceID where to create the ring chain
  @param gspace dSpaceID where to create the ghost ring chain (related to
  Verlet-list)
  @param N number of bonds
  @param bs list of 'dBodyID' objects (ring chain bodies)
  @param ds list of data attached to 'dBodyID'
  @param gs list of 'dGeomID' objects (ring chain geometries)
  @param ggs list of 'dGeomID' objects (ghost geometries)
  @param js list of 'dJointID' objects (ring chain joints)
  @param l length of the bond
  @param lbp length of the bond in bp
  @param r radius of the bond
  @param m mass of the bond
  @param xghost scale factor (related to Verlet-list)
  @param overtwist overtwist to inject in the ring chain
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
  @param mt Mersenne-Twister from std
*/
void create_linear_chain(dWorldID world, dSpaceID space, dSpaceID gspace, int N,
                         dBodyID **bs, int3p *&ds, dGeomID **gs, dGeomID **ggs,
                         dJointID **js, double l, double lbp, double r,
                         double m, double xghost, double overtwist,
                         int gyroscopic, std::mt19937_64 &mt) {
  std::uniform_real_distribution<> u1(.0, 1.);
  // joints feedback
  dJointFeedback *jfdna = new dJointFeedback[N - 1];
  // frames
  vec3 axe;
  vec3 *Ui = new vec3[N]();
  vec3 *Vi = new vec3[N]();
  vec3 *Ti = new vec3[N]();
  // linking number Lk
  double Lk0 = N * lbp / 10.5;
  double tw0 = sign_phase * 2. * pi * fmod(lbp, 10.5) / 10.5;
  if (tw0 > pi)
    tw0 -= 2. * pi;
  if (tw0 < (-pi))
    tw0 += 2. * pi;
  double Lk = overtwist * Lk0;
  double unif1, unif2, theta;
  // loop over the bond
  for (int i = 0; i < N; i++) {
    (*bs)[i] = b_capsule(world, space, (*gs)[i], r, l, m, gyroscopic);
    ds[i] = int3p(0, i, -1);
    dBodySetData((*bs)[i], &ds[i]);
    (*ggs)[i] = create_ghost_geometry(gspace, (*bs)[i], "capsule", "sphere",
                                      xghost, r, r, l);
    if (i == 0) {
      set_position((*bs)[i], vec3());
      Ui[i] = vec3(1., .0, .0);
      Vi[i] = vec3(.0, 1., .0);
      Ti[i] = vec3(.0, .0, 1.);
    } else {
      // parallel transport
      // small bending angle
      unif1 = 2. * arccos(-1.) * u1(mt);
      unif2 = arccos(2. * u1(mt) - 1.);
      axe = vec3(sin(unif2) * cos(unif1), sin(unif2) * sin(unif1), cos(unif2));
      theta = .02 * arccos(-1.) * (2. * u1(mt) - 1.);
      Ti[i] = rotation(axe, theta, Ti[i - 1]);
      // intrinsic twist + Lk
      Ui[i] =
          rotation(Ti[i - 1], 2. * pi * Lk / (double)(N - 1) + tw0, Ui[i - 1]);
      Vi[i] =
          rotation(Ti[i - 1], 2. * pi * Lk / (double)(N - 1) + tw0, Vi[i - 1]);
      axe = normalize3(cross_product(Ti[i - 1], Ti[i]));
      Ui[i] = rotation(axe, theta, Ui[i - 1]);
      Vi[i] = rotation(axe, theta, Vi[i - 1]);
      set_position((*bs)[i],
                   add3(PositionPosRelBodyODE((*bs)[i - 1], .0, .0, .5 * l),
                        mult3(.5 * l, Ti[i])));
    }
    set_axes((*bs)[i], Ti[i], Ui[i], Vi[i]);
    // joints
    if (i > 0) {
      (*js)[i - 1] =
          ball_joint(world, (*bs)[i - 1], (*bs)[i],
                     PositionPosRelBodyODE((*bs)[i - 1], .0, .0, .5 * l));
      dJointSetFeedback((*js)[i - 1], &jfdna[i - 1]);
    }
  }
  delete[] Ui;
  delete[] Vi;
  delete[] Ti;
}

/*! @brief create a ring chain with overtwist.
  @param world dWorlID where to create the ring chain
  @param space dSpaceID where to create the ring chain
  @param gspace dSpaceID where to create the ghost ring chain (related to
  Verlet-list)
  @param N number of bonds
  @param bs list of 'dBodyID' objects (ring chain bodies)
  @param ds list of data attached to 'dBodyID'
  @param gs list of 'dGeomID' objects (ring chain geometries)
  @param ggs list of 'dGeomID' objects (ghost geometries)
  @param js list of 'dJointID' objects (ring chain joints)
  @param l length of the bond
  @param lbp length of the bond in bp
  @param r radius of the bond
  @param m mass of the bond
  @param xghost scale factor (related to Verlet-list)
  @param overtwist overtwist to inject in the ring chain
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
  @param conformation initial conformation in 'circular', 'linear'
*/
void create_ring_chain(dWorldID world, dSpaceID space, dSpaceID gspace, int N,
                       dBodyID **bs, int3p *&ds, dGeomID **gs, dGeomID **ggs,
                       dJointID **js, double l, double lbp, double r, double m,
                       double xghost, double overtwist, int gyroscopic,
                       char *conformation) {
  double d0, d1, ba;
  int L = ((N % 2) == 0) ? (N - 2 - 2) / 2 : (N - 2 - 3) / 2;
  // number of helix turns
  int T = -1;
  char hname[100];
  if (strstr(conformation, "double_helix") != NULL) {
    if ((N % 2) == 1) {
      printf("The number of bonds has to be even for "
             "conformation=='double_helix', exit.\n");
      exit(EXIT_FAILURE);
    }
    for (int n = 0; n < N; n++) {
      sprintf(hname, "double_helix%i", n);
      if (strcmp(hname, conformation) == 0) {
        T = n;
        break;
      }
    }
    if (T == -1) {
      printf("The conformation %s you ask for is not valid (chain made of %i "
             "bonds), exit.",
             conformation, N);
      exit(EXIT_FAILURE);
    }
  }
  // starting frame
  vec3 t0 = vec3(.0, .0, 1.);
  vec3 u0 = vec3(1., .0, .0);
  vec3 v0 = vec3(.0, 1., .0);
  // joints feedback
  dJointFeedback *jfdna = new dJointFeedback[N];
  // frames
  vec3 *Ui = new vec3[N]();
  vec3 *Vi = new vec3[N]();
  vec3 *Ti = new vec3[N]();
  vec3 bn;
  // linking number Lk
  double Lk0 = N * lbp / 10.5;
  double tw0 = sign_phase * 2. * pi * fmod(lbp, 10.5) / 10.5;
  if (tw0 > pi)
    tw0 -= 2. * pi;
  if (tw0 < (-pi))
    tw0 += 2. * pi;
  double Lk = overtwist * Lk0;
  // loop over the bond
  for (int i = 0; i < N; i++) {
    (*bs)[i] = b_capsule(world, space, (*gs)[i], r, l, m, gyroscopic);
    ds[i] = int3p(0, i, -1);
    dBodySetData((*bs)[i], &ds[i]);
    (*ggs)[i] = create_ghost_geometry(gspace, (*bs)[i], "capsule", "sphere",
                                      xghost, r, r, l);
    // circular
    if (strcmp(conformation, "circular") == 0) {
      if (i == 0) {
        set_position((*bs)[i], vec3());
        Ui[i] = vec3(1., .0, .0);
        Vi[i] = v0;
        Ti[i] = vec3(.0, .0, 1.);
      } else {
        // intrinsic twist + Lk
        Ti[i] = Ti[i - 1];
        Ui[i] =
            rotation(Ti[i], 2. * pi * Lk / (double)(N - 1) + tw0, Ui[i - 1]);
        Vi[i] =
            rotation(Ti[i], 2. * pi * Lk / (double)(N - 1) + tw0, Vi[i - 1]);
        // circle (parallel transport = no twist added)
        d0 = 2. * pi / (double)N;
        Ti[i] = rotation(v0, d0, Ti[i]);
        Ui[i] = rotation(v0, d0, Ui[i]);
        Vi[i] = rotation(v0, d0, Vi[i]);
      }
    }
    // linear (two parallel lines and closure at both extremity)
    // if the number of bonds is odd then,
    // two bonds closes the two lines on the right
    // and three bonds close the two lines on the left
    if (strcmp(conformation, "linear") == 0) {
      if (i == 0) {
        set_position((*bs)[i], vec3());
        Ui[i] = vec3(1., .0, .0);
        Vi[i] = v0;
        Ti[i] = vec3(.0, .0, 1.);
      } else {
        // intrinsic twist + Lk
        Ti[i] = Ti[i - 1];
        Ui[i] =
            rotation(Ti[i], 2. * pi * Lk / (double)(N - 1) + tw0, Ui[i - 1]);
        Vi[i] =
            rotation(Ti[i], 2. * pi * Lk / (double)(N - 1) + tw0, Vi[i - 1]);
        // circle (parallel transport = no twist added)
        d0 = .0;
        if (i == 2)
          d0 = .5 * pi;
        else {
          if ((N % 2) == 1) {
            if (i >= (2 + L) && i < (2 + L + 3))
              d0 = pi / (3. + 1.);
          } else {
            if (i == (2 + L) || i == (4 + L))
              d0 = .5 * pi;
          }
        }
        if (d0 != .0) {
          Ti[i] = rotation(v0, d0, Ti[i]);
          Ui[i] = rotation(v0, d0, Ui[i]);
          Vi[i] = rotation(v0, d0, Vi[i]);
        }
      }
    }
    // double helix (no local twist)
    if (strstr(conformation, "double_helix") != NULL) {
      if (i < L) {
        // first helix
        d0 = 2. * pi / ((double)L / (double)T);
        d1 = arcsin(r / l);
        if (i == 0) {
          // helix step
          Ti[i] = rotation(u0, d1, t0);
          Ui[i] = u0;
          Vi[i] = rotation(u0, d1, v0);
          // helix turn
          Ti[i] = rotation(v0, d0, Ti[i]);
          Ui[i] = rotation(v0, d0, Ui[i]);
          Vi[i] = rotation(v0, d0, Vi[i]);
          // position
          set_position((*bs)[i], mult3(.5 * l, Ti[i]));
        } else {
          // helix turn
          Ti[i] = rotation(v0, d0, Ti[i - 1]);
        }
      }
      if (i == L || i == (L + 1)) {
        // first straight line between two helices
        Ti[i] = mult3(-1., u0);
      }
      if (i >= (L + 2) && i < (L + 2 + L)) {
        // second helix
        d0 = -2. * pi / ((double)L / (double)T);
        d1 = -arcsin(r / l);
        // helix turn
        if (i == (L + 2)) {
          Ti[i] = rotation(u0, d1, t0);
          Ui[i] = u0;
          Vi[i] = rotation(u0, d1, v0);
        } else
          Ti[i] = rotation(v0, d0, Ti[i - 1]);
      }
      if (i == (L + 2 + L) || i == (L + 2 + L + 1)) {
        // second straight line between two helices
        Ti[i] = u0;
      }
      if (i > 0) {
        // no intrinsic twist, parallel transport
        bn = normalize3(cross_product(Ti[i - 1], Ti[i]));
        ba = arccos(dot_product(Ti[i - 1], Ti[i]));
        Ui[i] = rotation(bn, ba, Ui[i - 1]);
        Vi[i] = rotation(bn, ba, Vi[i - 1]);
      }
    }
    // position
    if (i > 0)
      set_position((*bs)[i],
                   add3(PositionPosRelBodyODE((*bs)[i - 1], 0., 0., .5 * l),
                        mult3(.5 * l, Ti[i])));
    // frame
    set_axes((*bs)[i], Ti[i], Ui[i], Vi[i]);
    if (i == 0) {
      printf("%i=\n", i);
      print3(PositionPosRelBodyODE((*bs)[i], 0., 0., -.5 * l));
    }
    if (i == (L - 1) || i == (2 + L - 1) || i == (2 + L) || i == (N - 1)) {
      printf("%i=\n", i);
      print3(PositionPosRelBodyODE((*bs)[i], 0., 0., .5 * l));
    }
    // joints
    if (i > 0) {
      (*js)[i - 1] =
          ball_joint(world, (*bs)[i - 1], (*bs)[i],
                     PositionPosRelBodyODE((*bs)[i - 1], 0., 0., .5 * l));
      dJointSetFeedback((*js)[i - 1], &jfdna[i - 1]);
    }
  }
  (*js)[N - 1] =
      ball_joint(world, (*bs)[N - 1], (*bs)[0],
                 PositionPosRelBodyODE((*bs)[N - 1], 0., 0., .5 * l));
  dJointSetFeedback((*js)[N - 1], &jfdna[N - 1]);
  delete[] Ui;
  delete[] Vi;
  delete[] Ti;
}

/*! @brief center of mass to 0,0,0 (objects in [S,E]).
  @param bs 'dBodyID' objects
  @param S start
  @param E end
 */
void remove_com(const dBodyID *bs, int S, int E) {
  int H = (int)(.5 * (S + E));
  vec3 com[2];
#pragma omp parallel sections num_threads(2)
  {
#pragma omp section
    {
      com[0] = vec3();
      for (int i = S; i < H; i++)
        com[0] = add3(com[0], PositionBodyODE(bs[i]));
    }
#pragma omp section
    {
      com[1] = vec3();
      for (int i = H; i <= E; i++)
        com[1] = add3(com[1], PositionBodyODE(bs[i]));
    }
  }
  com[0] = mult3(1. / (E - S + 1), add3(com[0], com[1]));
#pragma omp parallel sections
  {
#pragma omp section
    {
      for (int i = S; i < H; i++)
        set_position(bs[i], sub3(PositionBodyODE(bs[i]), com[0]));
    }
#pragma omp section
    {
      for (int i = H; i <= E; i++)
        set_position(bs[i], sub3(PositionBodyODE(bs[i]), com[0]));
    }
  }
}

/*! @brief center of mass motion to 0,0,0 (objects in [S,E]).
  @param bs 'dBodyID' objects
  @param S start
  @param E end
 */
void remove_com_motion(const dBodyID *bs, int S, int E) {
  int H = (int)(.5 * (S + E));
  vec3 vcom[2];
#pragma omp parallel sections num_threads(2)
  {
#pragma omp section
    {
      vcom[0] = vec3();
      for (int i = S; i < H; i++)
        vcom[0] = add3(vcom[0], LinVelBodyODE(bs[i]));
    }
#pragma omp section
    {
      vcom[1] = vec3();
      for (int i = H; i <= E; i++)
        vcom[1] = add3(vcom[1], LinVelBodyODE(bs[i]));
    }
  }
  vcom[0] = mult3(1. / (E - S + 1), add3(vcom[0], vcom[1]));
#pragma omp parallel sections num_threads(2)
  {
#pragma omp section
    {
      for (int i = S; i < H; i++)
        set3_velocities(bs[i], sub3(LinVelBodyODE(bs[i]), vcom[0]),
                        AngVelBodyODE(bs[i]));
    }
#pragma omp section
    {
      for (int i = H; i <= E; i++)
        set3_velocities(bs[i], sub3(LinVelBodyODE(bs[i]), vcom[0]),
                        AngVelBodyODE(bs[i]));
    }
  }
}

/*! @brief "Flying-Ice-Cube" correction
  @param bs array of dBodyID objects
  @param B number of objects
 */
void flying_ice_cube_correction(dBodyID *bs, int B) {
  double d0, wx, wy, wz, I3[9], inv_I3[9];
  vec3 ri;
  // com linear velocity
  vec3 vcom = vec3(.0, .0, .0);
  vec3 com = vec3(.0, .0, .0);
  for (int b = 0; b < B; b++) {
    vcom = add3(vcom, LinVelBodyODE(bs[b]));
    com = add3(com, PositionBodyODE(bs[b]));
  }
  vcom = mult3(1. / B, vcom);
  com = mult3(1. / B, com);
  // com linear velocity to 0,0,0
  for (int i = 0; i < B; i++)
    set3_velocities(bs[i], sub3(LinVelBodyODE(bs[i]), vcom),
                    AngVelBodyODE(bs[i]));
  // inertia
  for (int i = 0; i < 9; i++)
    I3[i] = 0.;
  for (int i = 0; i < B; i++) {
    ri = sub3(PositionBodyODE(bs[i]), com);
    I3[0] += ri.y * ri.y + ri.z * ri.z;
    I3[1] -= ri.y * ri.x;
    I3[2] -= ri.z * ri.x;
    I3[4] += ri.x * ri.x + ri.z * ri.z;
    I3[5] -= ri.z * ri.y;
    I3[8] += ri.x * ri.x + ri.y * ri.y;
  }
  inv_I3[0] = I3[5] * I3[5] - I3[4] * I3[8];
  inv_I3[1] = I3[1] * I3[8] - I3[2] * I3[5];
  inv_I3[2] = I3[2] * I3[4] - I3[1] * I3[5];
  inv_I3[4] = I3[2] * I3[2] - I3[0] * I3[8];
  inv_I3[5] = I3[0] * I3[5] - I3[1] * I3[2];
  inv_I3[8] = I3[1] * I3[1] - I3[0] * I3[4];
  // determinant
  d0 = I3[0] * I3[5] * I3[5] + I3[1] * I3[1] * I3[8] -
       2. * I3[1] * I3[2] * I3[5] + I3[2] * I3[2] * I3[4] -
       I3[0] * I3[4] * I3[8];
  vec3 Am = vec3(.0, .0, .0);
  if (d0 != 0.) {
    // angular momentum
    for (int i = 0; i < B; i++)
      Am = add3(Am, cross_product(sub3(PositionBodyODE(bs[i]), com),
                                  LinVelBodyODE(bs[i])));
    // w=I^-1l
    wx = (inv_I3[0] * Am.x + inv_I3[1] * Am.y + inv_I3[2] * Am.z) / d0;
    wy = (inv_I3[1] * Am.x + inv_I3[4] * Am.y + inv_I3[5] * Am.z) / d0;
    wz = (inv_I3[2] * Am.x + inv_I3[5] * Am.y + inv_I3[8] * Am.z) / d0;
    // angular momentum to 0,0,0
    Am = vec3(wx, wy, wz);
    for (int i = 0; i < B; i++)
      set3_velocities(
          bs[i],
          sub3(LinVelBodyODE(bs[i]),
               cross_product(Am, sub3(PositionBodyODE(bs[i]), com))),
          AngVelBodyODE(bs[i]));
  }
}

/*! @brief semi-implicit Euler integration of velocities
 */
void vsemi_implicit_euler(dBodyID *bs, double dt, int S, int E) {
  vec3 v, w;
  dMass mass;
  for (int i = S; i < E; i++) {
    dBodyGetMass(bs[i], &mass);
    v = add3(LinVelBodyODE(bs[i]), mult3(dt / mass.mass, ForceBodyODE(bs[i])));
    w = add3(AngVelBodyODE(bs[i]),
             mult3(dt, mult_33_3(inertia_t(bs[i], 1), TorqueBodyODE(bs[i]))));
    set3_velocities(bs[i], v, w);
  }
}

/*! @brief semi-implicit Euler integration of both positions and orientations
 */
void semi_implicit_euler(dBodyID *bs, double dt, int S, int E) {
  vec3 w, u, t;
  for (int i = S; i < E; i++) {
    set_position(bs[i],
                 add3(PositionBodyODE(bs[i]), mult3(dt, LinVelBodyODE(bs[i]))));
    w = AngVelBodyODE(bs[i]);
    u = FrameODE(bs[i], 0);
    u = normalize3(add3(u, mult3(dt, cross_product(w, u))));
    t = FrameODE(bs[i], 2);
    t = normalize3(add3(t, mult3(dt, cross_product(w, t))));
    set_axes(bs[i], t, u, normalize3(cross_product(t, u)));
    ZeroForceTorqueBodyODE(bs[i]);
  }
}

/*! @brief add force and torque from Langevin-Euler dynamics to 'dBodyID'
  objects.
  @param bs 'dBodyID' objects
  @param sigmaT coupling frequencies (translation) to the thermostat
  @param sigmaR coupling frequencies (rotation) to the thermostat
  @param dt time-step
  @param beta inverse of kBT
  @param S from body 'S' ...
  @param E ... to body 'E'
*/
void langevin_euler(dBodyID *bs, const double *sigmaT, const double *sigmaR,
                    double dt, double beta, int S, int E, std::mt19937_64 &e1) {
  vec3 v, w, fr;
  double st, ft, xt = 1. / (beta * dt);
  dMass mass;
  std::normal_distribution<double> nd(.0, 1.);
  for (int i = S; i <= E; i++) {
    // if(is_linker_nucleosome[i]>0)
    //   continue;
    dBodyGetMass(bs[i], &mass);
    // linear
    ft = sigmaT[i] * mass.mass;
    v = LinVelBodyODE(bs[i]);
    st = sqrt(2. * ft * xt);
    dBodyAddForce(bs[i], st * nd(e1) - ft * v.x, st * nd(e1) - ft * v.y,
                  st * nd(e1) - ft * v.z);
    // angular
    if (sigmaR[i] == .0)
      continue;
    fr = mult3(sigmaR[i], vec3(mass.I[0], mass.I[5], mass.I[10]));
    w = AngVel2BodyODE(bs[i]);
    dBodyAddRelTorque(bs[i], sqrt(2. * xt * fr.x) * nd(e1) - fr.x * w.x,
                      sqrt(2. * xt * fr.y) * nd(e1) - fr.y * w.y,
                      sqrt(2. * xt * fr.z) * nd(e1) - fr.z * w.z);
  }
}

/*! @brief add force and torque from Langevin-Euler global thermostat to
  'dBodyID' object.
  @param b 'dBodyID' object
  @param m mass
  @param i principal inertia momenta
  @param gg global thermostat factor
*/
void langevin_euler_global(dBodyID b, double m, vec3 i, vec3 gg) {
  vec3 a, v, w, g;
  // linear motion
  v = mult3(gg.x * m, LinVelBodyODE(b));
  dBodyAddForce(b, v.x, v.y, v.z);
  // angular motion
  g = vec3(0., 0., 0.);
  w = AngVelBodyODE(b);
  a = FrameODE(b, 0);
  g = add3(g, mult3(gg.y * i.x * dot_product(w, a), a));
  a = FrameODE(b, 1);
  g = add3(g, mult3(gg.y * i.y * dot_product(w, a), a));
  a = FrameODE(b, 2);
  g = add3(g, mult3(gg.z * i.z * dot_product(w, a), a));
  addTorqueBodyODE(b, g);
}

/*! @brief rescale the linear and angular velocities.
  @param bodies list of dBodyID objects
*/
void global_rescaling(const dBodyID *bodies, int start, int end, int dofs,
                      double kBT, double sigma, double dt,
                      std::mt19937_64 &std_mt) {
  // J*V(t+h)=-erp*delta/dt-cfm*lambda
  // J*V+dt*J*M^-1*J^T*lambda+dt*J*M^-1*(Fext-diff(M,t)*V)=-erp*delta/dt-cfm*lambda
  double cglobal = exp(-2. * dt * sigma);
  double ket = Ktr(bodies, start, end, false, 2);
  double mke = .5 * dofs * kBT;
  std::normal_distribution<> nd(.0, 1.);
  // "Sglobal" is approximate because we suppose "dofs" -> inf
  // "dofs" has to be > 10000 (at least) to use this approximation
  double Rglobal = nd(std_mt), Sglobal = .0;
  // check Sglobal approximation
  if (Sglobal < .0) {
    printf("Sglobal<0, exit.\n");
    exit(EXIT_FAILURE);
  }
  if (dofs < 10000) {
    // brute force computation of "Sglobal"
    for (int i = 0; i < (dofs - 1); i++)
      Sglobal += pow(nd(std_mt), 2.);
  } else
    Sglobal = sqrt(2 * (dofs - 1)) * nd(std_mt) + (dofs - 1);
  double alpha = sqrt(
      cglobal +
      ((1. - cglobal) * (Sglobal + Rglobal * Rglobal) * mke) / (dofs * ket) +
      2 * Rglobal * sqrt(cglobal * (1. - cglobal) * mke / (dofs * ket)));
  double sign_alpha =
      1. - 2. * (int)((Rglobal + sqrt(cglobal * dofs * ket /
                                      ((1. - cglobal) * mke))) < .0);
  alpha *= sign_alpha;
  // printf("alpha=%f\n",alpha);
  for (int i = start; i <= end; i++)
    set3_velocities(bodies[i], mult3(alpha, LinVelBodyODE(bodies[i])),
                    mult3(alpha, AngVelBodyODE(bodies[i])));
}

/*! @brief add force and torque to the magnetic bead.
  @param b bead 'dBodyID'
  @param force force along the z-axis
  @param torque torque around the z-axis
  @param locked do we lock the bead rotation around the z-axis ?
*/
void bead(dBodyID b, double force, double torque, bool locked) {
  vec3 u1, v1, t1, bn;
  double theta;
  // add force and torque to the bead
  dBodyAddForce(b, 0., 0., force);
  // fixed the bead number of turns ?
  if (locked) {
    set_axes(b, vec3(1., .0, .0), vec3(.0, 1., .0), vec3(.0, .0, 1.));
    dBodySetAngularVel(b, 0., 0., 0.);
  } else {
    t1 = FrameODE(b, 2);
    theta = arccos(t1.z);
    bn = normalize3(cross_product(t1, vec3(0., 0., 1.)));
    t1 = rotation(bn, theta, t1);
    u1 = rotation(bn, theta, FrameODE(b, 0));
    v1 = rotation(bn, theta, FrameODE(b, 1));
    set_axes(b, t1, u1, v1);
    dBodySetAngularVel(b, 0., 0., AngVelBodyODE(b).z);
    dBodyAddTorque(b, 0., 0., torque);
  }
}

/*! @brief bending and twisting for each joints.
  @param lk Kuhn length
  @param lt twist persistence length
  @param l length of the bonds, N bonds
  @param gb bending strength
  @param gt twisting strength
  @param joints number of joints J with J<N
  @param beta inverse of kBT
  @param linear is the chain linear ?
*/
void bending_twisting_constants(double lk, double lt, double *l, double **gb,
                                double **gt, int joints, double beta,
                                bool linear) {
  double d0;
  delete[] * gb;
  *gb = new double[joints]();
  delete[] * gt;
  *gt = new double[joints]();
  // loop over the joints, bending energy is like 1-cos(theta) and twisting
  // energy is like Tw^2
  for (int j = 0; j < joints; j++) {
    // d0=lk/(.5*(l[j]+l[j+1]));
    d0 = lk / fmin(l[j], l[j + 1]);
    (*gb)[j] =
        dichotomy(flangevin, (d0 - 1.) / (d0 + 1.), 1e-12, 1e4, 1e-12) / beta;
    // (*gt)[j]=lt/(.5*(l[j]+l[j+1])*beta);
    (*gt)[j] = lt / (fmin(l[j], l[j + 1]) * beta);
  }
  // closure
  if (!linear) {
    // d0=lk/(.5*(l[joints]+l[0]));
    d0 = lk / fmin(l[joints], l[0]);
    (*gb)[joints] =
        dichotomy(flangevin, (d0 - 1.) / (d0 + 1.), 1e-12, 1e4, 1e-12) / beta;
    // (*gt)[joints]=lt/(.5*(l[joints]+l[0])*beta);
    (*gt)[joints] = lt / (fmin(l[joints], l[0]) * beta);
  }
}

/*! @brief add bending and twisting torques between 'dBodyID' object and ground.
  last torques is between object 'E-1' and object 'E'.
  return the cosine of bending and twisting angle.
  @param b 'dBodyID' object
  @param gb bending strength
  @param gt twisting strength
  @param u0 first vector of ground frame
  @param v0 second vector of ground frame
  @param t0 third vector of ground frame
*/
vec2 ground_bending_twist(dBodyID b, double gb, double gt, vec3 u0, vec3 v0,
                          vec3 t0) {
  vec3 t1 = FrameODE(b, 2);
  vec3 u1 = FrameODE(b, 0);
  vec3 v1 = FrameODE(b, 1);
  double Tw = sign(dot_product(u1, v0) - dot_product(v1, u0)) *
              arccos((dot_product(u1, u0) + dot_product(v1, v0)) /
                     (1. + dot_product(t0, t1)));
  vec3 cc = mult3(gt * Tw / (1. + dot_product(t0, t1)), add3(t0, t1));
  cc = add3(cc, mult3(gb, cross_product(t0, t1)));
  dBodyAddTorque(b, -cc.x, -cc.y, -cc.z);
  return vec2(dot_product(t0, t1), Tw * Tw);
}

/*! @brief return curvature between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
  @param l1 bond length (first object)
  @param l2 bond length (second object)
*/
double local_curvature(dBodyID bs1, dBodyID bs2, double l1, double l2) {
  vec3 t1 = FrameODE(bs1, 2);
  vec3 t2 = FrameODE(bs2, 2);
  return sqrt(2. * (1. - dot_product(t1, t2))) / (.5 * (l1 + l2));
}

/*! @brief return chain curvature (sum of local curvature) between I and S.
  @param bs 'dBodyID' objects
  @param lbond array of bond lengths
  @param N number of bonds
  @param I compute curvature between bond 'I' ...
  @param S ... and bond 'S'
  @param normalize if true return average of local curvature
*/
double sum_of_local_curvature(const dBodyID *bs, const double *lb, int N, int I,
                              int S, bool normalize) {
  double sum_curvature = .0;
  int i0 = I, i1, counts = 0;
  while (i0 != S) {
    i1 = i0 + 1 - N * ((i0 + 1) >= N);
    sum_curvature += sqrt(2. * (1. - dot_product(FrameODE(bs[i0], 2),
                                                 FrameODE(bs[i1], 2)))) /
                     (.5 * (lb[i0] + lb[i1]));
    i0 = i0 + 1 - N * ((i0 + 1) >= N);
    counts++;
  }
  return sum_curvature / ((normalize) ? (double)counts : 1.);
}

/*! @brief return cosine of bending between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
*/
double local_cosine_of_bending(dBodyID bs1, dBodyID bs2) {
  return dot_product(FrameODE(bs1, 2), FrameODE(bs2, 2));
}

/*! @brief return twist between two dBodyID objects.
  @param bs1 'dBodyID' object
  @param bs2 'dBodyID' object
  @param tw0 intrinsic twist
*/
double local_twist(dBodyID bs1, dBodyID bs2, double tw0) {
  vec3 u1 = FrameODE(bs1, 0);
  vec3 u2 = FrameODE(bs2, 0);
  vec3 v1 = FrameODE(bs1, 1);
  vec3 v2 = FrameODE(bs2, 1);
  double ct_p1 = 1. + dot_product(FrameODE(bs1, 2), FrameODE(bs2, 2));
  double ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
  double stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
  double tw = sign(stw) * arccos(ctw);
  return tw - tw0 - 2. * pi * ((tw - tw0) > pi) +
         2. * pi * ((tw - tw0) < (-pi));
}

/*! @brief return chain twist (sum of local twist) between I and S.
  @param bs 'dBodyID' objects
  @param N number of bonds
  @param I compute twist between bond 'I' ...
  @param S ... and bond 'S'
  @param tw0 array of local intrinsic twist
*/
double sum_of_local_twist(const dBodyID *bs, int N, int I, int S,
                          const double *tw0) {
  vec3 u1, u2, v1, v2;
  double ct_p1, ctw, stw, dtw, sum_tw = .0;
  int i0 = I, i1;
  while (i0 != S) {
    i1 = i0 + 1 - N * ((i0 + 1) >= N);
    u1 = FrameODE(bs[i0], 0);
    u2 = FrameODE(bs[i1], 0);
    v1 = FrameODE(bs[i0], 1);
    v2 = FrameODE(bs[i1], 1);
    ct_p1 = 1. + dot_product(FrameODE(bs[i0], 2), FrameODE(bs[i1], 2));
    ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
    stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
    dtw = stw * ctw - ((tw0 == NULL) ? .0 : tw0[i0]);
    sum_tw += dtw - 2. * pi * (dtw > pi) + 2. * pi * (dtw < (-pi));
    i0 = i0 + 1 - N * ((i0 + 1) >= N);
  }
  return sum_tw;
}

/*! @brief return local twist between I and S.
  @param bs 'dBodyID' objects
  @param N number of bonds
  @param I compute twist between bond 'I' ...
  @param S ... and bond 'S'
  @param tws to store local twist
*/
void chain_local_twist(const dBodyID *bs, int N, int I, int S, double *&tws) {
  vec3 t1, t2, u1, u2, v1, v2;
  double ct_p1, ctw, stw;
  int i0 = I, i1, counts = 0;
  while (i0 != S) {
    i1 = i0 + 1 - N * ((i0 + 1) >= N);
    // bending
    t1 = FrameODE(bs[i0], 2);
    t2 = FrameODE(bs[i1], 2);
    // twist
    u1 = FrameODE(bs[i0], 0);
    u2 = FrameODE(bs[i1], 0);
    v1 = FrameODE(bs[i0], 1);
    v2 = FrameODE(bs[i1], 1);
    ct_p1 = 1. + dot_product(t1, t2);
    ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
    stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
    // printf("%i/%i %i %i\n",i0,N,I,S);
    // printf("%i %i/%i\n",i0,counts,S);
    tws[i0] = sign(stw) * arccos(ctw);
    i0 = i0 + 1 - N * ((i0 + 1) >= N);
    counts++;
  }
}

/*! @brief return local torsion between 'I' and 'S'.
  @param bs 'dBodyID' objects
  @param lb array of bond lengths
  @param N number of bonds
  @param I compute torsion between bond 'I' ...
  @param S ... and bond 'S'
  @param torsion to store local torsion
*/
void chain_local_torsion(const dBodyID *bs, const double *lb, int N, int I,
                         int S, double *&torsion) {
  int is = (I < S) ? S - I + 1 : N - 1 - I + 1 + S + 1;
  vec3 *bn = new vec3[is - 1]();
  int i0 = I, i1, nc = 0;
  while (i0 != S) {
    i1 = i0 + 1 - N * ((i0 + 1) >= N);
    bn[nc] =
        normalize3(cross_product(FrameODE(bs[i0], 2), FrameODE(bs[i1], 2)));
    i0 = i0 + 1 - N * ((i0 + 1) >= N);
    nc++;
  }
  for (int i = 0; i < (is - 2); i++)
    torsion[i] = sqrt(2. * (1. - dot_product(bn[i], bn[i + 1]))) /
                 (.5 * (lb[i] + lb[i + 1]));
  delete[] bn;
  bn = NULL;
}

/*! @brief add bending and twisting torques between 'dBodyID' object 1 and
  'dBodyID' object 2. last torques is between object 'E-1' and object 'E'.
  return (by reference) the cosine average of bending and square twisting angle
  average.
  @param bs 'dBodyID' objects
  @param gb bending strength
  @param gt twisting strength
  @param xg multiply bending and twisting strengths with
  @param be bending equilibrium angle
  @param twe twisting equilibrium angle
  @param S start of the half-open interval
  @param E end of the half-open interval
  @param linear is the chain linear ? if yes add torques between object 'E' and
  object 'S'
*/
void bending_twist(const dBodyID *bs, const double *gb, const double *gt,
                   const double xg, const double *be, const double *twe, int S,
                   int E, bool linear, double &mcos, double &mtw2) {
  vec3 t1, t2, u1, u2, v1, v2, ggbt;
  double tt, ct_p1, ctw, stw, tw, dtw, cum_cos[2], cum_tw2[2];
  int H = (int)(.5 * (S + E)), J[2];
#pragma omp parallel sections private(t1, t2, ggbt, u1, u2, v1, v2, ct_p1,     \
                                      ctw, stw, tw) num_threads(2)
  {
#pragma omp section
    {
      cum_cos[0] = .0;
      cum_tw2[0] = .0;
      J[0] = 0;
      for (int i = S; i < (H - 1); i++) {
        // bending
        t1 = FrameODE(bs[i], 2);
        t2 = FrameODE(bs[i + 1], 2);
        tt = dot_product(t1, t2);
        if (be[i] != .0)
          ggbt = (tt > .999) ? vec3(.0, .0, .0)
                             : mult3(xg * gb[i] * sin(arccos(tt) - be[i]) /
                                         sqrt(1. - tt * tt),
                                     cross_product(t1, t2));
        else
          ggbt = mult3(xg * gb[i], cross_product(t1, t2));
        // twist
        u1 = FrameODE(bs[i], 0);
        u2 = FrameODE(bs[i + 1], 0);
        v1 = FrameODE(bs[i], 1);
        v2 = FrameODE(bs[i + 1], 1);
        ct_p1 = 1. + tt;
        ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
        stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
        tw = sign(stw) * arccos(ctw);
        dtw = tw - twe[i];
        // tw is in ]-pi,pi[
        // consider the following case
        // tw change sign in one time-step, from pi-a to -pi+b (a>0, b>0)
        // intrinsic twist is a constant tw0=pi-c, c>0
        // before: tw-tw0=-a+c
        // after : tw-tw0=-2*pi+b+c
        dtw = dtw - 2. * pi * (dtw > pi) + 2. * pi * (dtw < (-pi));
        // printf("d=%f(%f %f)\n",dtw,tw,twe[i]);
        ggbt = add3(ggbt, mult3(xg * gt[i] * dtw / ct_p1, add3(t1, t2)));
        // torques
        addTorqueBodyODE(bs[i], ggbt);
        addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
        cum_tw2[0] += dtw * dtw;
        cum_cos[0] += ct_p1 - 1.;
        J[0]++;
      }
    }
#pragma omp section
    {
      cum_cos[1] = .0;
      cum_tw2[1] = .0;
      J[1] = 0;
      for (int i = H; i < E; i++) {
        // bending
        t1 = FrameODE(bs[i], 2);
        t2 = FrameODE(bs[i + 1], 2);
        tt = dot_product(t1, t2);
        if (be[i] != .0)
          ggbt = (tt > .999) ? vec3(.0, .0, .0)
                             : mult3(xg * gb[i] * sin(arccos(tt) - be[i]) /
                                         sqrt(1. - tt * tt),
                                     cross_product(t1, t2));
        else
          ggbt = mult3(xg * gb[i], cross_product(t1, t2));
        // twist
        u1 = FrameODE(bs[i], 0);
        u2 = FrameODE(bs[i + 1], 0);
        v1 = FrameODE(bs[i], 1);
        v2 = FrameODE(bs[i + 1], 1);
        ct_p1 = 1. + tt;
        ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
        stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
        tw = sign(stw) * arccos(ctw);
        dtw = tw - twe[i];
        dtw = dtw - 2. * pi * (dtw > pi) + 2. * pi * (dtw < (-pi));
        // printf("d=%f(%f %f)\n",dtw,tw,twe[i]);
        ggbt = add3(ggbt, mult3(xg * gt[i] * dtw / ct_p1, add3(t1, t2)));
        // torques
        addTorqueBodyODE(bs[i], ggbt);
        addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
        cum_tw2[1] += dtw * dtw;
        cum_cos[1] += ct_p1 - 1.;
        J[1]++;
      }
    }
  }
  // middle joint
  for (int i = (H - 1); i < H; i++) {
    // bending
    t1 = FrameODE(bs[i], 2);
    t2 = FrameODE(bs[i + 1], 2);
    tt = dot_product(t1, t2);
    if (be[i] != .0)
      ggbt =
          (tt > .999)
              ? vec3(.0, .0, .0)
              : mult3(xg * gb[i] * sin(arccos(tt) - be[i]) / sqrt(1. - tt * tt),
                      cross_product(t1, t2));
    else
      ggbt = mult3(xg * gb[i], cross_product(t1, t2));
    // twist
    u1 = FrameODE(bs[i], 0);
    u2 = FrameODE(bs[i + 1], 0);
    v1 = FrameODE(bs[i], 1);
    v2 = FrameODE(bs[i + 1], 1);
    ct_p1 = 1. + tt;
    ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
    stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
    tw = sign(stw) * arccos(ctw);
    dtw = tw - twe[i];
    dtw = dtw - 2. * pi * (dtw > pi) + 2. * pi * (dtw < (-pi));
    // printf("d=%f(%f %f)\n",dtw,tw,twe[i]);
    ggbt = add3(ggbt, mult3(xg * gt[i] * dtw / ct_p1, add3(t1, t2)));
    // torques
    addTorqueBodyODE(bs[i], ggbt);
    addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
    cum_tw2[0] += dtw * dtw;
    cum_cos[0] += ct_p1 - 1.;
    J[0]++;
  }
  // closure
  if (!linear) {
    t1 = FrameODE(bs[E], 2);
    t2 = FrameODE(bs[S], 2);
    tt = dot_product(t1, t2);
    if (be[E] != .0)
      ggbt =
          (tt > .999)
              ? vec3(.0, .0, .0)
              : mult3(xg * gb[E] * sin(arccos(tt) - be[E]) / sqrt(1. - tt * tt),
                      cross_product(t1, t2));
    else
      ggbt = mult3(xg * gb[E], cross_product(t1, t2));
    u1 = FrameODE(bs[E], 0);
    u2 = FrameODE(bs[S], 0);
    v1 = FrameODE(bs[E], 1);
    v2 = FrameODE(bs[S], 1);
    ct_p1 = 1. + tt;
    ctw = (dot_product(u2, u1) + dot_product(v2, v1)) / ct_p1;
    stw = (dot_product(u2, v1) - dot_product(v2, u1)) / ct_p1;
    tw = sign(stw) * arccos(ctw);
    dtw = tw - twe[E];
    dtw = dtw - 2. * pi * (dtw > pi) + 2. * pi * (dtw < (-pi));
    ggbt = add3(ggbt, mult3(xg * gt[E] * dtw / ct_p1, add3(t1, t2)));
    addTorqueBodyODE(bs[E], ggbt);
    addTorqueBodyODE(bs[S], mult3(-1., ggbt));
    cum_tw2[1] += dtw * dtw;
    cum_cos[1] += ct_p1 - 1.;
    J[1]++;
  }
  mcos = (cum_cos[0] + cum_cos[1]) / (J[0] + J[1]);
  mtw2 = (cum_tw2[0] + cum_tw2[1]) / (J[0] + J[1]);
}

/*! @brief add intrinsic bending between 'dBodyID' object 1 and 'dBodyID'
  object 2. last torques is between object 'E-1' and object 'E'. return (by
  reference) the cosine average of bending and square twisting angle average.
  @param bs 'dBodyID' objects
  @param gb bending strength
  @param is_linker_nucleosome negative values if linker DNA
  @param S start of the half-open interval
  @param E end of the half-open interval
  @param linear is the chain linear ? if yes add torques between object 'E' and
  object 'S'
*/
void add_intrinsic_bending(const dBodyID *bs, const double gb,
                           const int *is_linker_nucleosome, int S, int E,
                           bool linear) {
  vec3 t1, t2, ggbt;
  int H = (int)(.5 * (S + E));
  double eps = 1e-6;
#pragma omp parallel sections private(t1, t2, ggbt) num_threads(2)
  {
#pragma omp section
    {
      for (int i = S; i < (H - 1); i++) {
        if ((is_linker_nucleosome[i] >= 0 && is_linker_nucleosome[i + 1] < 0) ||
            is_linker_nucleosome[i] < 0) {
          // bending
          t1 = FrameODE(bs[i], 2);
          t2 = FrameODE(bs[i + 1], 2);
          if (dot_product(t1, t2) < (1. - eps) &&
              dot_product(t1, t2) > (eps - 1.))
            ggbt = mult3(gb, normalize3(cross_product(t1, t2)));
          else
            ggbt = mult3(gb, FrameODE(bs[i], 0));
          // torques
          addTorqueBodyODE(bs[i], ggbt);
          addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
        }
      }
    }
#pragma omp section
    {
      for (int i = H; i < E; i++) {
        if ((is_linker_nucleosome[i] >= 0 && is_linker_nucleosome[i + 1] < 0) ||
            is_linker_nucleosome[i] < 0) {
          // bending
          t1 = FrameODE(bs[i], 2);
          t2 = FrameODE(bs[i + 1], 2);
          if (dot_product(t1, t2) < (1. - eps) &&
              dot_product(t1, t2) > (eps - 1.))
            ggbt = mult3(gb, normalize3(cross_product(t1, t2)));
          else
            ggbt = mult3(gb, FrameODE(bs[i], 0));
          // torques
          addTorqueBodyODE(bs[i], ggbt);
          addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
        }
      }
    }
  }
  // middle joint
  for (int i = (H - 1); i < H; i++) {
    if ((is_linker_nucleosome[i] >= 0 && is_linker_nucleosome[i + 1] < 0) ||
        is_linker_nucleosome[i] < 0) {
      // bending
      t1 = FrameODE(bs[i], 2);
      t2 = FrameODE(bs[i + 1], 2);
      if (dot_product(t1, t2) < (1. - eps) && dot_product(t1, t2) > (eps - 1.))
        ggbt = mult3(gb, normalize3(cross_product(t1, t2)));
      else
        ggbt = mult3(gb, FrameODE(bs[i], 0));
      // torques
      addTorqueBodyODE(bs[i], ggbt);
      addTorqueBodyODE(bs[i + 1], mult3(-1., ggbt));
    }
  }
  // closure
  if (!linear) {
    if ((is_linker_nucleosome[E] >= 0 && is_linker_nucleosome[S] < 0) ||
        is_linker_nucleosome[E] < 0) {
      t1 = FrameODE(bs[E], 2);
      t2 = FrameODE(bs[S], 2);
      if (dot_product(t1, t2) < (1. - eps) && dot_product(t1, t2) > (eps - 1.))
        ggbt = mult3(gb, normalize3(cross_product(t1, t2)));
      else
        ggbt = mult3(gb, FrameODE(bs[E], 0));
      addTorqueBodyODE(bs[E], ggbt);
      addTorqueBodyODE(bs[S], mult3(-1., ggbt));
    }
  }
}

/*! return total kinetic energy of the half-open interval [S,E].
  @param bs 'dBodyID' objects
  @param S start of the interval
  @param E end of the interval
  @param remove_com does the function remove com motion ?
  @param tr 0 returns kinetic energy, 1 returns rotational kinetic energy, 2
  returns total kinetic energy
*/
double Ktr(const dBodyID *bs, int S, int E, bool remove_com, int tr) {
  double T[2], R[2];
  dMass mass;
  vec3 v, w;
  int H = (int)(.5 * (S + E));
  // kinetic energy
#pragma omp parallel sections private(mass, v, w) num_threads(2)
  {
#pragma omp section
    {
      T[0] = .0;
      R[0] = .0;
      for (int i = S; i < H; i++) {
        dBodyGetMass(bs[i], &mass);
        v = LinVelBodyODE(bs[i]);
        T[0] += mass.mass * dot_product(v, v);
        w = AngVel2BodyODE(bs[i]);
        R[0] += (mass.I[0] * w.x * w.x + mass.I[5] * w.y * w.y +
                 mass.I[10] * w.z * w.z);
      }
    }
#pragma omp section
    {
      T[1] = .0;
      R[1] = .0;
      for (int i = H; i <= E; i++) {
        dBodyGetMass(bs[i], &mass);
        v = LinVelBodyODE(bs[i]);
        T[1] += mass.mass * dot_product(v, v);
        w = AngVel2BodyODE(bs[i]);
        R[1] += (mass.I[0] * w.x * w.x + mass.I[5] * w.y * w.y +
                 mass.I[10] * w.z * w.z);
      }
    }
  }
  // com motion to 0,0,0 ?
  if (remove_com)
    remove_com_motion(bs, S, E);
  return .5 * ((T[0] + T[1]) * int(tr == 0 || tr == 2) +
               (R[0] + R[1]) * int(tr == 1 || tr == 2));
}

/*! @brief assign position (x,y,z) and quaternion (w,i,j,k) to dBodyID 'b'.
  @param b 'dBodyID' object
  @param x x-coordinate
  @param y y-coordinate
  @param z z-coordinate
  @param w angle
  @param i i-axis
  @param j j-axis
  @param k k-axis
*/
void set_pos_quat(dBodyID b, vec3 p, double w, double i, double j, double k) {
  dQuaternion q;
  dBodySetPosition(b, p.x, p.y, p.z);
  q[0] = w;
  q[1] = i;
  q[2] = j;
  q[3] = k;
  dBodySetQuaternion(b, q);
}

/*! @brief assign linear and angular velocities.
  @param b 'dBodyID' object
*/
void set_velocities(dBodyID b, double vx, double vy, double vz, double wx,
                    double wy, double wz) {
  dBodySetLinearVel(b, vx, vy, vz);
  dBodySetAngularVel(b, wx, wy, wz);
}

/*! @brief assign linear and angular velocities.
  @param b 'dBodyID' object
  @param v linear velocity
  @param w angular velocity
*/
void set3_velocities(dBodyID b, vec3 v, vec3 w) {
  dBodySetLinearVel(b, v.x, v.y, v.z);
  dBodySetAngularVel(b, w.x, w.y, w.z);
}

/*! @brief assign position.
  @param b 'dBodyID' object
  @param r position
*/
void set_position(dBodyID b, vec3 r) { dBodySetPosition(b, r.x, r.y, r.z); }

/*! @brief assign axes to body.
  @param b 'dBodyID' object
  @param t tangent
  @param u normal
  @param v cross(t,u)
*/
void set_axes(dBodyID b, vec3 t, vec3 u, vec3 v) {
  dMatrix3 r;
  r[0] = u.x;
  r[4] = u.y;
  r[8] = u.z;
  r[1] = v.x;
  r[5] = v.y;
  r[9] = v.z;
  r[2] = t.x;
  r[6] = t.y;
  r[10] = t.z;
  r[3] = 0.;
  r[7] = 0.;
  r[11] = 0.;
  dBodySetRotation(b, r);
}

/*! @brief copy only the bodies to new world (no joints)
 */
void copy_system_with_no_joints(dWorldID world, const dBodyID *bs,
                                dBodyID **bs_copy, int B) {
  dMass mb, *mass = new dMass[B];
  for (int b = 0; b < B; b++) {
    dBodyGetMass(bs[b], &mb);
    dMassSetParameters(&mass[b], mb.mass, mb.c[0], mb.c[1], mb.c[2], mb.I[0],
                       mb.I[5], mb.I[10], mb.I[1], mb.I[2], mb.I[6]);
    (*bs_copy)[b] = dBodyCreate(world);
    dBodySetMass((*bs_copy)[b], &mass[b]);
    dBodySetFiniteRotationMode((*bs_copy)[b], 1);
    dBodySetFiniteRotationAxis((*bs_copy)[b], .0, .0, .0);
    dBodySetGyroscopicMode((*bs_copy)[b], 1);
  }
}

/*! @brief copy state of each body
  @param bs bodies used to step the world without contact joints
  @param copy_bs bodies used to step the world with only contact joints
  @param B number of bodies
  @param method it has to be 1
 */
bool copy_state_body(dBodyID **bs, dBodyID **copy_bs, int B, int method) {
  int J = 0;
  bool is_contact = false;
  for (int b = 0; b < B; b++) {
    set_position((*copy_bs)[b], PositionBodyODE((*bs)[b]));
    set_axes((*copy_bs)[b], FrameODE((*bs)[b], 2), FrameODE((*bs)[b], 0),
             FrameODE((*bs)[b], 1));
    set3_velocities((*copy_bs)[b], LinVelBodyODE((*bs)[b]),
                    AngVelBodyODE((*bs)[b]));
    // resolve the collisions in the case of no external forces
    ZeroForceTorqueBodyODE((*copy_bs)[b]);
    // addForceBodyODE((*copy_bs)[b],ForceBodyODE((*bs)[b]));
    // addTorqueBodyODE((*copy_bs)[b],TorqueBodyODE((*bs)[b]));
    if (!is_contact) {
      J = dBodyGetNumJoints((*bs)[b]);
      for (int j = 0; j < J; j++)
        is_contact = (is_contact || dJointGetType(dBodyGetJoint((*bs)[b], j)) ==
                                        dJointTypeContact);
    }
    dBodyDisable((*copy_bs)[b]);
  }
  // move contact joint(s) to the copies
  int3p d1, d2;
  if (is_contact) {
    if (method == 0) {
      // disable articulated system
      for (int b = 0; b < B; b++) {
        J = dBodyGetNumJoints((*bs)[b]);
        for (int j = 0; j < J; j++) {
          if (dJointGetType(dBodyGetJoint((*bs)[b], j)) != dJointTypeContact)
            dJointDisable(dBodyGetJoint((*bs)[b], j));
        }
      }
    } else {
      for (int b = 0; b < B; b++) {
        // disable body (articulated system)
        dBodyDisable((*bs)[b]);
        // enable copy of the body
        dBodyEnable((*copy_bs)[b]);
        // find the contact joints
        J = dBodyGetNumJoints((*bs)[b]);
        for (int j = (J - 1); j >= 0; j--) {
          if (dJointGetType(dBodyGetJoint((*bs)[b], j)) == dJointTypeContact) {
            // attach the contact joint
            d1 = *(int3p *)dBodyGetData(
                dJointGetBody(dBodyGetJoint((*bs)[b], j), 0));
            d2 = *(int3p *)dBodyGetData(
                dJointGetBody(dBodyGetJoint((*bs)[b], j), 1));
            dJointAttach(dBodyGetJoint((*bs)[b], j), (*copy_bs)[d1.j],
                         (*copy_bs)[d2.j]);
          }
        }
      }
    }
  }
  return is_contact;
}

/*! @brief step the world with contact joints only and get the feedback
  @param world world
  @param bs bodies used to step the world without contact joints
  @param copy_bs bodies used to step the world with only contact joints
  @param B number of bodies
  @param dt time-step
 */
void step_the_world_contact_joints(dWorldID world, dBodyID **bs,
                                   dBodyID **copy_bs, int B, double dt,
                                   int method) {
  int J = 0;
  dMass mass;
  // step the world with only the contact joints
  dWorldStep(world, dt);
  // get the feedback
  for (int b = 0; b < B; b++) {
    dBodyGetMass((*copy_bs)[b], &mass);
    // enable body
    dBodyEnable((*bs)[b]);
    // simple approximation of the feedback
    if (method == 0) {
      addForceBodyODE(
          (*bs)[b], mult3(mass.mass / dt, sub3(LinVelBodyODE((*bs)[b]),
                                               LinVelBodyODE((*copy_bs)[b]))));
      addTorqueBodyODE(
          (*bs)[b],
          mult_33_3(inertia_t((*copy_bs)[b], 0),
                    mult3(1. / dt, sub3(AngVelBodyODE((*bs)[b]),
                                        AngVelBodyODE((*copy_bs)[b])))));
      J = dBodyGetNumJoints((*bs)[b]);
      for (int j = (J - 1); j >= 0; j--) {
        if (dJointGetType(dBodyGetJoint((*bs)[b], j)) != dJointTypeContact)
          dJointEnable(dBodyGetJoint((*bs)[b], j));
        else
          dJointDestroy(dBodyGetJoint((*bs)[b], j));
      }
    } else {
      addForceBodyODE((*bs)[b],
                      mult3(mass.mass / dt, sub3(LinVelBodyODE((*copy_bs)[b]),
                                                 LinVelBodyODE((*bs)[b]))));
      addTorqueBodyODE(
          (*bs)[b], mult_33_3(inertia_t((*bs)[b], 0),
                              mult3(1. / dt, sub3(AngVelBodyODE((*copy_bs)[b]),
                                                  AngVelBodyODE((*bs)[b])))));
    }
    // disable copy
    ZeroForceTorqueBodyODE((*copy_bs)[b]);
    dBodyDisable((*copy_bs)[b]);
  }
}

/*! @brief create 'dBodyID' and its corresponding 'dGeomID' sphere.
  @param w world where to create the sphere body
  @param s space where to create the sphere geometry
  @param g 'dGeomID' object
  @param r radius
  @param m mass
*/
dBodyID b_sphere(dWorldID w, dSpaceID s, dGeomID &g, double r, double m) {
  dMass mid;
  dBodyID b = dBodyCreate(w);
  dMassSetSphereTotal(&mid, m, r);
  dBodySetMass(b, &mid);
  dBodySetFiniteRotationMode(b, 1);
  dBodySetFiniteRotationAxis(b, .0, .0, .0);
  dBodySetGyroscopicMode(b, 1);
  g = dCreateSphere(s, r);
  dGeomSetBody(g, b);
  return b;
}

/*! @brief returns 'dBodyID' and creates its corresponding 'dGeomID' capsule.
  @param w world where to create the capsule body
  @param s space where to create the capsule geometry
  @param g 'dGeomID' object
  @param r radius
  @param l length (without caps)
  @param m mass
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
*/
dBodyID b_capsule(dWorldID w, dSpaceID s, dGeomID &g, double r, double l,
                  double m, int gyroscopic) {
  dMass mid;
  dBodyID b = dBodyCreate(w);
  dMassSetCapsuleTotal(&mid, m, 3, r, l);
  dBodySetMass(b, &mid);
  dBodySetFiniteRotationMode(b, 1);
  dBodySetFiniteRotationAxis(b, .0, .0, .0);
  dBodySetGyroscopicMode(b, gyroscopic);
  g = dCreateCapsule(s, r, l);
  dGeomSetBody(g, b);
  return b;
}

/*! @brief create 'dBodyID' and its corresponding 'dGeomID' cylinder.
  @param w world where to create the cylinder body
  @param s space where to create the cylinder geometry
  @param g geometry passed by reference
  @param r radius
  @param l length
  @param m mass
  @param gyroscopic 0 : diff(inertia,t)=0, 1 : diff(inertia,t)!=0
*/
dBodyID b_cylinder(dWorldID w, dSpaceID s, dGeomID &g, double r, double l,
                   double m, int gyroscopic) {
  dMass mid;
  dBodyID b = dBodyCreate(w);
  dMassSetCylinderTotal(&mid, m, 3, r, l);
  dBodySetMass(b, &mid);
  dBodySetFiniteRotationMode(b, 1);
  dBodySetFiniteRotationAxis(b, .0, .0, .0);
  dBodySetGyroscopicMode(b, gyroscopic);
  g = dCreateCylinder(s, r, l);
  dGeomSetBody(g, b);
  return b;
}

/*! @brief return the inertia momenta with respect to the frame of dBodyID 'b'.
  @param b 'dBodyID' object
*/
vec3 inertia(dBodyID b) {
  dMass m;
  dBodyGetMass(b, &m);
  return vec3(m.I[0], m.I[5], m.I[10]);
}

/*! @brief return inertia matrix I(t) or its inverse.
  @param b 'dBodyID' object
  @param inverse 0 : I(t), 1 : I(t)^-1
*/
mat33 inertia_t(dBodyID b, int inverse) {
  mat33 mi;
  vec3 vi = inertia(b);
  if (inverse == 0) {
    mi.c1 = vec3(vi.x, 0., 0.);
    mi.c2 = vec3(0., vi.y, 0.);
    mi.c3 = vec3(0., 0., vi.z);
  } else {
    mi.c1 = vec3(1. / vi.x, 0., 0.);
    mi.c2 = vec3(0., 1. / vi.y, 0.);
    mi.c3 = vec3(0., 0., 1. / vi.z);
  }
  mat33 rot = RotationBodyODE(b);
  return mult33(rot, mult33(mi, mat3T(rot)));
}

/*! @brief return joint anchor
  @param j dJointID
  @param t add vec3 t to joint anchor
*/
double *get_joint_anchor(dJointID j, vec3 t) {
  double *anchor = new double[3];
  dVector3 result;
  if (dJointGetType(j) == dJointTypeBall)
    dJointGetBallAnchor(j, result);
  if (dJointGetType(j) == dJointTypeHinge)
    dJointGetHingeAnchor(j, result);
  if (dJointGetType(j) == dJointTypeUniversal)
    dJointGetUniversalAnchor(j, result);
  anchor[0] = result[0] + t.x;
  anchor[1] = result[1] + t.y;
  anchor[2] = result[2] + t.z;
  return anchor;
}

/*! @brief return an universal joint (no angle limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
  @param a1 first axis
  @param a2 second axis
*/
dJointID universal_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a, vec3 a1,
                         vec3 a2) {
  dJointID j = dJointCreateUniversal(w, 0);
  dJointAttach(j, b1, b2);
  dJointSetUniversalAnchor(j, a.x, a.y, a.z);
  dJointSetUniversalAxis1(j, a1.x, a1.y, a1.z);
  dJointSetUniversalAxis2(j, a2.x, a2.y, a2.z);
  dJointSetUniversalParam(j, dParamLoStop, -dInfinity);
  dJointSetUniversalParam(j, dParamHiStop, dInfinity);
  dJointSetUniversalParam(j, dParamLoStop2, -dInfinity);
  dJointSetUniversalParam(j, dParamHiStop2, dInfinity);
  dJointSetUniversalParam(j, dParamBounce, 1.);
  dJointSetUniversalParam(j, dParamBounce2, 1.);
  return j;
}

/*! @brief return an hinge joint (no angle limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
  @param ax hinge axis
*/
dJointID hinge_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a, vec3 ax) {
  dJointID j = dJointCreateHinge(w, 0);
  dJointAttach(j, b1, b2);
  dJointSetHingeAnchor(j, a.x, a.y, a.z);
  dJointSetHingeAxis(j, ax.x, ax.y, ax.z);
  dJointSetHingeParam(j, dParamLoStop, -dInfinity);
  dJointSetHingeParam(j, dParamHiStop, dInfinity);
  dJointSetHingeParam(j, dParamBounce, 1.);
  return j;
}

/*! @brief return a slider joint (no limits).
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a slider axis
*/
dJointID slider_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a) {
  dJointID j = dJointCreateSlider(w, 0);
  dJointAttach(j, b1, b2);
  dJointSetSliderAxis(j, a.x, a.y, a.z);
  dJointSetSliderParam(j, dParamLoStop, -dInfinity);
  dJointSetSliderParam(j, dParamHiStop, dInfinity);
  dJointSetSliderParam(j, dParamBounce, 1.);
  return j;
}

/*! @brief return a ball joint.
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a anchor point
*/
dJointID ball_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a) {
  dJointID j = dJointCreateBall(w, 0);
  dJointAttach(j, b1, b2);
  dJointSetBallAnchor(j, a.x, a.y, a.z);
  return j;
}

/*! @brief return a double ball joint.
  @param w world where to create the joint
  @param b1 first body
  @param b2 second body
  @param a1 first anchor point
  @param a2 second anchor point
*/
dJointID dball_joint(dWorldID w, dBodyID b1, dBodyID b2, vec3 a1, vec3 a2) {
  dJointID j = dJointCreateDBall(w, 0);
  dJointAttach(j, b1, b2);
  dJointSetDBallAnchor1(j, a1.x, a1.y, a1.z);
  dJointSetDBallAnchor2(j, a2.x, a2.y, a2.z);
  return j;
}

/*! @brief return the sign of 'a'.
  @param a value
*/
double sign(double a) { return (a < .0) ? -1. : ((a == .0) ? .0 : 1.); }

/*! @brief return the arc cosine of 'a'.
  @param a value
*/
double arccos(double a) { return (a >= 1.) ? .0 : ((a <= -1.) ? pi : acos(a)); }

/*! @brief return the arc sine of 'a'.
  @param a value
*/
double arcsin(double a) {
  return (a >= 1.) ? 1. : ((a <= -1.) ? -1. : asin(a));
}

/*! @brief return the solid angle described by four points.
  from Klenin & Langowski Computation of writhe in modeling of supercoiled DNA.
  https://doi.org/10.1002/1097-0282(20001015)54:5<307::AID-BIP20>3.0.CO;2-Y).
  @param r1 position of the point 1
  @param r2 position of the point 2
  @param r3 position of the point 3
  @param r4 position of the point 4
*/
double solid_angle(vec3 r1, vec3 r2, vec3 r3, vec3 r4) {
  vec3 r12 = sub3(r2, r1);
  vec3 r13 = sub3(r3, r1);
  vec3 r14 = sub3(r4, r1);
  vec3 r23 = sub3(r3, r2);
  vec3 r24 = sub3(r4, r2);
  vec3 r34 = sub3(r4, r3);
  vec3 n1 = normalize3(cross_product(r13, r14));
  vec3 n2 = normalize3(cross_product(r14, r24));
  vec3 n3 = normalize3(cross_product(r24, r23));
  vec3 n4 = normalize3(cross_product(r23, r13));
  double c12 = fmax(-1., fmin(1., dot_product(n1, n2)));
  double c23 = fmax(-1., fmin(1., dot_product(n2, n3)));
  double c34 = fmax(-1., fmin(1., dot_product(n3, n4)));
  double c41 = fmax(-1., fmin(1., dot_product(n4, n1)));
  // double
  // dot_cross=dot_product(cross_product(normalize3(r34),normalize3(r12)),normalize3(r13));
  // return
  // (fabs(dot_cross)>1e-12)?sign(dot_cross)*(arcsin(c12)+arcsin(c23)+arcsin(c34)+arcsin(c41)):.0;
  // double dot_cross=dot_product(cross_product(r34,r12),r13);
  // return sign(dot_cross)*(arcsin(c12)+arcsin(c23)+arcsin(c34)+arcsin(c41));
  double dot_cross = dot_product(
      cross_product(normalize3(r34), normalize3(r12)), normalize3(r13));
  return sign(dot_cross) *
         (arcsin(c12) + arcsin(c23) + arcsin(c34) + arcsin(c41));
}

/*! @brief This function returns the writhe of the chain (length N).
  The function uses OpenMP functionality to handle the different cases.
  @param positions positions of the N beads
*/
double chain_writhe(std::vector<vec3> positions) {
  // number of positions
  int N = (int)(positions.size());
  // nested loop to calculate the writhe
  double *writhe = new double[N]();
  vec3 p0, p1;
#pragma omp parallel for private(p0, p1)
  for (int i = 0; i < (N - 1); i++) {
    p0 = positions[i];
    p1 = positions[i + 1];
    for (int j = 0; j < (N - 1); j++)
      writhe[i] += solid_angle(p0, p1, positions[j], positions[j + 1]);
  }
  double sum_writhe = .0;
  for (int i = 0; i < N; i++)
    sum_writhe += writhe[i];
  delete[] writhe;
  writhe = NULL;
  return sum_writhe / (4.0 * acos(-1.));
}

/*! @brief This function returns the writhe of the chain (between I and S).
  See Klenin & Langowski Computation of writhe in modeling of supercoiled DNA.
  https://doi.org/10.1002/1097-0282(20001015)54:5<307::AID-BIP20>3.0.CO;2-Y).
  If the curve is closed the next bond to 'S' is 'I'.
  If the curve is non-closed, the function uses a closure A+B+E+D+F+C.
  See E. L. Starostin, ON THE WRITHING NUMBER OF A NON-CLOSED CURVE, Physical
  and Numerical Models in Knot Theory 2005. The function uses OpenMP
  functionality to handle the different cases.
  @param bs array of dBodyID objects (bonds)
  @param ls array of bond lengths
  @param I compute writhe between bond 'I' ...
  @param S ... and bond 'S' (interval [I,S] with I<S)
  @param nonclosed is the curve non-closed
  @param print print double integral for all the combinations
  @param nl number of steps per line
  @param sc number of smoothing steps
*/
double chain_writhe_ODE(const dBodyID *bs, const double *ls, int I, int S,
                        bool nonclosed, bool print, int nl, int sc) {
  double eps = 1e-6, epsD = 1e-6, xl = 1e0;
  FILE *ferror;
  char fname[100];
  // compute t(0), t(L) and tD
  vec3 t0 = FrameODE(bs[I], 2);
  vec3 tL = FrameODE(bs[S], 2);
  // nested loop to calculate the writhe
  int NA = S - I + 1;
  int NB = 3 * nl * NA;
  int NE = sc;
  int ND = 0;
  int NF = NE;
  int NC = NB;
  // build curve
  double avglbond = .0;
  vec3 *new_ps = new vec3[NA + (int)(nonclosed)]();
  vec3 *new_ts = new vec3[NA]();
  double *new_ls = new double[NA]();
  // anchor position correction
  vec3 p0 = sub3(PositionBodyODE(bs[I]), mult3(.5 * ls[I], FrameODE(bs[I], 2)));
  vec3 p1 = add3(PositionBodyODE(bs[S]), mult3(.5 * ls[S], FrameODE(bs[S], 2)));
  new_ps[0] = p0;
  for (int i = I; i < S; i++)
    new_ps[i - I + 1] =
        mult3(.5, add3(add3(PositionBodyODE(bs[i]),
                            mult3(.5 * ls[i], FrameODE(bs[i], 2))),
                       sub3(PositionBodyODE(bs[i + 1]),
                            mult3(.5 * ls[i + 1], FrameODE(bs[i + 1], 2)))));
  new_ps[NA] = p1;
  // compute new tangents
  for (int i = 0; i < NA; i++) {
    new_ls[i] = norm3(sub3(new_ps[i + 1], new_ps[i]));
    new_ts[i] = normalize3(sub3(new_ps[i + 1], new_ps[i]));
    avglbond += new_ls[i];
  }
  avglbond /= (double)NA;
  // compute and check tD
  vec3 rE = add3(p1, mult3(NB * xl * avglbond, tL));
  vec3 rF = sub3(p0, mult3(NC * xl * avglbond, t0));
  vec3 tD = normalize3(sub3(rF, rE));
  vec3 tDinf = normalize3(mult3(-1., add3(tL, t0)));
  vec3 tbefore = tD;
  vec3 tafter = vec3();
  // parallel case
  if (nonclosed && dot_product(t0, tL) > (1. - eps))
    return .0;
  // neither parallel nor antiparallel at the ends case (t(0) and t(L))
  vec3 *tE = new vec3[NE]();
  vec3 *tF = new vec3[NF]();
  // while(nonclosed && fabs(dot_product(t0,tL))<(1.-eps) &&
  // dot_product(tD,tDinf)<(1.-epsD)){
  while (nonclosed && fabs(dot_product(t0, tL)) < (1. - eps) &&
         (dot_product(tbefore, tafter) < (1. - epsD) ||
          norm3(sub3(rF, rE)) < (3. * avglbond))) {
    NB++;
    NC = NB;
    rE = add3(p1, mult3(NB * xl * avglbond, tL));
    rF = sub3(p0, mult3(NC * xl * avglbond, t0));
    tD = normalize3(sub3(rF, rE));
    tbefore = normalize3(sub3(rF, rE));
    for (int i = 0; i < NE; i++) {
      tE[i] = mult3(
          xl * avglbond,
          normalize3(add3(mult3((double)(NE - i) / (double)(NE + 1), tL),
                          mult3((double)(i + 1) / (double)(NE + 1), tD))));
      rE = add3(rE, tE[i]);
      // rE=add3(rE,mult3(xl*avglbond,normalize3(add3(mult3((double)(NE-i)/(double)(NE+1),tL),mult3((double)(i+1)/(double)(NE+1),tD)))));
    }
    for (int i = 0; i < NF; i++) {
      tF[i] = mult3(
          xl * avglbond,
          normalize3(add3(mult3((double)(NF - i) / (double)(NF + 1), tD),
                          mult3((double)(i + 1) / (double)(NF + 1), t0))));
      rF = sub3(rF, tF[i]);
      // rF=sub3(rF,mult3(xl*avglbond,normalize3(add3(mult3((double)(NF-i)/(double)(NF+1),tD),mult3((double)(i+1)/(double)(NF+1),t0)))));
    }
    tafter = normalize3(sub3(rF, rE));
  }
  tD = normalize3(sub3(rF, rE));
  // antiparallel case
  if (nonclosed && dot_product(t0, tL) < (eps - 1.))
    tD = normalize3(sub3(rF, rE));
  // bond size of straight line D ~ avglbond
  double lD = 100. * xl * avglbond;
  while (lD > (xl * avglbond)) {
    ND++;
    lD = norm3(sub3(rF, rE)) / ND;
  }
  // printf("(0) NA=%i ND=%i D=%f t(0).t(L)=%f
  // lD=%f/%f\n",NA,ND,norm3(sub3(rF,rE)),dot_product(t0,tL),lD,avglbond);
  int B = (nonclosed) ? NA + NB + NE + ND + NF + NC : NA;
  vec3 *points = new vec3[B]();
  vec3 *tb = new vec3[B]();
  // compute writhe
  double *writhe = new double[B](), *sub_writhe = new double[36](),
         sum_writhe = .0;
  // skip double integrals
  // A, B, E, D, F, C
  // 0, 1, 2, 3, 4, 5
  int *from_bond_to_curve = new int[B](), c0, c1;
  int *skip = new int[36](), *curve_start = new int[6](),
      *curve_end = new int[6]();
  skip[1 * 6 + 1] = 1;
  skip[1 * 6 + 2] = skip[2 * 6 + 1] = 1;
  skip[1 * 6 + 3] = skip[3 * 6 + 1] = 1;
  skip[2 * 6 + 2] = 1;
  skip[2 * 6 + 3] = skip[3 * 6 + 2] = 1;
  skip[3 * 6 + 3] = 1;
  skip[3 * 6 + 4] = skip[4 * 6 + 3] = 1;
  skip[3 * 6 + 5] = skip[5 * 6 + 3] = 1;
  skip[4 * 6 + 4] = 1;
  skip[4 * 6 + 5] = skip[5 * 6 + 4] = 1;
  skip[5 * 6 + 5] = 1;
  // t(0)=-t(L)
  if (dot_product(t0, tL) < (eps - 1.)) {
    skip[1 * 6 + 4] = skip[4 * 6 + 1] = 1;
    skip[2 * 6 + 4] = skip[4 * 6 + 2] = 1;
    skip[2 * 6 + 5] = skip[5 * 6 + 2] = 1;
  }
  // l>>1
  if (fabs(dot_product(t0, tL)) < (1. - eps)) {
    skip[1 * 6 + 4] = skip[4 * 6 + 1] = 1;
    skip[2 * 6 + 4] = skip[4 * 6 + 2] = 1;
    skip[2 * 6 + 5] = skip[5 * 6 + 2] = 1;
    skip[0 * 6 + 2] = skip[2 * 6 + 0] = 1;
    skip[0 * 6 + 3] = skip[3 * 6 + 0] = 1;
    skip[0 * 6 + 4] = skip[4 * 6 + 0] = 1;
  }
  // is the curve closed ?
  if (nonclosed) {
    // open curve + closure
    // curve A
    for (int i = 0; i < NA; i++)
      tb[i] = mult3(new_ls[i], new_ts[i]);
    // first straight line B
    for (int i = 0; i < NB; i++)
      tb[NA + i] = mult3(xl * avglbond, tL);
    // smooth curve E
    for (int i = 0; i < NE; i++)
      tb[NA + NB + i] = tE
          [i]; // mult3(xl*avglbond,normalize3(add3(mult3((double)(NE-i)/(double)(NE+1),tL),mult3((double)(i+1)/(double)(NE+1),tD))));
    // straight line D
    for (int i = 0; i < ND; i++)
      tb[NA + NB + NE + i] = mult3(lD, tD);
    // smooth curve F
    for (int i = 0; i < NF; i++)
      tb[NA + NB + NE + ND + i] = tF
          [i]; // mult3(xl*avglbond,normalize3(add3(mult3((double)(NF-i)/(double)(NF+1),tD),mult3((double)(i+1)/(double)(NF+1),t0))));
    // second straight line C
    for (int i = 0; i < NC; i++)
      tb[NA + NB + NE + ND + NF + i] = mult3(xl * avglbond, t0);
    // compute points
    points[0] = p0;
    for (int i = 0; i < (B - 1); i++)
      points[i + 1] = add3(points[i], tb[i]);
    // connectivity check
    for (int i = 0; i < B; i++) {
      if (norm3(sub3(add3(points[i], tb[i]),
                     points[i + 1 - B * (int)((i + 1) >= B)])) >
          (.01 * norm3(tb[i]))) {
        printf("avglbond=%f NA=%i NB=%i NE=%i ND=%i NF=%i NC=%i\n", avglbond,
               NA, NB, NE, ND, NF, NC);
        printf("t0.tL=%f\n", dot_product(t0, tL));
        printf("tD.tDinf=%f\n",
               dot_product(normalize3(sub3(points[-(NC + NF) + B],
                                           points[NA + NB + NE])),
                           tDinf));
        printf("%f\n", norm3(sub3(points[NA], p1)));
        printf("%f\n", norm3(sub3(points[NA + NB],
                                  add3(p1, mult3(NB * xl * avglbond, tL)))));
        printf("%f\n", norm3(sub3(points[NA + NB + NE], rE)));
        printf("%f\n", norm3(sub3(points[-(NC + NF) + B], rF)));
        printf("connectivity check %i-%i/%i %f/%f failed, exit.\n", i,
               i + 1 - B * (int)((i + 1) >= B), B,
               norm3(sub3(add3(points[i], tb[i]),
                          points[i + 1 - B * (int)((i + 1) >= B)])),
               .01 * norm3(tb[i]));
        // write error file and return zero writhe
        sprintf(fname, "error_connectivity_%i-%i_%f_%f.out", i,
                i + 1 - B * (int)((i + 1) >= B),
                norm3(sub3(add3(points[i], tb[i]),
                           points[i + 1 - B * (int)((i + 1) >= B)])),
                .01 * norm3(tb[i]));
        ferror = fopen(fname, "w");
        fclose(ferror);
        return .0;
        // exit(EXIT_FAILURE);
      }
      // from bond to curve
      c0 = 0;
      if (i >= NA && i < (NA + NB))
        c0 = 1;
      if (i >= (NA + NB) && i < (NA + NB + NE))
        c0 = 2;
      if (i >= (NA + NB + NE) && i < (NA + NB + NE + ND))
        c0 = 3;
      if (i >= (NA + NB + NE + ND) && i < (NA + NB + NE + ND + NF))
        c0 = 4;
      if (i >= (NA + NB + NE + ND + NF))
        c0 = 5;
      from_bond_to_curve[i] = c0;
    }
    curve_start[0] = 0;
    curve_start[1] = NA;
    curve_start[2] = NA + NB;
    curve_start[3] = NA + NB + NE;
    curve_start[4] = NA + NB + NE + ND;
    curve_start[5] = NA + NB + NE + ND + NF;
    for (int c = 0; c < 5; c++)
      curve_end[c] = curve_start[c + 1];
    curve_end[5] = B;
    delete[] tE;
    tE = NULL;
    delete[] tF;
    tF = NULL;
    // for(int c=1;c<6;c++)
    //   if(c==1 || c==5)
    // 	for(int i=curve_start[c];i<(curve_end[c]-1);i++)
    // 	  if(dot_product(normalize3(tb[i]),normalize3(tb[i+1]))<.999){
    // 	    printf("%i %i\n",c,i);
    // 	    print3(normalize3(tb[i]));
    // 	    print3(normalize3(tb[i+1]));
    // 	    printf("\n");
    // 	  }
    // compute writhe for all combinations
    if (0) {
      for (int i = 0; i < (B - 1); i++) {
        c0 = from_bond_to_curve[i];
        p0 = points[i];
        p1 = points[i + 1 - B * (int)((i + 1) >= B)];
        for (int j = (i + 1); j < B; j++) {
          c1 = from_bond_to_curve[j];
          // if(c0==0 && c1==0 && print) printf("00 %i %i/%i
          // %f(%f)\n",i,j,B,solid_angle(p0,p1,points[j],points[j+1-B*(int)((j+1)>=B)]),solid_angle(points[NA],points[NA+1],points[-1+B],points[0]));
          // if(c0==1 && c1==5 && print) printf("15 %i %i/%i %f
          // %f\n",i,j,B,solid_angle(p0,p1,points[j],points[j+1-B*(int)((j+1)>=B)]),solid_angle(points[0],points[1],points[13],points[14]));
          sub_writhe[min(c0, c1) * 6 + max(c0, c1)] +=
              (skip[c0 * 6 + c1] == 0)
                  ? solid_angle(p0, p1, points[j],
                                points[j + 1 - B * (int)((j + 1) >= B)])
                  : .0;
        }
      }
    } else {
      for (int c = 0; c < 6; c++)
        for (int d = c; d < 6; d++)
          if (skip[c * 6 + d] == 0)
            for (int i = curve_start[c]; i < (curve_end[c] - 1); i++) {
              p0 = points[i];
              p1 = points[i + 1 - B * (int)((i + 1) >= B)];
              for (int j = curve_start[d]; j < (curve_end[d] - 1); j++)
                sub_writhe[c * 6 + d] +=
                    (i != j)
                        ? solid_angle(p0, p1, points[j],
                                      points[j + 1 - B * (int)((j + 1) >= B)])
                        : .0;
              // sub_writhe[c*6+d]+=(i!=j)?dot_product(cross_product(tb[i],tb[j]),mult3(1./pow(norm3(sub3(p0,points[j])),3.),sub3(p0,points[j]))):.0;
            }
    }
    // sum over all the 36 double sums
    for (int c = 0; c < 6; c++)
      for (int d = c; d < 6; d++)
        if (skip[c * 6 + d] == 0)
          sum_writhe += (1 + (int)(c != d)) * sub_writhe[c * 6 + d];
    if (print) { // && ((sum_writhe/(4.*acos(-1.)))>-.1 ||
                 // (sum_writhe/(4.*acos(-1.)))<-2.5)){
      printf("avglbond=%f lD=%f NA=%i NB=%i NE=%i ND=%i NF=%i NC=%i\n",
             avglbond, lD, NA, NB, NE, ND, NF, NC);
      printf("Wr=%f\n", sum_writhe / (4. * acos(-1.)));
      for (int i = 0; i < 6; i++)
        for (int j = i; j < 6; j++)
          if (skip[i * 6 + j] == 0)
            printf("(%i,%i) %f (t(0).t(L)=%f)\n", i, j,
                   sub_writhe[i * 6 + j] / (4. * acos(-1.)),
                   dot_product(t0, tL));
      // exit(EXIT_FAILURE);
    }
  } else {
    // closed curve
#pragma omp parallel for private(p0, p1)
    for (int i = 0; i < NA; i++) {
      p0 = new_ps[i];
      p1 = new_ps[i + 1 - NA * (int)((i + 1) >= NA)];
      for (int j = 0; j < NA; j++)
        writhe[i] += solid_angle(p0, p1, new_ps[j],
                                 new_ps[j + 1 - NA * (int)((j + 1) >= NA)]);
      sum_writhe += writhe[i];
    }
  }
  delete[] from_bond_to_curve;
  from_bond_to_curve = NULL;
  delete[] curve_start;
  curve_start = NULL;
  delete[] curve_end;
  curve_end = NULL;
  delete[] points;
  points = NULL;
  delete[] new_ls;
  new_ls = NULL;
  delete[] new_ps;
  new_ps = NULL;
  delete[] new_ts;
  new_ts = NULL;
  delete[] tb;
  tb = NULL;
  delete[] skip;
  skip = NULL;
  delete[] sub_writhe;
  sub_writhe = NULL;
  delete[] writhe;
  writhe = NULL;
  return sum_writhe / (4. * acos(-1.));
}

/*! @brief read or write the conformation and return the number of elements in
  [S,E[ red/written.
  @param bs 'dBodyID' objects
  @param name name of the file
  @param what_to_do read or write
  @param S start to read/write
  @param E end
*/
int read_write_conformation(dBodyID *bs, char *name, const char *what_to_do,
                            int S, int E) {
  double d0, d1, d2, d3, d4, d5, d6;
  const dReal *rg;
  int retour = 0;
  if (strcmp(what_to_do, "read") == 0) {
    FILE *fSPQ = fopen(name, "r");
    if (fSPQ != NULL) {
      for (int i = S; i < E; i++) {
        retour += fread(&d0, sizeof(double), 1, fSPQ);
        retour += fread(&d1, sizeof(double), 1, fSPQ);
        retour += fread(&d2, sizeof(double), 1, fSPQ);
        retour += fread(&d3, sizeof(double), 1, fSPQ);
        retour += fread(&d4, sizeof(double), 1, fSPQ);
        retour += fread(&d5, sizeof(double), 1, fSPQ);
        retour += fread(&d6, sizeof(double), 1, fSPQ);
        set_pos_quat(bs[i], vec3(d0, d1, d2), d3, d4, d5, d6);
        retour += fread(&d0, sizeof(double), 1, fSPQ);
        retour += fread(&d1, sizeof(double), 1, fSPQ);
        retour += fread(&d2, sizeof(double), 1, fSPQ);
        retour += fread(&d3, sizeof(double), 1, fSPQ);
        retour += fread(&d4, sizeof(double), 1, fSPQ);
        retour += fread(&d5, sizeof(double), 1, fSPQ);
        set_velocities(bs[i], d0, d1, d2, d3, d4, d5);
      }
      fclose(fSPQ);
    }
  }
  if (strcmp(what_to_do, "write") == 0) {
    FILE *fSPQ = fopen(name, "wb");
    for (int i = S; i < E; i++) {
      rg = dBodyGetPosition(bs[i]);
      retour += fwrite(&rg[0], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[1], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[2], sizeof(double), 1, fSPQ);
      rg = dBodyGetQuaternion(bs[i]);
      retour += fwrite(&rg[0], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[1], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[2], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[3], sizeof(double), 1, fSPQ);
      rg = dBodyGetLinearVel(bs[i]);
      retour += fwrite(&rg[0], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[1], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[2], sizeof(double), 1, fSPQ);
      rg = dBodyGetAngularVel(bs[i]);
      retour += fwrite(&rg[0], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[1], sizeof(double), 1, fSPQ);
      retour += fwrite(&rg[2], sizeof(double), 1, fSPQ);
    }
    fclose(fSPQ);
  }
  return retour;
}

/*! @brief write (binary format) the conformation for visualisation and return
  the number of elements in [S,E[ written.
  @param bs 'dBodyID' objects
  @param name name of the file
  @param S start to write
  @param E end
  @param what_to_do write 'w' or append 'a', default to 'w'
*/
int write_visualisation(dBodyID *bs, char *name, int S, int E,
                        const char *what_to_do) {
  const dReal *rg;
  int retour = 0;
  vec3 t0;
  FILE *fVisualisation;
  if (strcmp(what_to_do, "w") == 0)
    fVisualisation = fopen(name, "wb");
  else {
    if (strcmp(what_to_do, "a") == 0)
      fVisualisation = fopen(name, "ab");
    else
      fVisualisation = fopen(name, "wb");
  }
  for (int i = S; i < E; i++) {
    rg = dBodyGetPosition(bs[i]);
    retour += fwrite(&rg[0], sizeof(double), 1, fVisualisation);
    retour += fwrite(&rg[1], sizeof(double), 1, fVisualisation);
    retour += fwrite(&rg[2], sizeof(double), 1, fVisualisation);
    t0 = FrameODE(bs[i], 2);
    retour += fwrite(&t0.x, sizeof(double), 1, fVisualisation);
    retour += fwrite(&t0.y, sizeof(double), 1, fVisualisation);
    retour += fwrite(&t0.z, sizeof(double), 1, fVisualisation);
  }
  fclose(fVisualisation);
  return retour;
}

/*! @brief write squared distances intra/inter nucleosomes (skip first and last
  one).
  @param bs 'dBodyID' objects
  @param N number of nucleosomes
  @param F number of DNA monomers
  @param name name of the file
  @param what_to_do write 'w' or append 'a', default to 'w'
  @param dn 0 for intra nucleosome, 1 for consecutive nucleosomes ...
*/
void write_sqdistances(dBodyID *bs, const int N, const int F, char *name,
                       const char *what_to_do, int dn) {
  const dReal *rgi, *rgj;
  vec3 t0;
  int retour = 0, iln1, iln2, np;
  double sqd;
  FILE *fsqd, *fnp;
  char np_name[1000];
  retour = sprintf(np_name, "%s.np", name);
  dn = max(dn, 0);
  if (strcmp(what_to_do, "a") == 0) {
    fsqd = fopen(name, "a");
    // if file contains already many lines do not add header
    fnp = fopen(np_name, "r");
    if (fnp != NULL) {
      retour = 0;
      while (!feof(fnp))
        retour++;
      fclose(fnp);
      fnp = fopen(np_name, "a");
      if (retour > 0)
        retour = fprintf(fnp, "%s %s %s\n", "from_nucleosome", "to_nucleosome",
                         "number_of_pairwize");
    } else {
      fnp = fopen(np_name, "a");
      retour = fprintf(fnp, "%s %s %s\n", "from_nucleosome", "to_nucleosome",
                       "number_of_pairwize");
    }
  } else {
    fsqd = fopen(name, "w");
    fnp = fopen(np_name, "w");
    retour = fprintf(fnp, "%s %s %s\n", "from_nucleosome", "to_nucleosome",
                     "number_of_pairwize");
  }
  // loop over the number of nucleosomes (skip first and last one)
  for (int n = 1; n < (N - dn - 1); n++) {
    // count the number of pairwize for the interval [n,n+dn]
    np = 0;
    // loop over the DNA monomers between nucleosome 'n' and nucleosome 'n+dn'
    for (int i = nucleosome_start[n]; i < (F + (n + dn) * NHC); i++) {
      rgi = dBodyGetPosition(bs[i]);
      t0 = FrameODE(bs[i], 2);
      iln1 = is_linker_nucleosome[i];
      // loop over the DNA monomers between nucleosome 'n' and nucleosome 'n+dn'
      for (int j = nucleosome_start[n]; j < (F + (n + dn) * NHC); j++) {
        if (j <= i)
          continue;
        rgj = dBodyGetPosition(bs[j]);
        iln2 = is_linker_nucleosome[j];
        sqd = pow(rgi[0] - rgj[0], 2.) + pow(rgi[1] - rgj[1], 2.) +
              pow(rgi[2] - rgj[2], 2.);
        // capsule index, first nucleosome index, capsule index, second
        // nucleosome index, distance, cosine
        retour = fprintf(fsqd, "%i %i %i %i %f %f\n", i, iln1, j, iln2, sqd,
                         dot_product(t0, FrameODE(bs[j], 2)));
        np++;
        // jump to histones
        if (j == (nucleosome_start[n + dn] + NNC - 1))
          j = F + n * NHC;
      }
      // jump to histones
      if (i == (nucleosome_start[n + dn] + NNC - 1))
        i = F + n * NHC;
    }
    retour = fprintf(fnp, "%i %i %i\n", n, n + dn, np);
  }
  fclose(fsqd);
  fclose(fnp);
}

/*! @brief write squared distances between pair of nucleosomes.
  @param bs 'dBodyID' objects
  @param N number of nucleosomes
  @param F number of DNA monomers
  @param name name of the file
  @param what_to_do write 'w' or append 'a', default to 'w'
*/
void write_nucnuc_sqdistances(dBodyID *bs, const int N, const int F, char *name,
                              const char *what_to_do) {
  vec3 com1, com2;
  FILE *fsqd = fopen(name, (strcmp(what_to_do, "a") == 0) ? "a" : "w");
  // loop over the number of nucleosomes (skip first and last one)
  for (int n = 0; n < (N - 1); n++) {
    com1 = vec3();
    for (int i = (F + n * NHC); i < (F + (n + 1) * NHC); i++)
      com1 = add3(com1, PositionBodyODE(bs[i]));
    com1 = mult3(1. / (double)NHC, com1);
    for (int p = (n + 1); p < N; p++) {
      // printf("%i %i\n",n,p);
      com2 = vec3();
      for (int i = (F + p * NHC); i < (F + (p + 1) * NHC); i++)
        com2 = add3(com2, PositionBodyODE(bs[i]));
      com2 = mult3(1. / (double)NHC, com2);
      fprintf(fsqd, "%i %i %f\n", n, p,
              dot_product2(sub3(com1, com2)) * fL * fL / (nm * nm));
    }
  }
  fclose(fsqd);
}

// return the determinant of the mat33 'm'
double determinant_mat3(mat33 m) {
  return m.c1.x * (m.c2.y * m.c3.z - m.c3.y * m.c2.z) -
         m.c2.x * (m.c1.y * m.c3.z - m.c3.y * m.c1.z) +
         m.c3.x * (m.c1.y * m.c2.z - m.c2.y * m.c1.z);
  // return dot_product(m.c1,cross_product(m.c2,m.c3));
}

// return the trace of the mat33 'm'
double trace_mat3(mat33 m) { return m.c1.x + m.c2.y + m.c3.z; }

// return invert mat33 'm' (Cayley-Hamilton)
mat33 inverseCH_mat3(mat33 m) {
  double tr = trace_mat3(m);
  mat33 m2 = mult33(m, m);
  double dtr = .5 * (tr * tr - trace_mat3(m2));
  double det = determinant_mat3(m);
  m = add33(mult_33_1(-tr, m), m2);
  m.c1.x += dtr;
  m.c2.y += dtr;
  m.c3.z += dtr;
  return mult_33_1(1. / det, m);
}

// return mat33 from cross-product with vec3 'u'
mat33 mat_vect(vec3 u) {
  mat33 m;
  m.c1 = vec3(0., u.z, -u.y);
  m.c2 = vec3(-u.z, 0., u.x);
  m.c3 = vec3(u.y, -u.x, 0.);
  return m;
}

// return mat33 'm' multiplied by scalar 'r'
mat33 mult_33_1(double r, mat33 m) {
  m.c1.x *= r;
  m.c1.y *= r;
  m.c1.z *= r;
  m.c2.x *= r;
  m.c2.y *= r;
  m.c2.z *= r;
  m.c3.x *= r;
  m.c3.y *= r;
  m.c3.z *= r;
  return m;
}

// LDLT decompostion to solve A * l = b where A is tridiagonal
void solution_Al_b_LDLT(std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                        std::vector<vec3> &rhs1, std::vector<vec3> &rhs2, int C,
                        std::vector<vec3> &lambda) {
  vector<vec3> lambda1(C), lambda2(C);
  vector<mat33> Dii(C), Lij(C - 1);
  mat33 LijT, Dii1;
  // Decomposition A = L * D * L^T
  Dii[0] = Hii[0];
  lambda1[0] = rhs1[0];
  lambda2[0] = rhs2[0];
  for (int c = 1; c < C; c++) {
    Lij[c - 1] = mult33(Hij[c - 1], inverseCH_mat3(Dii[c - 1]));
    Dii[c] = sub33(Hii[c],
                   mult33(Lij[c - 1], mult33(Dii[c - 1], mat3T(Lij[c - 1]))));
    // L * Y' = X
    lambda1[c] = sub3(rhs1[c], mult_33_3(Lij[c - 1], lambda1[c - 1]));
    lambda2[c] = sub3(rhs2[c], mult_33_3(Lij[c - 1], lambda2[c - 1]));
  }
  // Y' = D * L^T * Y
  Dii1 = inverseCH_mat3(Dii[C - 1]);
  lambda1[C - 1] = mult_33_3(Dii1, lambda1[C - 1]);
  lambda2[C - 1] = mult_33_3(Dii1, lambda2[C - 1]);
  lambda[C - 1] = add3(lambda1[C - 1], lambda2[C - 1]);
  for (int c = (C - 2); c > -1; c--) {
    LijT = mat3T(Lij[c]);
    Dii1 = inverseCH_mat3(Dii[c]);
    lambda1[c] =
        sub3(mult_33_3(Dii1, lambda1[c]), mult_33_3(LijT, lambda1[c + 1]));
    lambda2[c] =
        sub3(mult_33_3(Dii1, lambda2[c]), mult_33_3(LijT, lambda2[c + 1]));
    lambda[c] = add3(lambda1[c], lambda2[c]);
  }
  // // correction for the Lagrange multiplier lambda
  // vec3 rhs,n_rhs;
  // double alpha,norme;
  // for(int c=0;c<C;c++){
  //   rhs=lambda1[c];
  //   n_rhs=normaliser_vec3(rhs);
  //   alpha=dot_product(lambda2[c],n_rhs);
  //   norme=norme_vec3(rhs);
  //   if(alpha>=0. && alpha<=norme) rhs=add3(rhs,mult_vec3(-alpha,n_rhs));
  //   if(alpha>norme) rhs=creer_vec3(0.,0.,0.);
  //   lambda[c]=add3(rhs,lambda2[c]);
  // }
}

// LDU decompostion to solve A * l = b where A is tridiagonal
void solution_Al_b_LDU(std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                       std::vector<vec3> &rhs1, std::vector<vec3> &rhs2, int C,
                       std::vector<vec3> &lambda, bool split_rhs) {
  vector<vec3> lambda1((split_rhs)*C), lambda2((split_rhs)*C);
  // vector<mat33> Dii(C),Lij(C-1),Uij(C-1);
  vector<mat33> Dii(C), Uij(C - 1);
  mat33 Dii1, Lij;
  if (split_rhs) {
    // Decomposition A = L * D * U
    Dii[0] = Hii[0];
    lambda1[0] = rhs1[0];
    lambda2[0] = rhs2[0];
    for (int c = 1; c < C; c++) {
      Dii1 = inverseCH_mat3(Dii[c - 1]);
      // Lij[c-1]=mult33(Hij[c-1],Dii1);
      Lij = mult33(Hij[c - 1], Dii1);
      Uij[c - 1] = mult33(Dii1, Hij[c - 1]);
      // Dii[c]=sub33(Hii[c],mult33(Lij[c-1],mult33(Dii[c-1],Uij[c-1])));
      Dii[c] = sub33(Hii[c], mult33(Lij, mult33(Dii[c - 1], Uij[c - 1])));
      // L * Y' = X
      // lambda1[c]=sub3(rhs1[c],mult_33_3(Lij[c-1],lambda1[c-1]));
      // lambda2[c]=sub3(rhs2[c],mult_33_3(Lij[c-1],lambda2[c-1]));
      lambda1[c] = sub3(rhs1[c], mult_33_3(Lij, lambda1[c - 1]));
      lambda2[c] = sub3(rhs2[c], mult_33_3(Lij, lambda2[c - 1]));
    }
    // Y' = D * U * Y
    Dii1 = inverseCH_mat3(Dii[C - 1]);
    lambda1[C - 1] = mult_33_3(Dii1, lambda1[C - 1]);
    lambda2[C - 1] = mult_33_3(Dii1, lambda2[C - 1]);
    lambda[C - 1] = add3(lambda1[C - 1], lambda2[C - 1]);
    for (int c = (C - 2); c > -1; c--) {
      Dii1 = inverseCH_mat3(Dii[c]);
      lambda1[c] =
          sub3(mult_33_3(Dii1, lambda1[c]), mult_33_3(Uij[c], lambda1[c + 1]));
      lambda2[c] =
          sub3(mult_33_3(Dii1, lambda2[c]), mult_33_3(Uij[c], lambda2[c + 1]));
      lambda[c] = add3(lambda1[c], lambda2[c]);
    }
  } else {
    // Decomposition A = L * D * U
    // do not split rhs
    Dii[0] = Hii[0];
    lambda[0] = add3(rhs1[0], rhs2[0]);
    for (int c = 1; c < C; c++) {
      Dii1 = inverseCH_mat3(Dii[c - 1]);
      // Lij[c-1]=mult33(Hij[c-1],Dii1);
      Lij = mult33(Hij[c - 1], Dii1);
      Uij[c - 1] = mult33(Dii1, Hij[c - 1]);
      // Dii[c]=sub33(Hii[c],mult33(Lij[c-1],mult33(Dii[c-1],Uij[c-1])));
      Dii[c] = sub33(Hii[c], mult33(Lij, mult33(Dii[c - 1], Uij[c - 1])));
      // L * Y' = X
      // lambda[c]=sub3(add3(rhs1[c],rhs2[c]),mult_33_3(Lij[c-1],lambda[c-1]));
      lambda[c] = sub3(add3(rhs1[c], rhs2[c]), mult_33_3(Lij, lambda[c - 1]));
    }
    // Y' = D * U * Y
    Dii1 = inverseCH_mat3(Dii[C - 1]);
    lambda[C - 1] = mult_33_3(Dii1, lambda[C - 1]);
    for (int c = (C - 2); c > -1; c--) {
      Dii1 = inverseCH_mat3(Dii[c]);
      lambda[c] =
          sub3(mult_33_3(Dii1, lambda[c]), mult_33_3(Uij[c], lambda[c + 1]));
    }
  }
}

// constraints ("velocity-based" method) for a linear chain of rigid body
// connected with mechanical "Ball-in-Socket" joint
void solution_LC(dBodyID *&b, const double *m, const double *l, int Nchr[],
                 int C, int sol, int ext, double dt, vec3 anchor,
                 std::vector<mat33> &Hii, std::vector<mat33> &Hij,
                 std::vector<vec3> &lambda) {
  int N = 0, N_max = 0, cdofs, nc;
  int *chr0 = new int[C]();
  for (int c = 1; c < C; c++)
    chr0[c] = chr0[c - 1] + Nchr[c - 1];
  for (int c = 0; c < C; c++)
    N_max = max(N_max, Nchr[c]);
  vector<vec3> rhs1(N_max), rhs2(N_max);
  vec3 buffer, JV, g1, g2, t0, t1, f0, f1, T0, T1;
  double inv_m0, inv_m1, inv_m, inv_dt = 1. / dt, inv_dt2 = 1. / (dt * dt), hl0,
                                hl1;
  mat33 A1, JMJTi, JMJTj, JMJT, Ip, Ip1;
  // loop over the chains
  for (int c = 0; c < C; c++) {
    nc = chr0[c];
    N = Nchr[c];
    cdofs = N - 1 + sol + ext;
    if (sol == 1) {
      t0 = mult3(-.5 * l[nc], FrameODE(b[nc], 2));
      JV = sub3(vec3(.0, .0, .0), VelPosRelBodyODE(b[nc], .0, .0, -.5 * l[nc]));
      // error
      rhs1[0] = mult3(
          -wERP * inv_dt2,
          sub3(anchor, PositionPosRelBodyODE(b[nc], .0, .0, -.5 * l[nc])));
      // rhs-error
      buffer = mult3(-inv_dt, JV);
      buffer = sub3(buffer, mult3(-1. / m[nc], ForceBodyODE(b[nc])));
      rhs2[0] =
          sub3(buffer, cross_product(t0, mult_33_3(inertia_t(b[nc], 1),
                                                   TorqueBodyODE(b[nc]))));
      // H00
      A1 = mat_vect(t0);
      A1 = mult33(A1, mult33(inertia_t(b[nc], 1), mat3T(A1)));
      Hii[0] = A1;
      inv_m = 1. / m[nc];
      Hii[0].c1.x += inv_m + wCFM * inv_dt;
      Hii[0].c2.y += inv_m + wCFM * inv_dt;
      Hii[0].c3.z += inv_m + wCFM * inv_dt;
      // H01 = H10
      Hij[0] = A1;
      Hij[0].c1.x -= inv_m;
      Hij[0].c2.y -= inv_m;
      Hij[0].c3.z -= inv_m;
    }
    // loop over the monomers-1 from chain 'c'
    t0 = mult3(-.5 * l[nc], FrameODE(b[nc], 2));
    g1 = cross_product(AngVelBodyODE(b[nc]),
                       mult_33_3(inertia_t(b[nc], 0), AngVelBodyODE(b[nc])));
    Ip = inertia_t(b[nc], 1);
    f0 = ForceBodyODE(b[nc]);
    T0 = TorqueBodyODE(b[nc]);
    inv_m0 = 1. / m[nc];
    hl0 = .5 * l[nc];
    for (int p = 0; p < (N - 1); p++) {
      hl1 = .5 * l[nc + p + 1];
      t1 = mult3(-hl1, FrameODE(b[nc + p + 1], 2));
      Ip1 = inertia_t(b[nc + p + 1], 1);
      // gyroscopic term
      g2 = cross_product(
          AngVelBodyODE(b[nc + p + 1]),
          mult_33_3(inertia_t(b[nc + p + 1], 0), AngVelBodyODE(b[nc + p + 1])));
      // error
      rhs1[p + sol] =
          mult3(-wERP * inv_dt2,
                sub3(PositionPosRelBodyODE(b[nc + p], 0., 0., hl0),
                     PositionPosRelBodyODE(b[nc + p + 1], 0., 0., -hl1)));
      // rhs-error
      f1 = ForceBodyODE(b[nc + p + 1]);
      T1 = TorqueBodyODE(b[nc + p + 1]);
      inv_m1 = 1. / m[nc + p + 1];
      JV = sub3(VelPosRelBodyODE(b[nc + p], 0., 0., hl0),
                VelPosRelBodyODE(b[nc + p + 1], 0., 0., -hl1));
      buffer = mult3(-inv_dt, JV);
      buffer = sub3(buffer, mult3(inv_m0, f0));
      buffer = sub3(buffer, cross_product(t0, mult_33_3(Ip, sub3(T0, g1))));
      buffer = sub3(buffer, mult3(-inv_m1, f1));
      rhs2[p + sol] =
          sub3(buffer, cross_product(t1, mult_33_3(Ip1, sub3(T1, g2))));
      // Hii
      A1 = mat_vect(t0);
      JMJTi = mult33(A1, mult33(Ip, mat3T(A1)));
      A1 = mat_vect(t1);
      JMJTj = mult33(A1, mult33(Ip1, mat3T(A1)));
      JMJT = add33(JMJTi, JMJTj);
      inv_m = inv_m0 + inv_m1;
      JMJT.c1.x += inv_m + wCFM * inv_dt;
      JMJT.c2.y += inv_m + wCFM * inv_dt;
      JMJT.c3.z += inv_m + wCFM * inv_dt;
      Hii[p + sol] = JMJT;
      // Hij = Hji
      JMJTj.c1.x -= inv_m1;
      JMJTj.c2.y -= inv_m1;
      JMJTj.c3.z -= inv_m1;
      Hij[p + sol] = JMJTj;
      t0 = t1;
      g1 = g2;
      Ip = Ip1;
      f0 = f1;
      T0 = T1;
      inv_m0 = inv_m1;
      hl0 = hl1;
    }
    if (ext == 1) {
      t1 = mult3(-.5 * l[nc + N - 1], FrameODE(b[nc + N - 1], 2));
      JV = sub3(VelPosRelBodyODE(b[nc + N - 1], 0., 0., .5 * l[nc + N - 1]),
                vec3());
      rhs1[cdofs - 1] = mult3(
          -wERP / (dt * dt),
          sub3(PositionPosRelBodyODE(b[nc + N - 1], 0., 0., .5 * l[nc + N - 1]),
               vec3()));
      buffer = mult3(-inv_dt, JV);
      buffer =
          sub3(buffer, mult3(1. / m[nc + N - 1], ForceBodyODE(b[nc + N - 1])));
      rhs2[cdofs - 1] = sub3(
          buffer, cross_product(t1, mult_33_3(inertia_t(b[nc + N - 1], 1),
                                              TorqueBodyODE(b[nc + N - 1]))));
      // HCC
      A1 = mat_vect(t1);
      Hii[cdofs - 1] =
          mult33(A1, mult33(inertia_t(b[nc + N - 1], 1), mat3T(A1)));
      inv_m = 1. / m[nc + N - 1];
      Hii[cdofs - 1].c1.x += inv_m + wCFM * inv_dt;
      Hii[cdofs - 1].c2.y += inv_m + wCFM * inv_dt;
      Hii[cdofs - 1].c3.z += inv_m + wCFM * inv_dt;
    }
    // get lambda from A * lambda = b
    // solution_Al_b_LDLT(Hii,Hij,rhs1,rhs2,cdofs,lambda);
    solution_Al_b_LDU(Hii, Hij, rhs1, rhs2, cdofs, lambda, false);
    if (sol == 1) {
      t0 = mult3(-.5 * l[nc], FrameODE(b[nc], 2));
      buffer = lambda[0];
      addForceBodyODE(b[nc], mult3(-1., buffer));
      addTorqueBodyODE(b[nc], cross_product(mult3(-1., t0), buffer));
    }
    t0 = mult3(-.5 * l[nc], FrameODE(b[nc], 2));
    for (int p = 0; p < (N - 1); p++) {
      t1 = mult3(-.5 * l[nc + p + 1], FrameODE(b[nc + p + 1], 2));
      buffer = lambda[p + sol];
      addForceBodyODE(b[nc + p], buffer);
      addTorqueBodyODE(b[nc + p], cross_product(mult3(-1., t0), buffer));
      addForceBodyODE(b[nc + p + 1], mult3(-1., buffer));
      addTorqueBodyODE(b[nc + p + 1], cross_product(mult3(-1., t1), buffer));
      t0 = t1;
    }
    if (ext == 1) {
      t1 = mult3(-.5 * l[nc + N - 1], FrameODE(b[nc + N - 1], 2));
      buffer = lambda[cdofs - 1];
      addForceBodyODE(b[nc + N - 1], buffer);
      addTorqueBodyODE(b[nc + N - 1], cross_product(mult3(-1., t1), buffer));
    }
    // lambda to 0
    for (int i = 0; i < N_max; i++)
      lambda[i] = vec3(.0, .0, .0);
  }
  delete[] chr0;
  chr0 = NULL;
}

/*! @brief dot product u^Tv
  @param u first vector
  @param v first vector
*/
double dot_product(vec3 u, vec3 v) { return u.x * v.x + u.y * v.y + u.z * v.z; }

/*! @brief dot product u^Tu
  @param u first vector
  @param v first vector
*/
double dot_product2(vec3 u) { return u.x * u.x + u.y * u.y + u.z * u.z; }

/*! @brief This function returns the two points (each on one segment) such that
  the distance is minimal. This function compares the result to an "exact
  enumeration" too (if test_result is true). The number of points (along each
  segment) is given by the integer variable enumeration. The precision of the
  "enumeration" increases with the integer variable enumeration. The function
  uses OpenMP "section" functionality to handle the different cases.
  @param A0 start of the first bond
  @param B0 end of the first bond
  @param A1 start of the second bond
  @param B1 end of the second bond
  @param test_result does the function test the result with an "exact
  enumeration" ?
  @param enumeration number of points to consider in the "exact enumeration"
*/
void minDist2segments_KKT(vec3 A0, vec3 B0, vec3 A1, vec3 B1, bool test_result,
                          int enumeration, std::vector<vec3> &cp) {
  double r_star[9];
  double s_star[9];
  double i_star = -1.;
  double j_star = -1.;
  double r_star_min = -1.;
  double s_star_min = -1.;
  /*! Segment length of each of the two segments
   */
  double l0 = sqrt(dot_product2(sub3(B0, A0)));
  double l1 = sqrt(dot_product2(sub3(B1, A1)));
  /*! Tangent vectors of each of the two segments
   */
  vec3 T0 = normalize3(sub3(B0, A0));
  vec3 T1 = normalize3(sub3(B1, A1));
  /*! Dot product between tangent segments
   */
  double cos01 = dot_product(T0, T1);
  /* Distance vector between origin point of the two segments.
   */
  vec3 DELTA = sub3(A0, A1);
  double T0_DELTA = dot_product(T0, DELTA);
  double T1_DELTA = dot_product(T1, DELTA);
#pragma omp parallel sections
  {
#pragma omp section
    {
      /*! Solutions for r and s inside the feasible region.
       */
      /*! solution 1 is not valid if the two segments are parallel.
       */
      if (cos01 > (SMALL - 1.) && cos01 < (1. - SMALL)) {
        r_star[0] = (cos01 * T1_DELTA - T0_DELTA) / (1. - cos01 * cos01);
        s_star[0] = (T1_DELTA - cos01 * T0_DELTA) / (1. - cos01 * cos01);
      } else {
        r_star[0] = -1.;
        s_star[0] = -1.;
      }
      /*! Solutions for r and s outside the feasible region.
       */
      /*! solution 2
       */
      r_star[1] = 0.;
      s_star[1] = T1_DELTA;
      /*! solution 3
       */
      r_star[2] = -T0_DELTA;
      s_star[2] = 0.;
      /*! solution 4
       */
      r_star[3] = 0.;
      s_star[3] = 0.;
    }
#pragma omp section
    {
      /*! solution 5
       */
      r_star[4] = l0;
      s_star[4] = l0 * cos01 + T1_DELTA;
      /*! solution 6
       */
      r_star[5] = l0;
      s_star[5] = 0.;
      /*! solution 7
       */
      r_star[6] = l1 * cos01 - T0_DELTA;
      s_star[6] = l1;
      /*! solution 8
       */
      r_star[7] = 0.;
      s_star[7] = l1;
      /*! solution 9
       */
      r_star[8] = l0;
      s_star[8] = l1;
    }
  }
  /*! min of d^2, loop over the nine previous solutions
   */
  double d2_min_KKT = DBL_MAX, d2;
  for (int i = 0; i < 9; i++) {
    /*! The solution is valid if and only if the two points are lying to the
     * segments and the corresponding distance is minimal.
     */
    if (!(r_star[i] < 0. || r_star[i] > l0 || s_star[i] < 0. ||
          s_star[i] > l1)) {
      d2 = dot_product2(
          sub3(add3(A0, mult3(r_star[i], T0)), add3(A1, mult3(s_star[i], T1))));
      /*! Keep the minimal distance amongst the nine solutions.
       */
      if (d2 < d2_min_KKT) {
        d2_min_KKT = d2;
        r_star_min = r_star[i];
        s_star_min = s_star[i];
      }
    }
  }
  /*! Enumerate all the possible distances (segment 0,segment 1) and keep the
   * star one
   */
  double abs_tol = 1e-5;
  double rel_tol = 1e-3;
  double inv_enumeration = 1. / enumeration;
  double d2_min_enum = DBL_MAX;
  double tmp_j_star;
  vec3 P_STAR;
  if (test_result) {
    // loop over the first segment discrete parts
    for (int i = 0; i <= enumeration; i++) {
      P_STAR = add3(A0, mult3(i * l0 * inv_enumeration, T0));
      // vector from A1 to P_STAR projected along T1
      tmp_j_star = fmin(l1, fmax(.0, dot_product(sub3(P_STAR, A1), T1)));
      // square distance
      d2 = dot_product2(sub3(P_STAR, add3(A1, mult3(tmp_j_star, T1))));
      // temporary minimal distance ?
      if (d2 < d2_min_enum) {
        d2_min_enum = d2;
        i_star = i * inv_enumeration * l0;
        j_star = tmp_j_star;
      }
    }
    printf("The minimal distance between segment 1 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n", A0.x, A0.y, A0.z, B0.x, B0.y, B0.z);
    printf("and segment 2 :\n");
    printf("(%e,%e,%e) --- (%e,%e,%e)\n", A1.x, A1.y, A1.z, B1.x, B1.y, B1.z);
    printf("is :\n");
    printf("KKT conditions : position along segment 1 is %f, position along "
           "segment 2 is %f, minimal distance is %f\n",
           r_star_min / l0, s_star_min / l1, sqrt(d2_min_KKT));
    printf("enumeration : position along segment 1 is %f, position along "
           "segment 2 is %f, minimal distance is %f\n",
           i_star / l0, j_star / l1, sqrt(d2_min_enum));
    /*! Check for differences between KKT conditions and enumeration results.
     */
    if (fabs(i_star - r_star_min) > (abs_tol + rel_tol * r_star_min) ||
        fabs(j_star - s_star_min) > (abs_tol + rel_tol * s_star_min) ||
        fabs(sqrt(d2_min_enum) - sqrt(d2_min_KKT)) >
            (abs_tol + rel_tol * sqrt(d2_min_KKT))) {
      for (int i = 0; i < 1000000; i++) {
        printf("warning : the difference between KKT and enumeration solutions "
               "does not satisfy ||x-y||<%.5f+%.3f*y.\n",
               abs_tol, rel_tol);
        printf("          fabs(min_enum-min_KKT)=%e and %.5f+%.3f*min_KKT=%e\n",
               fabs(sqrt(d2_min_enum) - sqrt(d2_min_KKT)), abs_tol, rel_tol,
               abs_tol + rel_tol * sqrt(d2_min_KKT));
        printf("          fabs(p0_enum-p0_KKT)=%e and %.5f+%.3f*p0_KKT=%e\n",
               fabs(i_star - r_star_min), abs_tol, rel_tol,
               abs_tol + rel_tol * r_star_min);
        printf("          fabs(p1_enum-p1_KKT)=%e and %.5f+%.3f*p1_KKT=%e\n\n",
               fabs(j_star - s_star_min), abs_tol, rel_tol,
               abs_tol + rel_tol * s_star_min);
      }
    } else
      printf("\n");
  }
  /*! Return the result.
   */
  cp[0] = add3(A0, mult3(r_star_min, T0));
  cp[1] = add3(A1, mult3(s_star_min, T1));
  cp[2] = sub3(cp[0], cp[1]);
}

/*! @brief square minimal distance between point and segment
 */
double sqminDistPoint2Segment(vec3 p0, vec3 p1, vec3 p) {
  vec3 t = vec3(p1.x - p0.x, p1.y - p0.y, p1.z - p0.z);
  vec3 u = vec3(p.x - p0.x, p.y - p0.y, p.z - p0.z);
  double l = sqrt(t.x * t.x + t.y * t.y + t.z * t.z);
  double n = (u.x * t.x + u.y * t.y + u.z * t.z) / l;
  if (n > l) {
    return (p1.x - p.x) * (p1.x - p.x) + (p1.y - p.y) * (p1.y - p.y) +
           (p1.z - p.z) * (p1.z - p.z);
  } else {
    if (n < 0.)
      return u.x * u.x + u.y * u.y + u.z * u.z;
    else {
      vec3 d = vec3((n / l) * t.x, (n / l) * t.y, (n / l) * t.z);
      return (u.x - d.x) * (u.x - d.x) + (u.y - d.y) * (u.y - d.y) +
             (u.z - d.z) * (u.z - d.z);
    }
  }
}

/*! @brief For segmentation between 1 and 3 bp per DNA segment, length is lesser
  than radius. Therefore we remove collision(s) between segment 'i' and segment
  'i+x'.
 */
void remove_artificial_collisions(int nucleosomes, int F, const double *sbp,
                                  const double *lbp, const double *radius,
                                  const dBodyID *fibre, const dGeomID *gfibre,
                                  int &nc, double &sum_abs_depth) {
  int i0, i1, njf, cs, count;
  double lf, sf, rf;
  dContact contact[contact_array_size];
  for (int n = ((nucleosome_start[0] == 0) ? 0 : -1); n < nucleosomes; n++) {
    if (n == (nucleosomes - 1) && (nucleosome_start[n] + NNC) < F) {
      // check linker after the last nucleosome
      i0 = nucleosome_start[n] + NNC - 1;
      i1 = F;
    } else {
      i0 = (n == -1) ? 0 : nucleosome_start[n] + NNC - 1;
      i1 = (n == -1) ? nucleosome_start[0] + 1 : nucleosome_start[n + 1] + 1;
    }
    for (int f = i0; f < (i1 - 2); f++) {
      // 1d position
      lf = lbp[f];
      sf = sbp[f] + .5 * lf;
      rf = radius[f];
      // get number of joints
      njf = dBodyGetNumJoints(fibre[f]);
      count = f + 2;
      while ((3.5721 * nm * (sbp[count] - (sf + .5 * lf)) / (10.5 * fL)) <=
                 (rf + radius[count] + 3.5721 * (nm / fL) * (2. / 10.5)) &&
             count < F) {
        // if the two geoms are connected check for contact joint(s)
        if (dAreConnected(fibre[f], fibre[count]) == 1) {
          // printf("n=%i are connected %i(%f %f) %i(%f %f) F=%i nj=%i and
          // %i\n",n,f,sbp[f],lbp[f],count,sbp[count],lbp[count],F,njf,dBodyGetNumJoints(fibre[count]));
          for (int j = (njf - 1); j >= 0; j--) {
            if (dJointGetType(dBodyGetJoint(fibre[f], j)) ==
                dJointTypeContact) {
              if (dJointGetBody(dBodyGetJoint(fibre[f], j), 0) ==
                      fibre[count] ||
                  dJointGetBody(dBodyGetJoint(fibre[f], j), 1) ==
                      fibre[count]) {
                // printf("(first body) remove %i(%f %f) %i(%f %f)
                // F=%i\n",f,sbp[f],lbp[f],count,sbp[count],lbp[count],F);
                // disable contact joint
                dJointDisable(dBodyGetJoint(fibre[f], j));
                // number of collisions decreases
                nc--;
                // cs=dCollide(dBodyGetFirstGeom(fibre[f]),dBodyGetFirstGeom(fibre[count]),contact_array_size,&(contact[0].geom),sizeof(dContact));
                cs = dCollide(gfibre[f], gfibre[count], contact_array_size,
                              &(contact[0].geom), sizeof(dContact));
                if (cs >= contact_array_size)
                  printf("contact array size is too small.\n");
                for (int c = 0; c < cs; c++)
                  sum_abs_depth =
                      fmax(.0, sum_abs_depth - fabs(contact[c].geom.depth));
              }
            }
          }
        }
        count++;
      }
    }
  }
}

/*! @brief print collisions.
  @param B number of dBodyID objects
  @param array of dBodyID objects
 */
void print_collisions(int N, const dBodyID *fibre) {
  int nj;
  for (int i = 0; i < (N - 1); i++) {
    // get number of joints
    nj = dBodyGetNumJoints(fibre[i]);
    for (int j = (i + 1); j < N; j++) {
      if (dAreConnectedExcluding(fibre[i], fibre[j], dJointTypeContact) == 1)
        continue;
      for (int k = 0; k < nj; k++) {
        if (dJointGetType(dBodyGetJoint(fibre[i], k)) == dJointTypeContact &&
            dJointIsEnabled(dBodyGetJoint(fibre[i], k))) {
          if (dJointGetBody(dBodyGetJoint(fibre[i], k), 0) == fibre[j] ||
              dJointGetBody(dBodyGetJoint(fibre[i], k), 1) == fibre[j]) {
            printf("%i and %i\n", i, j);
          }
        }
      }
    }
  }
}

void solve_for_shls_forces(const dBodyID *bodies, const double *lb,
                           const vec3 *uvtm, double force, int seed) {
  int nc = nucleosome_start[0];
  std::mt19937_64 mt0(seed), mt1(2 * seed);
  std::uniform_real_distribution<> u01(.0, 1.);
  double d0;
  bool acceptation, random_try = false, LU = true;
  int nsteps = max(10, 20);
  double invnsteps = 1. / (double)(nsteps - 1);
  long int ishl[7], fshl[7], li0;
  vec3 fshls[14], f1shls[14], f2shls[14];
  vec3 li[13], qi[14], nqi[14];
  vec3 ri[14], Cbt[14], ti[14], R1[14], sumfshls;
  double oldEf[4], newEf[4], rqi[14], bestE[4], best_rqi_per[4][14],
      best_rqi[14], hi[14], minrqi[14], maxrqi[14];
  double maxpN = 1000. * pN / fF, gE = 512.;
  FILE *fOut;
  char name[1000];
  // solve Af=b where f are the SHLs forces
  bool replace = true;
  int ncom = (replace) ? 0 : 1;
  MatrixXd Aij((14 + ncom) * 3, 14 * 3), invAij((14 + ncom) * 3, 14 * 3),
      bij((14 + ncom) * 3, 1), fij(14 * 3, 1);
  double A[(14 + 1) * 3][14 * 3], bs[(14 + 1) * 3];
  for (int i = 0; i < ((14 + ncom) * 3); i++) {
    bs[i] = .0;
    for (int j = 0; j < (14 * 3); j++)
      A[i][j] = .0;
  }
  mat33 m33;
  for (int s = 0; s < 14; s++) {
    ri[s] = PositionBodyODE(bodies[nc + s]);
    Cbt[s] = TorqueBodyODE(bodies[nc + s]);
    ti[s] = FrameODE(bodies[nc + s], 2);
    hi[s] = .5 * lb[nc + s];
    R1[s] = mult3(uvtm[NHC - 1 + s].x, FrameODE(bodies[nc + s], 0));
    R1[s] =
        add3(R1[s], mult3(uvtm[NHC - 1 + s].y, FrameODE(bodies[nc + s], 1)));
    R1[s] = add3(R1[s], mult3(uvtm[NHC - 1 + s].z, ti[s]));
    qi[s] = (s != 13) ? sub3(R1[s], mult3(hi[s], ti[s]))
                      : add3(R1[s], mult3(hi[s], ti[s]));
    nqi[s] = normalize3(qi[s]);
    minrqi[s] = -maxpN;
    maxrqi[s] = maxpN;
  }
  // vec3 F0=mult3(-force,ti[0]);
  // vec3 F13=mult3(force,ti[13]);
  vec3 F0 =
      mult3(force, normalize3(cross_product(ti[0], FrameODE(bodies[nc], 0))));
  vec3 F13 = mult3(
      -force, normalize3(cross_product(ti[13], FrameODE(bodies[nc + 13], 0))));
  vec3 g0 = sub3(ri[0], mult3(hi[0], ti[0]));
  vec3 G0 = sub3(sub3(g0, PositionBodyODE(bodies[nc])), mult3(hi[0], ti[0]));
  vec3 g13 = add3(ri[13], mult3(hi[13], ti[13]));
  vec3 G13 = add3(sub3(g13, ri[13]), mult3(hi[13], ti[13]));
  vec3 G0xF0 = cross_product(G0, F0);
  vec3 G13xF13 = cross_product(G13, F13);
  fshl[0] = 1;
  for (int s = 1; s < 7; s++)
    fshl[s] = fshl[s - 1] * nsteps;
  printf("%li/%.1f\n", fshl[6] * nsteps, (double)(fshl[6] * nsteps));
  for (long int i = 0; i < ((LU) ? 1 : (fshl[6] * nsteps)); i++) {
    li0 = i;
    for (int s = 0; s < 7; s++) {
      ishl[s] = (li0 / fshl[s]) % nsteps;
      rqi[s] = (random_try) ? minrqi[s] + (maxrqi[s] - minrqi[s]) * u01(mt1)
                            : minrqi[s] + (maxrqi[s] - minrqi[s]) *
                                              (double)ishl[s] * invnsteps;
      // rqi[s]=.0;// ???
      rqi[13 - s] = rqi[s]; //+(.01*pN/fF)*(1.-2.*u01(mt1));
      li0 -= ishl[s] * fshl[s];
    }
    sumfshls = F0;
    //
    // shl 6.5
    // force : l_{1,2}+F_1+f_1=0
    // torque: h*cross(t_1,l_{1,2})+X_1+Y_1+C_1^{b+t}=0
    //         with X_1=cross(a_1-r_1,f_1)
    //         with Y_1=cross(g_1-r_1,F_1)
    //         cross(a_1-r_1-h*t_1,f_1)+C_1^{b+t}=0
    // f1shls[0]=solve_cross_a_b_equal_c_v1(qi[0],mult3(-1.,add3(G0xF0,Cbt[0])),(int)floor(1e6*u01(mt1)));
    // f1shls[0]=solve_cross_a_b_equal_c_v2(qi[0],mult3(-1.,add3(G0xF0,Cbt[0])),std::ref(mt0));
    f1shls[0] =
        solve_cross_a_b_equal_c_v3(qi[0], mult3(-1., add3(G0xF0, Cbt[0])));
    f2shls[0] = mult3(rqi[0], nqi[0]);
    fshls[0] = add3(f1shls[0], f2shls[0]);
    li[0] = mult3(-1., add3(F0, fshls[0]));
    sumfshls = add3(sumfshls, fshls[0]);
    // ???
    m33 = skew_from_vec3(qi[0]);
    A[0][0] = m33.c1.x;
    A[0][1] = m33.c2.x;
    A[0][2] = m33.c3.x;
    A[1][0] = m33.c1.y;
    A[1][1] = m33.c2.y;
    A[1][2] = m33.c3.y;
    A[2][0] = m33.c1.z;
    A[2][1] = m33.c2.z;
    A[2][2] = m33.c3.z;
    bs[0] = -G0xF0.x - Cbt[0].x;
    bs[1] = -G0xF0.y - Cbt[0].y;
    bs[2] = -G0xF0.z - Cbt[0].z;
    // ???
    // shl != +/-6.5
    // force : l_{i,i+1}-l_{i-1,i}+f_i=0
    //         l_{i,i+1}=-F_1-sum(f_j,j=1,i-1)
    // torque: h*cross(t_i,l_{i,i+1})+h*cross(-t_i,-l_{i-1,i})+X_i+C_i^{b+t}=0
    //         with X_i=cross(a_i-r_i,f_i)
    //         h*cross(t_i,l_{i-1,i}-f_i)+h*cross(t_i,l_{i-1,i})+cross(a_i-r_i,f_i)+C_i^{b+t}=0
    //         cross(a_i-r_i-h*t_i,f_i)+l*cross(t_i,l_{i-1,i})+C_i^{b+t}=0
    //         cross(h*t_i-(a_i-r_i),f_i)=l*cross(t_i,l_{i-1,i})+C_i^{b+t}
    for (int s = 1; s < 13; s++) {
      // f1shls[s]=solve_cross_a_b_equal_c_v1(qi[s],sub3(cross_product(mult3(2.*hi[s],ti[s]),sumfshls),Cbt[s]),(int)floor(1e6*u01(mt1)));
      // f1shls[s]=solve_cross_a_b_equal_c_v2(qi[s],sub3(cross_product(mult3(2.*hi[s],ti[s]),sumfshls),Cbt[s]),std::ref(mt0));
      f1shls[s] = solve_cross_a_b_equal_c_v3(
          qi[s],
          sub3(cross_product(mult3(2. * hi[s], ti[s]), sumfshls), Cbt[s]));
      f2shls[s] = mult3(rqi[s], nqi[s]);
      fshls[s] = add3(f1shls[s], f2shls[s]);
      li[s] = sub3(li[s - 1], fshls[s]);
      sumfshls = add3(sumfshls, fshls[s]);
      // ???
      m33 = skew_from_vec3(mult3(-2. * hi[s], ti[s]));
      for (int t = 0; t < s; t++) {
        A[s * 3 + 0][t * 3 + 0] += m33.c1.x;
        A[s * 3 + 0][t * 3 + 1] += m33.c2.x;
        A[s * 3 + 0][t * 3 + 2] += m33.c3.x;
        A[s * 3 + 1][t * 3 + 0] += m33.c1.y;
        A[s * 3 + 1][t * 3 + 1] += m33.c2.y;
        A[s * 3 + 1][t * 3 + 2] += m33.c3.y;
        A[s * 3 + 2][t * 3 + 0] += m33.c1.z;
        A[s * 3 + 2][t * 3 + 1] += m33.c2.z;
        A[s * 3 + 2][t * 3 + 2] += m33.c3.z;
      }
      m33 = skew_from_vec3(qi[s]);
      A[s * 3 + 0][s * 3 + 0] += m33.c1.x;
      A[s * 3 + 0][s * 3 + 1] += m33.c2.x;
      A[s * 3 + 0][s * 3 + 2] += m33.c3.x;
      A[s * 3 + 1][s * 3 + 0] += m33.c1.y;
      A[s * 3 + 1][s * 3 + 1] += m33.c2.y;
      A[s * 3 + 1][s * 3 + 2] += m33.c3.y;
      A[s * 3 + 2][s * 3 + 0] += m33.c1.z;
      A[s * 3 + 2][s * 3 + 1] += m33.c2.z;
      A[s * 3 + 2][s * 3 + 2] += m33.c3.z;
      bs[s * 3 + 0] = -Cbt[s].x + cross_product(mult3(2. * hi[s], ti[s]), F0).x;
      bs[s * 3 + 1] = -Cbt[s].y + cross_product(mult3(2. * hi[s], ti[s]), F0).y;
      bs[s * 3 + 2] = -Cbt[s].z + cross_product(mult3(2. * hi[s], ti[s]), F0).z;
      // ???
    }
    // shl -6.5
    // force : -l_{13,14}+F_14+f_14=0
    //         l_{13,14}=F_14+f_14
    // torque:
    // h*cross(-t_14,-l_{13,14})+cross(a_14-r_14,f_14)+cross(g_14-r_14,F_14)+C_14^{b+t}=0
    //         cross(-h*t_14-a_14+r_14,f_14)=C_14^{b+t}
    // f1shls[13]=solve_cross_a_b_equal_c_v1(qi[13],mult3(-1.,add3(G13xF13,Cbt[13])),(int)floor(1e6*u01(mt1)));
    // f1shls[13]=solve_cross_a_b_equal_c_v2(qi[13],mult3(-1.,add3(G13xF13,Cbt[13])),std::ref(mt0));
    f1shls[13] =
        solve_cross_a_b_equal_c_v3(qi[13], mult3(-1., add3(G13xF13, Cbt[13])));
    f2shls[13] = mult3(rqi[13], nqi[13]);
    fshls[13] = add3(f1shls[13], f2shls[13]);
    sumfshls = add3(sumfshls, add3(fshls[13], F13));
    // ???
    if (false) {
      for (int s = 13; s < 14; s++) {
        m33 = skew_from_vec3(qi[13]);
        A[s * 3 + 0][s * 3 + 0] += m33.c1.x;
        A[s * 3 + 0][s * 3 + 1] += m33.c2.x;
        A[s * 3 + 0][s * 3 + 2] += m33.c3.x;
        A[s * 3 + 1][s * 3 + 0] += m33.c1.y;
        A[s * 3 + 1][s * 3 + 1] += m33.c2.y;
        A[s * 3 + 1][s * 3 + 2] += m33.c3.y;
        A[s * 3 + 2][s * 3 + 0] += m33.c1.z;
        A[s * 3 + 2][s * 3 + 1] += m33.c2.z;
        A[s * 3 + 2][s * 3 + 2] += m33.c3.z;
        bs[s * 3 + 0] = -Cbt[s].x - G13xF13.x;
        bs[s * 3 + 1] = -Cbt[s].y - G13xF13.y;
        bs[s * 3 + 2] = -Cbt[s].z - G13xF13.z;
      }
    } else {
      for (int s = 13; s < 14; s++) {
        m33 = skew_from_vec3(mult3(-hi[s], ti[s]));
        for (int t = 0; t < s; t++) {
          A[s * 3 + 0][t * 3 + 0] += m33.c1.x;
          A[s * 3 + 0][t * 3 + 1] += m33.c2.x;
          A[s * 3 + 0][t * 3 + 2] += m33.c3.x;
          A[s * 3 + 1][t * 3 + 0] += m33.c1.y;
          A[s * 3 + 1][t * 3 + 1] += m33.c2.y;
          A[s * 3 + 1][t * 3 + 2] += m33.c3.y;
          A[s * 3 + 2][t * 3 + 0] += m33.c1.z;
          A[s * 3 + 2][t * 3 + 1] += m33.c2.z;
          A[s * 3 + 2][t * 3 + 2] += m33.c3.z;
        }
        m33 = skew_from_vec3(R1[s]);
        A[s * 3 + 0][s * 3 + 0] += m33.c1.x;
        A[s * 3 + 0][s * 3 + 1] += m33.c2.x;
        A[s * 3 + 0][s * 3 + 2] += m33.c3.x;
        A[s * 3 + 1][s * 3 + 0] += m33.c1.y;
        A[s * 3 + 1][s * 3 + 1] += m33.c2.y;
        A[s * 3 + 1][s * 3 + 2] += m33.c3.y;
        A[s * 3 + 2][s * 3 + 0] += m33.c1.z;
        A[s * 3 + 2][s * 3 + 1] += m33.c2.z;
        A[s * 3 + 2][s * 3 + 2] += m33.c3.z;
        bs[s * 3 + 0] = -Cbt[s].x + cross_product(mult3(hi[s], ti[s]), F0).x -
                        cross_product(sub3(g13, ri[s]), F13).x;
        bs[s * 3 + 1] = -Cbt[s].y + cross_product(mult3(hi[s], ti[s]), F0).y -
                        cross_product(sub3(g13, ri[s]), F13).y;
        bs[s * 3 + 2] = -Cbt[s].z + cross_product(mult3(hi[s], ti[s]), F0).z -
                        cross_product(sub3(g13, ri[s]), F13).z;
      }
    }
    // do we replace first equation on the torques by equation on the sum over
    // all the shls forces ?
    if (replace) {
      for (int r = 0; r < 3; r++)
        for (int c = 0; c < (14 * 3); c++)
          A[r][c] = .0;
      for (int s = 0; s < 14; s++) {
        A[0][s * 3 + 0] = 1.;
        A[1][s * 3 + 1] = 1.;
        A[2][s * 3 + 2] = 1.;
      }
      bs[0] = -F0.x - F13.x;
      bs[1] = -F0.y - F13.y;
      bs[2] = -F0.z - F13.z;
    }
    if (ncom == 1) {
      for (int s = 0; s < 14; s++) {
        A[(13 + 1) * 3 + 0][s * 3 + 0] = 1.;
        A[(13 + 1) * 3 + 1][s * 3 + 1] = 1.;
        A[(13 + 1) * 3 + 2][s * 3 + 2] = 1.;
      }
      bs[(13 + 1) * 3 + 0] = -F0.x - F13.x;
      bs[(13 + 1) * 3 + 1] = -F0.y - F13.y;
      bs[(13 + 1) * 3 + 2] = -F0.z - F13.z;
    }
    // ???
    for (int r = 0; r < ((14 + ncom) * 3); r++) {
      bij(r, 0) = bs[r];
      for (int c = 0; c < (14 * 3); c++)
        Aij(r, c) = A[r][c];
    }
    cout << Aij.row(0) << endl;
    cout << Aij.row(1) << endl;
    cout << Aij.row(2) << endl;
    printf("\n");
    cout << Aij.row(3) << endl;
    cout << Aij.row(4) << endl;
    cout << Aij.row(5) << endl;
    printf("\n");
    cout << Aij.row(6) << endl;
    cout << Aij.row(7) << endl;
    cout << Aij.row(8) << endl;
    printf("\n");
    cout << Aij.row(14 * 3 - 3) << endl;
    cout << Aij.row(14 * 3 - 2) << endl;
    cout << Aij.row(14 * 3 - 1) << endl;
    printf("\n");
    if (ncom == 1) {
      cout << Aij.row((14 + 1) * 3 - 3) << endl;
      cout << Aij.row((14 + 1) * 3 - 2) << endl;
      cout << Aij.row((14 + 1) * 3 - 1) << endl;
    }
    for (int r = 0; r < 7; r++)
      printf("bij(%i)=%f/%f\n", r,
             sqrt(pow(bij(3 * r + 0, 0), 2.) + pow(bij(3 * r + 1, 0), 2.) +
                  pow(bij(3 * r + 2, 0), 2.)) *
                 fF / pN,
             sqrt(pow(bij(3 * (13 - r) + 0, 0), 2.) +
                  pow(bij(3 * (13 - r) + 1, 0), 2.) +
                  pow(bij(3 * (13 - r) + 2, 0), 2.)) *
                 fF / pN);
    fij = Aij.fullPivLu().solve(bij);
    // JacobiSVD<MatrixXd,Eigen::ComputeThinU | Eigen::ComputeThinV> svd(Aij);
    // cout<<Aij.template bdcSvd<Eigen::ComputeThinU |
    // Eigen::ComputeThinV>().solve(bij)<<endl;
    // qcout<<bdcSvd<Eigen::ComputeThinU |
    // Eigen::ComputeThinV>(Aij).solve(bij)<<endl; fij=svd.solve(bij);
    // cout<<svd.solve(bij)<<endl;
    sumfshls.x = sumfshls.y = sumfshls.z = .0;
    for (int r = 0; r < 14; r++) {
      sumfshls.x += fij(r * 3 + 0, 0);
      sumfshls.y += fij(r * 3 + 1, 0);
      sumfshls.z += fij(r * 3 + 2, 0);
      d0 = fij(r * 3 + 0, 0) * nqi[r].x + fij(r * 3 + 1, 0) * nqi[r].y +
           fij(r * 3 + 2, 0) * nqi[r].z;
      f2shls[r].x = d0 * nqi[r].x;
      f2shls[r].y = d0 * nqi[r].y;
      f2shls[r].z = d0 * nqi[r].z;
      fshls[r].x = fij(r * 3 + 0, 0);
      fshls[r].y = fij(r * 3 + 1, 0);
      fshls[r].z = fij(r * 3 + 2, 0);
      f1shls[r].x = fshls[r].x - f2shls[r].x;
      f1shls[r].y = fshls[r].y - f2shls[r].y;
      f1shls[r].z = fshls[r].z - f2shls[r].z;
    }
    printf("sum(fshls)=%e %e %e pN\n", (fF / pN) * sumfshls.x,
           (fF / pN) * sumfshls.y, (fF / pN) * sumfshls.z);
    for (int r = 0; r < ((14 + ncom) * 3); r++)
      printf("(%i) %f %f (%f)\n", r, (Aij * fij)(r, 0), bij(r, 0),
             ((Aij * fij)(r, 0) - bij(r, 0)) * fF / pN);
    printf("relative error=%f\n",
           (Aij * fij - bij).norm() / bij.norm()); // L2 norm
    printf("rank=%li min(nrows,ncols)=%li (%lix%li)\n", Aij.fullPivLu().rank(),
           min(Aij.rows(), Aij.cols()), Aij.rows(), Aij.cols());
    printf("%li/%li %li/%li\n", bij.rows(), fij.rows(), bij.cols(), fij.cols());
    // ???
    // acceptation ?
    newEf[1] = newEf[2] = newEf[3] = .0;
    newEf[0] = gE * dot_product(sumfshls, sumfshls);
    for (int s = 0; s < 7; s++) {
      newEf[1] += gE * pow(norm3(f1shls[s]) - norm3(f1shls[13 - s]), 2.) / 7.;
      newEf[2] += gE * pow(norm3(f2shls[s]) - norm3(f2shls[13 - s]), 2.) / 7.;
      newEf[3] += gE * pow(norm3(fshls[s]) - norm3(fshls[13 - s]), 2.) / 7.;
    }
    // store best per
    for (int j = 0; j < 4; j++) {
      if (newEf[j] <= bestE[j] || i == 0) {
        bestE[j] = newEf[j];
        for (int s = 0; s < 14; s++)
          best_rqi_per[j][s] = rqi[s];
      }
    }
    printf("%f\r", (double)i / (double)(fshl[6] * nsteps));
    if (i == 0 && !LU) {
      // init
      sprintf(name, "best_shls_forces_F%fpN.in", force * fF / pN);
      fOut = fopen(name, "r");
      if (fOut != NULL) {
        fscanf(fOut, "%lf %lf %lf %lf\n", &oldEf[0], &oldEf[1], &oldEf[2],
               &oldEf[3]);
        fclose(fOut);
      } else {
        printf("(init) stretching force F=%f/%f pN\n", norm3(F0) * fF / pN,
               norm3(F13) * fF / pN);
        for (int j = 0; j < 4; j++) {
          printf("%f -> %f pN\n", sqrt(oldEf[j] / gE) * fF / pN,
                 sqrt(newEf[j] / gE) * fF / pN);
          oldEf[j] = newEf[j];
        }
        printf("sum(fshls)=%e %e %e\n", sumfshls.x, sumfshls.y, sumfshls.z);
        for (int s = 0; s < 7; s++)
          printf("f(%i)=%.2f/%.2f (%.2f/%.2f %.2f/%.2f) pN\n", s,
                 norm3(fshls[s]) * fF / pN, norm3(fshls[13 - s]) * fF / pN,
                 norm3(f1shls[s]) * fF / pN, norm3(f1shls[13 - s]) * fF / pN,
                 norm3(f2shls[s]) * fF / pN, norm3(f2shls[13 - s]) * fF / pN);
      }
      continue;
    } else {
      // acceptation
      // acceptation=(newEf[0]<=oldEf[0] && newEf[1]<=oldEf[1] &&
      // newEf[2]<=oldEf[2] && newEf[3]<=oldEf[3]);
      // acceptation=(newEf[0]<=oldEf[0] && newEf[1]<=oldEf[1] &&
      // newEf[3]<=oldEf[3]);
      acceptation = (newEf[0] <= oldEf[0] && newEf[1] <= oldEf[1]);
      // acceptation=((newEf[0]+newEf[1]+newEf[3])<=(oldEf[0]+oldEf[1]+oldEf[3]));
      // acceptation=(u01(mt1)<=exp(oldEf[0]-newEf[0]) &&
      // u01(mt1)<=exp(oldEf[1]-newEf[1]) && u01(mt1)<=exp(oldEf[3]-newEf[3]));
      // acceptation=(u01(mt1)<=exp(oldEf[0]+oldEf[1]+oldEf[3]-(newEf[0]+newEf[1]+newEf[3])));
      if (acceptation || LU) {
        if (random_try)
          printf("stretching force F=%f/%f pN\n", norm3(F0) * fF / pN,
                 norm3(F13) * fF / pN);
        else
          printf("(enumeration) stretching force F=%f/%f pN\n",
                 norm3(F0) * fF / pN, norm3(F13) * fF / pN);
        for (int j = 0; j < 4; j++) {
          printf("%f complete: %f -> %f pN\n",
                 (double)i / (double)(fshl[6] * nsteps),
                 sqrt(oldEf[j] / gE) * fF / pN, sqrt(newEf[j] / gE) * fF / pN);
          oldEf[j] = newEf[j];
        }
        printf("sum(fshls)=%e %e %e pN\n", sumfshls.x * fF / pN,
               sumfshls.y * fF / pN, sumfshls.z * fF / pN);
        for (int s = 0; s < 7; s++)
          printf("f(%i)=%.2f/%.2f (%.2f/%.2f %.2f/%.2f) pN\n", s,
                 norm3(fshls[s]) * fF / pN, norm3(fshls[13 - s]) * fF / pN,
                 norm3(f1shls[s]) * fF / pN, norm3(f1shls[13 - s]) * fF / pN,
                 norm3(f2shls[s]) * fF / pN, norm3(f2shls[13 - s]) * fF / pN);
        for (int s = 0; s < 7; s++)
          printf("dot(f1shl,f2shl)=%f/%f pN\n",
                 sqrt(fabs(dot_product(f1shls[s], f2shls[s]))) * fF / pN,
                 sqrt(fabs(dot_product(f1shls[13 - s], f2shls[13 - s]))) * fF /
                     pN);
        sprintf(name, "best_shls_forces_F%fpN.in", force * fF / pN);
        fOut = fopen(name, "w");
        fprintf(fOut, "%.8f %.8f %.8f %.8f\n", oldEf[0], oldEf[1], oldEf[2],
                oldEf[3]);
        fprintf(fOut, "f1x f1y f1z norm\n");
        for (int s = 0; s < 14; s++)
          fprintf(fOut, "%f %f %f %f\n", f1shls[s].x * fF / pN,
                  f1shls[s].y * fF / pN, f1shls[s].z * fF / pN,
                  norm3(f1shls[s]) * fF / pN);
        fprintf(fOut, "f2x f2y f2z norm\n");
        for (int s = 0; s < 14; s++)
          fprintf(fOut, "%f %f %f %f\n", f2shls[s].x * fF / pN,
                  f2shls[s].y * fF / pN, f2shls[s].z * fF / pN,
                  norm3(f2shls[s]) * fF / pN);
        fprintf(fOut, "fx fy fz norm\n");
        for (int s = 0; s < 14; s++)
          fprintf(fOut, "%f %f %f %f\n", fshls[s].x * fF / pN,
                  fshls[s].y * fF / pN, fshls[s].z * fF / pN,
                  norm3(fshls[s]) * fF / pN);
        fprintf(fOut, "\n");
        fprintf(fOut, "side - %f & %f & %f & %f & %f & %f & %f\n",
                norm3(fshls[0]) * fF / pN, norm3(fshls[1]) * fF / pN,
                norm3(fshls[2]) * fF / pN, norm3(fshls[3]) * fF / pN,
                norm3(fshls[4]) * fF / pN, norm3(fshls[5]) * fF / pN,
                norm3(fshls[6]) * fF / pN);
        fprintf(fOut, "side + %f & %f & %f & %f & %f & %f & %f\n",
                norm3(fshls[13]) * fF / pN, norm3(fshls[12]) * fF / pN,
                norm3(fshls[11]) * fF / pN, norm3(fshls[10]) * fF / pN,
                norm3(fshls[9]) * fF / pN, norm3(fshls[8]) * fF / pN,
                norm3(fshls[7]) * fF / pN);
        fclose(fOut);
        for (int s = 0; s < 14; s++)
          best_rqi[s] = rqi[s];
      }
    }
    // does it move to enumeration ?
    if (false && (double)i > (.5 * (double)(fshl[6] * nsteps)) && random_try) {
      printf("enumeration starts ...\n");
      random_try = false;
      i = -1;
      for (int s = 0; s < 14; s++) {
        minrqi[s] = -1.5 * fabs(best_rqi[s]);
        maxrqi[s] = 1.5 * fabs(best_rqi[s]);
        printf("%i: new interval [%f,%f]\n", s, minrqi[s], maxrqi[s]);
      }
    }
  }
  // print best per
  if (!LU) {
    for (int j = 0; j < 4; j++) {
      if (j == 2 || j == 3)
        continue;
      printf("best(%i)=%f pN\n", j, sqrt(bestE[j] / gE) * fF / pN);
      for (int s = 0; s < 7; s++)
        printf("    %f/%f pN\n", best_rqi_per[j][s] * fF / pN,
               best_rqi_per[j][13 - s] * fF / pN);
    }
  }
}

/*! @brief compute gyration tensor as-well-as eigenvalues and eigenvectors
  @param bodies list of dBodyID
  @param start compute gyration tensor from this dBodyID
  @param end to this dBodyID (included)
  @param ev to store eigenvalues
  @param evec1 to store first eigenvector
  @param evec2 to store second eigenvector
  @param evec3 to store third eigenvector
 */
void gyration_tensor(const dBodyID *bodies, int start, int end, vec3 &ev,
                     vec3 &evec1, vec3 &evec2, vec3 &evec3) {
  vec3 com = vec3(.0, .0, .0), ri;
  // center-of-mass
  int N = end - start + 1;
  for (int i = start; i <= end; i++)
    com = add3(com, PositionBodyODE(bodies[i]));
  com = mult3(1. / (double)N, com);
  // gyration tensor
  double dx, dy, dz, Tij[9];
  for (int i = 0; i < 9; i++)
    Tij[i] = .0;
  for (int i = start; i <= end; i++) {
    ri = PositionBodyODE(bodies[i]);
    dx = ri.x - com.x;
    dy = ri.y - com.y;
    dz = ri.z - com.z;
    Tij[0] += dx * dx;
    Tij[1] += dx * dy;
    Tij[2] += dx * dz;
    Tij[4] += dy * dy;
    Tij[5] += dy * dz;
    Tij[8] += dz * dz;
  }
  Tij[3] = Tij[1];
  Tij[6] = Tij[2];
  Tij[7] = Tij[5];
  for (int i = 0; i < 9; i++)
    Tij[i] /= (double)N;
  // compute eigenvalues and eigenvectors
  Matrix3d Gij(3, 3);
  SelfAdjointEigenSolver<MatrixXd> ES_Gij;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      Gij(i, j) = Tij[i * 3 + j];
  ES_Gij.compute(Gij, ComputeEigenvectors);
  ev.x = fmax(.0, ES_Gij.eigenvalues()[0]);
  ev.y = fmax(.0, ES_Gij.eigenvalues()[1]);
  ev.z = fmax(.0, ES_Gij.eigenvalues()[2]);
  evec1.x = (ES_Gij.eigenvectors().col(0))[0];
  evec1.y = (ES_Gij.eigenvectors().col(0))[1];
  evec1.z = (ES_Gij.eigenvectors().col(0))[2];
  evec2.x = (ES_Gij.eigenvectors().col(1))[0];
  evec2.y = (ES_Gij.eigenvectors().col(1))[1];
  evec2.z = (ES_Gij.eigenvectors().col(1))[2];
  evec3.x = (ES_Gij.eigenvectors().col(2))[0];
  evec3.y = (ES_Gij.eigenvectors().col(2))[1];
  evec3.z = (ES_Gij.eigenvectors().col(2))[2];
}
