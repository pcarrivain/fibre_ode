#include "functions.h"
#include "variables.h"
#include <getopt.h>
#include <thread>
#include <time.h>

using namespace std;

int vl, nc, retour;
double InitialPCTime, Et, Tw, d0, Tw_t0, Wr_t0;
char nom_s[3000], nom_g[3000], nom_v[3000], path_to[1000], parameters[1000],
    initial_conformation[30];
FILE *fSortie, *fSPQ, *fStructure, *fMT, *export_world_state;
unsigned long *s_mt = new unsigned long[624](), s_mt_i;
int s_mti;
int3p nc_test_Vl;
bool success;

// extern variables
int *is_linker_nucleosome = new int[1]();
int *nucleosome_start = new int[1]();
double xmass = 1., xfriction = 1.;
bool same_mass = false;
// variables: monomers partition
int nbins = 1, lhit = 0, nperb = 10, nnperb = 10;
int *binn = NULL;
int *ninb = NULL;
int *bhit = NULL;
int *nhit = NULL;
int *lperb = NULL;
// variables: nucleosomes partition
int *nbinn = NULL;
int *nninb = NULL;
int *nbhit = NULL;
int *nnhit = NULL;
int *nlperb = NULL;

// main
int main(int argc, char **argv) {
  // nearCallback function
  void (*my_nearCallback)(void *, dGeomID, dGeomID);
  my_nearCallback = &nearCallback;
  // default inputs
  int seed = 1;
  int N = 5714;
  double l_bp = 10.5; // we choose the number of bp per segment to be multiple
                      // of one DNA turn
  double force = 0.;
  double torque = 0.;
  double overtwist = .0;
  int dLk = 0;
  bool bovertwist = false;
  bool bdLk = false;
  int iterations = 1000000;
  int every = (int)(iterations / 100);
  bool brestart = false;
  retour = sprintf(initial_conformation, "%s", "circular");
  double x_ghost = 1.2;
  int gyroscopic = 1;
  bool test_Vl = false;
  bool split_solver = false;
  int Vl_every = 10;
  retour = sprintf(path_to, "%s", "/scratch/bacteria");
  bool bvisualisation = false;
  int n_args = 0, arg_i;
  const option long_opts[] = {
      {"seed", required_argument, nullptr, 's'},
      {"N", required_argument, nullptr, 'n'},
      {"l_bp", required_argument, nullptr, 'l'},
      {"force", required_argument, nullptr, 'f'},
      {"torque", required_argument, nullptr, 't'},
      {"overtwist", required_argument, nullptr, 'o'},
      {"dLk", required_argument, nullptr, 'd'},
      {"iterations", required_argument, nullptr, 'i'},
      {"every", required_argument, nullptr, 'e'},
      {"initial_conformation", required_argument, nullptr, 'c'},
      {"scale", required_argument, nullptr, 'X'},
      {"Vl_every", required_argument, nullptr, 'V'},
      {"test_Vl", required_argument, nullptr, 'T'},
      {"split_solver", required_argument, nullptr, 'S'},
      {"gyroscopic", required_argument, nullptr, 'g'},
      {"path_to", required_argument, nullptr, 'p'},
      {"visualisation", required_argument, nullptr, 'v'},
      {"xmass", required_argument, nullptr, 'x'},
      {"help", optional_argument, nullptr, 'h'}};
  while (
      (arg_i = getopt_long(argc, argv, "s:n:l:f:t:o:d:i:e:c:X:V:T:S:g:p:v:x:h",
                           long_opts, nullptr)) != -1) {
    switch (arg_i) {
    case 's':
      seed = (int)(atof(optarg));
      n_args++;
      break;
    case 'n':
      N = (int)(atof(optarg));
      n_args++;
      break;
    case 'l':
      l_bp = atof(optarg);
      n_args++;
      break;
    case 'f':
      force = atof(optarg) * pN / fF;
      n_args++;
      break;
    case 't':
      torque = atof(optarg) * pN * nm / (kB * temp);
      n_args++;
      break;
    case 'o':
      overtwist = atof(optarg);
      bovertwist = true;
      n_args++;
      break;
    case 'd':
      dLk = (int)(atof(optarg));
      bdLk = true;
      n_args++;
      break;
    case 'i':
      iterations = (int)(atof(optarg));
      n_args++;
      break;
    case 'e':
      every = (int)(atof(optarg));
      n_args++;
      break;
    case 'c':
      sprintf(initial_conformation, "%s", optarg);
      n_args++;
      break;
    case 'X':
      x_ghost = atof(optarg);
      n_args++;
      break;
    case 'V':
      Vl_every = (int)(atof(optarg));
      n_args++;
      break;
    case 'T':
      test_Vl = (strcmp(optarg, "yes") == 0);
      n_args++;
      break;
    case 'S':
      split_solver = (strcmp(optarg, "yes") == 0);
      n_args++;
      break;
    case 'g':
      gyroscopic = (int)(atof(optarg));
      n_args++;
      break;
    case 'p':
      retour = sprintf(path_to, "%s", optarg);
      n_args++;
      break;
    case 'v':
      bvisualisation = true;
      n_args++;
      break;
    case 'x':
      xmass = atof(optarg);
      n_args++;
      break;
    case 'h':
      printf("\nfibre_ODE, usage :\n");
      printf("\n");
      printf("      simulation of bacteria\n");
      printf("      example, bacteria (60 kbp) of 5714 monomers of 10.5 bp "
             "each and overtwist of -0.06 :\n");
      printf("      ./bacteria -n 5714 -l 10.5 -f 0.0 -t 0.0 -o -0.06 -s 1 -i "
             "100000 -e 10000 --scale 1.2 Vl_every 10 -p /scratch/bacteria\n");
      printf("      example, bacteria (5 kbp) of 476 monomers of 10.5 bp each "
             "and overtwist of -0.06 :\n");
      printf("      ./bacteria -n 476 -l 10.5 -f 0.0 -t 0.0 -o -0.06 -s 1 -i "
             "100000 -e 10000 --scale 1.2 Vl_every 10 --test_Vl yes -p "
             "/scratch/bacteria\n");
      printf("      monomers      (n) : number of monomers.\n");
      printf("      l_bp          (l) : bp per monomer.\n");
      printf("      force         (f) : stretching force applied to the "
             "magnetic bead (in pN).\n");
      printf("      torque        (t) : torque applied to the magnetic bead "
             "(in pN.nm).\n");
      printf("      overtwist     (o) : overtwist.\n");
      printf("      dLk           (d) : linking number deficit.\n");
      printf("      seed          (s) : seed for the pRNGs\n");
      printf("      iterations    (i) : number of iterations\n");
      printf("      every         (e) : save conformation every 'every' "
             "iterations\n");
      printf("      initial_conformation (c) : initial conformation in "
             "'circular', 'linear'\n");
      printf("      scale         (X) : scale the object in every dimension\n");
      printf("                          it is used for the within cut-off "
             "distance of the Verlet-list\n");
      printf("      Vl_every      (V) : build the Verlet-list every this many "
             "time-steps\n");
      printf("      test_Vl       (T) : test the Verlet-list implementation "
             "(--test_Vl yes)\n");
      printf("      split_solver  (S) : split solver, first resolve "
             "collisions, apply feedback forces to the articulated system "
             "(--split_solver yes)\n");
      printf("      gyroscopic    (g) : 0 do not consider term dot(inertia) "
             "while 1 does\n");
      printf("      path_to       (p) : where to write the data\n");
      printf("                          code expects to find two folders 'out' "
             "and 'restart' in directory 'path_to'.\n");
      printf("      visualisation (v) : save file to visualize with "
             "'visualisation with blender' module.\n\n");
      printf(
          "      xmass         (x): multiply all the masses by this factor\n");
      printf("If the Verlet list test failed try to modify the 'scale' and "
             "'Vl_every' arguments.\n");
      printf("If it still does not work, please consider contact me "
             "p.carrivain_at_gmail.com.\n");
      printf("Output file name is xyz.*.out and restart file name is "
             "restart.*.out.\n");
      printf("I use binary file to save data.\n");
      printf("xyz.*.out stores first com, first tangent, second com, second "
             "tangent, etc.\n");
      printf("restart.*.out stores positions, orientations and velocities.\n");
      return 0;
      break;
    }
  }

  double mass = xmass * l_bp * m1bp / fM;
  double l = (l_bp / 10.5) * 3.5721 * nm / fL;
  double Lk0 = N * l_bp / 10.5;
  double lt = 86. * nm / fL; // twist persistence length
  double radius = max(1.5 * nm / fL,
                      (1. + 1. / sqrt(.62 * 8. * pi * .7 * (.15))) * nm / fL);
  // double Lk=(overtwist+1.)*Lk0;// sigma=(Lk-Lk0)/Lk0
  double Lk = (bdLk) ? dLk : overtwist * Lk0; // sigma=Lk/Lk0
  if (bdLk)
    overtwist = dLk / Lk0;
  double *ldna = new double[N]();
  std::fill_n(ldna, N, l);
  dBodyID *dna = new dBodyID[N];
  dBodyID *copy_dna = new dBodyID[N];
  int3p *ddna = new int3p[N]();
  dGeomID *gdna = new dGeomID[N];
  dGeomID *ggdna = new dGeomID[N];
  dJointID *jdna = new dJointID[N];

  // ode-0.7
  dInitODE();
  InitialPCTime = clock();

  // pRNG
  std::mt19937_64 e1(2 * seed), e2(2 * seed + 1);

  // create the world and the spaces (ODE)
  dWorldID WorldODE = create_world_ODE(.0, wERP, wCFM, nSOR, wSOR);
  dSpaceID SpaceODE = hash_space(l, 1.01 * (l + 2. * radius));
  dSpaceID gSpaceODE = ghost_hash_space(1.01 * (l + 2. * radius), x_ghost);
  dJointGroupID contactgroup = dJointGroupCreate(0);
  double sum_abs_depth, max_abs_depth;

  // create bacterial dna
  create_ring_chain(WorldODE, SpaceODE, gSpaceODE, N, &dna, ddna, &gdna, &ggdna,
                    &jdna, l, l_bp, radius, mass, x_ghost, overtwist,
                    gyroscopic, initial_conformation);
  // export_world_state=fopen("world","w");
  // dWorldExportDIF(WorldODE,export_world_state,"test_export");//???
  // if(export_world_state!=NULL) fclose(export_world_state);
  // copy only the bodies from the world (no joints)
  if (split_solver)
    copy_system_with_no_joints(WorldODE, dna, &copy_dna, N);
  // initialize data
  initialize_body_data(&ddna, N, 20);
  // center of mass to 0,0,0
  remove_com(dna, 0, N - 1);
  // bending and twisting strengths
  double *gb = new double[N]();
  double *gt = new double[N]();
  bending_twisting_constants(lk / fL, lt, ldna, &gb, &gt, N, 1., false);
  // intrinsic twist angle
  double *be = new double[N]();
  double *twe = new double[N]();
  std::fill_n(twe, N, (2. * pi / 10.5) * fmod(l_bp, 10.5));
  double cos_Tw2[2];
  // Langevin parameters
  double dt = .0;
  double *friction = new double[N]();
  double *sigmaT = new double[N]();
  double *sigmaR = new double[N]();
  for (int n = 0; n < N; n++) {
    friction[n] = 6. * pi * (viscosity / (fM / (fL * fT))) * (.5 * l);
    sigmaT[n] = friction[n] / mass;
    sigmaR[n] = friction[n] / mass;
    dt = (n == 0) ? .01 / sigmaT[n] : fmin(dt, .01 / sigmaT[n]);
  }
  int dofs = 6 * N - 3 * N;
  double mE = .5 * dofs;

  // parameters
  if (bovertwist)
    retour =
        sprintf(parameters, "n%i_%ix%.1fbp_F%.2f_C%.2f_o%.2f", seed, N, l_bp,
                force * fF / pN, torque * kB * temp / (pN * nm), overtwist);
  else
    retour = sprintf(parameters, "n%i_%ix%.1fbp_F%.2f_C%.2f_%i", seed, N, l_bp,
                     force * fF / pN, torque * kB * temp / (pN * nm), dLk);
  // description of bodies
  retour = sprintf(nom_s, "%s/out/structure.out", path_to);
  fStructure = fopen(nom_s, "w");
  for (int i = 0; i < N; i++)
    fprintf(fStructure, "%i %f %f %i %i %i\n", 0, radius, l, i,
            (int)(i / (int)(N / 8)), 0);
  fclose(fStructure);

  // does a conformation can be found ?
  int start = 0;
  for (int s = (10000 * every); s > 0; s -= every) {
    retour = sprintf(nom_g, "%s/restart/restart.%i.out", path_to, s);
    fSPQ = fopen(nom_g, "rb");
    if (fSPQ != NULL) {
      fclose(fSPQ);
      start = s;
      break;
    }
  }
  if (!brestart)
    start = 0;
  // initial conformation writhe and Tw
  Tw_t0 = sum_of_local_twist(dna, N, 0, N - 1) / (2. * pi);
  Wr_t0 = chain_writhe_ODE(dna, ldna, 0, N - 1, false);
  // info file
  retour = sprintf(nom_s, "%s/info.out", path_to);
  fSortie = fopen(nom_s, "w");
  retour = fprintf(fSortie, "%s %s\n", "bond", "rigid");
  retour = fprintf(fSortie, "%s %s\n", "pair_style", "hard");
  retour = fprintf(fSortie, "%s %i\n", "bonds", N);
  retour = fprintf(fSortie, "%s %f\n", "mass_amu",
                   mass * fM / (1.66 * pow(10., -27.)));
  retour = fprintf(fSortie, "%s %f\n", "xmass", xmass);
  retour = fprintf(fSortie, "%s %f\n", "resolution_bp", l_bp);
  retour = fprintf(fSortie, "%s %f\n", "bond_length_nm", l * fL / nm);
  retour = fprintf(fSortie, "%s %f\n", "bending_persistence_nm", 50.);
  retour = fprintf(fSortie, "%s %f\n", "twisting_persistence_nm", 86.);
  retour = fprintf(fSortie, "%s %f\n", "Kb_kBT", gb[0]);
  retour = fprintf(fSortie, "%s %f\n", "pKb_kBT", .0);
  retour = fprintf(fSortie, "%s %f\n", "Kt_kBT", gt[0]);
  retour = fprintf(fSortie, "%s %f\n", "kBT",
                   kB * temp / (fM * fL * fL / (fT * fT)));
  retour = fprintf(fSortie, "%s %f\n", "kbond", 1e6);
  retour = fprintf(fSortie, "%s %f\n", "friction", friction[0]);
  retour = fprintf(fSortie, "%s %f\n", "Temp", temp);
  retour = fprintf(fSortie, "%s %f\n", "dt", dt);
  retour = fprintf(fSortie, "%s %f\n", "tau", 1. / sigmaT[0]);
  retour = fprintf(fSortie, "%s %i\n", "particles_per_bead", 1);
  retour = fprintf(fSortie, "%s %i\n", "dLk", dLk);
  retour = fprintf(fSortie, "%s %f\n", "overtwist", overtwist);
  retour = fprintf(fSortie, "%s %s\n", "thermostat", "langevin");
  retour =
      fprintf(fSortie, "%s %s\n", "initial_conformation", initial_conformation);
  retour = fprintf(fSortie, "%s %s\n", "linear", "no");
  retour = fprintf(fSortie, "%s %s\n", "platform", "ODE");
  retour = fprintf(fSortie, "%s %s\n", "precision", "double");
  retour = fprintf(fSortie, "%s %f\n", "Wr_t0", Wr_t0);
  retour = fprintf(fSortie, "%s %f\n", "Tw_t0", Tw_t0);
  retour = fprintf(fSortie, "%s %s\n", "use_double_twist", "no");
  retour = fprintf(fSortie, "%s %i\n", "step0", 0);
  retour = fprintf(fSortie, "%s %i\n", "step1", start + iterations);
  retour = fprintf(fSortie, "%s %i\n", "every", every);
  fclose(fSortie);
  // restart from conformation ?
  if (start > 0) {
    retour = sprintf(nom_g, "%s/restart/restart.%i.out", path_to, start);
    fSPQ = fopen(nom_g, "rb");
    if (fSPQ != NULL) {
      fclose(fSPQ);
      retour = read_write_conformation(dna, nom_g, "read", 0, N);
    }
  }

  // print the parameters
  printf("%s\n", dGetConfiguration());
  printf("%i geoms\n", dSpaceGetNumGeoms(SpaceODE));
  printf("fT=%e s\n", fT);
  printf("fM=%e kg\n", fM);
  printf("fL=%e m\n", fL);
  printf("fF=%e N\n", fF);
  printf("<E>=%f\n", mE);
  printf("dt=%f\n", dt);
  printf("l=%f nm, r=%f nm\n", l * fL / nm, radius * fL / nm);
  printf("erp=%.1f cfm=%.1e w(SOR)=%f n(SOR)=%i\n", wERP, wCFM,
         dWorldGetQuickStepW(WorldODE),
         dWorldGetQuickStepNumIterations(WorldODE));
  printf("%i iterations and sampling every %i steps\n", iterations, every);
  printf("force:%.2f pN,torque:%.2f pN.nm,Lk0:%.3f,Lk:%.3f\n", force * fF / pN,
         torque * kB * temp / (pN * nm), Lk0, Lk);
  printf("Tw(t=0)=%f Wr(t=0)=%f\n", Tw_t0, Wr_t0);
  retour = sprintf(nom_s, "%s/bacteria_%i.out", path_to, seed);
  fSortie = fopen(nom_s, "w");
  retour = fprintf(fSortie, "%s\n", dGetConfiguration());
  retour = fprintf(fSortie, "%i geoms\n", dSpaceGetNumGeoms(SpaceODE));
  retour = fprintf(fSortie, "fT=%e s\n", fT);
  retour = fprintf(fSortie, "fM=%e kg\n", fM);
  retour = fprintf(fSortie, "fL=%e m\n", fL);
  retour = fprintf(fSortie, "fF=%e N\n", fF);
  retour = fprintf(fSortie, "<E>=%f\n", mE);
  retour = fprintf(fSortie, "dt=%f\n", dt);
  retour =
      fprintf(fSortie, "l=%f nm, r=%f nm\n", l * fL / nm, radius * fL / nm);
  retour = fprintf(fSortie, "erp=%.1f cfm=%.1e w(SOR)=%f n(SOR)=%i\n", wERP,
                   wCFM, dWorldGetQuickStepW(WorldODE),
                   dWorldGetQuickStepNumIterations(WorldODE));
  retour = fprintf(fSortie, "%i iterations and sampling every %i steps\n",
                   iterations, every);
  retour =
      fprintf(fSortie, "force:%.2f pN, torque:%.2f pN.nm, Lk0:%.3f, Lk:%.3f\n",
              force * fF / pN, torque * kB * temp / (pN * nm), Lk0, Lk);
  retour = fprintf(fSortie, "Tw(t=0)=%f Wr(t=0)=%f\n", Tw_t0, Wr_t0);
  fclose(fSortie);

  // loop over time, confinement ?
  for (int s = (start + (start > 0)); s <= (start + iterations); s++) {

    // "flying-ice-cube" correction
    if (s % 100 == 0 && s > 0 && false)
      flying_ice_cube_correction(dna, N);
    // kinetic energy
    Et = Ktr(dna, 0, N - 1, (s % 100 == 0 && s > 0), 2);
    // bending and twist
    bending_twist(dna, gb, gt, 1., be, twe, 0, N - 1, false, cos_Tw2[0],
                  cos_Tw2[1]);
    // Langevin-Euler local thermostat
    std::thread first(langevin_euler, dna, sigmaT, sigmaR, dt, 1., 0,
                      (N - N % 2) / 2, std::ref(e1));
    std::thread second(langevin_euler, dna, sigmaT, sigmaR, dt, 1.,
                       (N - N % 2) / 2 + 1, N - 1, std::ref(e2));
    first.join();
    second.join();
    // test Verlet-list implementation ?
    if (test_Vl) {
      nc_test_Vl = int3p(4, 0, 0);
      dSpaceCollide(SpaceODE, &nc_test_Vl, &test_nearCallback);
    }
    // build the Verlet-list
    vl = build_Verlet_list(gSpaceODE, s - start, Vl_every, &ddna, N, 4,
                           my_nearCallback);
    // collide all in the normal space
    nc = collide_all(WorldODE, gdna, ddna, N, wERP, wCFM, contactgroup,
                     sum_abs_depth, max_abs_depth);
    // copy state of each body + no articulated system to resolve excluded
    // volume constraints
    if (split_solver)
      success = copy_state_body(&dna, &copy_dna, N, 1);
    // step the world with contact joints only
    // get the feedback and apply it to the articulated system
    if (split_solver && success)
      step_the_world_contact_joints(WorldODE, &dna, &copy_dna, N, dt, 1);
    if (test_Vl && nc != nc_test_Vl.j) {
      for (int n = 0; n < N; n++)
        for (int v = 2; v < (ddna[n].p)[0]; v++)
          printf("VL %i %i\n", n, (ddna[n].p)[v]);
      printf("s=%i ODE=%i VL=%i/%i\n", s, nc, nc_test_Vl.j, vl);
      retour = sprintf(nom_s, "%s/bacteria_%i.out", path_to, seed);
      fSortie = fopen(nom_s, "a");
      retour =
          fprintf(fSortie, "s=%i ODE=%i VL=%i/%i\n", s, nc, nc_test_Vl.j, vl);
      fclose(fSortie);
      return 0;
    }
    if (s == 0 && nc > 0) {
      printf("%i collision(s) in the initial state, exit.\n", nc);
      retour = sprintf(nom_s, "%s/bacteria_%i.out", path_to, seed);
      fSortie = fopen(nom_s, "a");
      retour =
          fprintf(fSortie, "%i collision(s) in the initial state, exit.\n", nc);
      fclose(fSortie);
      return 0;
    }
    // step the dWorldID
    dWorldQuickStep(WorldODE, dt);
    if (contactgroup)
      dJointGroupEmpty(contactgroup);

    if ((s % every) == 0) {
      // save the conformation
      retour = sprintf(nom_s, "%s/restart/restart.%i.out", path_to, s);
      read_write_conformation(dna, nom_s, "write", 0, N);
      // save for visualisation
      if (bvisualisation) {
        retour = sprintf(nom_v, "%s/out/xyz.%i.out", path_to, s);
        write_visualisation(dna, nom_v, 0, N, "w");
      }
      // print some info
      printf("%.3f e/<e>=%f %i collision(s)\n", (double)s / (double)iterations,
             Ktr(dna, 0, N - 1, false, 2) / mE, nc);
      retour = sprintf(nom_s, "%s/bacteria_%i.out", path_to, seed);
      fSortie = fopen(nom_s, "a");
      retour = fprintf(fSortie, "%.3f e/<e>=%f %i collision(s)\n",
                       (double)s / (double)iterations,
                       Ktr(dna, 0, N - 1, false, 2) / mE, nc);
      fclose(fSortie);
    }
  } // end of simulation loop

  // close ODE
  if (contactgroup)
    dJointGroupDestroy(contactgroup);
  dWorldDestroy(WorldODE);
  dSpaceSetCleanup(SpaceODE, 1);
  dSpaceDestroy(SpaceODE);
  dCloseODE();
  // clean pointers
  delete[] be;
  delete[] twe;
  delete[] ldna;
  delete[] dna;
  delete[] ddna;
  delete[] gdna;
  delete[] ggdna;
  delete[] jdna;
  delete[] friction;
  delete[] sigmaT;
  delete[] sigmaR;
  printf("PC time=%f seconds\n", (clock() - InitialPCTime) / CLOCKS_PER_SEC);
  retour = sprintf(nom_s, "%s/bacteria_%i.out", path_to, seed);
  fSortie = fopen(nom_s, "a");
  retour = fprintf(fSortie, "PC time=%f seconds\n",
                   (clock() - InitialPCTime) / CLOCKS_PER_SEC);
  fclose(fSortie);
  return 0;
}
